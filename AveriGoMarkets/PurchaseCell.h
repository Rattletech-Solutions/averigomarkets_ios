//
//  PurchaseCell.h
//  Averigo
//
//  Created by BTS on 24/11/16.
//  Copyright © 2016 BTS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PurchaseCell : UITableViewCell

@property(strong,nonatomic) IBOutlet UIView *view;

@property(strong,nonatomic) IBOutlet UILabel *itemname;
@property(strong,nonatomic) IBOutlet UILabel *itemprice;
@property(strong,nonatomic) IBOutlet UILabel *orderno;
@property(strong,nonatomic) IBOutlet UILabel *quantity;
@property(strong,nonatomic) IBOutlet UILabel *orderdate;

@end
