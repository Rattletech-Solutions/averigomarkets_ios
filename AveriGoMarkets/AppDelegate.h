    //
    //  AppDelegate.h
    //  Averigo
    //
    //  Created by BTS on 07/11/16.
    //  Copyright © 2016 BTS. All rights reserved.
    //

#import <UIKit/UIKit.h>
#import "LocationHelper.h"
#import <CoreLocation/CoreLocation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "Harpy.h"
#import <Google/Analytics.h>
#import "EasyJSWebView.h"
#import "MyJSInterface.h"
#import "DataBaseHandler.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate,CBCentralManagerDelegate,CLLocationManagerDelegate>
{
    DataBaseHandler *objDBHelper;
}

+ (instancetype)shared;

@property (strong, nonatomic) UIWindow *window;
@property (strong,nonatomic) UIButton *button;
@property float latitude;
@property float longitude;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property(strong,nonatomic)CBCentralManager *bluetoothManager;
@property CLProximity lastProximity;
@property BOOL loadFlag;
@property NSString *userName;
@property NSString *rootUrl;
@property BOOL activeFlag;
@property BOOL backgroundFlag;
@property BOOL notificationFlag;
@property BOOL exitFlag;

@property(strong,nonatomic)NSMutableArray *beaconSentArray;
@property CLBeaconRegion *beaconRegion;
@property(strong,nonatomic)NSMutableArray *majorValues;
@property(strong,nonatomic)NSArray *beacons;
@property(strong,nonatomic)NSMutableArray *beaconSelectedArray;
@property(strong,nonatomic)NSMutableArray *compareArray;
@property(strong,nonatomic)NSString *UDID;
@property(strong,nonatomic)NSMutableArray *beaconMajor;



@end

