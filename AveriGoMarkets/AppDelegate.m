    //
    //  AppDelegate.m
    //  Averigo
    //
    //  Created by BTS on 07/11/16.
    //  Copyright © 2016 BTS. All rights reserved.
    //

#import "AppDelegate.h"
#import "AllConstants.h"
#import "AFNHelper.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "LoginViewController.h"
#import "TabBarViewController.h"
#import "HomeViewController.h"
#import "InitialViewController.h"
#import "UserDefaultHelper.h"
#import "DeviceName.h"
#import <IQKeyboardManager.h>
#import "Reachability.h"
#import "RegisterViewController.h"
#import "RealReachability.h"

@interface AppDelegate ()
{
    int backgroundFlagValue;
    
}

@end

@implementation AppDelegate

+ (instancetype)shared {
    
    return (AppDelegate *)[UIApplication sharedApplication].delegate;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [Fabric with:@[[Crashlytics class]]];
    
    NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
    
    NSString *newappVersion = [infoDict objectForKey:@"CFBundleShortVersionString"]; // example: 1.0.0
    
    NSString *oldappVersion = [NSString stringWithFormat: @"%@",[[NSUserDefaults standardUserDefaults] valueForKey: @"oldAppVersionNumber"]];

    NSString *newBuildNumber = [infoDict objectForKey:@"CFBundleVersion"];
    
    NSString *oldBuildNumber = [NSString stringWithFormat: @"%@",[[NSUserDefaults standardUserDefaults] valueForKey: @"oldBuildNumber"]];
    
    objDBHelper = [[DataBaseHandler alloc]init];

    if ((oldBuildNumber != newBuildNumber) ||  (oldappVersion != newappVersion)) {
        
        [self resetDefaults];
        
        [[NSUserDefaults standardUserDefaults] setObject: newBuildNumber forKey: @"oldBuildNumber"];
        
        [[NSUserDefaults standardUserDefaults] setObject: newappVersion forKey: @"oldAppVersionNumber"];
        
        if (oldappVersion != nil && oldappVersion != (id)[NSNull null] && ![oldappVersion  isEqual: @"(null)"]) {
        [[NSUserDefaults standardUserDefaults]setObject:@"Y" forKey:@"IntroViewed"];
        }
        [[NSUserDefaults standardUserDefaults] synchronize];

        [objDBHelper deleteDatabaseFromBundletoDocumentDirectory];
    }

     [objDBHelper createDatabaseInstance];
    
    self.rootUrl = infoDict[@"ROOT_URL"];
    
    NSLog(@"ROOT_URL is ------%@", self.rootUrl);
    
    GLobalRealReachability.hostForPing = @"www.google.com";
    GLobalRealReachability.hostForCheck = @"www.apple.com";
    [GLobalRealReachability startNotifier];
    
    backgroundFlagValue=0;
    
    [[IQKeyboardManager sharedManager] setPreviousNextDisplayMode:IQPreviousNextDisplayModeAlwaysHide];
    [[IQKeyboardManager sharedManager]setToolbarTintColor:[UIColor colorWithRed:0.20 green:0.60 blue:0.86 alpha:1.0]];
    
    [STPopupNavigationBar appearance].barTintColor = [UIColor colorWithRed:0.45 green:0.73 blue:0.31 alpha:1.0];
    [STPopupNavigationBar appearance].tintColor = [UIColor whiteColor];
    [STPopupNavigationBar appearance].barStyle = UIBarStyleDefault;
    [STPopupNavigationBar appearance].titleTextAttributes = @{ NSFontAttributeName:[UIFont fontWithName:FONT_MEDIUM size:20], NSForegroundColorAttributeName: [UIColor whiteColor]};
    
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:0.45 green:0.73 blue:0.31 alpha:1.0]];
    [[UINavigationBar appearance]setTranslucent:NO];

    NSDictionary *barButtonAppearanceDict = @{NSFontAttributeName : [UIFont fontWithName:FONT_BOLD size:17], NSForegroundColorAttributeName: [UIColor colorWithRed:0.00 green:0.51 blue:0.69 alpha:1.0]};
    [[UIBarButtonItem appearance] setTitleTextAttributes:barButtonAppearanceDict forState:UIControlStateNormal];
    
    
    DeviceName * deviceName = [[DeviceName alloc]init];
    NSString* Identifier  = [[[UIDevice currentDevice] identifierForVendor] UUIDString]; // IOS 6+
    NSString* deviceModel = [deviceName deviceModel]; // IOS 6+
    NSString* deviceOS    = [[UIDevice currentDevice] systemVersion]; // IOS 6+
    
    [[NSUserDefaults standardUserDefaults] setObject:Identifier  forKey:@"UUID"];
    [[NSUserDefaults standardUserDefaults] setObject:deviceModel forKey:@"deviceModel"];
    [[NSUserDefaults standardUserDefaults] setObject:deviceOS    forKey:@"deviceOS"];
    
    NSString *Intro_Viewed=[[UserDefaultHelper sharedObject]verifyStringValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"IntroViewed"]];
    if([Intro_Viewed isEqualToString:@"Y"])
        {
            [self getlocationbeacons];
        }

    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"BeaconHomeID"];
    UILocalNotification *notification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    if (notification)
    {
        [self application:application didReceiveLocalNotification:notification];
    }
    
    [[Harpy sharedInstance] setPresentingViewController:_window.rootViewController];
    [[Harpy sharedInstance] setAppName:APP_NAME];
    [[Harpy sharedInstance] setAlertType:HarpyAlertTypeOption];
    [[Harpy sharedInstance] checkVersion];
    
    GAI *gai = [GAI sharedInstance];
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIAppName
           value:APP_NAME];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    [gai trackerWithName:APP_NAME trackingId:@"UA-115070135-1"];
    gai.trackUncaughtExceptions = NO;
    gai.logger.logLevel = kGAILogLevelVerbose;
    gai.dispatchInterval = 5;
    
    NSString *SessionGUID=[NSString stringWithFormat:@"%@",[[NSProcessInfo processInfo] globallyUniqueString]];
    [[NSUserDefaults standardUserDefaults]setObject:SessionGUID forKey:@"SessionGUID"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
    return YES;
}

- (void)resetDefaults {
    NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
    NSDictionary * dict = [defs dictionaryRepresentation];
    for (id key in dict) {
        [defs removeObjectForKey:key];
    }
    [defs synchronize];
}

-(void)getlocationbeacons
{
    [self getLocation];
    [self SetUpBeaconUUID];
}

-(void)runMethod:(NSArray *)beacon
{
    
}

-(void)SetUpBeaconUUID
{
    UIUserNotificationType types = UIUserNotificationTypeBadge |
    UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
    
    UIUserNotificationSettings *mySettings =
    [UIUserNotificationSettings settingsForTypes:types categories:nil];
    
    [[UIApplication sharedApplication] registerUserNotificationSettings:mySettings];
    
    NSUUID *beaconUUID = [[NSUUID alloc] initWithUUIDString:@"74278BDA-B644-4520-8F0C-720EAF059920"];
    NSString *regionIdentifier = @"us.iBeaconModules";
    CLBeaconRegion *beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:beaconUUID identifier:regionIdentifier];
    switch ([CLLocationManager authorizationStatus])
    {
        case kCLAuthorizationStatusAuthorizedAlways:
        NSLog(@"Authorized Always");
        break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
        NSLog(@"Authorized when in use");
        break;
        case kCLAuthorizationStatusDenied:
        NSLog(@"Denied");
        break;
        case kCLAuthorizationStatusNotDetermined:
        NSLog(@"Not determined");
        break;
        case kCLAuthorizationStatusRestricted:
        NSLog(@"Restricted");
        break;
        
        default:
        break;
    }
    self.locationManager = [[CLLocationManager alloc] init];
    if([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    self.locationManager.delegate = self;
    //self.locationManager.pausesLocationUpdatesAutomatically = NO;
    beaconRegion.notifyOnEntry=YES;
    beaconRegion.notifyOnExit=YES;
    beaconRegion.notifyEntryStateOnDisplay=YES;
    [self.locationManager startMonitoringForRegion:beaconRegion];
    [self.locationManager startRangingBeaconsInRegion:beaconRegion];
    [self.locationManager startUpdatingLocation];
    
}

-(void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region
{
    [manager startRangingBeaconsInRegion:(CLBeaconRegion*)region];
    [self.locationManager startUpdatingLocation];
    _loadFlag=YES;
    _notificationFlag=NO;
    _exitFlag=NO;
}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region
{
    _loadFlag=NO;
    _activeFlag=NO;
    _exitFlag=YES;
    if(!_backgroundFlag)
        {
        NSLog(@"exit region called");
        }
    
}

- (void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region {
    
    NSDictionary *beaconDictionary = [NSDictionary dictionaryWithObject:beacons forKey:@"Beacons"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"BeaconNotification" object:nil userInfo:beaconDictionary];
    NSLog(@"beacon %@",beacons);
    
}


-(void)getLocation
{
    [[LocationHelper sharedObject]startLocationUpdatingWithBlock:^(CLLocation *newLocation, CLLocation *oldLocation, NSError *error)
     {
     _latitude=newLocation.coordinate.latitude;
     _longitude=newLocation.coordinate.longitude;
     [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%f",_latitude] forKey:@"Latitude"];
     [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%f",_longitude] forKey:@"Longitude"];
     [[NSUserDefaults standardUserDefaults]synchronize];
     if (!error)
         {
         [[LocationHelper sharedObject]stopLocationUpdating];
         }
     }];
}

- (void)applicationWillResignActive:(UIApplication *)application {


}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Background" object:@"true"];
    backgroundFlagValue=1;
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"LOCATION_BEACON"];
    [self updateCheckOutTime];
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    
    NSString * signedIn = [[NSUserDefaults standardUserDefaults]objectForKey:@"signedin"];
    
    if(backgroundFlagValue==1 && signedIn != nil && [[[NSUserDefaults standardUserDefaults]objectForKey:@"registerscreen"] isEqual:@"false"] )
        {
            [self openInitialViewController];
        }else if([[NSUserDefaults standardUserDefaults]objectForKey:@"otpscreen"]!=nil && [[[NSUserDefaults standardUserDefaults] objectForKey:@"terminateApp"]  isEqual: @"Y"]) {
            if([[[NSUserDefaults standardUserDefaults]objectForKey:@"otpscreen"] isEqual:@"true"]){
                [self openInitialViewController];
            }
        }else if(backgroundFlagValue==1 && signedIn != nil && [[NSUserDefaults standardUserDefaults] objectForKey:@"registerscreen"] == nil){
            [self openInitialViewController];
        }
     [[NSNotificationCenter defaultCenter] postNotificationName:@"appDidComeToForeGround" object:@"true"];
     [[NSUserDefaults standardUserDefaults]setObject:@"N" forKey:@"terminateApp"];
}

-(void)openInitialViewController {
    UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    InitialViewController *controller = (InitialViewController*)[mainStoryboard  instantiateViewControllerWithIdentifier: @"InitialViewController"];
    [navigationController pushViewController:controller animated:YES];
    backgroundFlagValue=0;
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    _backgroundFlag=YES;
    _notificationFlag=YES;
    _activeFlag=YES;
    [application cancelAllLocalNotifications];
    [[Harpy sharedInstance] checkVersionDaily];
    
}

- (void)applicationWillTerminate:(UIApplication *)application {
    _backgroundFlag=NO;
    _notificationFlag=NO;
    [[NSUserDefaults standardUserDefaults]setObject:@"Y" forKey:@"terminateApp"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"LOCATION_BEACON"];
    [self updateCheckOutTime];
}



-(void)updateCheckOutTime {
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString * checkOutTime=[dateFormatter stringFromDate:[NSDate date]];
    NSString * SessionGUID = [[NSUserDefaults standardUserDefaults] objectForKey:@"SessionGUID"];
    [objDBHelper UpdateUserAnalytics:SessionGUID andCheckOutTime:checkOutTime];
}

- (void)centralManagerDidUpdateState:(nonnull CBCentralManager *)central {

}

@end
