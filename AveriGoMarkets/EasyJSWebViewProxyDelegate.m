//
//  EasyJSWebViewDelegate.m
//  EasyJS
//
//  Created by Lau Alex on 19/1/13.
//  Copyright (c) 2013 Dukeland. All rights reserved.
//

#import "EasyJSWebViewProxyDelegate.h"
#import "EasyJSDataFunction.h"
#import <objc/runtime.h>

/*
 This is the content of easyjs-inject.js
 Putting it inline in order to prevent loading from files
 */
NSString* INJECT_JS = @"window.EasyJS = {\
__callbacks: {},\
\
invokeCallback: function (cbID, removeAfterExecute){\
var args = Array.prototype.slice.call(arguments);\
args.shift();\
args.shift();\
\
for (var i = 0, l = args.length; i < l; i++){\
args[i] = decodeURIComponent(args[i]);\
}\
\
var cb = EasyJS.__callbacks[cbID];\
if (removeAfterExecute){\
EasyJS.__callbacks[cbID] = undefined;\
}\
return cb.apply(null, args);\
},\
\
call: function (obj, functionName, args){\
var formattedArgs = [];\
for (var i = 0, l = args.length; i < l; i++){\
if (typeof args[i] == \"function\"){\
formattedArgs.push(\"f\");\
var cbID = \"__cb\" + (+new Date);\
EasyJS.__callbacks[cbID] = args[i];\
formattedArgs.push(cbID);\
}else{\
formattedArgs.push(\"s\");\
formattedArgs.push(encodeURIComponent(args[i]));\
}\
}\
\
var argStr = (formattedArgs.length > 0 ? \":\" + encodeURIComponent(formattedArgs.join(\":\")) : \"\");\
\
var iframe = document.createElement(\"IFRAME\");\
iframe.setAttribute(\"src\", \"easy-js:\" + obj + \":\" + encodeURIComponent(functionName) + argStr);\
document.documentElement.appendChild(iframe);\
iframe.parentNode.removeChild(iframe);\
iframe = null;\
\
var ret = EasyJS.retValue;\
EasyJS.retValue = undefined;\
\
if (ret){\
return decodeURIComponent(ret);\
}\
},\
\
inject: function (obj, methods){\
window[obj] = {};\
var jsObj = window[obj];\
\
for (var i = 0, l = methods.length; i < l; i++){\
(function (){\
var method = methods[i];\
var jsMethod = method.replace(new RegExp(\":\", \"g\"), \"\");\
jsObj[jsMethod] = function (){\
return EasyJS.call(obj, method, Array.prototype.slice.call(arguments));\
};\
})();\
}\
}\
};";

@implementation EasyJSWebViewProxyDelegate

@synthesize realDelegate;
@synthesize javascriptInterfaces;

NSTimer *timer;


- (void) addJavascriptInterfaces:(NSObject*) interface WithName:(NSString*) name{
    if (! self.javascriptInterfaces){
        self.javascriptInterfaces = [[NSMutableDictionary alloc] init];
    }
    [self.javascriptInterfaces setValue:interface forKey:name];
}

- (void) injectJavascript:(WKWebView *)webView{
    if (! self.javascriptInterfaces){
		self.javascriptInterfaces = [[NSMutableDictionary alloc] init];
	}
	
	NSMutableString* injection = [[NSMutableString alloc] init];
	
	//inject the javascript interface
	for(id key in self.javascriptInterfaces) {
		NSObject* interface = [self.javascriptInterfaces objectForKey:key];
		
		[injection appendString:@"EasyJS.inject(\""];
		[injection appendString:key];
		[injection appendString:@"\", ["];
		
		unsigned int mc = 0;
		Class cls = object_getClass(interface);
		Method * mlist = class_copyMethodList(cls, &mc);
		for (int i = 0; i < mc; i++){
			[injection appendString:@"\""];
			[injection appendString:[NSString stringWithUTF8String:sel_getName(method_getName(mlist[i]))]];
			[injection appendString:@"\""];
			
			if (i != mc - 1){
				[injection appendString:@", "];
			}
		}
		
		free(mlist);
		
		[injection appendString:@"]);"];
	}
	
	
	NSString* js = INJECT_JS;
	//inject the basic functions first
	[webView evaluateJavaScript:js completionHandler:nil];
	//inject the function interface
	[webView evaluateJavaScript:injection completionHandler:nil];
	
	//[injection release];
}

- (void)webView:(WKWebView *)webView didFailNavigation:(null_unspecified WKNavigation *)navigation withError:(nonnull NSError *)error
{
    if ([self.realDelegate respondsToSelector:@selector(webView:didFailNavigation:withError:)])
    [self.realDelegate webView:webView didFailNavigation:navigation withError:error];
    NSLog(@"Erro desc :%@",error.localizedDescription);
    
    //    [[NSNotificationCenter defaultCenter]
    //     postNotificationName:@"didFailLoadWithError"
    //     object:self];
    [timer invalidate];
    
}



- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(nonnull WKNavigationAction *)navigationAction decisionHandler:(nonnull void (^)(WKNavigationActionPolicy))decisionHandler{
    
    NSString *requestString = [[navigationAction.request URL] absoluteString];
    
    if ([requestString hasPrefix:@"easy-js:"]) {
        
        NSArray *components = [requestString componentsSeparatedByString:@":"];
        //NSLog(@"req: %@", requestString);
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"decidePolicyForNavigationAction"
         object:self];
        NSLog(@"Request String:%@",requestString);
        
        NSString* obj = (NSString*)[components objectAtIndex:1];
        NSString* method = [(NSString*)[components objectAtIndex:2]
                            stringByRemovingPercentEncoding];
        
        NSObject* interface = [javascriptInterfaces objectForKey:obj];
        
        // execute the interfacing method
        SEL selector = NSSelectorFromString(method);
        NSMethodSignature* sig = [[interface class] instanceMethodSignatureForSelector:selector];
        NSInvocation* invoker = [NSInvocation invocationWithMethodSignature:sig];
        invoker.selector = selector;
        invoker.target = interface;
        
        NSMutableArray* args = [[NSMutableArray alloc] init];
        
        if ([components count] > 3){
            NSString *argsAsString = [(NSString*)[components objectAtIndex:3]
                                      stringByRemovingPercentEncoding];
            
            NSArray* formattedArgs = [argsAsString componentsSeparatedByString:@":"];
            for (int i = 0, j = 0, l = [formattedArgs count]; i < l; i+=2, j++){
                NSString* type = ((NSString*) [formattedArgs objectAtIndex:i]);
                NSString* argStr = ((NSString*) [formattedArgs objectAtIndex:i + 1]);
                
                if ([@"f" isEqualToString:type]){
                    EasyJSDataFunction* func = [[EasyJSDataFunction alloc] initWithWebView:(EasyJSWebView *)webView];
                    func.funcID = argStr;
                    [args addObject:func];
                    [invoker setArgument:&func atIndex:(j + 2)];
                }else if ([@"s" isEqualToString:type]){
                    NSString* arg = [argStr stringByRemovingPercentEncoding];
                    [args addObject:arg];
                    [invoker setArgument:&arg atIndex:(j + 2)];
                }
            }
        }
        [invoker invoke];
        
        // https cant shown error..
        
        NSURL *url = [navigationAction.request URL];
        if (![url.scheme isEqual:@"http"] && ![url.scheme isEqual:@"https"]) {
            if ([[UIApplication sharedApplication]canOpenURL:url]) {
                [[UIApplication sharedApplication]openURL:url options:@{} completionHandler:nil];
                //decisionHandler(WKNavigationActionPolicyCancel);
                //return NO;
            }
        }
        else{
            //decisionHandler(WKNavigationActionPolicyAllow);
            
            //return YES;
            
        }
        
        //return the value by using javascript
        if ([sig methodReturnLength] > 0){
            NSString* retValue;
            [invoker getReturnValue:&retValue];
            
            if (retValue == NULL || retValue == nil){
                [webView evaluateJavaScript:@"EasyJS.retValue=null;" completionHandler:nil];
            }else{
                //retValue = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,(CFStringRef) retValue, NULL, (CFStringRef)@"!*'();:@&=+$,/?%#[]", kCFStringEncodingUTF8));
                NSCharacterSet* set = [NSCharacterSet characterSetWithCharactersInString:@"!*'();:@&=+$,/?%#[]"];
                retValue = [retValue stringByAddingPercentEncodingWithAllowedCharacters:set];
                [webView evaluateJavaScript:[@"" stringByAppendingFormat:@"EasyJS.retValue=\"%@\";", retValue] completionHandler:nil];
               
            }
        }
        
        //decisionHandler(WKNavigationActionPolicyCancel);
        
        //return NO;
    }
    
    if (! self.realDelegate){
        //decisionHandler(WKNavigationActionPolicyAllow);
        
        //return YES;
    }
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    if ([self.realDelegate respondsToSelector:@selector(webView:decidePolicyForNavigationAction:decisionHandler:)])
    [self.realDelegate webView:webView decidePolicyForNavigationAction:navigationAction decisionHandler:decisionHandler];
    decisionHandler(WKNavigationActionPolicyAllow);
    
    //return YES;
}



- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
    if ([self.realDelegate respondsToSelector:@selector(webView:didFinishNavigation:)])
    [self.realDelegate webView:webView didFinishNavigation:navigation];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"didFinishNavigation"
     object:self];
    [self injectJavascript:webView];
    [timer invalidate];
    
}

- (void)timeout
{
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"didFailNavigation"
     object:self];
}

- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation{
    if ([self.realDelegate respondsToSelector:@selector(webView:didStartProvisionalNavigation:)])
    [self.realDelegate webView:webView didStartProvisionalNavigation:navigation];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:25 target:self selector:@selector(timeout) userInfo:nil repeats:NO];
    
    if (! self.javascriptInterfaces)
    {
        self.javascriptInterfaces = [[NSMutableDictionary alloc] init];
    }
    
    NSMutableString* injection = [[NSMutableString alloc] init];
    
    //inject the javascript interface
    for(id key in self.javascriptInterfaces) {
        NSObject* interface = [self.javascriptInterfaces objectForKey:key];
        
        [injection appendString:@"EasyJS.inject(\""];
        [injection appendString:key];
        [injection appendString:@"\", ["];
        
        unsigned int mc = 0;
        Class cls = object_getClass(interface);
        Method * mlist = class_copyMethodList(cls, &mc);
        for (int i = 0; i < mc; i++){
            [injection appendString:@"\""];
            [injection appendString:[NSString stringWithUTF8String:sel_getName(method_getName(mlist[i]))]];
            [injection appendString:@"\""];
            
            if (i != mc - 1){
                [injection appendString:@", "];
            }
        }
        
        free(mlist);
        
        [injection appendString:@"]);"];
    }
    
    
    NSString* js = INJECT_JS;
    //inject the basic functions first
    [webView evaluateJavaScript:js completionHandler:nil];
    //inject the function interface
    [webView evaluateJavaScript:injection completionHandler:nil];
    
}

- (void)dealloc{
	if (self.javascriptInterfaces){
		//[self.javascriptInterfaces release];
		self.javascriptInterfaces = nil;
	}
	
	if (self.realDelegate){
		//[self.realDelegate release];
		self.realDelegate = nil;
	}
	
	//[super dealloc];
}

@end
