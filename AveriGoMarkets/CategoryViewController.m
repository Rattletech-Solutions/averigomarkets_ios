    //
    //  CategoryViewController.m
    //  Averigo
    //
    //  Created by BTS on 09/11/16.
    //  Copyright © 2016 BTS. All rights reserved.
    //

#import "CategoryViewController.h"
#import "CollectionViewCell.h"
#import <QuartzCore/QuartzCore.h>
#import "ItemsViewController.h"
#import "BarCodeController.h"
#import "AFNHelper.h"
#import "AllConstants.h"
#import "MBProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "ShoppingCartViewController.h"
#import "ProfileViewController.h"
#import "AlertVC.h"
#import "ActivityIndicator.h"
#import "UIImage+FontAwesome.h"
#import "Reachability.h"
#import <sys/utsname.h>
#import "RealReachability.h"
#import "AppDelegate.h"

#define IS_IPHONE        (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_IPHONE_5      (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 568.0)
#define IS_IPHONE_6PLUS  (IS_IPHONE && [[UIScreen mainScreen] nativeScale] == 3.0f)
#define IS_IPHONE_6_PLUS (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 736.0)
#define IS_IPHONE_X      (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 812.0)

#define IS_IPHONE_XS      (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 812.0)
#define IS_IPHONE_X_MAX      (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 896.0)
#define IS_RETINA        ([[UIScreen mainScreen] scale] >= 2.0) // 3.0 for iPhone X, 2.0 for others

#define IS_IPAD_DEVICE   [(NSString*)[UIDevice currentDevice].model hasPrefix:@"iPad"]

@interface CategoryViewController ()
{
    NSString *search_txt;
    int total_count;
    UILabel *count_lbl;
    UIRefreshControl *refreshController;
    BOOL beaconIsThere;
    NSString *featuredCategoryId;
    UITextField *searchField;
    BOOL noNetworkDidntLoad;
}

@end

@implementation CategoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
        // Do any additional setup after loading the view.
    objDBHelper = [[DataBaseHandler alloc]init];
    total_count=0;
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    NSLog(@"navigation controller %@",self.navigationController);
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor colorWithRed:0.98 green:0.96 blue:0.95 alpha:1.0],NSFontAttributeName:[UIFont fontWithName:FONT_REGULAR size:20]}];
    [self InitNavBar];
    [_category_view registerNib:[UINib nibWithNibName:@"CollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CollectionView_Cell"];
    refreshController = [[UIRefreshControl alloc] init];
    [refreshController addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    [_category_view addSubview:refreshController];
    
}


- (void) scanBeaconNotification:(NSNotification *) notification
{
    beaconIsThere = NO;
    NSDictionary *dict = notification.userInfo;
    _beacons=[dict valueForKey:@"Beacons"];
    NSMutableArray * UDIDS = [NSMutableArray array];
    for(int i=0;i<_beacons.count;i++)
        {
        CLBeacon *beacon = (CLBeacon*)[self.beacons objectAtIndex:i];
        NSString *BUUID=[NSString stringWithFormat:@"%d-%d",beacon.major.intValue,beacon.minor.intValue];
        [UDIDS addObject:BUUID];
        }
    _UDID= [UDIDS componentsJoinedByString:@","];
    NSString *location_beacon =  [[NSUserDefaults standardUserDefaults]objectForKey:@"LOCATION_BEACON"];
    NSLog(@"locationBeacon %@",location_beacon);
    if(location_beacon != nil){
        if(_beacons.count == 1){
            CLBeacon *beacon = (CLBeacon*)[self.beacons objectAtIndex:0];
            NSString *BUUID=[NSString stringWithFormat:@"%d-%d",beacon.major.intValue,beacon.minor.intValue];
            NSLog(@"location beacon %@",location_beacon);
            NSLog(@"beacon %@",BUUID);
            if(location_beacon == BUUID){
                
            }else{
                self.tabBarController.selectedIndex=0;
            }
        }else if(_beacons.count > 1){
            for(int i = 0; i<_beacons.count;i++){
                CLBeacon *beacon = (CLBeacon*)[self.beacons objectAtIndex:i];
                NSString *BUUID=[NSString stringWithFormat:@"%d-%d",beacon.major.intValue,beacon.minor.intValue];
                NSLog(@"location beacon %@",location_beacon);
                NSLog(@"beacon %@",BUUID);
                if(location_beacon == BUUID){
                    beaconIsThere = YES;
                }
            }
            if(!beaconIsThere){
                self.tabBarController.selectedIndex=0;
            }
        }else{
            self.tabBarController.selectedIndex=0;
        }
    }else{
        self.tabBarController.selectedIndex=0;
    }
    
}


-(void)viewDidAppear:(BOOL)animated
{
    if(self.microMarketId != [[NSUserDefaults standardUserDefaults]objectForKey:@"micromarketid"])
        {
        self.categoryArray = nil;
        [_category_view reloadData];
        }
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(scanBeaconNotification:)
                                                name:BEACON_NOTIFICATION object:nil];
    self.microMarketId = [[NSUserDefaults standardUserDefaults]objectForKey:@"micromarketid"];
    self.cartArray=[[NSMutableArray alloc]init];
    self.cartArray=[objDBHelper getCartItems:self.microMarketId];
    NSString *count=[objDBHelper getTotalItems:self.microMarketId];
    if(count!=nil)
        {
        count_lbl.text=count;
        }
    else{
        count_lbl.text=@"0";
    }
    
    if([self connected]){
         [self getCategories];
    }else{
        [AlertVC showAlertWithTitleForView:APP_NAME message:NO_INTERNET_CONNECTION controller:self];
        noNetworkDidntLoad = YES;
    }
   
    [super viewDidDisappear:YES];
}



-(void)viewDidDisappear:(BOOL)animated{
   
    [[ActivityIndicator sharedInstance] hideLoader];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:BARCODE_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:BEACON_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRealReachabilityChangedNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"showrecentpurchases" object:nil];
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    noNetworkDidntLoad = YES;
    self.tabBarController.tabBar.hidden=NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:SCAN_NOTIFICATION object:SHOW];
    [self setScreenAnalytics];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillTerminate:) name:UIApplicationWillTerminateNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillTerminate:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkChanged:)
                                                 name:kRealReachabilityChangedNotification
                                               object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showRecentPurchases:) name:@"showrecentpurchases" object:nil];
}

- (void)showRecentPurchases:(NSNotification *)notification {
    NSMutableArray *recentPurchaseArray = [[NSMutableArray alloc] init];
    recentPurchaseArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"recentpurchase"];
    
    NSDictionary *dictobject;
    dictobject = [recentPurchaseArray objectAtIndex:0];
    
    ItemsViewController *vc;
    vc = [self.storyboard instantiateViewControllerWithIdentifier: ITEMS_VC];
    vc.categoryId=[NSString stringWithFormat:@"'%@'",[dictobject valueForKey:@"CATEGORY_ID"]];
    vc.microMarketId=[dictobject valueForKey:@"LOCATION_ID"];
    vc.categoryName = [dictobject valueForKey:@"CATEGORY_DESC"];
    vc.itemsArray=nil;
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (void)networkChanged:(NSNotification *)notification {
    RealReachability *reachability = (RealReachability *)notification.object;
    ReachabilityStatus status = [reachability currentReachabilityStatus];
    if (status == 1 || status == 2){
        if(noNetworkDidntLoad){
            [self getCategories];
        }
    }
    NSLog(@"currentStatus:%@",@(status));
}

- (BOOL)connected {
    ReachabilityStatus status = [GLobalRealReachability currentReachabilityStatus];
    NSLog(@"Initial reachability status:%@",@(status));
    
    return status == 1 || status == 2;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}

-(IBAction)handleRefresh:(id)sender {
    [refreshController endRefreshing];
    [self getCategories];
}

-(void)InitNavBar {
    UISearchBar *searchBar = [[UISearchBar alloc] initWithFrame:CGRectZero];
    searchBar.delegate=self;
    searchBar.backgroundColor = [UIColor clearColor];
    searchBar.tintColor= [UIColor colorWithRed:0.00 green:0.51 blue:0.69 alpha:1.0];
    searchBar.backgroundImage = [[UIImage alloc] init];
    searchBar.backgroundColor = [UIColor clearColor];
    [searchBar sizeToFit];
    
    searchField = [searchBar valueForKey:@"searchField"];
    
        // To change background color
    searchField.backgroundColor = [UIColor whiteColor];
    searchField.layer.borderWidth = 2;
    searchField.layer.borderColor = [UIColor colorWithRed:0.00 green:0.51 blue:0.69 alpha:1.0].CGColor;
    searchField.layer.cornerRadius = 5;
    searchField.font = [UIFont fontWithName:FONT_REGULAR size:17];
        // To change text color
    searchField.textColor = [UIColor blackColor];
        // To change placeholder text color
    searchField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search by Product name"];
    UILabel *placeholderLabel = [searchField valueForKey:@"placeholderLabel"];
    placeholderLabel.textColor = [UIColor grayColor];
    //UISearchDisplayController *searchDisplayController= [[UISearchDisplayController alloc] initWithSearchBar:searchBar
                                                            //                              contentsController:self];
    //[searchDisplayController.searchBar setBackgroundImage:[self imageFromColor:[UIColor clearColor]]];
    
    //self.searchDisplayController.searchResultsDelegate = self;
    //self.searchDisplayController.searchResultsDataSource = self;
    //self.searchDisplayController.delegate = self;
    
    CGFloat topbarHeight = ([UIApplication sharedApplication].statusBarFrame.size.height +
                            (self.navigationController.navigationBar.frame.size.height ?: 0.0));
    UIView* titleView;
    if(IS_IPHONE_X) {
        titleView = [[UIView alloc] initWithFrame:CGRectMake(2, 10, CGRectGetWidth(self.view.bounds)-4, 44)];
    }else if (IS_IPHONE_X_MAX){
         titleView = [[UIView alloc] initWithFrame:CGRectMake(2, 10, CGRectGetWidth(self.view.bounds)-4, 44)];
    }
    else{
        titleView = [[UIView alloc] initWithFrame:CGRectMake(2, 5, CGRectGetWidth(self.view.bounds)-4, 44)];
    }
    searchBar.frame = titleView.bounds;
    titleView.backgroundColor = UIColor.clearColor;
    [titleView addSubview:searchBar];
    [self.view addSubview:titleView];
    
    self.navigationItem.title = SEARCH_TITLE;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor colorWithRed:0.98 green:0.96 blue:0.95 alpha:1.0],NSFontAttributeName:[UIFont fontWithName:FONT_REGULAR size:20]}];
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    
    [button setImage: [UIImage imageWithIcon:@"fa-cart-plus" backgroundColor:[UIColor clearColor] iconColor:[UIColor whiteColor] andSize:CGSizeMake(35,35)] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(CartClicked:)forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 32, 32)];
    count_lbl = [[UILabel alloc]initWithFrame:CGRectMake(20, -3, 25, 25)];
    [count_lbl setFont:[UIFont fontWithName:FONT_REGULAR size:13]];
    count_lbl.text=[NSString stringWithFormat:@"%d",total_count];
    count_lbl.textAlignment = NSTextAlignmentCenter;
    [count_lbl setTextColor:[UIColor colorWithRed:0.22 green:0.50 blue:0.24 alpha:1.0]];
    [count_lbl setBackgroundColor:[UIColor yellowColor]];
    count_lbl.layer.cornerRadius=12.5;
    count_lbl.clipsToBounds=YES;
    [button addSubview:count_lbl];
    self.cartArray=[[NSMutableArray alloc]init];
    self.cartArray=[objDBHelper getCartItems:self.microMarketId];
    
    NSString *count=[objDBHelper getTotalItems:self.microMarketId];
    if(count!=nil)
        {
        count_lbl.text=count;
        }
    else{
        count_lbl.text=@"0";
    }
    
    UIBarButtonItem *cart = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem=cart;
    
    self.button  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [self.button setImage:[UIImage imageWithIcon:@"fa-user-circle" backgroundColor:[UIColor clearColor] iconColor:[UIColor whiteColor] andSize:CGSizeMake(35,35)] forState:UIControlStateNormal];
    [self.button addTarget:self action:@selector(UserProfileClicked:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.button];
    self.navigationItem.leftBarButtonItem = rightBarButtonItem;
    
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    
}


- (IBAction)btnAllProductsClicked:(id)sender {
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    NSString *serviceName;
    serviceName = [[NSUserDefaults standardUserDefaults]objectForKey:@"serviceName"];
    
    [[ActivityIndicator sharedInstance] showLoader];
    
    if( serviceName != nil && self.microMarketId != nil ) {
        [dictParam setObject:self.microMarketId  forKey:@"MicroMarketID"];
        [dictParam setObject:serviceName           forKey:PARAM_SERVICE_NAME_KEY];
        [dictParam setObject:@"'all'"              forKey:@"CategoryID"];
        [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"username"] forKey:PARAM_EMAIL_KEY];
        NSLog(@"DICTPARAM:%@",dictParam);
        
        NSString *rootURL =  [[AppDelegate shared] rootUrl];

        AFNHelper *afn=[[AFNHelper alloc]init];
        [afn postDataFromPath:METHOD_ITEMS getUrl:rootURL withParamData:dictParam withBlock:^(id response, NSError *error)
         {
         if(error)
             {
             NSLog(@"ERROR DESC:%@",error.localizedDescription);
             [[ActivityIndicator sharedInstance] hideLoader];
             }
         else
             {
             NSLog(@"response %@",response);
             NSDictionary *dictionary;
             dictionary=response;
             NSString *status=[dictionary valueForKey:STATUS];
             
             NSLog(@"%ld",[self.categoryArray count]);
             if([status isEqualToString:SUCCESS])
                 {
                 [[ActivityIndicator sharedInstance] hideLoader];
                 self.itemsArray=[[NSMutableArray alloc]init];
                 self.itemsArray=[dictionary valueForKey:@"Items"];
                 ItemsViewController *vc;
                 vc = [self.storyboard instantiateViewControllerWithIdentifier: ITEMS_VC];
                 vc.categoryId=@"'all'" ;
                     vc.categoryName = @"All Products";
                 vc.microMarketId=self.microMarketId;
                 vc.itemsArray=self.itemsArray;
                 [self.navigationController pushViewController:vc animated:YES];
                 }
             else
                 {
                 [[ActivityIndicator sharedInstance] hideLoader];
                 NSString *message=[dictionary valueForKey:MESSAGE];
                 [AlertVC showAlertWithTitleForView:APP_NAME message:message controller:self];
                 }
             }
             [[ActivityIndicator sharedInstance] hideLoader];
         }];
        } else {
            // More Stuffs...
            [[ActivityIndicator sharedInstance] hideLoader];
        }
}


-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


-(IBAction)UserProfileClicked:(id)sender {
    ProfileViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    controller.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:controller animated:YES completion:nil];
}


- (UIImage *)imageFromColor:(UIColor *)color {
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldBeRequiredToFailByGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.categoryArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CollectionView_Cell";
    
    NSDictionary *dictobject;
    dictobject=[self.categoryArray objectAtIndex:indexPath.row];
    
    CollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if([[dictobject valueForKey:@"CATEGORY_ID"] isEqualToString:@"BESTSELLING"]){
        cell.cell_title.backgroundColor = [UIColor colorWithRed:0.45 green:0.73 blue:0.31 alpha:1.0];
    }else if([[dictobject valueForKey:@"CATEGORY_ID"] isEqualToString:@"FEATURE"]){
        cell.cell_title.backgroundColor = [UIColor redColor];
    }else if([[dictobject valueForKey:@"CATEGORY_ID"] isEqualToString:@"RECENTPURCHASE"]){
        cell.cell_title.backgroundColor = [UIColor colorWithRed:0.22 green:0.50 blue:0.69 alpha:1.0];
    }else{
        cell.cell_title.backgroundColor = [[UIColor colorWithRed:0.26 green:0.26 blue:0.26 alpha:1.0] colorWithAlphaComponent:0.7f];
    }
    
    cell.cell_title.text=[dictobject valueForKey:@"CATEGORY_DESC"];
    NSString *encoded = [[dictobject valueForKey:@"CAT_IMAGE"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"https://%@",encoded]];

    NSString *urlString = [NSString stringWithFormat:@"https://%@",[[dictobject valueForKey:@"CAT_IMAGE"]stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]]];
    if([urlString isEqualToString:@"https://"]){
        [cell.cell_img setContentMode:UIViewContentModeScaleAspectFit];
    }else{
        [cell.cell_img setContentMode:UIViewContentModeScaleAspectFill];
    }

    CGRect screenRect = [self.view bounds];
    CGFloat screenWidth = screenRect.size.width;
    [cell.cell_img sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"cart_placeholder.png"]];
    
    CALayer *layer = ((CollectionViewCell *) cell).layer;
    layer.cornerRadius = 10;
    layer.borderColor = [UIColor colorWithRed:0.26 green:0.26 blue:0.26 alpha:0.5].CGColor;
    layer.borderWidth = 1.0;
    layer.masksToBounds = YES;
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGRect screenRect = [self.view bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    float cellWidth = screenWidth / 2.2; //Replace the divisor with the column count requirement. Make sure to have it in float.
    float cellHeight = screenHeight/3;
    CGSize size = CGSizeMake(cellWidth,200);
    NSLog(@"%f,%f",cellWidth,cellHeight);
    return size;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    
    return UIEdgeInsetsMake(10,10,10,10);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dictobject;
    dictobject=[self.categoryArray objectAtIndex:indexPath.row];
    
    ItemsViewController *vc;
    vc = [self.storyboard instantiateViewControllerWithIdentifier: ITEMS_VC];
    vc.categoryId=[NSString stringWithFormat:@"'%@'",[dictobject valueForKey:@"CATEGORY_ID"]];
    vc.microMarketId=self.microMarketId;
    vc.categoryName = [dictobject valueForKey:@"CATEGORY_DESC"];
    vc.itemsArray=nil;
    //vc.categoryArray=self.categoryArray;
    [self.navigationController pushViewController:vc animated:YES];
    
}

-(IBAction)ScanClicked:(id)sender
{
    BarCodeController *vc;
    vc = [self.storyboard instantiateViewControllerWithIdentifier: BARCODE_VC];
    vc.micromarketid=self.microMarketId;
    [self.navigationController pushViewController:vc animated:YES];
}

-(IBAction)CartClicked:(id)sender
{
    if(self.cartArray.count>0)
        {
        ShoppingCartViewController *vc;
        vc=[self.storyboard instantiateViewControllerWithIdentifier:SHOPPING_CART_VC];
        vc.cart_array=self.cartArray;
        vc.micromarketid=self.microMarketId;
        [self.navigationController pushViewController:vc animated:YES];
        }
    else
        {
        [AlertVC showAlertWithTitleForView:APP_NAME message:THERE_ARE_NO_ITEMS controller:self];
        }
    
}

-(void)getCategories
{
    self.microMarketId=[[NSUserDefaults standardUserDefaults]objectForKey:@"micromarketid"];
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    NSLog(@"micromarketid:%@",self.microMarketId);
    
    NSString *beaconMajor;
    NSString *beaconMinor;
    NSString *serviceName;
    
    beaconMajor = [[NSUserDefaults standardUserDefaults]objectForKey:@"beaconMajor"];
    beaconMinor = [[NSUserDefaults standardUserDefaults]objectForKey:@"beaconMinor"];
    serviceName = [[NSUserDefaults standardUserDefaults]objectForKey:@"serviceName"];
    NSLog(@"DICTPARAM:%@",serviceName);
    
    if (self.microMarketId != nil && serviceName != nil)
        {
        [dictParam setObject:serviceName         forKey:@"ServiceName"];
        [dictParam setObject:beaconMajor         forKey:@"BeaconMajor"];
        [dictParam setObject:beaconMinor         forKey:@"BeaconMinor"];
        [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"username"] forKey:PARAM_EMAIL_KEY];
        NSLog(@"DICTPARAM:%@",dictParam);
        
        NSString *rootURL =  [[AppDelegate shared] rootUrl];

        AFNHelper *afn=[[AFNHelper alloc]init];
        [afn postDataFromPath:METHOD_RECONCILECATEGORYS getUrl:rootURL withParamData:dictParam withBlock:^(id response, NSError *error)
         {
         if(error)
             {
                 if(self.categoryArray != nil){
                    self.categoryArray=[[NSMutableArray alloc]init];
                    [self.category_view reloadData];
                 }
             NSLog(@"ERROR DESC:%@",error.localizedDescription);
                 if(error.code == NSURLErrorTimedOut){
                     [AlertVC showAlertWithTitleForView:APP_NAME message:ERROR_TIMED_OUT controller:self];
                 }else if(error.code == NSURLErrorNotConnectedToInternet){
                     [AlertVC showAlertWithTitleForView:APP_NAME message:NO_INTERNET_CONNECTION controller:self];
                 }
             }
         else
             {
             NSLog(@"response %@",response);
             NSDictionary *dictionary;
             dictionary=response;
             NSString *status=[dictionary valueForKey:STATUS];
             self.categoryArray=[[NSMutableArray alloc]init];
             self.categoryArray=[dictionary valueForKey:@"Categories"];
             NSLog(@"%ld",[self.categoryArray count]);
             if([status isEqualToString:SUCCESS])
                 {
                 [self.category_view reloadData];
                 [self.category_view setHidden:NO];
                 [self.no_lbl setHidden:YES];
                 }
             else
                 {
                 NSString *message=[dictionary valueForKey:MESSAGE];
                 [AlertVC showAlertWithTitleForView:APP_NAME message:message controller:self];
                 [self.category_view setHidden:YES];
                 [self.no_lbl setHidden:NO];
                 
                 }
                 noNetworkDidntLoad = NO;
             }
         
         }];
        
        }
    
    else
        {
            // do nothing...
            // [[ActivityIndicator sharedInstance] hideLoader];
        }
    
}

-(void) searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
        //This'll Show The cancelButton with Animation
    [searchBar setShowsCancelButton:YES animated:YES];
        //remaining Code'll go here
    UIToolbar* keyboardToolbar = [[UIToolbar alloc] init];
    [keyboardToolbar sizeToFit];
    UIBarButtonItem *flexBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                      target:nil action:nil];
    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                      target:self.view action:@selector(endEditing:)];
    keyboardToolbar.items = @[flexBarButton, doneBarButton];
    searchField.inputAccessoryView = keyboardToolbar;
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    [searchBar setShowsCancelButton:NO animated:YES];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    search_txt=searchBar.text;
    [self searchItems];
}

-(void)searchItems
{
    [[ActivityIndicator sharedInstance] showLoader];

    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    NSString *serviceName;
    serviceName = [[NSUserDefaults standardUserDefaults]objectForKey:@"serviceName"];
    
    if (serviceName != nil && self.microMarketId != nil )
        {
        [dictParam setObject:self.microMarketId   forKey:@"MicroMarketID"];
        [dictParam setObject:serviceName            forKey:@"ServiceName"];
        [dictParam setObject:search_txt             forKey:@"ItemDesc"];
        [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"username"] forKey:PARAM_EMAIL_KEY];
        NSLog(@"DICTPARAM:%@",dictParam);

        NSString *rootURL =  [[AppDelegate shared] rootUrl];

        AFNHelper *afn=[[AFNHelper alloc]init];
        [afn postDataFromPath:METHOD_SEARCHITEMS getUrl:rootURL withParamData:dictParam withBlock:^(id response, NSError *error)
         {
         if(error)
             {
             NSLog(@"ERROR DESC:%@",error.localizedDescription);
             [[ActivityIndicator sharedInstance] hideLoader];
                 if(error.code == NSURLErrorTimedOut){
                     [AlertVC showAlertWithTitleForView:APP_NAME message:ERROR_TIMED_OUT controller:self];
                 }else if(error.code == NSURLErrorNotConnectedToInternet){
                     [AlertVC showAlertWithTitleForView:APP_NAME message:NO_INTERNET_CONNECTION controller:self];
                 }

             }
         else
             {
             NSLog(@"response %@",response);
             NSDictionary *dictionary;
             dictionary=response;
             NSString *status=[dictionary valueForKey:STATUS];
             
             NSLog(@"%ld",[self.categoryArray count]);
             if([status isEqualToString:SUCCESS])
                 {
                 [[ActivityIndicator sharedInstance] hideLoader];

                 self.itemsArray=[[NSMutableArray alloc]init];
                 self.itemsArray=[dictionary valueForKey:@"Items"];
                 ItemsViewController *vc;
                 vc = [self.storyboard instantiateViewControllerWithIdentifier: ITEMS_VC];
                 vc.categoryId=EMP_STR;
                 vc.microMarketId=self.microMarketId;
                 vc.itemsArray=self.itemsArray;
                 [self.navigationController pushViewController:vc animated:YES];
                 }
             else
                 {
                 [[ActivityIndicator sharedInstance] hideLoader];

                 NSString *message=[dictionary valueForKey:MESSAGE];
                 [AlertVC showAlertWithTitleForView:APP_NAME message:message controller:self];
                 }
             }
            [[ActivityIndicator sharedInstance] hideLoader];
         }];
        }
    else
        {
            [[ActivityIndicator sharedInstance] hideLoader];
        }
    
    
}

-(void)setScreenAnalytics {
    NSString * SessionGUID = [[NSUserDefaults standardUserDefaults] objectForKey:@"SessionGUID"];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString * checkInTime=[dateFormatter stringFromDate:[NSDate date]];
    NSString * micromarketid = [[NSUserDefaults standardUserDefaults] valueForKey:@"micromarketid"];
    NSString * micromarketname = [[NSUserDefaults standardUserDefaults] valueForKey:@"micromarketname"];
    NSString * userID = [[NSUserDefaults standardUserDefaults] objectForKey:@"username"];
    
    NSString * analyticsCount = [objDBHelper getScreenAnalyticsBySessionID:SessionGUID andScreenName:@"Category"];
    int visitCount = [analyticsCount intValue] +1;
    
    if(visitCount == 1) {
        [objDBHelper insertScreenAnalytics:userID andsessionGUID:SessionGUID andMircoMarketID:[micromarketid intValue] andMicroMarketName:micromarketname andScreenName:@"Category" andscreenVisits:visitCount andCheckInTime:checkInTime andCheckOutTime:@"" andSyncFlag:@"Y"];
    } else {
        [objDBHelper UpdateScreenAnalytics:SessionGUID andVisitCount:visitCount andScreenName:@"Category"];
    }
    
}


-(void)updateScreenAnalyticsCheckOutTime {
    NSString * SessionGUID = [[NSUserDefaults standardUserDefaults] objectForKey:@"SessionGUID"];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString * checkOutTime=[dateFormatter stringFromDate:[NSDate date]];
    [objDBHelper UpdateScreenAnalyticsCheckOutTime:SessionGUID andCheckOutTime:checkOutTime andScreenName:@"Category"];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillTerminateNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
}

-(void)viewWillDisappear:(BOOL)animated {
    [self updateScreenAnalyticsCheckOutTime];
    [super viewWillDisappear:true];
}


-(void)appWillTerminate:(NSNotification*)note
{
    [self updateScreenAnalyticsCheckOutTime];
    
    
}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
