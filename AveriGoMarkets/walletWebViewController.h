    //
    //  walletWebViewController.h
    //  VendBeam
    //
    //  Created by BTS on 10/07/18.
    //  Copyright © 2018 BTS. All rights reserved.
    //

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>
#import "MyJSInterface.h"
#import "EasyJSWebView.h"
#import "DataBaseHandler.h"

@interface walletWebViewController : UIViewController<WKNavigationDelegate,UITabBarDelegate, WKUIDelegate, UIScrollViewDelegate>
{
DataBaseHandler *objDBHelper;
}

@property (strong, nonatomic) IBOutlet EasyJSWebView *webview;
@property (strong, nonatomic)IBOutlet UIProgressView* progressView;
@property (strong, nonatomic)IBOutlet UILabel *noInternetLbl;
@property (strong,nonatomic) NSTimer *timer;
@property (strong,nonatomic) UIButton *button;
@property BOOL checkFlag;
@property BOOL webViewLoaded;
@property (strong,nonatomic)  NSMutableArray  *beacons;
@property (strong,nonatomic)  NSString *UDID;



@end
