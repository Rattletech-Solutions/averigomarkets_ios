//
//  MyJSInterface.h
//  EasyJSWebViewSample
//
//  Created by Lau Alex on 19/1/13.
//  Copyright (c) 2013 Dukeland. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <WebKit/WebKit.h>

@interface MyJSInterface : NSObject
- (void)showPopup:(NSString *)msg :(NSString* )options;

@end
