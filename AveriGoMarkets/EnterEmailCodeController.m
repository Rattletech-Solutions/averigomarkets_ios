//
//  EnterEmailCodeController.m
//  AveriGo Markets
//
//  Created by Macmini on 11/12/18.
//  Copyright © 2018 BTS. All rights reserved.
//

#import "EnterEmailCodeController.h"
#import <STPopup.h>
#import "IQKeyBoardManager/IQKeyboardManager.h"
#import "IQKeyBoardManager/IQKeyboardReturnKeyHandler.h"
#import "AFNHelper.h"
#import "AFHTTPSessionManager.h"
#import "MBProgressHUD.h"
#import "Reachability.h"
#import "ActivityIndicator.h"
#import "AllConstants.h"
#import "SignUpUtiltiy.h"
#import "RealReachability.h"


@interface EnterEmailCodeController ()

@end

@implementation EnterEmailCodeController
{
    IQKeyboardReturnKeyHandler *returnKeyHandler;
    NSString *txtPhoneToPass;
    NSString *txtEmail;
    int  randomNo;
    NSString *concatPhoneNum;
    STPopupControllerTransitioningContext *stContext;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self){
        [self customInit];
    }
    return self;
}

- (instancetype)init
{
    if (self = [super init]) {
        self.contentSizeInPopup = CGSizeMake(375, 250);
    }
    return self;
}

-(void)customInit{
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    self.contentSizeInPopup = CGSizeMake(screenWidth - 10, 250);
    
    
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.title = @"Enter your Verification Code";
    self.field1.tag = 20;
    self.field2.tag = 21;
    self.field3.tag = 22;
    self.field4.tag = 23;
    [self SetTextFieldBorder:self.field1];
    [self SetTextFieldBorder:self.field2];
    [self SetTextFieldBorder:self.field3];
    [self SetTextFieldBorder:self.field4];
    
    self.verifyBtn.layer.cornerRadius = 5;
    self.verifyBtn.clipsToBounds = YES;
    
    
   
    // Do any additional setup after loading the view.
}


-(void)viewDidDisappear:(BOOL)animated{
    
}

-(void)viewWillDisappear:(BOOL)animated{
    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
    if (state == UIApplicationStateBackground || state == UIApplicationStateInactive)
    {
        
    }else{
        [[NSUserDefaults standardUserDefaults]setObject:@"false" forKey:@"otpscreen"];
    }
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear: animated];
   
     [[NSUserDefaults standardUserDefaults]setObject:@"true" forKey:@"otpscreen"];
    
}

-(void)SetTextFieldBorder :(UITextField *)textField{
    
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1.5;
    border.borderColor = [UIColor blackColor].CGColor;
    border.frame = CGRectMake(0, textField.frame.size.height - borderWidth, textField.frame.size.width, textField.frame.size.height);
    border.borderWidth = borderWidth;
    
    [textField.layer addSublayer:border];
    textField.layer.masksToBounds = YES;
    textField.font =[UIFont fontWithName:FONT_MEDIUM size:15.f];
    textField.textAlignment=NSTextAlignmentCenter;
    textField.textColor=[UIColor blackColor];
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.tag >= 20 && textField.tag <= 23)
    {
        if(textField.tag >= 20 && textField.tag <= 23 && string.length > 0){
            [textField setText:string];
            NSInteger nextText = textField.tag + 1;
            UIResponder* nextResponder = [textField.superview viewWithTag:nextText];
            if (! nextResponder)
            {
                nextResponder = [textField.superview viewWithTag:1];
            }
            else
            {
                [nextResponder becomeFirstResponder];
                UITextField *nextTextfield = [textField.superview viewWithTag:nextText];
                [nextTextfield setText:EMP_STR];
                return NO;
            }
            return NO;
        }
        else if (textField.tag >= 20 && textField.tag <= 23  && string.length <= 0)
        {
            NSInteger beforeText = textField.tag - 1;
            UIResponder* previousResponder = [textField.superview viewWithTag:beforeText];
            
            if (previousResponder == nil)
            {
                previousResponder = [textField.superview viewWithTag:1];
            }
            textField.text = EMP_STR;
            [previousResponder becomeFirstResponder];
            return NO;
        }
        return YES;
    }
    
    if (textField.tag  == 30)
    {
        NSString* totalString = [NSString stringWithFormat:@"%@%@",textField.text,string];
        
        if (range.length == 1) {
            // Delete button was hit.. so tell the method to delete the last char.
            textField.text = [self formatPhoneNumber:totalString deleteLastChar:YES];
        } else {
            textField.text = [self formatPhoneNumber:totalString deleteLastChar:NO ];
        }
        return false;
    }
    
    return YES;
}

-(NSString*) formatPhoneNumber:(NSString*) simpleNumber deleteLastChar:(BOOL)deleteLastChar {
    if(simpleNumber.length==0) return EMP_STR;
    // use regex to remove non-digits(including spaces) so we are left with just the numbers
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[\\s-\\(\\)]" options:NSRegularExpressionCaseInsensitive error:&error];
    simpleNumber = [regex stringByReplacingMatchesInString:simpleNumber options:0 range:NSMakeRange(0, [simpleNumber length]) withTemplate:EMP_STR];
    
    // check if the number is to long
    if(simpleNumber.length>10) {
        // remove last extra chars.
        simpleNumber = [simpleNumber substringToIndex:10];
    }
    
    if(deleteLastChar) {
        // should we delete the last digit?
        simpleNumber = [simpleNumber substringToIndex:[simpleNumber length] - 1];
    }
    
    // 123 456 7890
    // format the number.. if it's less then 7 digits.. then use this regex.
    if(simpleNumber.length<7)
        simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d+)"
                                                               withString:@"$1-$2"
                                                                  options:NSRegularExpressionSearch
                                                                    range:NSMakeRange(0, [simpleNumber length])];
    
    else   // else do this one..
        simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d{3})(\\d+)"
                                                               withString:@"$1-$2-$3"
                                                                  options:NSRegularExpressionSearch
                                                                    range:NSMakeRange(0, [simpleNumber length])];
    return simpleNumber;
}

-(IBAction)verifyCode:(id)sender
{
    
    if (self.field1.text.length == 1 && self.field2.text.length == 1 && self.field3.text.length == 1 && self.field4.text.length == 1)
    {
        NSString *concatPopUpValue = [NSString stringWithFormat: @"%@%@%@%@", self.field1.text, self.field2.text, self.field3.text, self.field4.text ];
        
        int intOfRandomNumber = concatPopUpValue.intValue;
        
        if ( [[[NSUserDefaults standardUserDefaults] valueForKey:@"popCode"] intValue] == intOfRandomNumber )
        {

            [[NSUserDefaults standardUserDefaults]setObject:@"false" forKey:@"otpscreen"];
            [[NSNotificationCenter defaultCenter] postNotificationName:
             @"EmailIdVerified" object:nil userInfo:nil];
            [self.popupController dismiss];
            
        }
        else
        {
            [self showAlert:@"Incorrect Verification Code. Please try again."];
        }
    }
    else
    {
        [self showAlert:@"Please Enter Valid Value"];
    }
    
}

-(IBAction)sendCode:(id)sender{
    [self sendSMSCode];
    [self sendEmailThroughSendGrid];
}

-(void)showAlert:(NSString *) message {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:APP_NAME message:message preferredStyle:UIAlertControllerStyleAlert];
    NSMutableAttributedString *titleAttr = [[NSMutableAttributedString alloc] initWithString:APP_NAME];
    [titleAttr addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:FONT_MEDIUM size:20]
                      range:NSMakeRange(0, [titleAttr length])];
    
    [alertController setValue:titleAttr forKey:@"attributedTitle"];
    
    NSMutableAttributedString *messageAttr = [[NSMutableAttributedString alloc] initWithString:message];
    [messageAttr addAttribute:NSFontAttributeName
                        value:[UIFont fontWithName:FONT_REGULAR size:14]
                        range:NSMakeRange(0, [messageAttr length])];
    
    
    [alertController setValue:messageAttr forKey:@"attributedMessage"];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alertController addAction:defaultAction];
    [self presentViewController:alertController animated:YES completion:nil];
}


-(void)sendSMSCode
{

    [[SignUpUtiltiy sharedInstance] sendCode:[[[NSUserDefaults standardUserDefaults] valueForKey:@"popCode"] intValue] andMobile:[[NSUserDefaults standardUserDefaults] valueForKey:@"popMobile"] andCountry:[[NSUserDefaults standardUserDefaults] valueForKey:@"popCountry"]  andEmail:[[NSUserDefaults standardUserDefaults] valueForKey:@"popEmail"] andController:self.popupController];
}

-(void)sendEmailThroughSendGrid
{
    
    [[SignUpUtiltiy sharedInstance] sendEmailThroughSendGrid:[[[NSUserDefaults standardUserDefaults] valueForKey:@"popCode"] intValue]  andEmail:[[NSUserDefaults standardUserDefaults] valueForKey:@"popEmail"] andController:self.popupController];
    
}

- (BOOL)connected
{
    ReachabilityStatus status = [GLobalRealReachability currentReachabilityStatus];
    NSLog(@"Initial reachability status:%@",@(status));
    
    return status == 1 || status == 2;
}





/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
