//
//  HomeCell.m
//  Averigo
//
//  Created by R VIKRAM on 23/03/17.
//  Copyright © 2017 BTS. All rights reserved.
//

#import "HomeCell.h"

@implementation HomeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _banner_lbl.backgroundColor = [UIColor clearColor];
    self.layer.masksToBounds = false;
    self.layer.shadowColor = [UIColor colorWithRed:0.45 green:0.73 blue:0.31 alpha:1.0].CGColor;
    self.layer.shadowOpacity = 5.0;
    self.layer.shadowOffset = CGSizeMake(0.0, 5.0);
    self.layer.shadowRadius = 5.0;
    self.layer.cornerRadius = 5.0;
    
    _top_view.layer.shadowOffset = CGSizeMake(0,1);
    _top_view.layer.masksToBounds = false;
    _top_view.layer.shadowColor = [UIColor colorWithRed:0.45 green:0.73 blue:0.31 alpha:1.0].CGColor;
    _top_view.layer.shadowOpacity = 5.0;
    _top_view.layer.shadowRadius = 5.0;
    
    self.bestSellingBtn.layer.cornerRadius = 5;
    self.bestSellingBtn.clipsToBounds = true;
    
//    _viewButton.backgroundColor = [UIColor clearColor];
    // Initialization code
}

@end
