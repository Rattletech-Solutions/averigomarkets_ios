//
//  ChoosePhotoViewController.m
//  AveriGo Markets
//
//  Created by Macmini on 6/12/19.
//  Copyright © 2019 BTS. All rights reserved.
//

#import "ChoosePhotoViewController.h"
#import <STPopup.h>
#import "CustomImageCell.h"
#import "AllConstants.h"

@interface ChoosePhotoViewController (){
    NSMutableArray *imageArray;
}

@end

@implementation ChoosePhotoViewController

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self){
        [self customInit];
    }
    return self;
}

- (instancetype)init
{
    if (self = [super init]) {
        self.contentSizeInPopup = CGSizeMake(375, 200);
    }
    return self;
}

-(void)customInit{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    self.contentSizeInPopup = CGSizeMake(screenWidth - 25, 280);
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Attach Image";
   
    self.fromCameraBtn.layer.cornerRadius = 5;
    self.fromCameraBtn.clipsToBounds = YES;
    
    self.fromGalleryBtn.layer.cornerRadius = 5;
    self.fromGalleryBtn.clipsToBounds = YES;
    
    self.includeBtn.layer.cornerRadius = 5;
    self.includeBtn.clipsToBounds = YES;
    [self.includeBtn setHidden:YES];
    
    imageArray = [[NSMutableArray alloc]init];
    
    [self.attachmentView registerNib:[UINib nibWithNibName:@"CustomImageCell" bundle:nil] forCellWithReuseIdentifier:@"CustomImageCell"];
}

-(IBAction)fromCameraClicked:(id)sender{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:NULL];
}

-(IBAction)fromGalleryClicked:(id)sender{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];

}

-(IBAction)includeAttachmentClicked:(id)sender{
     NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithCapacity:1];
     [userInfo setObject:imageArray forKey:@"image"];
    
    if(imageArray.count > 0){
     NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
     [nc postNotificationName:@"imageSelected" object:self userInfo:userInfo];
    
     [self.popupController popViewControllerAnimated:YES];
    }else{
       [AlertVC showAlertWithTitleForView:APP_NAME message:@"Attach atleast one image" controller:self];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    //self.imageView.image = chosenImage;
    NSData *data = UIImagePNGRepresentation(chosenImage);
    
    [imageArray addObject:chosenImage];
    
    if(imageArray.count > 0){
        [self.includeBtn setHidden:NO];
    }
    [_attachmentView reloadData];
   
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [imageArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CustomImageCell";
    
    CustomImageCell *cell=[self.attachmentView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.attachmentImage.layer.cornerRadius = 5;
    cell.attachmentImage.clipsToBounds = true;
    cell.attachmentImage.layer.borderColor = [UIColor grayColor].CGColor;
    cell.attachmentImage.layer.borderWidth = 1;
    cell.attachmentImage.contentMode = UIViewContentModeScaleToFill;
    cell.attachmentImage.image = [imageArray objectAtIndex:indexPath.row];
    
    return cell;
}

//- (CGSize)collectionView:(UICollectionView *)collectionView
//                  layout:(UICollectionViewLayout *)collectionViewLayout
//  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
//{
//
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
