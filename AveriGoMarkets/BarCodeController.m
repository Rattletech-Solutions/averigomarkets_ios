//
//  BarCodeController.m
//  Averigo
//
//  Created by BTS on 10/11/16.
//  Copyright © 2016 BTS. All rights reserved.
//

#import "BarCodeController.h"
#import "ScanCell.h"
#import "ShoppingCartViewController.h"
#import "KGModal.h"
#import "MBProgressHUD.h"
#import "AllConstants.h"
#import "AFNHelper.h"
#import "Reachability.h"
#import "CheckOutWebViewController.h"
#import "HomeViewController.h"
#import "CategoryViewController.h"
#import "TabBarViewController.h"
#import "AlertVC.h"
#import "ActivityIndicator.h"
#import "AppDelegate.h"

@interface BarCodeController (){
    BOOL isFirstTime;
    UIView *pop_view;
    UIView *itemPopView;
    NSString *total;
    NSString *barcode;
    BOOL addCode;
    BOOL beaconIsThere;
    BOOL flashOff;
    NSString *discountTotal;
    NSString *itemNoStr;
    NSData *imageData;
    NSString *imageStr;
    NSString *detectionString;
    BOOL notViewController;
    CGAffineTransform _captureSizeTransform;
    AVCaptureDeviceInput *input;
    NSString *scannedBarCodeStr;
}

@end

@implementation BarCodeController

- (void)dealloc {
    [self.capture.layer removeFromSuperlayer];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //[self startScan];
    scancount=0;
    addCode = NO;
    self.scanning = NO;
    [self InitNavBar];
    [self.notScanningBtn.titleLabel setAdjustsFontSizeToFitWidth:YES];
    objDBHelper = [[DataBaseHandler alloc]init];
    self.micromarketid=[[NSUserDefaults standardUserDefaults]objectForKey:@"micromarketid"];
    total=[objDBHelper getcount:self.micromarketid];
    discountTotal = [objDBHelper getDiscountTotal:self.micromarketid];
    // _total_lbl.layer.cornerRadius = 5.0;
    // _total_lbl.clipsToBounds = YES;
    
    if(total!=nil)
    {
        self.total_lbl.text=[NSString stringWithFormat:@"Total : $%@",total];
    }else
    {
        self.total_lbl.text=@"Total : $0.00";
    }
    self.item_tableview.layer.borderWidth = 2.0;
    self.item_tableview.layer.borderColor = [UIColor blackColor].CGColor;
    [self.item_tableview registerNib:[UINib nibWithNibName:SCAN_CELL bundle:nil] forCellReuseIdentifier:@"Scan_Cell"];
    self.checkout_btn.layer.cornerRadius=5;
    self.checkout_btn.clipsToBounds=YES;
    _demo_title=[[NSMutableArray alloc]init];
    _dollar_price=[[NSMutableArray alloc]init];
    
    self.items_array=[[NSMutableArray alloc]init];
    self.temp_array=[[NSArray alloc]init];
    if([[objDBHelper getCartItems:self.micromarketid] count]!=0)
    {
        self.items_array=[objDBHelper getCartItems:self.micromarketid];
    }
    itemNoStr = @"";
    for(int i=0;i<self.items_array.count;i++){
        NSDictionary *dictobject;
        dictobject = [self.items_array objectAtIndex:i];
        if(i == 0){
            itemNoStr = [NSString stringWithFormat:@"'%@'",[dictobject valueForKey:@"ItemNo"]];
        }else{
            itemNoStr = [NSString stringWithFormat:@"%@,'%@'",itemNoStr,[dictobject valueForKey:@"ItemNo"]];
        }
    }
    NSLog(@"itemNos %@",itemNoStr);
    //    [[ActivityIndicator sharedInstance] showLoader];
    //    [self validateCartItem];
    [_hints removePossibleFormat:kBarcodeFormatEan8];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)scanBeaconNotification:(NSNotification *) notification {
    beaconIsThere = NO;
    NSDictionary *dict = notification.userInfo;
    _beacons=[dict valueForKey:@"Beacons"];
    NSMutableArray * UDIDS = [NSMutableArray array];
    for(int i=0;i<_beacons.count;i++)
    {
        CLBeacon *beacon = (CLBeacon*)[self.beacons objectAtIndex:i];
        NSString *BUUID=[NSString stringWithFormat:@"%d-%d",beacon.major.intValue,beacon.minor.intValue];
        [UDIDS addObject:BUUID];
    }
    _UDID= [UDIDS componentsJoinedByString:@","];
    NSString *location_beacon =  [[NSUserDefaults standardUserDefaults]objectForKey:@"LOCATION_BEACON"];
    if(location_beacon != nil){
        if(_beacons.count == 1){
            CLBeacon *beacon = (CLBeacon*)[self.beacons objectAtIndex:0];
            NSString *BUUID=[NSString stringWithFormat:@"%d-%d",beacon.major.intValue,beacon.minor.intValue];
            
            if(location_beacon == BUUID){
            }else{
                self.tabBarController.selectedIndex=0;
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
        }else if(_beacons.count > 1){
            for(int i = 0; i<_beacons.count;i++){
                CLBeacon *beacon = (CLBeacon*)[self.beacons objectAtIndex:i];
                NSString *BUUID=[NSString stringWithFormat:@"%d-%d",beacon.major.intValue,beacon.minor.intValue];
                
                if(location_beacon == BUUID){
                    beaconIsThere = YES;
                }
            }
            if(!beaconIsThere){
                [self.navigationController popToRootViewControllerAnimated:YES];
                self.tabBarController.selectedIndex=0;
                
            }
        }else{
            [self.navigationController popToRootViewControllerAnimated:YES];
            self.tabBarController.selectedIndex=0;
        }
    }else{
        [self.navigationController popToRootViewControllerAnimated:YES];
        self.tabBarController.selectedIndex=0;
        
    }
    
}

- (void)InitNavBar {
    self.navigationItem.title = SCAN_BARCODE;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor colorWithRed:0.98 green:0.96 blue:0.95 alpha:1.0],NSFontAttributeName:[UIFont fontWithName:FONT_MEDIUM size:20]}];
    
    [self flashNavigationItem];
}

- (void)flashNavigationItem {
    self.navigationItem.rightBarButtonItem = nil;
    UIButton *flashBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [flashBtn setImage:[UIImage imageNamed:@"flash_off"] forState:UIControlStateNormal];
    [flashBtn setTintColor:UIColor.whiteColor];
    flashBtn.frame = CGRectMake(0, 0, 32, 32);
    [flashBtn addTarget:self action:@selector(flashClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *backButtonView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
    [backButtonView addSubview:flashBtn];
    
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButtonView];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    flashOff = YES;
}

- (void)viewDidAppear:(BOOL)animated {
    scannedBarCodeStr = @"";
    isFirstTime = YES;
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(scanBeaconNotification:)
                                                name:BEACON_NOTIFICATION object:nil];
    scancount=0;
    self.micromarketid=[[NSUserDefaults standardUserDefaults]objectForKey:@"micromarketid"];
    self.items_array=[objDBHelper getCartItems:self.micromarketid];
    [self.item_tableview reloadData];
    total=[objDBHelper getcount:self.micromarketid];
    discountTotal = [objDBHelper getDiscountTotal:self.micromarketid];
    if(total!=nil)
    {
        self.total_lbl.text=[NSString stringWithFormat:@"Total : $%@",total];
    }else
    {
        self.total_lbl.text=@"Total : $0.00";
    }
    
    [[ActivityIndicator sharedInstance] showLoader];
    
    [self startScan];
    
    [self validateCartItem];
    
    
    [super viewDidAppear:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear: animated];

    notViewController = NO;
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self setScreenAnalytics];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillTerminate:) name:UIApplicationWillTerminateNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillTerminate:) name:UIApplicationDidEnterBackgroundNotification object:nil];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear: animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:BEACON_NOTIFICATION object:nil];
    notViewController = YES;
    [[KGModal sharedInstance] hideAnimated:YES];
    [self.decodedLabel setText:@""];
    [self stopReading];
}

- (IBAction)flashClicked:(id)sender {
    self.navigationItem.rightBarButtonItem = nil;
    UIButton *flashBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [flashBtn setTintColor:UIColor.whiteColor];
    flashBtn.frame = CGRectMake(0, 0, 32, 32);
    [flashBtn addTarget:self action:@selector(flashClicked:) forControlEvents:UIControlEventTouchUpInside];
    if(flashOff){
        [flashBtn setImage:[UIImage imageNamed:@"flash_on"] forState:UIControlStateNormal];
        flashOff = false;
    }else{
        [flashBtn setImage:[UIImage imageNamed:@"flash_off"] forState:UIControlStateNormal];
        flashOff = true;
    }
    
    UIView *backButtonView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
    [backButtonView addSubview:flashBtn];
    
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButtonView];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    
    [self handleFlashLight];
}

- (void)handleFlashLight {
    AVCaptureDevice *flashLight = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if ([flashLight isTorchAvailable] && [flashLight isTorchModeSupported:AVCaptureTorchModeOn])
    {
        BOOL success = [flashLight lockForConfiguration:nil];
        if (success)
        {
            if ([flashLight isTorchActive])
            {
                [flashLight setTorchMode:AVCaptureTorchModeOff];
            }
            else
            {
                [flashLight setTorchMode:AVCaptureTorchModeOn];
            }
            [flashLight unlockForConfiguration];
        }
    }
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.items_array count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Scan_Cell";
    
    NSDictionary *dictobject;
    dictobject=[self.items_array objectAtIndex:indexPath.row];
    ScanCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.title_lbl.text=[dictobject valueForKey:@"ItemName"];
    cell.price_lbl.text=[NSString stringWithFormat:@"$%@",[dictobject valueForKey:@"ItemPrice"]];
    cell.quantity_lbl.text=[dictobject valueForKey:@"Quantity"];
    cell.delete_btn.tag=indexPath.row+1;
    cell.add_btn.tag=indexPath.row+1;
    cell.minus_btn.tag=indexPath.row+1;
    [cell.add_btn addTarget:self action:@selector(onAddClick:) forControlEvents:UIControlEventTouchUpInside];
    [cell.minus_btn addTarget:self action:@selector(onMinusClick:) forControlEvents:UIControlEventTouchUpInside];
    [cell.delete_btn addTarget:self action:@selector(onDeleteClick:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

- (IBAction)onNotScanningButtonClicked:(id)sender {
    self.tabBarController.selectedIndex=1;
    [[NSNotificationCenter defaultCenter] postNotificationName:SCAN_NOTIFICATION object:@"Shop"];
}

- (IBAction)onAddClick:(id)sender {
    NSDictionary *dictobject;
    NSString *salestax;
    NSString *crvtax;
    NSString *discountPrice;
    NSLog(@"tag %ld",(long)[sender tag]-1);
    dictobject=[self.items_array objectAtIndex:[sender tag]-1];
    if([[dictobject valueForKey:@"SalesTaxEnable"] isEqualToString:@"Y"])
    {
        NSString *salestaxfromdb=[objDBHelper getSalesTax:[dictobject valueForKey:@"ItemNo"]];
        float price=[[dictobject valueForKey:@"DiscountPrice"]floatValue];
        float tax = [[dictobject valueForKey:@"SalesTaxPercent"] floatValue];
        double total_taxprice=(price/100)*tax;
        double taxtotal=[salestaxfromdb floatValue]+total_taxprice;
        salestax=[NSString stringWithFormat:@"%f",taxtotal];
        [objDBHelper UpdateSalesTax:salestax andItemNo:[dictobject valueForKey:@"ItemNo"]];
    }
    if([[dictobject valueForKey:@"CRVTAXEnable"] isEqualToString:@"Y"])
    {
        float tax = [[dictobject valueForKey:@"CRVTAXPercent"] floatValue];
        NSString *crvtaxfromdb=[objDBHelper getCRVTax:[dictobject valueForKey:@"ItemNo"]];
        float taxtotal=[crvtaxfromdb floatValue]+tax;
        crvtax=[NSString stringWithFormat:@"%f",taxtotal];
        NSLog(@"crvtax %@",crvtax);
        [objDBHelper UpdateCRVTax:crvtax andItemNo:[dictobject valueForKey:@"ItemNo"]];
    }
    if([[dictobject valueForKey:@"DiscountPriceEnable"] isEqualToString:@"Y"]){
        float originalPrice = [[dictobject valueForKey:@"OriginalPrice"] floatValue];
        float itemPrice = [[dictobject valueForKey:@"DiscountPrice"] floatValue];
        float discount;
        if(originalPrice > itemPrice){
            discount = originalPrice - itemPrice;
        }else{
            discount = itemPrice - originalPrice;
        }
        discountPrice=[NSString stringWithFormat:@"%f",discount];
    }else{
        discountPrice = @"0.00";
    }
    
    [objDBHelper UpdateQuantity:[dictobject valueForKey:@"ItemNo"] andQuantity:@"3" andMicroMarketID:self.micromarketid];
    [objDBHelper UpdatePrice:[dictobject valueForKey:@"ItemNo"] andPrice:[dictobject valueForKey:@"DiscountPrice"] andMicroMarketID:self.micromarketid];
    [objDBHelper UpdateDiscountPriceDiff:[dictobject valueForKey:@"ItemNo"] andPrice:discountPrice andMicroMarketID:self.micromarketid andDiscountPriceDiff:[dictobject valueForKey:@"DiscountPriceDiff"]];
    self.items_array=[[NSMutableArray alloc]init];
    self.items_array=[objDBHelper getCartItems:self.micromarketid];
    total=[objDBHelper getcount:self.micromarketid];
    if(total!=nil)
    {
        self.total_lbl.text=[NSString stringWithFormat:@"Total : $ %@",total];
    }
    else{
        self.total_lbl.text=@"Total : $0.00";
    }
    
    discountTotal = [objDBHelper getDiscountTotal:self.micromarketid];
    if(discountTotal==nil){
        discountTotal = ZERO_POINT_ZERO;
    }
    [self.item_tableview reloadData];
}

- (IBAction)onMinusClick:(id)sender {
    NSDictionary *dictobject;
    NSString *salestax;
    NSString *crvtax;
    NSString *discountPrice;
    NSLog(@"tag %ld",(long)[sender tag]-1);
    dictobject=[self.items_array objectAtIndex:[sender tag]-1];
    if(![[dictobject valueForKey:@"Quantity"] isEqualToString:@"1"]){
        if([[dictobject valueForKey:@"SalesTaxEnable"] isEqualToString:@"Y"])
        {
            NSString *salestaxfromdb=[objDBHelper getSalesTax:[dictobject valueForKey:@"ItemNo"]];
            float price=[[dictobject valueForKey:@"DiscountPrice"]floatValue];
            float tax = [[dictobject valueForKey:@"SalesTaxPercent"] floatValue];
            double total_taxprice=(price/100)*tax;
            double taxtotal= [salestaxfromdb floatValue] - total_taxprice;
            salestax=[NSString stringWithFormat:@"%f",taxtotal];
            [objDBHelper UpdateSalesTax:salestax andItemNo:[dictobject valueForKey:@"ItemNo"]];
        }
        if([[dictobject valueForKey:@"CRVTAXEnable"] isEqualToString:@"Y"])
        {
            float tax = [[dictobject valueForKey:@"CRVTAXPercent"] floatValue];
            NSString *crvtaxfromdb=[objDBHelper getCRVTax:[dictobject valueForKey:@"ItemNo"]];
            float taxtotal=[crvtaxfromdb floatValue]-tax;
            crvtax=[NSString stringWithFormat:@"%.02f",taxtotal];
            NSLog(@"crvtax %@",crvtax);
            [objDBHelper UpdateCRVTax:crvtax andItemNo:[dictobject valueForKey:@"ItemNo"]];
        }
        if([[dictobject valueForKey:@"DiscountPriceEnable"] isEqualToString:@"Y"]){
            float originalPrice = [[dictobject valueForKey:@"OriginalPrice"] floatValue];
            float itemPrice = [[dictobject valueForKey:@"DiscountPrice"] floatValue];
            float discount;
            if(originalPrice > itemPrice){
                discount = originalPrice - itemPrice;
            }else{
                discount = itemPrice - originalPrice;
            }
            discountPrice=[NSString stringWithFormat:@"%f",discount];
        }else{
            discountPrice = @"0.00";
        }
        if(![[dictobject valueForKey:@"Quantity"] isEqualToString:@"1"])
        {
            [objDBHelper ReduceQuantity:[dictobject valueForKey:@"ItemNo"] andQuantity:@"3" andMicroMarketID:self.micromarketid];
            [objDBHelper ReducePrice:[dictobject valueForKey:@"ItemNo"] andPrice:[dictobject valueForKey:@"DiscountPrice"] andMicroMarketID:self.micromarketid];
            [objDBHelper ReduceDiscountDiff:[dictobject valueForKey:@"ItemNo"] andPrice:discountPrice andMicroMarketID:self.micromarketid andDiscountPriceDiff:[dictobject valueForKey:@"DiscountPriceDiff"]];
        }else {
        }
        self.items_array=[[NSMutableArray alloc]init];
        self.items_array=[objDBHelper getCartItems:self.micromarketid];
        total=[objDBHelper getcount:self.micromarketid];
        if(total!=nil)
        {
            self.total_lbl.text=[NSString stringWithFormat:@"Total : $ %@",total];
        }
        else{
            self.total_lbl.text=@"Total : $ 0.00";
        }
        discountTotal = [objDBHelper getDiscountTotal:self.micromarketid];
        if(discountTotal==nil){
            discountTotal = ZERO_POINT_ZERO;
        }
        [self.item_tableview reloadData];
    }
    
}

- (IBAction)onDeleteClick:(id)sender {
    NSDictionary *dictobject;
    NSLog(@"tag %ld",(long)[sender tag]-1);
    dictobject=[self.items_array objectAtIndex:[sender tag]-1];
    
    NSString * SessionGUID = [[NSUserDefaults standardUserDefaults] objectForKey:@"SessionGUID"];
    NSString * micromarketid = [[NSUserDefaults standardUserDefaults] valueForKey:@"micromarketid"];
    NSString * micromarketname = [[NSUserDefaults standardUserDefaults] valueForKey:@"micromarketname"];
    NSString * userID = [[NSUserDefaults standardUserDefaults] objectForKey:@"username"];
    float price=[[dictobject valueForKey:@"ItemPrice"]floatValue];
    NSString * categoryID = [dictobject valueForKey:@"CategoryID"];
    NSString * productID = [dictobject valueForKey:@"ItemNo"];
    NSString * productName = [dictobject valueForKey:@"ItemName"];
    
    [objDBHelper insertProductAnalytics:userID andsessionGUID:SessionGUID andMircoMarketID:[micromarketid intValue] andMicroMarketName:micromarketname andProductID:productID andProductName:productName andCategoryID:categoryID andUPCCode:@"" andItemQty:1 andItemPrice:price andType:@"Product_Removed" andSyncFlag:@"Y"];
    
    
    [objDBHelper deleteItem:[dictobject valueForKey:@"ItemNo"] andMicroMarketID:self.micromarketid];
    self.items_array=[[NSMutableArray alloc]init];
    self.items_array=[objDBHelper getCartItems:self.micromarketid];
    total=[objDBHelper getcount:self.micromarketid];
    if(total!=nil)
    {
        self.total_lbl.text=[NSString stringWithFormat:@"Total : $%@",total];
    }
    else{
        self.total_lbl.text=@"Total : $0.00";
    }
    discountTotal = [objDBHelper getDiscountTotal:self.micromarketid];
    if(discountTotal==nil){
        discountTotal = ZERO_POINT_ZERO;
    }
    [self.item_tableview reloadData];
}

- (void)startScan {
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        // Do something...
        dispatch_async(dispatch_get_main_queue(), ^{
            [self startReading];
        });
    });
    // Set to the flag the exact opposite value of the one that currently has.
    _isReading = !_isReading;
}

- (BOOL)startReading {
    addCode = NO;
    imageStr = @"";
    self.capture = [[ZXCapture alloc] init];
    self.capture.sessionPreset = AVCaptureSessionPreset1920x1080;
    self.capture.camera = self.capture.back;
    self.capture.focusMode = AVCaptureFocusModeContinuousAutoFocus;
    self.capture.delegate = self;
    [self.capture setLuminance: TRUE];
    
    self.scanning = YES;
    
    self.capture.layer.frame = self.viewPreview.frame;
    [self.viewPreview.layer addSublayer:self.capture.layer];
    
    self.scanRectView.layer.borderWidth = 1;
    self.scanRectView.layer.borderColor = [UIColor redColor].CGColor;
    [self.view bringSubviewToFront:self.scanRectView];
    
    [self applyRectOfInterest:UIInterfaceOrientationPortrait];
    
    
    return YES;
}

- (void)stopReading {
    self.scanning = NO;
    [self.capture stop];
    [self.capture.layer removeFromSuperlayer];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self applyOrientation];
}

#pragma mark - Private
- (void)applyOrientation {
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    float scanRectRotation;
    float captureRotation;
    
    switch (orientation) {
        case UIInterfaceOrientationPortrait:
            captureRotation = 0;
            scanRectRotation = 90;
            break;
        case UIInterfaceOrientationLandscapeLeft:
            captureRotation = 90;
            scanRectRotation = 180;
            break;
        case UIInterfaceOrientationLandscapeRight:
            captureRotation = 270;
            scanRectRotation = 0;
            break;
        case UIInterfaceOrientationPortraitUpsideDown:
            captureRotation = 180;
            scanRectRotation = 270;
            break;
        default:
            captureRotation = 0;
            scanRectRotation = 90;
            break;
    }
    self.capture.layer.frame = self.viewPreview.frame;
    CGAffineTransform transform = CGAffineTransformMakeRotation((CGFloat) (captureRotation / 180 * M_PI));
    [self.capture setTransform:transform];
    [self.capture setRotation:scanRectRotation];
    
    [self applyRectOfInterest:orientation];
}

- (void)applyRectOfInterest:(UIInterfaceOrientation)orientation {
    CGFloat scaleVideoX, scaleVideoY;
    CGFloat videoSizeX, videoSizeY;
    CGRect transformedVideoRect = self.scanRectView.frame;
    NSLog(@"%@", NSStringFromCGRect(self.scanRectView.frame));
    
    if([self.capture.sessionPreset isEqualToString:AVCaptureSessionPreset1920x1080]) {
        videoSizeX = 1080;
        videoSizeY = 1920;
    } else {
        videoSizeX = 720;
        videoSizeY = 1280;
    }
    if(UIInterfaceOrientationIsPortrait(orientation)) {
        scaleVideoX = self.capture.layer.frame.size.width / videoSizeX;
        scaleVideoY = self.capture.layer.frame.size.height / videoSizeY;
        
        CGFloat realX = transformedVideoRect.origin.y;
        CGFloat realY = self.capture.layer.frame.size.width - transformedVideoRect.size.width - transformedVideoRect.origin.x;
        CGFloat realWidth = transformedVideoRect.size.height + transformedVideoRect.size.height;
        CGFloat realHeight = transformedVideoRect.size.width;
        transformedVideoRect = CGRectMake(realX, realY, realWidth, realHeight);
        
    } else {
        scaleVideoX = self.capture.layer.frame.size.width / videoSizeY;
        scaleVideoY = self.capture.layer.frame.size.height / videoSizeX;
    }
    
    _captureSizeTransform = CGAffineTransformMakeScale(1.0/scaleVideoX, 1.0/scaleVideoY);
    self.capture.scanRect = CGRectApplyAffineTransform(transformedVideoRect, _captureSizeTransform);
    
    NSLog(@"NSStringFromCGPoint = %@", NSStringFromCGRect(self.capture.scanRect));
    
    double delayInSeconds = 0.2;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        UILabel *scanText = [[UILabel alloc]initWithFrame:CGRectMake(10,10. , self.view.frame.size.width - 20, 75)];
        scanText.text = @"Hold product steady & gently move phone back & forth until camera gets focused on barcode";
        scanText.numberOfLines = 0;
        scanText.font = [UIFont fontWithName:@"Montserrat-Medium" size:13];
        scanText.textAlignment = NSTextAlignmentCenter;
        scanText.textColor = [UIColor redColor];
        [self.view addSubview:scanText];
        if (self.scanning) {
            self.scanning = YES;
            [self.capture start];
        }
    });
}

- (void)captureCameraIsReady:(ZXCapture *)capture {
    self.scanning = YES;
    [self.capture start];
    NSLog(@"CAMERA STARTED !!!!!!");
    if(!flashOff) {
        [self handleFlashLight];
    }
}

-(void)captureResult:(ZXCapture *)capture result:(ZXResult *)result {
    if (!self.scanning) return;
    if (!result) return;
    NSLog(@"result text:%@",result);
    NSLog(@"result barcode format:%u",result.barcodeFormat);
    
    NSString *formatString = [self barcodeFormatToString:result.barcodeFormat];
    if(![formatString isEqualToString:@"QR Code"] && ![formatString isEqualToString:@"EAN-8"]){
        NSString *display = [NSString stringWithFormat:@"%@", result.text];
        if(![display isEqualToString:scannedBarCodeStr]){
            scannedBarCodeStr = display;
            scancount = 0;
        }else{
            scancount = scancount + 1;
        }
        
        if(scancount == 2){
            scancount = 0;
            [self.capture stop];
            self.scanning = NO;
            
            detectionString = result.text;
            NSLog(@"result code %@",result.text);
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
            
            UIImage* uiImage = [[UIImage alloc] initWithCGImage:self.capture.lastScannedImage];
            
            if(uiImage != nil){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[ActivityIndicator sharedInstance] showLoader];
                });
                UIImage *compressedImage = [UIImage compressImage:uiImage
                                                    compressRatio:0.0f];
                NSData *dataImage = UIImageJPEGRepresentation(compressedImage, 0.0);
                imageStr = [dataImage base64Encoding];
                
                [self getitems:detectionString];
            } else {
                self.scanning = YES;
                [self.capture start];
            }
        }
    }
}

- (NSString *)barcodeFormatToString:(ZXBarcodeFormat)format {
    switch (format) {
        case kBarcodeFormatAztec:
            return @"Aztec";
            
        case kBarcodeFormatCodabar:
            return @"CODABAR";
            
        case kBarcodeFormatCode39:
            return @"Code 39";
            
        case kBarcodeFormatCode93:
            return @"Code 93";
            
        case kBarcodeFormatCode128:
            return @"Code 128";
            
        case kBarcodeFormatDataMatrix:
            return @"Data Matrix";
            
        case kBarcodeFormatEan8:
            return @"EAN-8";
            
        case kBarcodeFormatEan13:
            return @"EAN-13";
            
        case kBarcodeFormatITF:
            return @"ITF";
            
        case kBarcodeFormatPDF417:
            return @"PDF417";
            
        case kBarcodeFormatQRCode:
            return @"QR Code";
            
        case kBarcodeFormatRSS14:
            return @"RSS 14";
            
        case kBarcodeFormatRSSExpanded:
            return @"RSS Expanded";
            
        case kBarcodeFormatUPCA:
            return @"UPCA";
            
        case kBarcodeFormatUPCE:
            return @"UPCE";
            
        case kBarcodeFormatUPCEANExtension:
            return @"UPC/EAN extension";
            
        default:
            return @"Unknown";
    }
}

- (void)loadBeepSound {
    // Get the path to the beep.mp3 file and convert it to a NSURL object.
    NSString *beepFilePath = [[NSBundle mainBundle] pathForResource:@"beep" ofType:@"mp3"];
    NSURL *beepURL = [NSURL URLWithString:beepFilePath];
    
    NSError *error;
    
    // Initialize the audio player object using the NSURL object previously set.
    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:beepURL error:&error];
    if (error) {
        // If the audio player cannot be initialized then log a message.
        NSLog(@"Could not play beep file.");
        NSLog(@"%@", [error localizedDescription]);
        
    }
    else{
        // If the audio player was successfully initialized then load it in memory.
        [_audioPlayer prepareToPlay];
    }
}

- (void)showconfirmpopup {
    
    //[self stopReading];
    
    UIViewController *currentVC = self.navigationController.visibleViewController;
    if ([NSStringFromClass([currentVC class]) isEqualToString:BARCODE_VC])
    {
        NSDictionary *dictobject;
        dictobject=[self.temp_array objectAtIndex:0];
        NSLog(@"detection string:%@",detectionString);
        
        NSString * SessionGUID = [[NSUserDefaults standardUserDefaults] objectForKey:@"SessionGUID"];
        NSString * micromarketid = [[NSUserDefaults standardUserDefaults] valueForKey:@"micromarketid"];
        NSString * micromarketname = [[NSUserDefaults standardUserDefaults] valueForKey:@"micromarketname"];
        NSString * userID = [[NSUserDefaults standardUserDefaults] objectForKey:@"username"];
        float price=[[dictobject valueForKey:@"ITEM_PRICE"]floatValue];
        NSString * categoryID = [dictobject valueForKey:@"CATEGORY_ID"];
        NSString * productID = [dictobject valueForKey:@"ITEM_NO"];
        NSString * productName = [dictobject valueForKey:@"ITEM_DESC"];
        
        
        [objDBHelper insertProductAnalytics:userID andsessionGUID:SessionGUID andMircoMarketID:[micromarketid intValue] andMicroMarketName:micromarketname andProductID:productID andProductName:productName andCategoryID:categoryID andUPCCode:detectionString andItemQty:1 andItemPrice:price andType:@"Product_Scanned" andSyncFlag:@"Y"];
        
        pop_view = [[UIView alloc] initWithFrame:CGRectMake(0, 0,self.view.frame.size.width-20, 110)];
        pop_view.backgroundColor     = [UIColor whiteColor];
        pop_view.layer.cornerRadius  = 10.0;
        pop_view.layer.masksToBounds = YES;
        UILabel *title_lbl=[[UILabel alloc]initWithFrame:CGRectMake(10,10,self.view.frame.size.width/2 , 40)];
        title_lbl.text=[dictobject valueForKey:@"ITEM_DESC"];
        title_lbl.font=[UIFont fontWithName:FONT_REGULAR size:17.f];
        title_lbl.textAlignment=NSTextAlignmentCenter;
        title_lbl.textColor=[UIColor colorWithRed:0.17 green:0.24 blue:0.31 alpha:1.0];
        
        UILabel *price_lbl=[[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2+10,10,self.view.frame.size.width/2-15 , 40)];
        price_lbl.text=[NSString stringWithFormat:@"$ %@",[dictobject valueForKey:@"ITEM_PRICE"]];
        price_lbl.font=[UIFont fontWithName:FONT_REGULAR size:17.f];
        price_lbl.textColor=[UIColor colorWithRed:0.17 green:0.24 blue:0.31 alpha:1.0];
        price_lbl.textAlignment=NSTextAlignmentCenter;
        
        
        UIButton *add_btn=[[UIButton alloc]initWithFrame:CGRectMake(pop_view.frame.size.width/2+10, 60, pop_view.frame.size.width/2-20 , 30)];
        [add_btn setTitle:ADD_TO_CART forState:UIControlStateNormal];
        [add_btn setBackgroundColor:[UIColor colorWithRed:0.20 green:0.60 blue:0.86 alpha:1.0]];
        [add_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [add_btn addTarget:self action:@selector(onConfirmClick:) forControlEvents:UIControlEventTouchUpInside];
        add_btn.layer.cornerRadius=5;
        add_btn.clipsToBounds=YES;
        
        UIButton *cancel_btn=[[UIButton alloc]initWithFrame:CGRectMake(10, 60, pop_view.frame.size.width/2-10 , 30)];
        [cancel_btn setTitle:@"Cancel" forState:UIControlStateNormal];
        [cancel_btn setBackgroundColor:[UIColor colorWithRed:0.33 green:0.33 blue:0.33 alpha:1.0]];
        [cancel_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [cancel_btn addTarget:self action:@selector(onCancelClick:) forControlEvents:UIControlEventTouchUpInside];
        cancel_btn.layer.cornerRadius=5;
        cancel_btn.clipsToBounds=YES;
        
        UIButton *view_btn=[[UIButton alloc]initWithFrame:CGRectMake(10,160 , 230, 30)];
        [view_btn setTitle:@"View" forState:UIControlStateNormal];
        [view_btn setBackgroundColor:[UIColor redColor]];
        [view_btn setTintColor:[UIColor whiteColor]];
        [pop_view addSubview:view_btn];
        [pop_view addSubview:title_lbl];
        [pop_view addSubview:price_lbl];
        [pop_view addSubview:add_btn];
        [pop_view addSubview:cancel_btn];
        
        if(!notViewController){
            [[KGModal sharedInstance] showWithContentView:pop_view andAnimated:YES];
            [[KGModal sharedInstance] setTapOutsideToDismiss:NO];
        }
    } else {
        [[KGModal sharedInstance] hideAnimated:YES];
    }
}

-(void)showItemNotFoundPopUp {
    
    //[self stopReading];
    
    UIViewController *currentVC = self.navigationController.visibleViewController;
    if ([NSStringFromClass([currentVC class]) isEqualToString:BARCODE_VC])
    {
        itemPopView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,self.view.frame.size.width-20, 110)];
        itemPopView.backgroundColor     = [UIColor whiteColor];
        itemPopView.layer.cornerRadius  = 10.0;
        itemPopView.layer.masksToBounds = YES;
        UILabel *title_lbl=[[UILabel alloc]initWithFrame:CGRectMake(0,10,self.view.frame.size.width, 20)];
        title_lbl.text=@"Item not found";
        title_lbl.font=[UIFont fontWithName:FONT_REGULAR size:20.f];
        title_lbl.textAlignment=NSTextAlignmentCenter;
        title_lbl.textColor=[UIColor colorWithRed:0.17 green:0.24 blue:0.31 alpha:1.0];
        
        UIButton *scanBtn=[[UIButton alloc]initWithFrame:CGRectMake(10, 60, itemPopView.frame.size.width/2-10 , 35)];
        [scanBtn setTitle:@"Scan Again" forState:UIControlStateNormal];
        [scanBtn setBackgroundColor:[UIColor colorWithRed:0.33 green:0.33 blue:0.33 alpha:1.0]];
        [scanBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        scanBtn.titleLabel.font = [UIFont fontWithName:FONT_REGULAR size:15.0f];
        [scanBtn addTarget:self action:@selector(onScanAgainClicked:) forControlEvents:UIControlEventTouchUpInside];
        scanBtn.layer.cornerRadius=5;
        scanBtn.clipsToBounds=YES;
        
        UIButton *productsBtn=[[UIButton alloc]initWithFrame:CGRectMake(itemPopView.frame.size.width/2+10, 60, itemPopView.frame.size.width/2-20 , 35)];
        [productsBtn setTitle:@"Browse or Search" forState:UIControlStateNormal];
        [productsBtn setBackgroundColor:[UIColor colorWithRed:0.45 green:0.73 blue:0.31 alpha:1.0]];
        [productsBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        productsBtn.titleLabel.font = [UIFont fontWithName:FONT_REGULAR size:14.0f];
        [productsBtn addTarget:self action:@selector(onSearchProductsClicked:) forControlEvents:UIControlEventTouchUpInside];
        productsBtn.layer.cornerRadius=5;
        productsBtn.clipsToBounds=YES;
        
        [itemPopView addSubview:title_lbl];
        [itemPopView addSubview:scanBtn];
        [itemPopView addSubview:productsBtn];
        
        if(!notViewController){
            [[KGModal sharedInstance] showWithContentView:itemPopView andAnimated:YES];
            [[KGModal sharedInstance] setTapOutsideToDismiss:NO];
        }
    }else{
        [[KGModal sharedInstance] hideAnimated:YES];
    }
}


- (IBAction)onScanAgainClicked:(id)sender {
    [[KGModal sharedInstance] hideAnimated:YES];
    self.scanning = YES;
    [self.capture start];
    if(!flashOff) {
        [self handleFlashLight];
    }
}


- (IBAction)onSearchProductsClicked:(id)sender {
    [[KGModal sharedInstance] hideAnimated:YES];
    [self.navigationController setNavigationBarHidden:YES];
    self.tabBarController.selectedIndex=1;
    //  [[NSNotificationCenter defaultCenter] postNotificationName:SCAN_NOTIFICATION object:SHOW];
    [[NSNotificationCenter defaultCenter] postNotificationName:SCAN_NOTIFICATION object:@"Shop"];
    
}


- (IBAction)onConfirmClick:(id)sender {
    NSDictionary *dictobject;
    dictobject=[self.temp_array objectAtIndex:0];
    NSString *crvtax;
    NSString *salestax;
    NSString *discountPrice;
    
    NSString * SessionGUID = [[NSUserDefaults standardUserDefaults] objectForKey:@"SessionGUID"];
    NSString * micromarketid = [[NSUserDefaults standardUserDefaults] valueForKey:@"micromarketid"];
    NSString * micromarketname = [[NSUserDefaults standardUserDefaults] valueForKey:@"micromarketname"];
    NSString * userID = [[NSUserDefaults standardUserDefaults] objectForKey:@"username"];
    float price=[[dictobject valueForKey:@"ITEM_PRICE"]floatValue];
    NSString * categoryID = [dictobject valueForKey:@"CATEGORY_ID"];
    NSString * productID = [dictobject valueForKey:@"ITEM_NO"];
    NSString * productName = [dictobject valueForKey:@"ITEM_DESC"];
    
    [objDBHelper insertProductAnalytics:userID andsessionGUID:SessionGUID andMircoMarketID:[micromarketid intValue] andMicroMarketName:micromarketname andProductID:productID andProductName:productName andCategoryID:categoryID andUPCCode:detectionString andItemQty:1 andItemPrice:price andType:@"Product_Added" andSyncFlag:@"Y"];
    
    
    if(![objDBHelper CheckItem:[dictobject valueForKey:@"ITEM_NO"] andMicroMarketID:self.micromarketid])
    {
        if([[dictobject valueForKey:@"TAXABLE"] isEqualToString:@"Y"])
        {
            float price=[[dictobject valueForKey:@"ITEM_PRICE"]floatValue];
            float tax = [[dictobject valueForKey:@"SALESTAX"] floatValue];
            double total_taxprice=(price/100)*tax;
            salestax=[NSString stringWithFormat:@"%f",total_taxprice];
            NSLog(@"salestax %@",salestax);
        }
        else
        {
            salestax=ZERO_POINT_ZERO;
        }
        if([[dictobject valueForKey:@"CRV_ENABLE"] isEqualToString:@"Y"])
        {
            float tax = [[dictobject valueForKey:@"CRVTAX"] floatValue];
            crvtax=[NSString stringWithFormat:@"%f",tax];
            NSLog(@"crvtax %@",crvtax);
        }
        else
        {
            crvtax=ZERO_POINT_ZERO;
        }
        //DISCOUNT PRICE CALCULATION
        if([[dictobject valueForKey:@"DISCOUNT_APPLICABLE"] isEqualToString:@"Y"]){
            float originalPrice = [[dictobject valueForKey:@"ORIGINAL_PRICE"] floatValue];
            float itemPrice = [[dictobject valueForKey:@"ITEM_PRICE"] floatValue];
            float discount;
            if(originalPrice > itemPrice){
                discount = originalPrice - itemPrice;
            }else{
                discount = itemPrice - originalPrice;
            }
            discountPrice=[NSString stringWithFormat:@"%f",discount];
        }else{
            discountPrice = @"0.00";
        }
        
        [objDBHelper insertCartItems:self.micromarketid andCategoryID:[dictobject valueForKey:@"CATEGORY_ID"] andDescription:[dictobject valueForKey:@"ITEM_DESC_LONG"] andItemNo:[dictobject valueForKey:@"ITEM_NO"] andcrvtax:crvtax andSalesTax:salestax andDiscountPrice:[dictobject valueForKey:@"DISCOUNT_PRICE"] andItemName:[dictobject valueForKey:@"ITEM_DESC"] andItemImage:[dictobject valueForKey:@"ITEM_IMAGE"] andItemPrice:[dictobject valueForKey:@"ITEM_PRICE"] andStock:[dictobject valueForKey:@"STOCK"] andQuantity:@"1" andOriginalPrice:[dictobject valueForKey:@"ORIGINAL_PRICE"] andCRVPercent:[dictobject valueForKey:@"CRVTAX"] andSalesPercent:[dictobject valueForKey:@"SALESTAX"] andCRVTAXEnable:[dictobject valueForKey:@"CRV_ENABLE"] andSalesTaxEnable:[dictobject valueForKey:@"TAXABLE"] andDiscountPriceDiff:discountPrice andDiscountPriceEnable:[dictobject valueForKey:@"DISCOUNT_APPLICABLE"] andBarcode:[dictobject valueForKey:@"BAR_CODE"]];
    }
    else
    {
        if([[dictobject valueForKey:@"TAXABLE"] isEqualToString:@"Y"])
        {
            float price=[[dictobject valueForKey:@"ITEM_PRICE"] floatValue];
            float tax = [[dictobject valueForKey:@"SALESTAX"] floatValue];
            double total_taxprice=(price/100)*tax;
            NSString *salestaxfromdb=[objDBHelper getSalesTax:[dictobject valueForKey:@"ITEM_NO"]];
            double taxtotal=[salestaxfromdb floatValue]+total_taxprice;
            salestax=[NSString stringWithFormat:@"%f",taxtotal];
            NSLog(@"salestax %@",salestax);
        }
        else
        {
            salestax=ZERO_POINT_ZERO;
        }
        if([[dictobject valueForKey:@"CRV_ENABLE"] isEqualToString:@"Y"])
        {
            float tax = [[dictobject valueForKey:@"CRVTAX"] floatValue];
            NSString *crvtaxfromdb=[objDBHelper getCRVTax:[dictobject valueForKey:@"ITEM_NO"]];
            float taxtotal=[crvtaxfromdb floatValue]+tax;
            crvtax=[NSString stringWithFormat:@"%f",taxtotal];
            NSLog(@"crvtax %@",crvtax);
        }
        else
        {
            crvtax=ZERO_POINT_ZERO;
        }
        
        if([[dictobject valueForKey:@"DISCOUNT_APPLICABLE"] isEqualToString:@"Y"]){
            float originalPrice = [[dictobject valueForKey:@"ORIGINAL_PRICE"] floatValue];
            float itemPrice = [[dictobject valueForKey:@"ITEM_PRICE"] floatValue];
            float discount;
            if(originalPrice > itemPrice){
                discount = originalPrice - itemPrice;
            }else{
                discount = itemPrice - originalPrice;
            }
            discountPrice=[NSString stringWithFormat:@"%f",discount];
        }else{
            discountPrice = @"0.00";
        }
        
        [objDBHelper UpdateQuantity:[dictobject valueForKey:@"ITEM_NO"] andQuantity:@"3" andMicroMarketID:self.micromarketid];
        [objDBHelper UpdatePrice:[dictobject valueForKey:@"ITEM_NO"] andPrice:[dictobject valueForKey:@"ITEM_PRICE"] andMicroMarketID:self.micromarketid];
        NSString *discountPriceDiff = [objDBHelper getDiscountPriceDiff:[dictobject valueForKey:@"ITEM_NO"]];
        [objDBHelper UpdateDiscountPriceDiff:[dictobject valueForKey:@"ITEM_NO"] andPrice:discountPrice andMicroMarketID:self.micromarketid andDiscountPriceDiff:discountPriceDiff];
        [objDBHelper UpdateSalesTax:salestax andItemNo:[dictobject valueForKey:@"ITEM_NO"]];
        [objDBHelper UpdateCRVTax:crvtax andItemNo:[dictobject valueForKey:@"ITEM_NO"]];
    }
    
    self.items_array=[objDBHelper getCartItems:self.micromarketid];
    total=[objDBHelper getcount:self.micromarketid];
    if(total!=nil) {
        self.total_lbl.text=[NSString stringWithFormat:@"Total : $ %@",total];
    } else {
        self.total_lbl.text=@"Total : $ 0.00";
    }
    
    discountTotal = [objDBHelper getDiscountTotal:self.micromarketid];
    if(discountTotal==nil) {
        discountTotal = ZERO_POINT_ZERO;
    }
    [self.item_tableview reloadData];
    [[KGModal sharedInstance] hideAnimated:YES];
    self.scanning = YES;
    [self.capture start];
    if(!flashOff) {
        [self handleFlashLight];
    }
    //    [[KGModal sharedInstance] hideWithCompletionBlock:^{
    //        [self startScan];
    //        if(!self->flashOff) {
    //            [self handleFlashLight];
    //        }
    //    }];
}

- (IBAction)onCancelClick:(id)sender {
    [[KGModal sharedInstance] hideAnimated:YES];
    self.scanning = YES;
    [self.capture start];
    if(!flashOff) {
        [self handleFlashLight];
    }
    
    //    [[KGModal sharedInstance] hideWithCompletionBlock:^{
    //        [self startScan];
    //        if(!self->flashOff) {
    //            [self handleFlashLight];
    //        }
    //    }];
}

- (IBAction)onScanClicked:(id)sender {
    self.scanning = YES;
    [self.capture start];
}

- (IBAction)onCheckOutClicked:(id)sender {
    if(self.items_array.count>0) {
        [[NSNotificationCenter defaultCenter] removeObserver: self];

        //[[NSNotificationCenter defaultCenter] postNotificationName:SCAN_NOTIFICATION object:@"checkout"];
        CheckOutWebViewController *vc;
        vc=[self.storyboard instantiateViewControllerWithIdentifier:CHECKOUT_WEB_VC];
        vc.hidesBottomBarWhenPushed = YES;
        vc.total_price=total;
        vc.discountTotal = discountTotal;
        vc.micromarketid=self.micromarketid;
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        [AlertVC showAlertWithTitleForView:APP_NAME message:THERE_ARE_NO_ITEMS controller:self];
    }
}

- (void)getitems:(NSString *)item {
    
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    NSString *serviceName;
    NSString *userName;
    serviceName = [[NSUserDefaults standardUserDefaults]objectForKey:@"serviceName"];
    userName =  [[NSUserDefaults standardUserDefaults]objectForKey:@"username"];
    
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:DATEFORMAT_TYPE_TWO];
    NSString *currentTime = [dateFormatter stringFromDate:today];
    NSString *marketname_str;
    NSString *paramMicroMarketId;
    paramMicroMarketId = [[NSUserDefaults standardUserDefaults]objectForKey:@"micromarketid"];
    marketname_str = [[NSUserDefaults standardUserDefaults]objectForKey:@"micromarketname"];
    NSString *appVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    NSString *deviceManufacturer = @"Apple Inc.";
    NSString *deviceModel = [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceModel"];
    NSString *deviceOS = [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceOS"];
    
    if (serviceName != nil) {
        [dictParam setObject:marketname_str               forKey:@"MicroMarketName"];
        [dictParam setObject:paramMicroMarketId           forKey:@"MicroMarketID"];
        [dictParam setObject:serviceName                  forKey:@"ServiceName"];
        [dictParam setObject:item                         forKey:@"BarCodeID"];
        [dictParam setObject:userName                     forKey:@"UserName"];
        [dictParam setObject:currentTime                  forKey:@"DeviceDataTime"];
        [dictParam setObject:@"iOS"                       forKey:@"DevicePlatform"];
        [dictParam setObject:deviceManufacturer           forKey:@"DeviceManufacturer"];
        [dictParam setObject:deviceModel                  forKey:@"DeviceModel"];
        [dictParam setObject:deviceOS                     forKey:@"DeviceOS"];
        [dictParam setObject:appVersion                   forKey:@"AppVersion"];
        [dictParam setObject:imageStr                     forKey:@"BarcodeImageByteString"];
        [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"username"] forKey:PARAM_EMAIL_KEY];
        
        NSLog(@"DICTPARAM:%@",dictParam);
        
        NSString *rootURL =  [[AppDelegate shared] rootUrl];

        AFNHelper *afn=[[AFNHelper alloc]init];
        [afn postDataFromPath1:METHOD_BARCODESCAN getUrl:rootURL withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             if(error)
             {
                 
                 NSLog(@"ERROR DESC:%@",error.localizedDescription);
                 dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
                     if(error.code == NSURLErrorTimedOut){
                         [AlertVC showAlertWithTitleForView:APP_NAME message:ERROR_TIMED_OUT controller:self];
                     }else if(error.code == NSURLErrorNotConnectedToInternet){
                         [AlertVC showAlertWithTitleForView:APP_NAME message:NO_INTERNET_CONNECTION controller:self];
                     }
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [[ActivityIndicator sharedInstance] hideLoader];
                         self.scanning = YES;
                         [self.capture start];
                         
                     });
                 });
             } else {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [[ActivityIndicator sharedInstance] hideLoader];
                 });
                 imageStr = @"";
                 UIViewController *currentVC = self.navigationController.visibleViewController;
                 if ([NSStringFromClass([currentVC class]) isEqualToString:BARCODE_VC])
                 {
                     NSLog(@"response %@",response);
                     NSDictionary *dictionary;
                     dictionary=response;
                     NSString *status=[dictionary valueForKey:@"Status"];
                     
                     // Check Current View Controller.
                     if([status isEqualToString:@"Success"])
                     {
                         UIViewController *currentVC = self.navigationController.visibleViewController;
                         if ([NSStringFromClass([currentVC class]) isEqualToString:BARCODE_VC])
                         {
                             self.temp_array=[dictionary valueForKey:@"ItemsUsingBarcode"];
                             [self showconfirmpopup];
                         }
                         else
                         {
                             // Hide PopUp.
                         }
                     }
                     else
                     {
                         UIViewController *currentVC = self.navigationController.visibleViewController;
                         if ([NSStringFromClass([currentVC class]) isEqualToString:BARCODE_VC])
                         {
                             [self showItemNotFoundPopUp];
                         }
                         else
                         {
                             // Hide PopUp.
                         }
                     }
                 }
                 self->detectionString = @"";
             }
         }];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[ActivityIndicator sharedInstance] hideLoader];
        });   //MORE STUFFS....
    }
}

- (void)validateCartItem {
    self.view.userInteractionEnabled = false;
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    NSString *serviceName;
    serviceName = [[NSUserDefaults standardUserDefaults]objectForKey:@"serviceName"];
    NSString *paramMicroMarketId;
    paramMicroMarketId = [[NSUserDefaults standardUserDefaults]objectForKey:@"micromarketid"];
    
    if (serviceName != nil)
    {
        [dictParam setObject:paramMicroMarketId           forKey:@"MicroMarketID"];
        [dictParam setObject:serviceName                  forKey:@"ServiceName"];
        [dictParam setObject:itemNoStr forKey:@"ItemNumber"];
        [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"username"] forKey:PARAM_EMAIL_KEY];
        
        NSLog(@"DICTPARAM:%@",dictParam);
        
        NSString *rootURL =  [[AppDelegate shared] rootUrl];

        AFNHelper *afn=[[AFNHelper alloc]init];
        [afn postDataFromPath:METHOD_VALIDATE_CART getUrl:rootURL withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             if(error)
             {
                 
                 [[ActivityIndicator sharedInstance] hideLoader];
                 [self displayTotal];
                 self.view.userInteractionEnabled = true;
             }else {
                 NSLog(@"response %@",response);
                 NSDictionary *dictionary;
                 dictionary=response;
                 NSString *status=[dictionary valueForKey:@"Status"];
                 if([status isEqualToString:@"Success"]){
                     self.validate_array = [[NSMutableArray alloc]init];
                     self.validate_array = [dictionary valueForKey:@"Cart Item List"];
                     [self validateItems];
                 }
             }
             [[ActivityIndicator sharedInstance] hideLoader];
             [self displayTotal];
             self.view.userInteractionEnabled = true;
         }];
    }
    else{
        //....
        self.view.userInteractionEnabled = true;
    }
    
}

- (void)displayTotal {
    total = @"";
    total=[objDBHelper getcount:self.micromarketid];
    discountTotal = [objDBHelper getDiscountTotal:self.micromarketid];
    if(total!=nil)
    {
        self.total_lbl.text=[NSString stringWithFormat:@"Total : $%@",total];
    }
    else{
        self.total_lbl.text=@"Total : $0.00";
    }
    
    if(discountTotal==nil){
        discountTotal = ZERO_POINT_ZERO;
    }
}

- (void)validateItems {
    for(int i=0;i<self.validate_array.count;i++){
        NSMutableArray *itemArray = [[NSMutableArray alloc]init];
        NSDictionary *dict;
        dict = [self.validate_array objectAtIndex:i];
        itemArray = [objDBHelper getCartItemsByItemNo:self.micromarketid andItemNo:[dict valueForKey:@"ITEM_NO"]];
        NSDictionary *itemArrayDict;
        if(itemArray.count > 0){
            itemArrayDict = [itemArray objectAtIndex:0];
            NSString *salestax;
            NSString *crvtax;
            NSString *discountPrice;
            float productPrice;
            if([[itemArrayDict valueForKey:@"Quantity"] isEqualToString:@"1"]){
                productPrice = [[dict valueForKey:@"ITEM_PRICE"] floatValue];
            }else{
                productPrice = [[dict valueForKey:@"ITEM_PRICE"] floatValue] * [[itemArrayDict valueForKey:@"Quantity"] floatValue];
            }
            if([[dict valueForKey:@"TAXABLE"] isEqualToString:@"Y"])
            {
                float price=[[dict valueForKey:@"ITEM_PRICE"]floatValue];
                float tax = [[dict valueForKey:@"SALESTAX"] floatValue];
                double total_taxprice;
                if([[itemArrayDict valueForKey:@"Quantity"] isEqualToString:@"1"]){
                    total_taxprice=(price/100)*tax;
                }else{
                    float taxPercent = ([[dict valueForKey:@"ITEM_PRICE"] floatValue]/100)*tax;
                    total_taxprice = taxPercent * [[itemArrayDict valueForKey:@"Quantity"] floatValue];
                }
                salestax=[NSString stringWithFormat:@"%f",total_taxprice];
                NSLog(@"salestax %@",salestax);
            }
            else
            {
                salestax=ZERO_POINT_ZERO;
            }
            if([[dict valueForKey:@"CRV_ENABLE"] isEqualToString:@"Y"])
            {
                float tax;
                if([[itemArrayDict valueForKey:@"Quantity"] isEqualToString:@"1"]){
                    tax = [[dict valueForKey:@"CRVTAX"] floatValue];
                }else{
                    tax = [[dict valueForKey:@"CRVTAX"] floatValue] * [[itemArrayDict valueForKey:@"Quantity"] floatValue];
                }
                crvtax=[NSString stringWithFormat:@"%f",tax];
                NSLog(@"crvtax %@",crvtax);
            }
            else
            {
                crvtax=ZERO_POINT_ZERO;
            }
            
            //DISCOUNT PRICE CALCULATION
            if([[dict valueForKey:@"DISCOUNT_APPLICABLE"] isEqualToString:@"Y"]){
                float originalPrice = [[dict valueForKey:@"ORIGINAL_PRICE"] floatValue];
                float itemPrice = [[dict valueForKey:@"ITEM_PRICE"] floatValue];
                float discount;
                if([[itemArrayDict valueForKey:@"Quantity"] isEqualToString:@"1"]){
                    discount = originalPrice - itemPrice;
                }else{
                    discount = (originalPrice - itemPrice) * [[itemArrayDict valueForKey:@"Quantity"] floatValue];
                }
                discountPrice=[NSString stringWithFormat:@"%f",discount];
            }else{
                discountPrice = @"0.00";
            }
            [objDBHelper UpdateNewPrice:[dict valueForKey:@"ITEM_NO"] andPrice:[NSString stringWithFormat:@"%f",productPrice] andMicroMarketID:self.micromarketid];
            //NSString *discountPriceDiff = [objDBHelper getDiscountPriceDiff:[dict valueForKey:@"ITEM_NO"]];
            [objDBHelper UpdateDiscountPriceDiff:[dict valueForKey:@"ITEM_NO"] andPrice:discountPrice andMicroMarketID:self.micromarketid andDiscountPriceDiff:@"0.0"];
            [objDBHelper UpdateSalesTax:salestax andItemNo:[dict valueForKey:@"ITEM_NO"]];
            [objDBHelper UpdateCRVTax:crvtax andItemNo:[dict valueForKey:@"ITEM_NO"]];
            [objDBHelper UpdateOriginalPrice:[dict valueForKey:@"ITEM_NO"] andPrice:[dict valueForKey:@"ORIGINAL_PRICE"] andMicroMarketID:self.micromarketid];
            [objDBHelper UpdateDiscountPrice:[dict valueForKey:@"ITEM_NO"] andPrice:[dict valueForKey:@"DISCOUNT_PRICE"] andMicroMarketID:self.micromarketid];
            [objDBHelper UpdateSalesTaxPercent:[dict valueForKey:@"SALESTAX"] andItemNo:[dict valueForKey:@"ITEM_NO"]];
            [objDBHelper UpdateCRVTaxPercent:[dict valueForKey:@"CRVTAX"] andItemNo:[dict valueForKey:@"ITEM_NO"]];
            self.items_array=[[NSMutableArray alloc]init];
            self.items_array=[objDBHelper getCartItems:self.micromarketid];
            [self.item_tableview reloadData];
        }
    }
}

- (void)setScreenAnalytics {
    NSString * SessionGUID = [[NSUserDefaults standardUserDefaults] objectForKey:@"SessionGUID"];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString * checkInTime=[dateFormatter stringFromDate:[NSDate date]];
    NSString * micromarketid = [[NSUserDefaults standardUserDefaults] valueForKey:@"micromarketid"];
    NSString * micromarketname = [[NSUserDefaults standardUserDefaults] valueForKey:@"micromarketname"];
    NSString * userID = [[NSUserDefaults standardUserDefaults] objectForKey:@"username"];
    
    NSString * analyticsCount = [objDBHelper getScreenAnalyticsBySessionID:SessionGUID andScreenName:@"Scan"];
    int visitCount = [analyticsCount intValue] +1;
    
    if(visitCount == 1) {
        [objDBHelper insertScreenAnalytics:userID andsessionGUID:SessionGUID andMircoMarketID:[micromarketid intValue] andMicroMarketName:micromarketname andScreenName:@"Scan" andscreenVisits:visitCount andCheckInTime:checkInTime andCheckOutTime:@"" andSyncFlag:@"Y"];
    } else {
        [objDBHelper UpdateScreenAnalytics:SessionGUID andVisitCount:visitCount andScreenName:@"Scan"];
    }
    
}

- (void)updateScreenAnalyticsCheckOutTime {
    NSString * SessionGUID = [[NSUserDefaults standardUserDefaults] objectForKey:@"SessionGUID"];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString * checkOutTime=[dateFormatter stringFromDate:[NSDate date]];
    [objDBHelper UpdateScreenAnalyticsCheckOutTime:SessionGUID andCheckOutTime:checkOutTime andScreenName:@"Scan"];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillTerminateNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
}

- (void)appWillTerminate:(NSNotification*)note {
    notViewController = YES;
    [self.capture stop];
    [self.capture.layer removeFromSuperlayer];
    [self updateScreenAnalyticsCheckOutTime];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear: animated];

    [self updateScreenAnalyticsCheckOutTime];
    
    [[NSNotificationCenter defaultCenter] removeObserver: self];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id <UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
    } completion:^(id<UIViewControllerTransitionCoordinatorContext> context)
     {
         [self applyOrientation];
     }];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

