//
//  CustomImageCell.h
//  AveriGo Markets
//
//  Created by Macmini on 6/12/19.
//  Copyright © 2019 BTS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CustomImageCell : UICollectionViewCell

@property(strong,nonatomic) IBOutlet UIImageView *attachmentImage;

@end

NS_ASSUME_NONNULL_END
