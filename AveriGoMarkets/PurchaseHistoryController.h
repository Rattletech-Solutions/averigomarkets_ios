    //
    //  PurchaseHistoryController.h
    //  Averigo
    //
    //  Created by BTS on 24/11/16.
    //  Copyright © 2016 BTS. All rights reserved.
    //

#import <UIKit/UIKit.h>
#import "DataBaseHandler.h"
#import "AlertVC.h"
#import "ActivityIndicator.h"
#import "OrderHistoryCell.h"

@interface PurchaseHistoryController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextViewDelegate,OrderHistoryCellDelegate>
{
    DataBaseHandler *objDBHelper;
}

@property(strong,nonatomic)  IBOutlet UITableView *history_table;
@property(strong,nonatomic) IBOutlet UIButton *recentPurchaseBtn;
@property(strong,nonatomic)  NSMutableArray *history_array;
@property(strong,nonatomic)  NSMutableArray *items_array;
@property(strong,nonatomic)  IBOutlet UILabel *no_lbl;
@property(strong,nonatomic)  IBOutlet UIButton *button;
@property(strong,nonatomic)  NSString *status;
@property(strong,nonatomic)  NSString *message;
@property(strong,nonatomic)  IBOutlet NSMutableArray *cart_array;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *recentPurchaseBtnHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *recentPurchaseBtnTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *recentPurchaseBtnBottom;
@property(strong,nonatomic)  NSString *micromarketid;
@property(strong,nonatomic)  NSMutableArray  *beacons;
@property(strong,nonatomic)  NSString *UDID;

@end
