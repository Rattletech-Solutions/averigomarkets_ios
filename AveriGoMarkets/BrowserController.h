//
//  BrowserController.h
//  AveriGo Markets
//
//  Created by Rattletech on 30/10/18.
//  Copyright © 2018 BTS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>
#import "MBProgressHUD.h"

NS_ASSUME_NONNULL_BEGIN

@interface BrowserController : UIViewController<UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *inAppView;

@end

NS_ASSUME_NONNULL_END
