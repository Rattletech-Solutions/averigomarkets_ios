//
//  HomeViewController.h
//  Averigo
//
//  Created by BTS on 02/03/17.
//  Copyright © 2017 BTS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import <CoreLocation/CoreLocation.h>
#import <STPopup.h>
#import "HomeCell.h"
#import "HomeCollectionViewLayout.h"
#import "DataBaseHandler.h"
#import "MKDropdownMenu.h"
#import "STPopupController.h"
#import "RealReachability.h"

@interface HomeViewController : UIViewController<UITextViewDelegate,CBCentralManagerDelegate,UICollectionViewDelegate,UICollectionViewDataSource,MKDropdownMenuDelegate,MKDropdownMenuDataSource>
{
    DataBaseHandler *objDBHelper;
}

@property (strong,nonatomic) UIButton *button;
@property (strong,nonatomic) IBOutlet UICollectionView *micromarket_view;
@property (strong,nonatomic) IBOutlet UILabel *noMicroMarketLbl;
@property(strong,nonatomic) NSMutableArray *userDetailsArray;
@property int Counter ;
@property (assign) EBCardCollectionLayoutType layoutType;
@property int Index;
@property NSString* canLoad;

@property (strong,nonatomic) NSMutableArray *microMarketArray;
@property (strong,nonatomic) NSMutableArray *microMarketPrefArray;
@property (strong,nonatomic) NSString *type;
@property (nonatomic,assign) BOOL isMicroMarket;

@property(strong,nonatomic) NSMutableArray     *beacons;
@property(strong,nonatomic) NSMutableArray     *beaconCountCheck;
@property(nonatomic,strong) CBCentralManager   *bluetoothManager;
@property(strong,nonatomic) NSString *nearby;
@property BOOL loadFlag;

@property(strong,nonatomic)NSMutableArray *beaconSentArray;
@property(strong,nonatomic)NSMutableArray *poiNearbyArray;
@property(strong,nonatomic)NSMutableArray *poiCompare_array;
@property(strong,nonatomic)NSMutableArray *sectionNearby_array;
@property(strong,nonatomic)NSMutableArray *MajorValues;
@property(strong,nonatomic)NSMutableArray *BeaconSelectedArray;
@property(strong,nonatomic)NSMutableArray *CompareArray;
@property(strong,nonatomic)NSString *UDID;
@property(strong,nonatomic)NSMutableArray *beacon_major;
@property(strong,nonatomic)NSMutableArray *micromarket_compare_array;
@property(strong,nonatomic) CLLocationManager *locationManager;




@end

