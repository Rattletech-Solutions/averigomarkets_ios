//
//  ChoosePhotoViewController.h
//  AveriGo Markets
//
//  Created by Macmini on 6/12/19.
//  Copyright © 2019 BTS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlertVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface ChoosePhotoViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource>

@property(strong,nonatomic) IBOutlet UIButton *fromCameraBtn;
@property(strong,nonatomic) IBOutlet UIButton *fromGalleryBtn;
@property(strong,nonatomic) IBOutlet UICollectionView *attachmentView;
@property(strong,nonatomic) IBOutlet UIButton *includeBtn;


@end

NS_ASSUME_NONNULL_END
