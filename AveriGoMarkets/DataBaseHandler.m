    //
    //  DataBaseHandler.m
    //  Averigo
    //
    //  Created by BTS on 12/01/17.
    //  Copyright © 2017 BTS. All rights reserved.
    //

#import "DataBaseHandler.h"

@implementation DataBaseHandler

- (id)init
{
    [self createDatabaseInstance];
    return self;
}

    //Copies the database from the Project bundle to the document directory
- (BOOL)copyDatabaseFromBundletoDocumentDirectory
{
    //check if old db file exists and delete if avaialble.
    
    if([[NSFileManager defaultManager] fileExistsAtPath:[self olddocumentDirectoryDatabaseLocation]]){
        NSError *error = nil;
        [[NSFileManager defaultManager] removeItemAtPath:[self olddocumentDirectoryDatabaseLocation] error:&error];

    }
        // If sqlite file does not exists at the document dir then get it from bundle and copy to doc directory
    if (![[NSFileManager defaultManager] fileExistsAtPath:[self documentDirectoryDatabaseLocation]])
        {
        
        NSString *bundlePathofDatabase = [[NSBundle mainBundle]pathForResource:@"AveriGo_MarketsV4" ofType:@"sqlite"];
        if (bundlePathofDatabase.length!=0)
            {
            NSString *docdirLocation = [self documentDirectoryDatabaseLocation];
            
                // it should return YES which indicates that the file is copied
            return [[NSFileManager defaultManager] copyItemAtPath:bundlePathofDatabase toPath:docdirLocation error:nil];
            }
        
            // This means that your file is not copied in the document directory
        return NO;
        }
    else
        {
        return YES;
        }
}

- (void)deleteDatabaseFromBundletoDocumentDirectory
{
    //check if old db file exists and delete if avaialble.
    
    if([[NSFileManager defaultManager] fileExistsAtPath:[self documentDirectoryDatabaseLocation]]){
        NSError *error = nil;
        [[NSFileManager defaultManager] removeItemAtPath:[self documentDirectoryDatabaseLocation] error:&error];

    }
}

    // gets the location of the database present in the document directory
- (NSString*)documentDirectoryDatabaseLocation
{
    NSArray *documentDirectoryFolderLocation = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSLog(@"DOCUMENT DIR LOCATION : %@",[[documentDirectoryFolderLocation objectAtIndex:0] stringByAppendingPathComponent:@"AveriGo_MarketsV4.sqlite"]);
    return [[documentDirectoryFolderLocation objectAtIndex:0] stringByAppendingPathComponent:@"AveriGo_MarketsV4.sqlite"];
}


- (NSString*)olddocumentDirectoryDatabaseLocation
{
    NSArray *documentDirectoryFolderLocation = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSLog(@"DOCUMENT DIR LOCATION : %@",[[documentDirectoryFolderLocation objectAtIndex:0] stringByAppendingPathComponent:@"AveriGo_MarketsV3.sqlite"]);
    return [[documentDirectoryFolderLocation objectAtIndex:0] stringByAppendingPathComponent:@"AveriGo_MarketsV3.sqlite"];
}


    // creates the database instance at document directory
- (void)createDatabaseInstance
{
    NSLog(@"datasbe created");
    if([self copyDatabaseFromBundletoDocumentDirectory])
        {
        NSLog(@"DB copied in doc dir");
        }
    else
        {
        NSLog(@"DB copying failed");
        }
}

    // checks if any previous db connection is open before firing a new query
    //and if the connection is open then closes it
- (void)closeanyOpenConnection
{
    if(sqlite3_open([[self documentDirectoryDatabaseLocation]UTF8String],&databaseReference)!= SQLITE_OK)
        {
        sqlite3_close(databaseReference);
        }
}

-(BOOL)insertCartItems:(NSString *)MicroMarketID andCategoryID:(NSString *)CategoryID andDescription:(NSString *)ItemDescription andItemNo:(NSString *)ItemNo andcrvtax:(NSString *)CRVTAX andSalesTax:(NSString *)SalesTax andDiscountPrice:(NSString *)DiscountPrice andItemName:(NSString *)ItemName andItemImage:(NSString *)ItemImage andItemPrice:(NSString *)ItemPrice andStock:(NSString *)Stock andQuantity:(NSString *)Quantity andOriginalPrice:(NSString *)OriginalPrice andCRVPercent:(NSString *)CRVPercent andSalesPercent:(NSString *)SalesPercent andCRVTAXEnable:(NSString *)CRVTAXEnable andSalesTaxEnable:(NSString *)SalesTaxEnable andDiscountPriceDiff:(NSString *)DiscountPriceDiff andDiscountPriceEnable:(NSString *)DiscountPriceEnable andBarcode:(NSString *)Barcode
{
    [self closeanyOpenConnection];
        // preparing my sqlite query           BG_ClientConfig
    const char *sqliteQuery = "insert into VB_Cart (`MicroMarketID`,`CategoryID`, `ItemDescription`, `ItemNo`, `CRVTAX`,`SalesTax`,`DiscountPrice`,`ItemName`,`ItemImage`,`ItemPrice`,`Stock`,`Quantity`,`OriginalPrice`,`CRVTAXPercent`,`SalesTaxPercent`,`CRVTAXEnable`,`SalesTaxEnable`,`DiscountPriceDiff`,`DiscountPriceEnable`, `Barcode`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    sqlite3_stmt *sqlstatement=nil;
    NSLog(@"sqlitequery %s",sqliteQuery);
    
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
        {
        sqlite3_bind_text(sqlstatement, 1, [MicroMarketID UTF8String],   -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 2, [CategoryID UTF8String],      -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 3, [ItemDescription UTF8String], -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 4, [ItemNo UTF8String],          -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 5, [CRVTAX UTF8String],          -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 6, [SalesTax UTF8String],        -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 7, [DiscountPrice UTF8String],   -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 8, [ItemName UTF8String],        -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 9, [ItemImage UTF8String],       -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,10, [ItemPrice UTF8String],       -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,11, [Stock UTF8String],           -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,12, [Quantity UTF8String],        -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,13, [OriginalPrice UTF8String],   -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,14, [CRVPercent UTF8String],      -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,15, [SalesPercent UTF8String],    -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,16, [CRVTAXEnable UTF8String],    -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,17, [SalesTaxEnable UTF8String],  -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,18, [DiscountPriceDiff UTF8String],    -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,19, [DiscountPriceEnable UTF8String],  -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,20, [Barcode UTF8String],  -1,  SQLITE_TRANSIENT);

        sqlite3_step(sqlstatement);
        
            // clearing the sql statement
        sqlite3_finalize(sqlstatement);
            //closing the database after the query execution
        sqlite3_close(databaseReference);
        NSLog(@"client Values Inserted");
        return YES;
        }
    else
        {
        return NO;
        }
    return NO;
    
}
-(BOOL)insertBeacons:(NSString *)BeaconMajor andDistance:(NSString *)Distance andMicroMarketID:(NSString *)MicroMarketID andStateName:(NSString *)StateName andAddress:(NSString *)Address andLongitude:(NSString *)Longitude andZip:(NSString *)Zip andImage:(NSString *)Image andDescription:(NSString *)Description andCountry:(NSString *)Country andLatitude:(NSString *)Latitude andOrderBy:(NSString *)OrderBy andCity:(NSString *)City andSalesTax:(NSString *)SalesTax andCompany:(NSString *)Company andBannerImage:(NSString *)BannerImage andBannerText:(NSString *)BannerText andisFeatured:(NSString *)isFeatured andcategoryId:(NSString *)categoryId andPayrollStatus:(NSString *)payrollStatus andPayUserId:(NSString *)payUserId andHasBestSelling:(NSString *)HasBestSelling andCreditStatus:(NSString *)CreditStatus andCreditUserId:(NSString *)CreditUserId
{
    [self closeanyOpenConnection];
        // preparing my sqlite query           BG_ClientConfig
    const char *sqliteQuery = "insert into Beacon (`BeaconMajor`,`Distance`, `MicroMarketID`, `StateName`, `Address`,`Longitude`,`Zip`,`Image`,`Description`,`Country`,`Latitude`,`OrderBy`,`City`,`SalesTax`,`Company`,`BannerImage`,`BannerText`,'ISFEATURED','CATEGORY_ID','PayrollStatus','PayUserID','HasBestSelling','CreditStatus','CreditStatusId') values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    sqlite3_stmt *sqlstatement=nil;
    NSLog(@"sqlitequery %s",sqliteQuery);
    
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
        {
        sqlite3_bind_text(sqlstatement,  1, [BeaconMajor UTF8String],   -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,  2, [Distance UTF8String],      -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,  3, [MicroMarketID UTF8String], -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,  4, [StateName UTF8String],     -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,  5, [Address UTF8String],       -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,  6, [Longitude UTF8String],     -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,  7, [Zip UTF8String],           -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,  8, [Image UTF8String],         -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,  9, [Description UTF8String],   -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 10, [Country UTF8String],       -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 11, [Latitude UTF8String],      -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 12, [OrderBy UTF8String],       -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 13, [City UTF8String],          -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 14, [SalesTax UTF8String],      -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 15, [Company UTF8String],      -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 16, [BannerImage UTF8String],      -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 17, [BannerText UTF8String],      -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 18, [isFeatured UTF8String],      -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 19, [categoryId UTF8String],      -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 20, [payrollStatus UTF8String],      -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 21, [payUserId UTF8String],      -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 22, [HasBestSelling UTF8String],      -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 23, [CreditStatus UTF8String],      -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 24, [CreditUserId UTF8String],      -1,  SQLITE_TRANSIENT);
        
        sqlite3_step(sqlstatement);
        
            // clearing the sql statement
        sqlite3_finalize(sqlstatement);
            //closing the database after the query execution
        sqlite3_close(databaseReference);
        NSLog(@"client Values Inserted");
        return YES;
        }
    else
        {
        return NO;
        }
    return NO;
    
}

-(BOOL)insertOperatorBeacons:(NSString *)BeaconMajor andBeaconMinor:(NSString *)BeaconMinor andAppMailId:(NSString *)AppRelatedMailId andBannerImage:(NSString *)BannerImage andBannerText:(NSString *)BannerText andBeaconValue:(NSString *)BeaconValue andCategoryId:(NSString *)CategoryId andCompanyName:(NSString *)CompanyName andCountryName:(NSString *)CountryName andOrderBy:(NSString *)OrderBy andDistance:(NSString *)Distance andIsFeatured:(NSString *)IsFeatured andLocationDesc:(NSString *)LocationDesc andLocationId:(NSString *)LocationId andLocationAdd:(NSString *)LocationAdd andLocationCity:(NSString *)LocationCity andLocationImage:(NSString *)LocationImage andLocationZip:(NSString *)LocationZip andMerchantId:(NSString *)MerchantId andSalesTax:(NSString *)SalesTax andTerminalId:(NSString *)TerminalId andWebLink:(NSString *)WebLink andWebLinkUrl:(NSString *)WebLinkUrl
{
    [self closeanyOpenConnection];
        // preparing my sqlite query           BG_ClientConfig
    const char *sqliteQuery = "insert into BeaconOperator ('BeaconMajor','BeaconMinor', 'AppRelatedMailId', 'BannerImage','BannerText','BeaconValue','CategoryId','CompanyName','CountryName','OrderBy','Distance','IsFeatured','LocationDesc','LocationId','LocationAdd','LocationCity','LocationImage','LocationZip','MerchantId','SalesTax','TerminalId','WebLink','WebLinkUrl') values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    sqlite3_stmt *sqlstatement=nil;
    NSLog(@"sqlitequery %s",sqliteQuery);
    
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
        {
        sqlite3_bind_text(sqlstatement,  1, [BeaconMajor UTF8String],   -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,  2, [BeaconMinor UTF8String],      -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,  3, [AppRelatedMailId UTF8String], -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,  4, [BannerImage UTF8String],     -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,  5, [BannerText UTF8String],       -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,  6, [BeaconValue UTF8String],     -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,  7, [CategoryId UTF8String],           -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,  8, [CompanyName UTF8String],         -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,  9, [CountryName UTF8String],   -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 10, [OrderBy UTF8String],       -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 11, [Distance UTF8String],      -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 12, [IsFeatured UTF8String],       -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 13, [LocationDesc UTF8String],          -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 14, [LocationId UTF8String],      -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 14, [LocationAdd UTF8String],      -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 16, [LocationCity UTF8String],      -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 17, [LocationImage UTF8String],      -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 18, [LocationZip UTF8String],      -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 19, [MerchantId UTF8String],      -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 20, [SalesTax UTF8String],      -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 21, [TerminalId UTF8String],      -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 22, [WebLink UTF8String],      -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 23, [WebLinkUrl UTF8String],      -1,  SQLITE_TRANSIENT);
        
        sqlite3_step(sqlstatement);
        
            // clearing the sql statement
        sqlite3_finalize(sqlstatement);
            //closing the database after the query execution
        sqlite3_close(databaseReference);
        NSLog(@"client Values Inserted");
        return YES;
        }
    else
        {
        return NO;
        }
    return NO;
}
- (NSMutableArray *)getOperatorBeacons{
    NSMutableArray *recordSet = [[NSMutableArray alloc]init];
    [self closeanyOpenConnection];
    
        // Query to fetch the emp name and images from the database
    NSString *fetchQuery = [NSString stringWithFormat:@"select * from BeaconOperator order by Distance"];
    NSLog(@"fetch query:%@",fetchQuery);
    const char *selectQuery = [fetchQuery UTF8String];
    sqlite3_stmt *sqlstatement = nil;
    
    if (sqlite3_prepare_v2(databaseReference, selectQuery, -1, &sqlstatement , NULL)==SQLITE_OK) {
        
            // declaring a dictionary so that response can be saved in KVC
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        
        while (sqlite3_step(sqlstatement) == SQLITE_ROW)
            {
            char* major = (char*)sqlite3_column_text(sqlstatement, 0);
            NSString *majorstr  = [NSString stringWithUTF8String:major];
            [dict setValue:majorstr  forKey:@"BEACONMAJOR"];
            
            char* distance = (char*)sqlite3_column_text(sqlstatement, 1);
            NSString *distancestr  = [NSString stringWithUTF8String:distance];
            [dict setValue:distancestr  forKey:@"BEACONMINOR"];
            
            char* locationid = (char*)sqlite3_column_text(sqlstatement, 2);
            NSString *locationidstr  = [NSString stringWithUTF8String:locationid];
            [dict setValue:locationidstr  forKey:@"APP_RELATED_MAILID"];
            
            char* state = (char*)sqlite3_column_text(sqlstatement, 3);
            NSString *statestr  = [NSString stringWithUTF8String:state];
            [dict setValue:statestr  forKey:@"BANNER_IMAGE"];
            
            char* address = (char*)sqlite3_column_text(sqlstatement, 4);
            NSString *addressstr  = [NSString stringWithUTF8String:address];
            [dict setValue:addressstr  forKey:@"BANNER_TEXT"];
            
            char* longitude = (char*)sqlite3_column_text(sqlstatement, 5);
            NSString *longitudestr  = [NSString stringWithUTF8String:longitude];
            [dict setValue:longitudestr  forKey:@"BEACONVALUE"];
            
            char* zip = (char*)sqlite3_column_text(sqlstatement, 6);
            NSString *zipstr  = [NSString stringWithUTF8String:zip];
            [dict setValue:zipstr  forKey:@"CATEGORY_ID"];
            
            char* image = (char*)sqlite3_column_text(sqlstatement, 7);
            NSString *imagestr  = [NSString stringWithUTF8String:image];
            [dict setValue:imagestr  forKey:@"COMPANYNAME"];
            
            char* desc = (char*)sqlite3_column_text(sqlstatement, 8);
            NSString *descstr  = [NSString stringWithUTF8String:desc];
            [dict setValue:descstr  forKey:@"COUNTRY_NAME"];
            
            char* country = (char*)sqlite3_column_text(sqlstatement, 9);
            NSString *countrystr  = [NSString stringWithUTF8String:country];
            [dict setValue:countrystr  forKey:@"ORDERBY"];
            
            char* latitude = (char*)sqlite3_column_text(sqlstatement, 10);
            NSString *latitudestr = [NSString stringWithUTF8String:latitude];
            [dict setValue:latitudestr forKey:@"Distance"];
            
            char* orderby = (char*)sqlite3_column_text(sqlstatement, 11);
            NSString *orderbystr = [NSString stringWithUTF8String:orderby];
            [dict setValue:orderbystr forKey:@"ISFEATURED"];
            
            char* city = (char*)sqlite3_column_text(sqlstatement, 12);
            NSString *citystr = [NSString stringWithUTF8String:city];
            [dict setValue:citystr forKey:@"LOCATION_DESC"];
            
            char* salestax = (char*)sqlite3_column_text(sqlstatement, 13);
            NSString *salestaxstr = [NSString stringWithUTF8String:salestax];
            [dict setValue:salestaxstr forKey:@"LOCATION_ID"];
            
            char* company = (char*)sqlite3_column_text(sqlstatement, 13);
            NSString *companystr = [NSString stringWithUTF8String:company];
            [dict setValue:companystr forKey:@"LOC_ADD"];
            
            char* bannerImage = (char*)sqlite3_column_text(sqlstatement, 15);
            NSString *bannerImageStr = [NSString stringWithUTF8String:bannerImage];
            [dict setValue:bannerImageStr forKey:@"LOC_CITY"];
            
            char* bannerText = (char*)sqlite3_column_text(sqlstatement, 16);
            NSString *bannerTextStr = [NSString stringWithUTF8String:bannerText];
            [dict setValue:bannerTextStr forKey:@"LOC_IMAGE"];
            
            char* isFeatured = (char*)sqlite3_column_text(sqlstatement, 17);
            NSString *isFeaturedStr = [NSString stringWithUTF8String:isFeatured];
            [dict setValue:isFeaturedStr forKey:@"LOC_ZIP"];
            
            char* categoryId = (char*)sqlite3_column_text(sqlstatement, 18);
            NSString *categoryIdStr = [NSString stringWithUTF8String:categoryId];
            [dict setValue:categoryIdStr forKey:@"MERCHANT_ID"];
            
            char* salestaxx = (char*)sqlite3_column_text(sqlstatement, 19);
            NSString *salestaxxstr = [NSString stringWithUTF8String:salestaxx];
            [dict setValue:salestaxxstr forKey:@"SALESTAX"];
            
            char* termianlid = (char*)sqlite3_column_text(sqlstatement, 20);
            NSString *termianlidstr = [NSString stringWithUTF8String:termianlid];
            [dict setValue:termianlidstr forKey:@"TERMINAL_ID"];
            
            char* weblink = (char*)sqlite3_column_text(sqlstatement, 21);
            NSString *weblinkstr = [NSString stringWithUTF8String:weblink];
            [dict setValue:weblinkstr forKey:@"WEB_LINK"];
            
            char* weblinkurl = (char*)sqlite3_column_text(sqlstatement, 22);
            NSString *weblinkurlstr = [NSString stringWithUTF8String:weblinkurl];
            [dict setValue:weblinkurlstr forKey:@"WEB_LINK_URL"];
                // saving the record set in a MutableArray
            [recordSet addObject:dict];
                // clearing off the dictionary to make sure that it does not contain any garbage data
            dict = nil;
                // re- initalizing the dictionary for next record
            dict = [[NSMutableDictionary alloc]init];
            }
        sqlite3_finalize(sqlstatement);
        sqlite3_close(databaseReference);
            // once all the fetching is one clearing off the dict variable for good.
        dict = nil;
        
    }
    
    return  recordSet;
    
}

- (NSMutableArray *)getUserAnalyticsToBeSynced
{
    NSMutableArray *recordSet = [[NSMutableArray alloc]init];
    [self closeanyOpenConnection];
    
    // Query to fetch the emp name and images from the database
    NSString *fetchQuery = [NSString stringWithFormat:@"select * from analytics_user where sync_flag ='Y'"];
    NSLog(@"fetch query:%@",fetchQuery);
    const char *selectQuery = [fetchQuery UTF8String];
    sqlite3_stmt *sqlstatement = nil;
    
    if (sqlite3_prepare_v2(databaseReference, selectQuery, -1, &sqlstatement , NULL)==SQLITE_OK) {
        
        // declaring a dictionary so that response can be saved in KVC
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        
        while (sqlite3_step(sqlstatement) == SQLITE_ROW)
                {
            char* major = (char*)sqlite3_column_text(sqlstatement, 0);
            NSString *majorstr  = [NSString stringWithUTF8String:major];
            [dict setValue:majorstr  forKey:@"seq_id"];
            
            char* distance = (char*)sqlite3_column_text(sqlstatement, 1);
            NSString *distancestr  = [NSString stringWithUTF8String:distance];
            [dict setValue:distancestr  forKey:@"user_id"];
            
            char* locationid = (char*)sqlite3_column_text(sqlstatement, 2);
            NSString *locationidstr  = [NSString stringWithUTF8String:locationid];
            [dict setValue:locationidstr  forKey:@"session_guid"];
            
            char* state = (char*)sqlite3_column_text(sqlstatement, 3);
            NSString *statestr  = [NSString stringWithUTF8String:state];
            [dict setValue:statestr  forKey:@"first_name"];
            
            char* address = (char*)sqlite3_column_text(sqlstatement, 4);
            NSString *addressstr  = [NSString stringWithUTF8String:address];
            [dict setValue:addressstr  forKey:@"last_name"];
            
            char* longitude = (char*)sqlite3_column_text(sqlstatement, 5);
            NSString *longitudestr  = [NSString stringWithUTF8String:longitude];
            [dict setValue:longitudestr  forKey:@"device_udid"];
            
            char* zip = (char*)sqlite3_column_text(sqlstatement, 6);
            NSString *zipstr  = [NSString stringWithUTF8String:zip];
            [dict setValue:zipstr  forKey:@"device_type"];
            
            char* image = (char*)sqlite3_column_text(sqlstatement, 7);
            NSString *imagestr  = [NSString stringWithUTF8String:image];
            [dict setValue:imagestr  forKey:@"device_model"];
            
            char* desc = (char*)sqlite3_column_text(sqlstatement, 8);
            NSString *descstr  = [NSString stringWithUTF8String:desc];
            [dict setValue:descstr  forKey:@"device_os"];
            
            char* country = (char*)sqlite3_column_text(sqlstatement, 9);
            NSString *countrystr  = [NSString stringWithUTF8String:country];
            [dict setValue:countrystr  forKey:@"latitude"];
            
            char* latitude = (char*)sqlite3_column_text(sqlstatement, 10);
            NSString *latitudestr = [NSString stringWithUTF8String:latitude];
            [dict setValue:latitudestr forKey:@"longitude"];
            
            char* orderby = (char*)sqlite3_column_text(sqlstatement, 11);
            NSString *orderbystr = [NSString stringWithUTF8String:orderby];
            [dict setValue:orderbystr forKey:@"check_in_time"];
            
            char* city = (char*)sqlite3_column_text(sqlstatement, 12);
            NSString *citystr = [NSString stringWithUTF8String:city];
            [dict setValue:citystr forKey:@"check_out_time"];
            
            char* salestax = (char*)sqlite3_column_text(sqlstatement, 13);
            NSString *salestaxstr = [NSString stringWithUTF8String:salestax];
            [dict setValue:salestaxstr forKey:@"sync_flag"];
            
            // saving the record set in a MutableArray
            [recordSet addObject:dict];
            // clearing off the dictionary to make sure that it does not contain any garbage data
            dict = nil;
            // re- initalizing the dictionary for next record
            dict = [[NSMutableDictionary alloc]init];
                }
        sqlite3_finalize(sqlstatement);
        sqlite3_close(databaseReference);
        // once all the fetching is one clearing off the dict variable for good.
        dict = nil;
        
    }
    
    return  recordSet;
    
}

-(NSMutableArray *)getFeedbackAnalytics{
    NSMutableArray *recordSet = [[NSMutableArray alloc]init];
    [self closeanyOpenConnection];
    
    // Query to fetch the emp name and images from the database
    NSString *fetchQuery = [NSString stringWithFormat:@"select * from analytics_feedback"];
    NSLog(@"fetch query:%@",fetchQuery);
    const char *selectQuery = [fetchQuery UTF8String];
    sqlite3_stmt *sqlstatement = nil;
    
    if (sqlite3_prepare_v2(databaseReference, selectQuery, -1, &sqlstatement , NULL)==SQLITE_OK) {
        
        // declaring a dictionary so that response can be saved in KVC
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        
        while (sqlite3_step(sqlstatement) == SQLITE_ROW)
        {
            char* seqid = (char*)sqlite3_column_text(sqlstatement, 0);
            NSString *seqidstr  = [NSString stringWithUTF8String:seqid];
            [dict setValue:seqidstr  forKey:@"seq_id"];
            
            char* recipient = (char*)sqlite3_column_text(sqlstatement, 1);
            NSString *recipientstr  = [NSString stringWithUTF8String:recipient];
            [dict setValue:recipientstr  forKey:@"recipient"];
            
            char* date = (char*)sqlite3_column_text(sqlstatement, 2);
            NSString *datestr  = [NSString stringWithUTF8String:date];
            [dict setValue:datestr  forKey:@"date"];
            
            char* time = (char*)sqlite3_column_text(sqlstatement, 3);
            NSString *timestr  = [NSString stringWithUTF8String:time];
            [dict setValue:timestr  forKey:@"time"];
            
            char* name = (char*)sqlite3_column_text(sqlstatement, 4);
            NSString *namestr  = [NSString stringWithUTF8String:name];
            [dict setValue:namestr  forKey:@"name"];
            
            char* userid = (char*)sqlite3_column_text(sqlstatement, 5);
            NSString *useridstr  = [NSString stringWithUTF8String:userid];
            [dict setValue:useridstr  forKey:@"user_id"];
            
            char* marketname = (char*)sqlite3_column_text(sqlstatement, 6);
            NSString *marketnamestr  = [NSString stringWithUTF8String:marketname];
            [dict setValue:marketnamestr  forKey:@"micro_market_name"];
            
            char* marketid = (char*)sqlite3_column_text(sqlstatement, 7);
            NSString *marketidstr  = [NSString stringWithUTF8String:marketid];
            [dict setValue:marketidstr  forKey:@"micro_market_id"];
            
            char* message = (char*)sqlite3_column_text(sqlstatement, 8);
            NSString *messagestr  = [NSString stringWithUTF8String:message];
            [dict setValue:messagestr  forKey:@"message"];
            
            char* manufacturer = (char*)sqlite3_column_text(sqlstatement, 9);
            NSString *manufacturerstr  = [NSString stringWithUTF8String:manufacturer];
            [dict setValue:manufacturerstr  forKey:@"device_manufacturer"];
            
            char* model = (char*)sqlite3_column_text(sqlstatement, 10);
            NSString *modelstr  = [NSString stringWithUTF8String:model];
            [dict setValue:modelstr  forKey:@"device_model"];
            
            char* os = (char*)sqlite3_column_text(sqlstatement, 11);
            NSString *osstr  = [NSString stringWithUTF8String:os];
            [dict setValue:osstr  forKey:@"device_os"];
            
            char* version = (char*)sqlite3_column_text(sqlstatement, 12);
            NSString *versionstr  = [NSString stringWithUTF8String:version];
            [dict setValue:versionstr  forKey:@"app_version"];
            
            // saving the record set in a MutableArray
            [recordSet addObject:dict];
            // clearing off the dictionary to make sure that it does not contain any garbage data
            dict = nil;
            // re- initalizing the dictionary for next record
            dict = [[NSMutableDictionary alloc]init];
        }
        sqlite3_finalize(sqlstatement);
        sqlite3_close(databaseReference);
        // once all the fetching is one clearing off the dict variable for good.
        dict = nil;
        
    }
    
    return  recordSet;
    
}

- (NSMutableArray *)getScreenAnalyticsToBeSynced
{
    NSMutableArray *recordSet = [[NSMutableArray alloc]init];
    [self closeanyOpenConnection];
    
    // Query to fetch the emp name and images from the database
    NSString *fetchQuery = [NSString stringWithFormat:@"select * from analytics_screen where sync_flag ='Y'"];
    NSLog(@"fetch query:%@",fetchQuery);
    const char *selectQuery = [fetchQuery UTF8String];
    sqlite3_stmt *sqlstatement = nil;
    
    if (sqlite3_prepare_v2(databaseReference, selectQuery, -1, &sqlstatement , NULL)==SQLITE_OK) {
        
        // declaring a dictionary so that response can be saved in KVC
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        
        while (sqlite3_step(sqlstatement) == SQLITE_ROW)
                {
            char* major = (char*)sqlite3_column_text(sqlstatement, 0);
            NSString *majorstr  = [NSString stringWithUTF8String:major];
            [dict setValue:majorstr  forKey:@"seq_id"];
            
            char* distance = (char*)sqlite3_column_text(sqlstatement, 1);
            NSString *distancestr  = [NSString stringWithUTF8String:distance];
            [dict setValue:distancestr  forKey:@"user_id"];
            
            char* locationid = (char*)sqlite3_column_text(sqlstatement, 2);
            NSString *locationidstr  = [NSString stringWithUTF8String:locationid];
            [dict setValue:locationidstr  forKey:@"session_guid"];
            
            char* state = (char*)sqlite3_column_text(sqlstatement, 3);
            NSString *statestr  = [NSString stringWithUTF8String:state];
            [dict setValue:statestr  forKey:@"micro_market_id"];
            
            char* address = (char*)sqlite3_column_text(sqlstatement, 4);
            NSString *addressstr  = [NSString stringWithUTF8String:address];
            [dict setValue:addressstr  forKey:@"micro_market_name"];
            
            char* longitude = (char*)sqlite3_column_text(sqlstatement, 5);
            NSString *longitudestr  = [NSString stringWithUTF8String:longitude];
            [dict setValue:longitudestr  forKey:@"screen_name"];
            
            char* zip = (char*)sqlite3_column_text(sqlstatement, 6);
            NSString *zipstr  = [NSString stringWithUTF8String:zip];
            [dict setValue:zipstr  forKey:@"screen_visits"];
            
            char* image = (char*)sqlite3_column_text(sqlstatement, 7);
            NSString *imagestr  = [NSString stringWithUTF8String:image];
            [dict setValue:imagestr  forKey:@"check_in_time"];
            
            char* desc = (char*)sqlite3_column_text(sqlstatement, 8);
            NSString *descstr  = [NSString stringWithUTF8String:desc];
            [dict setValue:descstr  forKey:@"check_out_time"];
            
            char* country = (char*)sqlite3_column_text(sqlstatement, 9);
            NSString *countrystr  = [NSString stringWithUTF8String:country];
            [dict setValue:countrystr  forKey:@"sync_flag"];
            
            // saving the record set in a MutableArray
            [recordSet addObject:dict];
            // clearing off the dictionary to make sure that it does not contain any garbage data
            dict = nil;
            // re- initalizing the dictionary for next record
            dict = [[NSMutableDictionary alloc]init];
                }
        sqlite3_finalize(sqlstatement);
        sqlite3_close(databaseReference);
        // once all the fetching is one clearing off the dict variable for good.
        dict = nil;
        
    }
    
    return  recordSet;
    
}

- (NSMutableArray *)getProductAnalyticsToBeScanned
{
    NSMutableArray *recordSet = [[NSMutableArray alloc]init];
    [self closeanyOpenConnection];
    
    // Query to fetch the emp name and images from the database
    NSString *fetchQuery = [NSString stringWithFormat:@"select * from analytics_cartproduct where sync_flag ='Y'"];
    NSLog(@"fetch query:%@",fetchQuery);
    const char *selectQuery = [fetchQuery UTF8String];
    sqlite3_stmt *sqlstatement = nil;
    
    if (sqlite3_prepare_v2(databaseReference, selectQuery, -1, &sqlstatement , NULL)==SQLITE_OK) {
        
        // declaring a dictionary so that response can be saved in KVC
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        
        while (sqlite3_step(sqlstatement) == SQLITE_ROW)
                {
            char* major = (char*)sqlite3_column_text(sqlstatement, 0);
            NSString *majorstr  = [NSString stringWithUTF8String:major];
            [dict setValue:majorstr  forKey:@"seq_id"];
            
            char* distance = (char*)sqlite3_column_text(sqlstatement, 1);
            NSString *distancestr  = [NSString stringWithUTF8String:distance];
            [dict setValue:distancestr  forKey:@"user_id"];
            
            char* locationid = (char*)sqlite3_column_text(sqlstatement, 2);
            NSString *locationidstr  = [NSString stringWithUTF8String:locationid];
            [dict setValue:locationidstr  forKey:@"product_id"];
            
            char* state = (char*)sqlite3_column_text(sqlstatement, 3);
            NSString *statestr  = [NSString stringWithUTF8String:state];
            [dict setValue:statestr  forKey:@"category_id"];
            
            char* address = (char*)sqlite3_column_text(sqlstatement, 4);
            NSString *addressstr  = [NSString stringWithUTF8String:address];
            [dict setValue:addressstr  forKey:@"session_guid"];
            
            char* longitude = (char*)sqlite3_column_text(sqlstatement, 5);
            NSString *longitudestr  = [NSString stringWithUTF8String:longitude];
            [dict setValue:longitudestr  forKey:@"product_name"];
            
            char* zip = (char*)sqlite3_column_text(sqlstatement, 6);
            NSString *zipstr  = [NSString stringWithUTF8String:zip];
            [dict setValue:zipstr  forKey:@"micro_market_id"];
            
            char* image = (char*)sqlite3_column_text(sqlstatement, 7);
            NSString *imagestr  = [NSString stringWithUTF8String:image];
            [dict setValue:imagestr  forKey:@"micro_market_name"];
            
            char* desc = (char*)sqlite3_column_text(sqlstatement, 8);
            NSString *descstr  = [NSString stringWithUTF8String:desc];
            [dict setValue:descstr  forKey:@"upc_code"];
            
            char* country = (char*)sqlite3_column_text(sqlstatement, 9);
            NSString *countrystr  = [NSString stringWithUTF8String:country];
            [dict setValue:countrystr  forKey:@"item_qty"];
            
            char* latitude = (char*)sqlite3_column_text(sqlstatement, 10);
            NSString *latitudestr = [NSString stringWithUTF8String:latitude];
            [dict setValue:latitudestr forKey:@"item_price"];
            
            char* orderby = (char*)sqlite3_column_text(sqlstatement, 11);
            NSString *orderbystr = [NSString stringWithUTF8String:orderby];
            [dict setValue:orderbystr forKey:@"type"];
            
            char* city = (char*)sqlite3_column_text(sqlstatement, 12);
            NSString *citystr = [NSString stringWithUTF8String:city];
            [dict setValue:citystr forKey:@"sync_flag"];
            
            // saving the record set in a MutableArray
            [recordSet addObject:dict];
            // clearing off the dictionary to make sure that it does not contain any garbage data
            dict = nil;
            // re- initalizing the dictionary for next record
            dict = [[NSMutableDictionary alloc]init];
                }
        sqlite3_finalize(sqlstatement);
        sqlite3_close(databaseReference);
        // once all the fetching is one clearing off the dict variable for good.
        dict = nil;
        
    }
    
    return  recordSet;
    
}

-(BOOL)insertFeedbackAnalytics:(NSString *)recipient andDate:(NSString *)date andTime:(NSString *)time andName:(NSString *)name andUserId:(NSString *)userId andMicroMarketName:(NSString *)microMarketName andMicroMarketId:(NSString *)microMarketId andMessage:(NSString *)message andDeviceManufacturer:(NSString *)deviceManufacturer andDeviceModel:(NSString *)deviceModel andDeviceOS:(NSString *)deviceOS andAppVersion:(NSString *)appVersion{
    [self closeanyOpenConnection];
    // preparing my sqlite query           BG_ClientConfig
    const char *sqliteQuery = "insert into analytics_feedback ('recipient','date', 'time', 'name','user_id','micro_market_name','micro_market_id','message','device_manufacturer','device_model','device_os','app_version') values(?,?,?,?,?,?,?,?,?,?,?,?)";
    sqlite3_stmt *sqlstatement=nil;
    
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
    {
        sqlite3_bind_text(sqlstatement, 1, [recipient UTF8String],      -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,  2, [date UTF8String],      -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,  3, [time UTF8String], -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,  4, [name UTF8String],     -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,  5, [userId UTF8String],       -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,  6, [microMarketName UTF8String],     -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,  7, [microMarketId UTF8String] , -1 ,SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,  8, [message UTF8String],         -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,  9, [deviceManufacturer UTF8String],   -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 10, [deviceModel UTF8String],       -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 11, [deviceOS UTF8String],      -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 12, [appVersion UTF8String],       -1,  SQLITE_TRANSIENT);
        
        
        sqlite3_step(sqlstatement);
        
        // clearing the sql statement
        sqlite3_finalize(sqlstatement);
        //closing the database after the query execution
        sqlite3_close(databaseReference);
        NSLog(@"client Values Inserted");
        return YES;
    }
    else
    {
        return NO;
    }
    return NO;
}


-(BOOL)insertUserAnalytics:(NSString *)userid andsessionGUID:(NSString *)sessionGuid andFirstName:(NSString *)firstName andLastName:(NSString *)lastName andDeviceUDID:(NSString *)deviceUDID andDeviceType:(NSString *)deviceType andDeviceModel:(NSString *)deviceModel andeDeviceOS:(NSString *)deviceOS andLatitude:(NSString *)latitude andLongitude:(NSString *)longitude andCheckInTime:(NSString *)checkInTime andCheckOutTime:(NSString *)checkOutTime andSyncFlag:(NSString *)syncFlag{
    
    [self closeanyOpenConnection];
    // preparing my sqlite query           BG_ClientConfig
    const char *sqliteQuery = "insert into analytics_user ('user_id','session_guid', 'first_name', 'last_name','device_udid','device_type','device_model','device_os','latitude','longitude','check_in_time','check_out_time','sync_flag') values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
    sqlite3_stmt *sqlstatement=nil;
    
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
            {
        sqlite3_bind_text(sqlstatement, 1, [userid UTF8String],      -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,  2, [sessionGuid UTF8String],      -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,  3, [firstName UTF8String], -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,  4, [lastName UTF8String],     -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,  5, [deviceUDID UTF8String],       -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,  6, [deviceType UTF8String],     -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,  7, [deviceModel UTF8String],           -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,  8, [deviceOS UTF8String],         -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,  9, [latitude UTF8String],   -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 10, [longitude UTF8String],       -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 11, [checkInTime UTF8String],      -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 12, [checkOutTime UTF8String],       -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 13, [syncFlag UTF8String],          -1,  SQLITE_TRANSIENT);
       
        
        sqlite3_step(sqlstatement);
        
        // clearing the sql statement
        sqlite3_finalize(sqlstatement);
        //closing the database after the query execution
        sqlite3_close(databaseReference);
        NSLog(@"client Values Inserted");
        return YES;
            }
    else
            {
        return NO;
            }
    return NO;
    
}


- (BOOL)UpdateUserAnalytics:(NSString *)sessionGuid andCheckOutTime:(NSString *)checkOutTime {
    
    [self closeanyOpenConnection];
    
    NSString *update_query = [NSString stringWithFormat:@"update analytics_user SET check_out_time='%@' where session_guid='%@'",checkOutTime,sessionGuid];
    NSLog(@"Update Query:%@",update_query);
    const char *sqliteQuery = [update_query UTF8String];
    
    
    sqlite3_stmt *sqlstatement = nil;
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
            {
        sqlite3_bind_text(sqlstatement, 1, [checkOutTime UTF8String],    -1,  SQLITE_TRANSIENT);
        // executes the sql statement with the data you need to insert in the db
        sqlite3_step(sqlstatement);
        
        // clearing the sql statement
        sqlite3_finalize(sqlstatement);
        //closing the database after the query execution
        sqlite3_close(databaseReference);
        
        return YES;
            }
    
    return NO;
    
}

- (BOOL)UpdateScreenAnalytics:(NSString *)sessionGuid andVisitCount:(int)visitCount andScreenName:(NSString *)screenName{
    [self closeanyOpenConnection];
    
    NSString *update_query = [NSString stringWithFormat:@"update analytics_screen SET screen_visits=%d where session_guid='%@' and screen_name='%@'",visitCount,sessionGuid,screenName];
    NSLog(@"Update Query:%@",update_query);
    const char *sqliteQuery = [update_query UTF8String];
    
    
    sqlite3_stmt *sqlstatement = nil;
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
            {
        sqlite3_bind_int(sqlstatement, 1, visitCount);
        // executes the sql statement with the data you need to insert in the db
        sqlite3_step(sqlstatement);
        
        // clearing the sql statement
        sqlite3_finalize(sqlstatement);
        //closing the database after the query execution
        sqlite3_close(databaseReference);
        
        return YES;
            }
    
    return NO;
    
}

- (BOOL)UpdateScreenAnalyticsMicroMarket:(int)microMarketId andSessionGuid:(NSString *)sessionGuid{
    [self closeanyOpenConnection];
    
    NSString *update_query = [NSString stringWithFormat:@"update analytics_screen SET micro_market_id=%d where  session_guid='%@'",microMarketId,sessionGuid];
    NSLog(@"Update Query:%@",update_query);
    const char *sqliteQuery = [update_query UTF8String];
    
    
    sqlite3_stmt *sqlstatement = nil;
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
    {
        sqlite3_bind_int(sqlstatement, 1, microMarketId);
        // executes the sql statement with the data you need to insert in the db
        sqlite3_step(sqlstatement);
        
        // clearing the sql statement
        sqlite3_finalize(sqlstatement);
        //closing the database after the query execution
        sqlite3_close(databaseReference);
        
        return YES;
    }
    
    return NO;
    
}

- (BOOL)UpdateUserAnalyticsCart:(int)microMarketId andSessionGuid:(NSString *)sessionGuid{
    [self closeanyOpenConnection];
    
    NSString *update_query = [NSString stringWithFormat:@"update analytics_cartproduct SET micro_market_id=%d where  session_guid='%@'",microMarketId,sessionGuid];
    NSLog(@"Update Query:%@",update_query);
    const char *sqliteQuery = [update_query UTF8String];
    
    
    sqlite3_stmt *sqlstatement = nil;
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
    {
        sqlite3_bind_int(sqlstatement, 1, microMarketId);
        // executes the sql statement with the data you need to insert in the db
        sqlite3_step(sqlstatement);
        
        // clearing the sql statement
        sqlite3_finalize(sqlstatement);
        //closing the database after the query execution
        sqlite3_close(databaseReference);
        
        return YES;
    }
    
    return NO;
    
}


- (BOOL)UpdateProductAnalytics:(NSString *)sessionGuid andQty:(int)qty andProductID:(NSString *)productID andType:(NSString *)type {
    [self closeanyOpenConnection];
    
    NSString *update_query = [NSString stringWithFormat:@"update analytics_cartproduct SET item_qty=%d where session_guid='%@' and product_id='%@' and type='%@'",qty,sessionGuid,productID,type];
    NSLog(@"Update Query:%@",update_query);
    const char *sqliteQuery = [update_query UTF8String];
    
    
    sqlite3_stmt *sqlstatement = nil;
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
            {
        sqlite3_bind_int(sqlstatement, 1, qty);
        // executes the sql statement with the data you need to insert in the db
        sqlite3_step(sqlstatement);
        
        // clearing the sql statement
        sqlite3_finalize(sqlstatement);
        //closing the database after the query execution
        sqlite3_close(databaseReference);
        
        return YES;
            }
    
    return NO;
    
}

- (BOOL)UpdateScreenAnalyticsCheckOutTime:(NSString *)sessionGuid andCheckOutTime:(NSString *)checkOutTime andScreenName:(NSString *)screenName {
    [self closeanyOpenConnection];
    
    NSString *update_query = [NSString stringWithFormat:@"update analytics_screen SET check_out_time='%@' where session_guid='%@' and screen_name='%@'",checkOutTime,sessionGuid,screenName];
    NSLog(@"Update Query:%@",update_query);
    const char *sqliteQuery = [update_query UTF8String];
    
    
    sqlite3_stmt *sqlstatement = nil;
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
            {
        sqlite3_bind_text(sqlstatement, 1, [checkOutTime UTF8String],    -1,  SQLITE_TRANSIENT);
        // executes the sql statement with the data you need to insert in the db
        sqlite3_step(sqlstatement);
        
        // clearing the sql statement
        sqlite3_finalize(sqlstatement);
        //closing the database after the query execution
        sqlite3_close(databaseReference);
        
        return YES;
            }
    
    return NO;
}

-(BOOL)insertScreenAnalytics:(NSString *)userid andsessionGUID:(NSString *)sessionGuid andMircoMarketID:(int)microMakrketID andMicroMarketName:(NSString *)microMarketName andScreenName:(NSString *)screenName andscreenVisits:(int)screenVisits andCheckInTime:(NSString *)checkInTime andCheckOutTime:(NSString *)checkOutTime andSyncFlag:(NSString *)syncFlag {
    
    [self closeanyOpenConnection];
    // preparing my sqlite query           BG_ClientConfig
    const char *sqliteQuery = "insert into analytics_screen ('user_id','session_guid', 'micro_market_id', 'micro_market_name','screen_name','screen_visits','check_in_time','check_out_time','sync_flag') values(?,?,?,?,?,?,?,?,?)";
    sqlite3_stmt *sqlstatement=nil;
    NSLog(@"sqlitequery %s",sqliteQuery);
    
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
            {
        sqlite3_bind_text(sqlstatement, 1, [userid UTF8String],      -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 2, [sessionGuid UTF8String],      -1,  SQLITE_TRANSIENT);
        sqlite3_bind_int(sqlstatement, 3, microMakrketID);
        sqlite3_bind_text(sqlstatement,  4, [microMarketName UTF8String],     -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,  5, [screenName UTF8String],       -1,  SQLITE_TRANSIENT);
        sqlite3_bind_int(sqlstatement, 6, screenVisits);
        sqlite3_bind_text(sqlstatement,  7, [checkInTime UTF8String],     -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,  8, [checkOutTime UTF8String],           -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,  9, [syncFlag UTF8String],         -1,  SQLITE_TRANSIENT);
        
        sqlite3_step(sqlstatement);
        
        // clearing the sql statement
        sqlite3_finalize(sqlstatement);
        //closing the database after the query execution
        sqlite3_close(databaseReference);
        NSLog(@"client Values Inserted");
        return YES;
            }
    else
            {
        return NO;
            }
    return NO;
}


-(BOOL)insertProductAnalytics:(NSString *)userid andsessionGUID:(NSString *)sessionGuid andMircoMarketID:(int)microMakrketID andMicroMarketName:(NSString *)microMarketName andProductID:(NSString *)productID andProductName:(NSString *)productName andCategoryID:(NSString *)categoryID andUPCCode:(NSString *)upcCode andItemQty:(int)qty andItemPrice:(float)itemPrice andType:(NSString *)type andSyncFlag:(NSString *)syncFlag {
    
    [self closeanyOpenConnection];
    const char *sqliteQuery = "insert into analytics_cartproduct ('user_id','product_id', 'category_id', 'session_guid','product_name','micro_market_id','micro_market_name','upc_code','item_qty','item_price','type','sync_flag') values(?,?,?,?,?,?,?,?,?,?,?,?)";
    sqlite3_stmt *sqlstatement=nil;
    NSLog(@"sqlitequery %s",sqliteQuery);
    
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
            {
        sqlite3_bind_text(sqlstatement, 1, [userid UTF8String],      -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 2, [productID UTF8String],      -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 3, [categoryID UTF8String],      -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 4, [sessionGuid UTF8String],      -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,  5, [productName UTF8String],     -1,  SQLITE_TRANSIENT);
        sqlite3_bind_int(sqlstatement, 6, microMakrketID);
        sqlite3_bind_text(sqlstatement,  7, [microMarketName UTF8String],     -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,  8, [upcCode UTF8String],     -1,  SQLITE_TRANSIENT);
        sqlite3_bind_int(sqlstatement, 9, qty);
        sqlite3_bind_double(sqlstatement, 10, itemPrice);
        sqlite3_bind_text(sqlstatement,  11, [type UTF8String],         -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement,  12, [syncFlag UTF8String],         -1,  SQLITE_TRANSIENT);
        sqlite3_step(sqlstatement);
        
        // clearing the sql statement
        sqlite3_finalize(sqlstatement);
        //closing the database after the query execution
        sqlite3_close(databaseReference);
        NSLog(@"client Values Inserted");
        return YES;
            }
    else
            {
        return NO;
            }
    return NO;
    
}

- (NSMutableArray *)getBeacons
{
    NSMutableArray *recordSet = [[NSMutableArray alloc]init];
    [self closeanyOpenConnection];
    
        // Query to fetch the emp name and images from the database
    NSString *fetchQuery = [NSString stringWithFormat:@"select * from Beacon order by Distance"];
    NSLog(@"fetch query:%@",fetchQuery);
    const char *selectQuery = [fetchQuery UTF8String];
    sqlite3_stmt *sqlstatement = nil;
    
    
    if (sqlite3_prepare_v2(databaseReference, selectQuery, -1, &sqlstatement , NULL)==SQLITE_OK) {
        
            // declaring a dictionary so that response can be saved in KVC
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        
        while (sqlite3_step(sqlstatement) == SQLITE_ROW)
            {
            char* major = (char*)sqlite3_column_text(sqlstatement, 0);
            NSString *majorstr  = [NSString stringWithUTF8String:major];
            [dict setValue:majorstr  forKey:@"BeaconMajor"];
            
            char* distance = (char*)sqlite3_column_text(sqlstatement, 1);
            NSString *distancestr  = [NSString stringWithUTF8String:distance];
            [dict setValue:distancestr  forKey:@"Distance"];
            
            char* locationid = (char*)sqlite3_column_text(sqlstatement, 2);
            NSString *locationidstr  = [NSString stringWithUTF8String:locationid];
            [dict setValue:locationidstr  forKey:@"LOCATION_ID"];
            
            char* state = (char*)sqlite3_column_text(sqlstatement, 3);
            NSString *statestr  = [NSString stringWithUTF8String:state];
            [dict setValue:statestr  forKey:@"StateName"];
            
            char* address = (char*)sqlite3_column_text(sqlstatement, 4);
            NSString *addressstr  = [NSString stringWithUTF8String:address];
            [dict setValue:addressstr  forKey:@"LOC_ADD"];
            
            char* longitude = (char*)sqlite3_column_text(sqlstatement, 5);
            NSString *longitudestr  = [NSString stringWithUTF8String:longitude];
            [dict setValue:longitudestr  forKey:@"Longitude"];
            
            char* zip = (char*)sqlite3_column_text(sqlstatement, 6);
            NSString *zipstr  = [NSString stringWithUTF8String:zip];
            [dict setValue:zipstr  forKey:@"Zip"];
            
            char* image = (char*)sqlite3_column_text(sqlstatement, 7);
            NSString *imagestr  = [NSString stringWithUTF8String:image];
            [dict setValue:imagestr  forKey:@"Image"];
            
            char* desc = (char*)sqlite3_column_text(sqlstatement, 8);
            NSString *descstr  = [NSString stringWithUTF8String:desc];
            [dict setValue:descstr  forKey:@"LOCATION_DESC"];
            
            char* country = (char*)sqlite3_column_text(sqlstatement, 9);
            NSString *countrystr  = [NSString stringWithUTF8String:country];
            [dict setValue:countrystr  forKey:@"Country"];
            
            char* latitude = (char*)sqlite3_column_text(sqlstatement, 10);
            NSString *latitudestr = [NSString stringWithUTF8String:latitude];
            [dict setValue:latitudestr forKey:@"Latitude"];
            
            char* orderby = (char*)sqlite3_column_text(sqlstatement, 11);
            NSString *orderbystr = [NSString stringWithUTF8String:orderby];
            [dict setValue:orderbystr forKey:@"OrderBy"];
            
            char* city = (char*)sqlite3_column_text(sqlstatement, 12);
            NSString *citystr = [NSString stringWithUTF8String:city];
            [dict setValue:citystr forKey:@"City"];
            
            char* salestax = (char*)sqlite3_column_text(sqlstatement, 13);
            NSString *salestaxstr = [NSString stringWithUTF8String:salestax];
            [dict setValue:salestaxstr forKey:@"SALESTAX"];
            
            char* company = (char*)sqlite3_column_text(sqlstatement, 14);
            NSString *companystr = [NSString stringWithUTF8String:company];
            [dict setValue:companystr forKey:@"COMPANYNAME"];
            
            char* bannerImage = (char*)sqlite3_column_text(sqlstatement, 15);
            NSString *bannerImageStr = [NSString stringWithUTF8String:bannerImage];
            [dict setValue:bannerImageStr forKey:@"BANNER_IMAGE"];
            
            char* bannerText = (char*)sqlite3_column_text(sqlstatement, 16);
            NSString *bannerTextStr = [NSString stringWithUTF8String:bannerText];
            [dict setValue:bannerTextStr forKey:@"BANNER_TEXT"];
            
            char* isFeatured = (char*)sqlite3_column_text(sqlstatement, 17);
            NSString *isFeaturedStr = [NSString stringWithUTF8String:isFeatured];
            [dict setValue:isFeaturedStr forKey:@"ISFEATURED"];
            
            char* categoryId = (char*)sqlite3_column_text(sqlstatement, 18);
            NSString *categoryIdStr = [NSString stringWithUTF8String:categoryId];
            [dict setValue:categoryIdStr forKey:@"CATEGORY_ID"];
                
            char* payrollStatus = (char*)sqlite3_column_text(sqlstatement, 19);
            NSString *payrollStatusStr = [NSString stringWithUTF8String:payrollStatus];
            [dict setValue:payrollStatusStr forKey:@"PayrollStatus"];
                
            char* payUserId = (char*)sqlite3_column_text(sqlstatement, 20);
            NSString *payUserIdStr = [NSString stringWithUTF8String:payUserId];
            [dict setValue:payUserIdStr forKey:@"PayUserID"];
                
            char* bestSelling = (char*)sqlite3_column_text(sqlstatement, 21);
            NSString *bestSellingStr = [NSString stringWithUTF8String:bestSelling];
            [dict setValue:bestSellingStr forKey:@"HasBestSelling"];
                
            char* creditStatus = (char*)sqlite3_column_text(sqlstatement, 22);
            NSString *creditStatusStr = [NSString stringWithUTF8String:creditStatus];
            [dict setValue:creditStatusStr forKey:@"CreditStatus"];
                
            char* CreditUserId = (char*)sqlite3_column_text(sqlstatement, 23);
            NSString *CreditUserIdStr = [NSString stringWithUTF8String:CreditUserId];
            [dict setValue:CreditUserIdStr forKey:@"CreditUserId"];
            
            
                // saving the record set in a MutableArray
            [recordSet addObject:dict];
            
            
                // clearing off the dictionary to make sure that it does not contain any garbage data
            dict = nil;
            
                // re- initalizing the dictionary for next record
            dict = [[NSMutableDictionary alloc]init];
            }
        sqlite3_finalize(sqlstatement);
        sqlite3_close(databaseReference);
            // once all the fetching is one clearing off the dict variable for good.
        dict = nil;
        
    }
    
    return  recordSet;
    
}
- (NSMutableArray *)getCartItemsByItemNo:(NSString *)MicroMarketID andItemNo:(NSString *)ItemNo {
    NSMutableArray *recordSet = [[NSMutableArray alloc]init];
    [self closeanyOpenConnection];
    
    // Query to fetch the emp name and images from the database
    NSString *fetchQuery = [NSString stringWithFormat:@"select * from VB_Cart where MicroMarketID='%@' and ItemNo='%@'",MicroMarketID,ItemNo];
    NSLog(@"fetch query:%@",fetchQuery);
    const char *selectQuery = [fetchQuery UTF8String];
    sqlite3_stmt *sqlstatement = nil;
    
    
    if (sqlite3_prepare_v2(databaseReference, selectQuery, -1, &sqlstatement , NULL)==SQLITE_OK) {
        
        // declaring a dictionary so that response can be saved in KVC
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        
        while (sqlite3_step(sqlstatement) == SQLITE_ROW)
        {
            char* MicroMarketID = (char*)sqlite3_column_text(sqlstatement, 0);
            NSString *MicroMarketIDstr  = [NSString stringWithUTF8String:MicroMarketID];
            [dict setValue:MicroMarketIDstr  forKey:@"MicroMarketID"];
            
            char* categoryid = (char*)sqlite3_column_text(sqlstatement, 1);
            NSString *categoryidstr  = [NSString stringWithUTF8String:categoryid];
            [dict setValue:categoryidstr  forKey:@"CategoryID"];
            
            char* desc = (char*)sqlite3_column_text(sqlstatement, 2);
            NSString *descstr  = [NSString stringWithUTF8String:desc];
            [dict setValue:descstr  forKey:@"ItemDescription"];
            
            char* itemno = (char*)sqlite3_column_text(sqlstatement, 3);
            NSString *itemnostr  = [NSString stringWithUTF8String:itemno];
            [dict setValue:itemnostr  forKey:@"ItemNo"];
            
            char* crvtax = (char*)sqlite3_column_text(sqlstatement, 4);
            NSString *crvtaxstr  = [NSString stringWithUTF8String:crvtax];
            [dict setValue:crvtaxstr  forKey:@"CRVTAX"];
            
            char* salestax = (char*)sqlite3_column_text(sqlstatement, 5);
            NSString *salestaxstr  = [NSString stringWithUTF8String:salestax];
            [dict setValue:salestaxstr  forKey:@"SalesTax"];
            
            char* discountprice = (char*)sqlite3_column_text(sqlstatement, 6);
            NSString *discountpricestr  = [NSString stringWithUTF8String:discountprice];
            [dict setValue:discountpricestr  forKey:@"DiscountPrice"];
            
            char* name = (char*)sqlite3_column_text(sqlstatement, 7);
            NSString *namestr  = [NSString stringWithUTF8String:name];
            [dict setValue:namestr  forKey:@"ItemName"];
            
            char* image = (char*)sqlite3_column_text(sqlstatement, 8);
            NSString *imagestr  = [NSString stringWithUTF8String:image];
            [dict setValue:imagestr  forKey:@"ItemImage"];
            
            char* price = (char*)sqlite3_column_text(sqlstatement, 9);
            NSString *pricestr  = [NSString stringWithUTF8String:price];
            NSString *count_float=[NSString stringWithFormat:@"%.2f", [pricestr doubleValue]];
            [dict setValue:count_float  forKey:@"ItemPrice"];
            
            char* stock = (char*)sqlite3_column_text(sqlstatement, 10);
            NSString *stockstr = [NSString stringWithUTF8String:stock];
            [dict setValue:stockstr forKey:@"Stock"];
            
            char* quantity = (char*)sqlite3_column_text(sqlstatement, 11);
            NSString *quantitystr = [NSString stringWithUTF8String:quantity];
            [dict setValue:quantitystr forKey:@"Quantity"];
            
            char* originalprice = (char*)sqlite3_column_text(sqlstatement, 12);
            NSString *originalpricestr = [NSString stringWithUTF8String:originalprice];
            [dict setValue:originalpricestr forKey:@"OriginalPrice"];
            
            char* crvpercent = (char*)sqlite3_column_text(sqlstatement, 13);
            NSString *crvpercentstr = [NSString stringWithUTF8String:crvpercent];
            [dict setValue:crvpercentstr forKey:@"CRVTAXPercent"];
            
            char* salespercent = (char*)sqlite3_column_text(sqlstatement, 14);
            NSString *salespercentstr = [NSString stringWithUTF8String:salespercent];
            [dict setValue:salespercentstr forKey:@"SalesTaxPercent"];
            
            char* crvenable = (char*)sqlite3_column_text(sqlstatement, 15);
            NSString *crvenablestr = [NSString stringWithUTF8String:crvenable];
            [dict setValue:crvenablestr forKey:@"CRVTAXEnable"];
            
            char* salesenable = (char*)sqlite3_column_text(sqlstatement, 16);
            NSString *salesenablestr = [NSString stringWithUTF8String:salesenable];
            [dict setValue:salesenablestr forKey:@"SalesTaxEnable"];
            
            char* discountpricediff = (char*)sqlite3_column_text(sqlstatement, 17);
            NSString *discountpricediffstr = [NSString stringWithUTF8String:discountpricediff];
            [dict setValue:discountpricediffstr forKey:@"DiscountPriceDiff"];
            
            char* discountenable = (char*)sqlite3_column_text(sqlstatement, 18);
            NSString *discountenablestr = [NSString stringWithUTF8String:discountenable];
            [dict setValue:discountenablestr forKey:@"DiscountPriceEnable"];
            
            char* upcCode = (char*)sqlite3_column_text(sqlstatement, 19);
            NSString *upcCodestr = [NSString stringWithUTF8String:upcCode];
            [dict setValue:upcCodestr forKey:@"Barcode"];
            
            // saving the record set in a MutableArray
            [recordSet addObject:dict];
            
            
            
            // clearing off the dictionary to make sure that it does not contain any garbage data
            dict = nil;
            
            // re- initalizing the dictionary for next record
            dict = [[NSMutableDictionary alloc]init];
        }
        sqlite3_finalize(sqlstatement);
        sqlite3_close(databaseReference);
        
        // once all the fetching is one clearing off the dict variable for good.
        dict = nil;
        
    }
    
    return  recordSet;
    
}
- (NSMutableArray *)getCartItems:(NSString *)MicroMarketID {
    NSMutableArray *recordSet = [[NSMutableArray alloc]init];
    [self closeanyOpenConnection];
    
        // Query to fetch the emp name and images from the database
    NSString *fetchQuery = [NSString stringWithFormat:@"select * from VB_Cart where MicroMarketID='%@'",MicroMarketID];
    NSLog(@"fetch query:%@",fetchQuery);
    const char *selectQuery = [fetchQuery UTF8String];
    sqlite3_stmt *sqlstatement = nil;
    
    
    if (sqlite3_prepare_v2(databaseReference, selectQuery, -1, &sqlstatement , NULL)==SQLITE_OK) {
        
            // declaring a dictionary so that response can be saved in KVC
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        
        while (sqlite3_step(sqlstatement) == SQLITE_ROW)
            {
            char* MicroMarketID = (char*)sqlite3_column_text(sqlstatement, 0);
            NSString *MicroMarketIDstr  = [NSString stringWithUTF8String:MicroMarketID];
            [dict setValue:MicroMarketIDstr  forKey:@"MicroMarketID"];
            
            char* categoryid = (char*)sqlite3_column_text(sqlstatement, 1);
            NSString *categoryidstr  = [NSString stringWithUTF8String:categoryid];
            [dict setValue:categoryidstr  forKey:@"CategoryID"];
            
            char* desc = (char*)sqlite3_column_text(sqlstatement, 2);
            NSString *descstr  = [NSString stringWithUTF8String:desc];
            [dict setValue:descstr  forKey:@"ItemDescription"];
            
            char* itemno = (char*)sqlite3_column_text(sqlstatement, 3);
            NSString *itemnostr  = [NSString stringWithUTF8String:itemno];
            [dict setValue:itemnostr  forKey:@"ItemNo"];
            
            char* crvtax = (char*)sqlite3_column_text(sqlstatement, 4);
            NSString *crvtaxstr  = [NSString stringWithUTF8String:crvtax];
            [dict setValue:crvtaxstr  forKey:@"CRVTAX"];
            
            char* salestax = (char*)sqlite3_column_text(sqlstatement, 5);
            NSString *salestaxstr  = [NSString stringWithUTF8String:salestax];
            [dict setValue:salestaxstr  forKey:@"SalesTax"];
            
            char* discountprice = (char*)sqlite3_column_text(sqlstatement, 6);
            NSString *discountpricestr  = [NSString stringWithUTF8String:discountprice];
            [dict setValue:discountpricestr  forKey:@"DiscountPrice"];
            
            char* name = (char*)sqlite3_column_text(sqlstatement, 7);
            NSString *namestr  = [NSString stringWithUTF8String:name];
            [dict setValue:namestr  forKey:@"ItemName"];
            
            char* image = (char*)sqlite3_column_text(sqlstatement, 8);
            NSString *imagestr  = [NSString stringWithUTF8String:image];
            [dict setValue:imagestr  forKey:@"ItemImage"];
            
            char* price = (char*)sqlite3_column_text(sqlstatement, 9);
            NSString *pricestr  = [NSString stringWithUTF8String:price];
            NSString *count_float=[NSString stringWithFormat:@"%.2f", [pricestr doubleValue]];
            [dict setValue:count_float  forKey:@"ItemPrice"];
            
            char* stock = (char*)sqlite3_column_text(sqlstatement, 10);
            NSString *stockstr = [NSString stringWithUTF8String:stock];
            [dict setValue:stockstr forKey:@"Stock"];
            
            char* quantity = (char*)sqlite3_column_text(sqlstatement, 11);
            NSString *quantitystr = [NSString stringWithUTF8String:quantity];
            [dict setValue:quantitystr forKey:@"Quantity"];
            
            char* originalprice = (char*)sqlite3_column_text(sqlstatement, 12);
            NSString *originalpricestr = [NSString stringWithUTF8String:originalprice];
            [dict setValue:originalpricestr forKey:@"OriginalPrice"];
            
            char* crvpercent = (char*)sqlite3_column_text(sqlstatement, 13);
            NSString *crvpercentstr = [NSString stringWithUTF8String:crvpercent];
            [dict setValue:crvpercentstr forKey:@"CRVTAXPercent"];
            
            char* salespercent = (char*)sqlite3_column_text(sqlstatement, 14);
            NSString *salespercentstr = [NSString stringWithUTF8String:salespercent];
            [dict setValue:salespercentstr forKey:@"SalesTaxPercent"];
            
            char* crvenable = (char*)sqlite3_column_text(sqlstatement, 15);
            NSString *crvenablestr = [NSString stringWithUTF8String:crvenable];
            [dict setValue:crvenablestr forKey:@"CRVTAXEnable"];
            
            char* salesenable = (char*)sqlite3_column_text(sqlstatement, 16);
            NSString *salesenablestr = [NSString stringWithUTF8String:salesenable];
            [dict setValue:salesenablestr forKey:@"SalesTaxEnable"];
                
            char* discountpricediff = (char*)sqlite3_column_text(sqlstatement, 17);
            NSString *discountpricediffstr = [NSString stringWithUTF8String:discountpricediff];
            [dict setValue:discountpricediffstr forKey:@"DiscountPriceDiff"];
                
            char* discountenable = (char*)sqlite3_column_text(sqlstatement, 18);
            NSString *discountenablestr = [NSString stringWithUTF8String:discountenable];
            [dict setValue:discountenablestr forKey:@"DiscountPriceEnable"];

            char* upcCode = (char*)sqlite3_column_text(sqlstatement, 19);
            NSString *upcCodestr = [NSString stringWithUTF8String:upcCode];
            [dict setValue:upcCodestr forKey:@"Barcode"];
            
                // saving the record set in a MutableArray
            [recordSet addObject:dict];
            
            
                // clearing off the dictionary to make sure that it does not contain any garbage data
            dict = nil;
            
                // re- initalizing the dictionary for next record
            dict = [[NSMutableDictionary alloc]init];
            }
        sqlite3_finalize(sqlstatement);
        sqlite3_close(databaseReference);
        
            // once all the fetching is one clearing off the dict variable for good.
        dict = nil;
        
    }
    
    return  recordSet;
    
}
-(BOOL)deleteBeacon
{
    BOOL flag = NO ;
    [self closeanyOpenConnection];
    NSString *sqliteQuery = [NSString stringWithFormat:@"Delete FROM Beacon"];
    
    const char *deleteQuery = [sqliteQuery UTF8String];
    sqlite3_stmt *sqlStatement = nil;
    if (sqlite3_prepare_v2(databaseReference, deleteQuery, -1, &sqlStatement, NULL) == SQLITE_OK)
        {
        sqlite3_step(sqlStatement);
        sqlite3_finalize(sqlStatement);
        sqlite3_close(databaseReference);
        flag = YES;
        NSLog(@"Beacon has been deleted successfully..!!");
        }
    else
        {
        flag = NO;
        }
    
    return flag;
    
}

-(BOOL)deleteOperatorBeacon
{
    BOOL flag = NO ;
    [self closeanyOpenConnection];
    NSString *sqliteQuery = [NSString stringWithFormat:@"Delete FROM BeaconOperator"];
    
    const char *deleteQuery = [sqliteQuery UTF8String];
    sqlite3_stmt *sqlStatement = nil;
    if (sqlite3_prepare_v2(databaseReference, deleteQuery, -1, &sqlStatement, NULL) == SQLITE_OK)
        {
        sqlite3_step(sqlStatement);
        sqlite3_finalize(sqlStatement);
        sqlite3_close(databaseReference);
        flag = YES;
        NSLog(@"Beacon has been deleted successfully..!!");
        }
    else
        {
        flag = NO;
        }
    
    return flag;
    
}
-(BOOL)deleteCart
{
    BOOL flag = NO ;
    [self closeanyOpenConnection];
    NSString *sqliteQuery = [NSString stringWithFormat:@"Delete FROM VB_Cart"];
    
    const char *deleteQuery = [sqliteQuery UTF8String];
    sqlite3_stmt *sqlStatement = nil;
    if (sqlite3_prepare_v2(databaseReference, deleteQuery, -1, &sqlStatement, NULL) == SQLITE_OK)
        {
        sqlite3_step(sqlStatement);
        sqlite3_finalize(sqlStatement);
        sqlite3_close(databaseReference);
        flag = YES;
        NSLog(@"VB_Cart has been deleted successfully..!!");
        }
    else
        {
        flag = NO;
        }
    
    return flag;
    
    
}

-(BOOL)deleteUserAnalytics
{
    BOOL flag = NO ;
    [self closeanyOpenConnection];
    NSString *sqliteQuery = [NSString stringWithFormat:@"Delete FROM analytics_user where sync_flag ='N'"];
    
    const char *deleteQuery = [sqliteQuery UTF8String];
    sqlite3_stmt *sqlStatement = nil;
    if (sqlite3_prepare_v2(databaseReference, deleteQuery, -1, &sqlStatement, NULL) == SQLITE_OK)
            {
        sqlite3_step(sqlStatement);
        sqlite3_finalize(sqlStatement);
        sqlite3_close(databaseReference);
        flag = YES;

            }
    else
            {
        flag = NO;
            }
    
    return flag;
    
}

-(BOOL)deleteProductAnalytics
{
    BOOL flag = NO ;
    [self closeanyOpenConnection];
    NSString *sqliteQuery = [NSString stringWithFormat:@"Delete FROM analytics_cartproduct where sync_flag ='N'"];
    
    const char *deleteQuery = [sqliteQuery UTF8String];
    sqlite3_stmt *sqlStatement = nil;
    if (sqlite3_prepare_v2(databaseReference, deleteQuery, -1, &sqlStatement, NULL) == SQLITE_OK)
            {
        sqlite3_step(sqlStatement);
        sqlite3_finalize(sqlStatement);
        sqlite3_close(databaseReference);
        flag = YES;
            }
    else
            {
        flag = NO;
            }
    
    return flag;
    
}

-(BOOL)deleteScreenAnalytics
{
    BOOL flag = NO ;
    [self closeanyOpenConnection];
    NSString *sqliteQuery = [NSString stringWithFormat:@"Delete FROM analytics_screen where sync_flag ='N'"];
    
    const char *deleteQuery = [sqliteQuery UTF8String];
    sqlite3_stmt *sqlStatement = nil;
    if (sqlite3_prepare_v2(databaseReference, deleteQuery, -1, &sqlStatement, NULL) == SQLITE_OK)
            {
        sqlite3_step(sqlStatement);
        sqlite3_finalize(sqlStatement);
        sqlite3_close(databaseReference);
        flag = YES;
            }
    else
            {
        flag = NO;
            }
    
    return flag;
    
}

-(BOOL)deleteFeedbackAnalytics{
    BOOL flag = NO ;
    [self closeanyOpenConnection];
    NSString *sqliteQuery = [NSString stringWithFormat:@"Delete FROM analytics_feedback"];
    
    const char *deleteQuery = [sqliteQuery UTF8String];
    sqlite3_stmt *sqlStatement = nil;
    if (sqlite3_prepare_v2(databaseReference, deleteQuery, -1, &sqlStatement, NULL) == SQLITE_OK)
    {
        sqlite3_step(sqlStatement);
        sqlite3_finalize(sqlStatement);
        sqlite3_close(databaseReference);
        flag = YES;
    }
    else
    {
        flag = NO;
    }
    
    return flag;
}

-(BOOL)deleteCartByMicroMarket:(NSString *)MicroMarketID
{
    BOOL flag = NO ;
    [self closeanyOpenConnection];
    NSString *sqliteQuery = [NSString stringWithFormat:@"Delete FROM VB_Cart where MicroMarketID='%@'",MicroMarketID];
    
    const char *deleteQuery = [sqliteQuery UTF8String];
    sqlite3_stmt *sqlStatement = nil;
    if (sqlite3_prepare_v2(databaseReference, deleteQuery, -1, &sqlStatement, NULL) == SQLITE_OK)
        {
        sqlite3_step(sqlStatement);
        sqlite3_finalize(sqlStatement);
        sqlite3_close(databaseReference);
        flag = YES;
        NSLog(@"VB_Cart has been deleted successfully..!!");
        }
    else
        {
        flag = NO;
        }
    
    return flag;
    
}
-(BOOL)deleteItem:(NSString *)ItemNo andMicroMarketID:(NSString *)MicroMarketID
{
    BOOL flag = NO ;
    
    NSString *sqliteQuery = [NSString stringWithFormat:@"Delete FROM VB_Cart where ItemNo='%@' and MicroMarketID='%@'",ItemNo,MicroMarketID];
    [self closeanyOpenConnection];
    const char *deleteQuery = [sqliteQuery UTF8String];
    sqlite3_stmt *sqlStatement = nil;
    if (sqlite3_prepare_v2(databaseReference, deleteQuery, -1, &sqlStatement, NULL) == SQLITE_OK)
        {
        sqlite3_step(sqlStatement);
        sqlite3_finalize(sqlStatement);
        sqlite3_close(databaseReference);
        flag = YES;
        NSLog(@"VB_Cart has been deleted successfully..!!");
        }
    else
        {
        flag = NO;
        }
    
    return flag;
    
}
- (BOOL)UpdateBeacon:(NSString *)Major andDistance:(NSString *)Distance
{
    [self closeanyOpenConnection];
    
    NSString *update_query = [NSString stringWithFormat:@"update Beacon SET Distance='%@' where BeaconMajor=%@",Distance,Major];
    NSLog(@"Update Query:%@",update_query);
    const char *sqliteQuery = [update_query UTF8String];
    
    
    sqlite3_stmt *sqlstatement = nil;
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
        {
        sqlite3_bind_text(sqlstatement, 1, [Major UTF8String],    -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 2, [Distance UTF8String], -1,  SQLITE_TRANSIENT);
            // executes the sql statement with the data you need to insert in the db
        sqlite3_step(sqlstatement);
        
            // clearing the sql statement
        sqlite3_finalize(sqlstatement);
            //closing the database after the query execution
        sqlite3_close(databaseReference);
        return YES;
        }
    
    return NO;
    
}

- (BOOL)UpdateOperatorBeacon:(NSString *)Major andDistance:(NSString *)Distance
{
    [self closeanyOpenConnection];
    
    NSString *update_query = [NSString stringWithFormat:@"update BeaconOperator SET Distance='%@' where BeaconValue=%@",Distance,Major];
    NSLog(@"Update Query:%@",update_query);
    const char *sqliteQuery = [update_query UTF8String];
    
    
    sqlite3_stmt *sqlstatement = nil;
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
        {
        sqlite3_bind_text(sqlstatement, 6, [Major UTF8String],    -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 11, [Distance UTF8String], -1,  SQLITE_TRANSIENT);
            // executes the sql statement with the data you need to insert in the db
        sqlite3_step(sqlstatement);
        
            // clearing the sql statement
        sqlite3_finalize(sqlstatement);
            //closing the database after the query execution
        sqlite3_close(databaseReference);
    
        return YES;
        }
    
    return NO;
    
}
-(BOOL)UpdateSalesTax:(NSString *)SalesTax andItemNo:(NSString *)ItemNo
{
    [self closeanyOpenConnection];
    
    NSString *update_query = [NSString stringWithFormat:@"update VB_Cart SET SalesTax='%@' where ItemNo='%@'",SalesTax,ItemNo];
    NSLog(@"Update Query:%@",update_query);
    const char *sqliteQuery = [update_query UTF8String];
    
    
    sqlite3_stmt *sqlstatement = nil;
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
        {
        sqlite3_bind_text(sqlstatement, 1, [SalesTax UTF8String],    -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 2, [ItemNo UTF8String], -1,  SQLITE_TRANSIENT);
            // executes the sql statement with the data you need to insert in the db
        sqlite3_step(sqlstatement);
        
            // clearing the sql statement
        sqlite3_finalize(sqlstatement);
            //closing the database after the query execution
        sqlite3_close(databaseReference);
        
        return YES;
        }
    
    return NO;
    
}
-(BOOL)UpdateCRVTax:(NSString *)CRVTax andItemNo:(NSString *)ItemNo
{
    [self closeanyOpenConnection];
    
    NSString *update_query = [NSString stringWithFormat:@"update VB_Cart SET CRVTAX='%@' where ItemNo='%@'",CRVTax,ItemNo];
    NSLog(@"Update Query:%@",update_query);
    const char *sqliteQuery = [update_query UTF8String];
    
    
    sqlite3_stmt *sqlstatement = nil;
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
        {
        sqlite3_bind_text(sqlstatement, 1, [CRVTax UTF8String],    -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 2, [ItemNo UTF8String], -1,  SQLITE_TRANSIENT);
            // executes the sql statement with the data you need to insert in the db
        sqlite3_step(sqlstatement);
        
            // clearing the sql statement
        sqlite3_finalize(sqlstatement);
            //closing the database after the query execution
        sqlite3_close(databaseReference);
        
        return YES;
        }
    
    return NO;
    
}
-(BOOL)UpdateCRVTaxPercent:(NSString *)CRVTaxPercent andItemNo:(NSString *)ItemNo
{
    [self closeanyOpenConnection];
    
    NSString *update_query = [NSString stringWithFormat:@"update VB_Cart SET CRVTAXPercent='%@' where ItemNo='%@'",CRVTaxPercent,ItemNo];
    NSLog(@"Update Query:%@",update_query);
    const char *sqliteQuery = [update_query UTF8String];
    
    
    sqlite3_stmt *sqlstatement = nil;
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
    {
        sqlite3_bind_text(sqlstatement, 1, [CRVTaxPercent UTF8String],    -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 2, [ItemNo UTF8String], -1,  SQLITE_TRANSIENT);
        // executes the sql statement with the data you need to insert in the db
        sqlite3_step(sqlstatement);
        
        // clearing the sql statement
        sqlite3_finalize(sqlstatement);
        //closing the database after the query execution
        sqlite3_close(databaseReference);
        
        return YES;
    }
    
    return NO;
    
}
-(BOOL)UpdateSalesTaxPercent:(NSString *)SalesTaxPercent andItemNo:(NSString *)ItemNo
{
    [self closeanyOpenConnection];
    
    NSString *update_query = [NSString stringWithFormat:@"update VB_Cart SET SalesTaxPercent='%@' where ItemNo='%@'",SalesTaxPercent,ItemNo];
    NSLog(@"Update Query:%@",update_query);
    const char *sqliteQuery = [update_query UTF8String];
    
    
    sqlite3_stmt *sqlstatement = nil;
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
    {
        sqlite3_bind_text(sqlstatement, 1, [SalesTaxPercent UTF8String],    -1,  SQLITE_TRANSIENT);
        sqlite3_bind_text(sqlstatement, 2, [ItemNo UTF8String], -1,  SQLITE_TRANSIENT);
        // executes the sql statement with the data you need to insert in the db
        sqlite3_step(sqlstatement);
        
        // clearing the sql statement
        sqlite3_finalize(sqlstatement);
        //closing the database after the query execution
        sqlite3_close(databaseReference);
        
        return YES;
    }
    
    return NO;
    
}
-(BOOL)ReduceQuantity:(NSString *)ItemNo andQuantity:(NSString *)Quantity andMicroMarketID:(NSString *)MicroMarketID
{
    [self closeanyOpenConnection];
    
    NSString *update_query = [NSString stringWithFormat:@"update  VB_Cart SET Quantity=Quantity-1 where ItemNo='%@' and MicroMarketID='%@'",ItemNo,MicroMarketID];
    
    NSLog(@"Update Query:%@",update_query);
    const char *sqliteQuery = [update_query UTF8String];
    
    
    sqlite3_stmt *sqlstatement = nil;
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
        {
            //sqlite3_bind_text(sqlstatement, 1, [Quantity UTF8String],    -1,  SQLITE_TRANSIENT);
            // executes the sql statement with the data you need to insert in the db
        
        if(sqlite3_step(sqlstatement)== SQLITE_DONE)
            {
            NSLog(@"Values Updated");
            }
        
        
            //clearing the sql statement
        sqlite3_finalize(sqlstatement);
            //closing the database after the query execution
        sqlite3_close(databaseReference);
        
        return YES;
        }
    
    return NO;
    
}

-(BOOL)ReducePrice:(NSString *)ItemNo andPrice:(NSString *)Price andMicroMarketID:(NSString *)MicroMarketID

{
    [self closeanyOpenConnection];
    
    NSString *update_query = [NSString stringWithFormat:@"update  VB_Cart SET ItemPrice=ItemPrice-%@ where ItemNo='%@' and MicroMarketID='%@'",Price,ItemNo,MicroMarketID];
    
    NSLog(@"Update Query:%@",update_query);
    const char *sqliteQuery = [update_query UTF8String];
    
    
    sqlite3_stmt *sqlstatement = nil;
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
        {
        sqlite3_bind_text(sqlstatement, 1, [Price UTF8String],    -1,  SQLITE_TRANSIENT);
            // executes the sql statement with the data you need to insert in the db
        
        if(sqlite3_step(sqlstatement)== SQLITE_DONE)
            {
            NSLog(@"Values Updated");
            }
        
        
            //clearing the sql statement
        sqlite3_finalize(sqlstatement);
            //closing the database after the query execution
        sqlite3_close(databaseReference);
        
        return YES;
        }
    
    return NO;
    
}
-(BOOL)ReduceDiscountDiff:(NSString *)ItemNo andPrice:(NSString *)Price andMicroMarketID:(NSString *)MicroMarketID andDiscountPriceDiff:(NSString *)DiscountPriceDiff

{
    [self closeanyOpenConnection];
    
    NSString *update_query = [NSString stringWithFormat:@"update  VB_Cart SET DiscountPriceDiff = %@ - %@ where ItemNo='%@' and MicroMarketID='%@'",DiscountPriceDiff,Price,ItemNo,MicroMarketID];
    
    NSLog(@"Update Query:%@",update_query);
    const char *sqliteQuery = [update_query UTF8String];
    
    
    sqlite3_stmt *sqlstatement = nil;
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
    {
        sqlite3_bind_text(sqlstatement, 1, [Price UTF8String],    -1,  SQLITE_TRANSIENT);
        // executes the sql statement with the data you need to insert in the db
        
        if(sqlite3_step(sqlstatement)== SQLITE_DONE)
        {
            NSLog(@"Values Updated");
        }
        
        
        //clearing the sql statement
        sqlite3_finalize(sqlstatement);
        //closing the database after the query execution
        sqlite3_close(databaseReference);
        
        return YES;
    }
    
    return NO;
    
}
-(BOOL)UpdateQuantity:(NSString *)ItemNo andQuantity:(NSString *)Quantity andMicroMarketID:(NSString *)MicroMarketID
{
    
    [self closeanyOpenConnection];
    
    NSString *update_query = [NSString stringWithFormat:@"update  VB_Cart SET Quantity=Quantity+1 where ItemNo='%@' and MicroMarketID='%@'",ItemNo,MicroMarketID];
    
    NSLog(@"Update Query:%@",update_query);
    const char *sqliteQuery = [update_query UTF8String];
    
    
    sqlite3_stmt *sqlstatement = nil;
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
        {
            //sqlite3_bind_text(sqlstatement, 1, [Quantity UTF8String],    -1,  SQLITE_TRANSIENT);
            // executes the sql statement with the data you need to insert in the db
        
        if(sqlite3_step(sqlstatement)== SQLITE_DONE)
            {
            NSLog(@"Values Updated");
            }
        
        
            //clearing the sql statement
        sqlite3_finalize(sqlstatement);
            //closing the database after the query execution
        sqlite3_close(databaseReference);
        
        return YES;
        }
    
    return NO;
    
}

-(BOOL)UpdateNewPrice:(NSString *)ItemNo andPrice:(NSString *)Price andMicroMarketID:(NSString *)MicroMarketID{
    [self closeanyOpenConnection];
    
    NSString *update_query = [NSString stringWithFormat:@"update  VB_Cart SET ItemPrice= '%@' where ItemNo='%@' and MicroMarketID='%@'",Price,ItemNo,MicroMarketID];
    
    NSLog(@"Update Query:%@",update_query);
    const char *sqliteQuery = [update_query UTF8String];
    
    
    sqlite3_stmt *sqlstatement = nil;
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
    {
        sqlite3_bind_text(sqlstatement, 1, [Price UTF8String],    -1,  SQLITE_TRANSIENT);
        // executes the sql statement with the data you need to insert in the db
        
        if(sqlite3_step(sqlstatement)== SQLITE_DONE)
        {
            NSLog(@"Values Updated");
        }
        
        
        //clearing the sql statement
        sqlite3_finalize(sqlstatement);
        //closing the database after the query execution
        sqlite3_close(databaseReference);
        
        return YES;
    }
    
    return NO;
}
-(BOOL)UpdatePrice:(NSString *)ItemNo andPrice:(NSString *)Price andMicroMarketID:(NSString *)MicroMarketID
{
    [self closeanyOpenConnection];
    
    NSString *update_query = [NSString stringWithFormat:@"update  VB_Cart SET ItemPrice=ItemPrice+%@ where ItemNo='%@' and MicroMarketID='%@'",Price,ItemNo,MicroMarketID];
    
    NSLog(@"Update Query:%@",update_query);
    const char *sqliteQuery = [update_query UTF8String];
    
    
    sqlite3_stmt *sqlstatement = nil;
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
        {
        sqlite3_bind_text(sqlstatement, 1, [Price UTF8String],    -1,  SQLITE_TRANSIENT);
            // executes the sql statement with the data you need to insert in the db
        
        if(sqlite3_step(sqlstatement)== SQLITE_DONE)
            {
            NSLog(@"Values Updated");
            }
        
        
            //clearing the sql statement
        sqlite3_finalize(sqlstatement);
            //closing the database after the query execution
        sqlite3_close(databaseReference);
        
        return YES;
        }
    
    return NO;
    
}

-(BOOL)UpdateOriginalPrice:(NSString *)ItemNo andPrice:(NSString *)Price andMicroMarketID:(NSString *)MicroMarketID
{
    [self closeanyOpenConnection];
    
    NSString *update_query = [NSString stringWithFormat:@"update  VB_Cart SET OriginalPrice = '%@' where ItemNo='%@' and MicroMarketID='%@'",Price,ItemNo,MicroMarketID];
    
    NSLog(@"Update Query:%@",update_query);
    const char *sqliteQuery = [update_query UTF8String];
    
    
    sqlite3_stmt *sqlstatement = nil;
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
    {
        sqlite3_bind_text(sqlstatement, 1, [Price UTF8String],    -1,  SQLITE_TRANSIENT);
        // executes the sql statement with the data you need to insert in the db
        
        if(sqlite3_step(sqlstatement)== SQLITE_DONE)
        {
            NSLog(@"Values Updated");
        }
        
        
        //clearing the sql statement
        sqlite3_finalize(sqlstatement);
        //closing the database after the query execution
        sqlite3_close(databaseReference);
        
        return YES;
    }
    
    return NO;
    
}

-(BOOL)UpdateDiscountPrice:(NSString *)ItemNo andPrice:(NSString *)Price andMicroMarketID:(NSString *)MicroMarketID
{
    [self closeanyOpenConnection];
    
    NSString *update_query = [NSString stringWithFormat:@"update  VB_Cart SET DiscountPrice = '%@' where ItemNo='%@' and MicroMarketID='%@'",Price,ItemNo,MicroMarketID];
    
    NSLog(@"Update Query:%@",update_query);
    const char *sqliteQuery = [update_query UTF8String];
    
    
    sqlite3_stmt *sqlstatement = nil;
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
    {
        sqlite3_bind_text(sqlstatement, 1, [Price UTF8String],    -1,  SQLITE_TRANSIENT);
        // executes the sql statement with the data you need to insert in the db
        
        if(sqlite3_step(sqlstatement)== SQLITE_DONE)
        {
            NSLog(@"Values Updated");
        }
        
        
        //clearing the sql statement
        sqlite3_finalize(sqlstatement);
        //closing the database after the query execution
        sqlite3_close(databaseReference);
        
        return YES;
    }
    
    return NO;
    
}


-(BOOL)UpdateScreenAnalytics:(NSString *)syncFlag
{
    [self closeanyOpenConnection];
    
    NSString *update_query = [NSString stringWithFormat:@"update  analytics_screen SET sync_flag = '%@'",syncFlag];
    
    NSLog(@"Update Query:%@",update_query);
    const char *sqliteQuery = [update_query UTF8String];
    
    
    sqlite3_stmt *sqlstatement = nil;
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
            {
        sqlite3_bind_text(sqlstatement, 1, [syncFlag UTF8String],    -1,  SQLITE_TRANSIENT);
        // executes the sql statement with the data you need to insert in the db
        
        if(sqlite3_step(sqlstatement)== SQLITE_DONE)
                {
            NSLog(@"Values Updated");
                }
        
        
        //clearing the sql statement
        sqlite3_finalize(sqlstatement);
        //closing the database after the query execution
        sqlite3_close(databaseReference);
        
        return YES;
            }
    
    return NO;
    
}

-(BOOL)UpdateUserAnalytics:(NSString *)syncFlag
{
    [self closeanyOpenConnection];
    
    NSString *update_query = [NSString stringWithFormat:@"update  analytics_user SET sync_flag = '%@'",syncFlag];
    
    NSLog(@"Update Query:%@",update_query);
    const char *sqliteQuery = [update_query UTF8String];
    
    
    sqlite3_stmt *sqlstatement = nil;
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
            {
        sqlite3_bind_text(sqlstatement, 1, [syncFlag UTF8String],    -1,  SQLITE_TRANSIENT);
        // executes the sql statement with the data you need to insert in the db
        
        if(sqlite3_step(sqlstatement)== SQLITE_DONE)
                {
            NSLog(@"Values Updated");
                }
        
        
        //clearing the sql statement
        sqlite3_finalize(sqlstatement);
        //closing the database after the query execution
        sqlite3_close(databaseReference);
        
        return YES;
            }
    
    return NO;
    
}


-(BOOL)UpdateCartProductAnalytics:(NSString *)syncFlag
{
    [self closeanyOpenConnection];
    
    NSString *update_query = [NSString stringWithFormat:@"update  analytics_cartproduct SET sync_flag = '%@'",syncFlag];
    
    NSLog(@"Update Query:%@",update_query);
    const char *sqliteQuery = [update_query UTF8String];
    
    
    sqlite3_stmt *sqlstatement = nil;
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
            {
        sqlite3_bind_text(sqlstatement, 1, [syncFlag UTF8String],    -1,  SQLITE_TRANSIENT);
        // executes the sql statement with the data you need to insert in the db
        
        if(sqlite3_step(sqlstatement)== SQLITE_DONE)
                {
            NSLog(@"Values Updated");
                }
        
        
        //clearing the sql statement
        sqlite3_finalize(sqlstatement);
        //closing the database after the query execution
        sqlite3_close(databaseReference);
        
        return YES;
            }
    
    return NO;
    
}

-(BOOL)UpdateDiscountPriceDiff:(NSString *)ItemNo andPrice:(NSString *)Price andMicroMarketID:(NSString *)MicroMarketID andDiscountPriceDiff:(NSString *)DiscountPriceDiff
{
    [self closeanyOpenConnection];
    
    NSString *update_query = [NSString stringWithFormat:@"update  VB_Cart SET DiscountPriceDiff = %@ + %@ where ItemNo='%@' and MicroMarketID='%@'",DiscountPriceDiff , Price,ItemNo,MicroMarketID];
    
    NSLog(@"Update Query:%@",update_query);
    const char *sqliteQuery = [update_query UTF8String];
    
    
    sqlite3_stmt *sqlstatement = nil;
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
    {
        sqlite3_bind_text(sqlstatement, 1, [Price UTF8String],    -1,  SQLITE_TRANSIENT);
        // executes the sql statement with the data you need to insert in the db
        
        if(sqlite3_step(sqlstatement)== SQLITE_DONE)
        {
            NSLog(@"Values Updated");
        }
        
        
        //clearing the sql statement
        sqlite3_finalize(sqlstatement);
        //closing the database after the query execution
        sqlite3_close(databaseReference);
        
        return YES;
    }
    
    return NO;
    
}

-(NSString *)getBeaconCount
{
    NSString *count;
    [self closeanyOpenConnection];
    NSString *update_query = [NSString stringWithFormat:@"select Count(*) as Total from Beacon"];
    NSLog(@"Update Query:%@",update_query);
    const char *sqliteQuery = [update_query UTF8String];
    
    
    sqlite3_stmt *sqlstatement = nil;
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
        {
        while (sqlite3_step(sqlstatement) == SQLITE_ROW)
            {
            char *one = (char *) sqlite3_column_text(sqlstatement,0);
            NSLog(@"one %s",one);
            if(one!=nil)
                {
                count = [[NSString alloc] initWithUTF8String:one];
                sqlite3_finalize(sqlstatement);
                    //closing the database after the query execution
                sqlite3_close(databaseReference);
                return count;
                }
            else{
                sqlite3_finalize(sqlstatement);
                sqlite3_close(databaseReference);
                return nil;
            }
            }
        
        
            //return nil;
        }
    else{
        
        
    }
    
    return nil;
    
    
}
-(NSString *)getDiscountPriceDiff:(NSString *)ItemNo
{
    NSString *count;
    NSString *count_float;
    [self closeanyOpenConnection];
    NSString *update_query = [NSString stringWithFormat:@"Select DiscountPriceDiff from VB_Cart where ItemNo='%@'",ItemNo];
    NSLog(@"Update Query:%@",update_query);
    const char *sqliteQuery = [update_query UTF8String];
    
    
    sqlite3_stmt *sqlstatement = nil;
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
    {
        while (sqlite3_step(sqlstatement) == SQLITE_ROW)
        {
            char *one = (char *) sqlite3_column_text(sqlstatement,0);
            NSLog(@"one %s",one);
            if(one!=nil)
            {
                count = [[NSString alloc] initWithUTF8String:one];
                count_float=[NSString stringWithFormat:@"%f", [count doubleValue]];
                sqlite3_finalize(sqlstatement);
                //closing the database after the query execution
                sqlite3_close(databaseReference);
                return count_float;
            }
            else{
                sqlite3_finalize(sqlstatement);
                sqlite3_close(databaseReference);
                return nil;
            }
        }
        
        
        //return nil;
    }
    else{
        
        
    }
    
    return nil;
    
}
-(NSString *)getSalesTax:(NSString *)ItemNo
{
    NSString *count;
    NSString *count_float;
    [self closeanyOpenConnection];
    NSString *update_query = [NSString stringWithFormat:@"Select SalesTax from VB_Cart where ItemNo='%@'",ItemNo];
    NSLog(@"Update Query:%@",update_query);
    const char *sqliteQuery = [update_query UTF8String];
    
    
    sqlite3_stmt *sqlstatement = nil;
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
        {
        while (sqlite3_step(sqlstatement) == SQLITE_ROW)
            {
            char *one = (char *) sqlite3_column_text(sqlstatement,0);
            NSLog(@"one %s",one);
            if(one!=nil)
                {
                count = [[NSString alloc] initWithUTF8String:one];
                count_float=[NSString stringWithFormat:@"%f", [count doubleValue]];
                sqlite3_finalize(sqlstatement);
                    //closing the database after the query execution
                sqlite3_close(databaseReference);
                return count_float;
                }
            else{
                sqlite3_finalize(sqlstatement);
                sqlite3_close(databaseReference);
                return nil;
            }
            }
        
        
            //return nil;
        }
    else{
        
        
    }
    
    return nil;
    
}

-(NSString *)getCRVTax:(NSString *)ItemNo
{
    NSString *count;
    NSString *count_float;
    [self closeanyOpenConnection];
    NSString *update_query = [NSString stringWithFormat:@"Select CRVTAX from VB_Cart where ItemNo='%@'",ItemNo];
    NSLog(@"Update Query:%@",update_query);
    const char *sqliteQuery = [update_query UTF8String];
    
    
    sqlite3_stmt *sqlstatement = nil;
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
        {
        while (sqlite3_step(sqlstatement) == SQLITE_ROW)
            {
            char *one = (char *) sqlite3_column_text(sqlstatement,0);
            NSLog(@"one %s",one);
            if(one!=nil)
                {
                count = [[NSString alloc] initWithUTF8String:one];
                count_float=[NSString stringWithFormat:@"%.2f", [count doubleValue]];
                sqlite3_finalize(sqlstatement);
                    //closing the database after the query execution
                sqlite3_close(databaseReference);
                return count_float;
                }
            else{
                sqlite3_finalize(sqlstatement);
                sqlite3_close(databaseReference);
                return nil;
            }
            }
        
        
            //return nil;
        }
    else{
        
        
    }
    
    return nil;
    
}
-(NSString *)getTotalItems:(NSString *)MicroMarketID
{
    NSString *count;
    NSString *count_float;
    [self closeanyOpenConnection];
    NSString *update_query = [NSString stringWithFormat:@"Select SUM(Quantity) as total from VB_Cart where MicroMarketID='%@'",MicroMarketID];
    NSLog(@"Update Query:%@",update_query);
    const char *sqliteQuery = [update_query UTF8String];
    
    
    sqlite3_stmt *sqlstatement = nil;
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
        {
        while (sqlite3_step(sqlstatement) == SQLITE_ROW)
            {
            char *one = (char *) sqlite3_column_text(sqlstatement,0);
            NSLog(@"one %s",one);
            if(one!=nil)
                {
                count = [[NSString alloc] initWithUTF8String:one];
                count_float=[NSString stringWithFormat:@"%@",count];
                sqlite3_finalize(sqlstatement);
                    //closing the database after the query execution
                sqlite3_close(databaseReference);
                return count_float;
                }
            else{
                sqlite3_finalize(sqlstatement);
                sqlite3_close(databaseReference);
                return nil;
            }
            }
        
        
            //return nil;
        }
    else{
        
        
    }
    
    return nil;
    
}
-(NSString *)getcount:(NSString *)MicroMarketID
{
    NSString *count;
    NSString *count_float;
    [self closeanyOpenConnection];
    NSString *update_query = [NSString stringWithFormat:@"Select SUM(ItemPrice) as total from VB_Cart where MicroMarketID='%@'",MicroMarketID];
    NSLog(@"Update Query:%@",update_query);
    const char *sqliteQuery = [update_query UTF8String];
    
    
    sqlite3_stmt *sqlstatement = nil;
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
        {
        while (sqlite3_step(sqlstatement) == SQLITE_ROW)
            {
            char *one = (char *) sqlite3_column_text(sqlstatement,0);
            NSLog(@"one %s",one);
            if(one!=nil)
                {
                count = [[NSString alloc] initWithUTF8String:one];
                count_float=[NSString stringWithFormat:@"%.2f", [count doubleValue]];
                sqlite3_finalize(sqlstatement);
                    //closing the database after the query execution
                sqlite3_close(databaseReference);
                return count_float;
                }
            else{
                sqlite3_finalize(sqlstatement);
                sqlite3_close(databaseReference);
                return nil;
            }
            }
        
        
            //return nil;
        }
    else{
        
        
    }
    
    return nil;
    
}

-(NSString *)getDiscountTotal:(NSString *)MicroMarketID
{
    NSString *count;
    NSString *count_float;
    [self closeanyOpenConnection];
    NSString *update_query = [NSString stringWithFormat:@"Select SUM(DiscountPriceDiff) as total from VB_Cart where MicroMarketID='%@'",MicroMarketID];
    NSLog(@"Update Query:%@",update_query);
    const char *sqliteQuery = [update_query UTF8String];
    
    
    sqlite3_stmt *sqlstatement = nil;
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
    {
        while (sqlite3_step(sqlstatement) == SQLITE_ROW)
        {
            char *one = (char *) sqlite3_column_text(sqlstatement,0);
            NSLog(@"one %s",one);
            if(one!=nil)
            {
                count = [[NSString alloc] initWithUTF8String:one];
                count_float=[NSString stringWithFormat:@"%.2f", [count doubleValue]];
                sqlite3_finalize(sqlstatement);
                //closing the database after the query execution
                sqlite3_close(databaseReference);
                return count_float;
            }
            else{
                sqlite3_finalize(sqlstatement);
                sqlite3_close(databaseReference);
                return nil;
            }
        }
        
        
        //return nil;
    }
    else{
        
        
    }
    
    return nil;
    
}

-(NSString *)getSalesTaxTotalItem:(NSString *)MicroMarketID andItemNo:(NSString *)ItemNo andQuantity:(NSString *)Quantity{
    NSString *count;
    NSString *count_float;
    [self closeanyOpenConnection];
    NSString *update_query = [NSString stringWithFormat:@"Select SalesTax from VB_Cart where ItemNo='%@' and MicroMarketID='%@'",ItemNo,MicroMarketID];
    NSLog(@"Update Query:%@",update_query);
    const char *sqliteQuery = [update_query UTF8String];
    
    
    sqlite3_stmt *sqlstatement = nil;
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
    {
        while (sqlite3_step(sqlstatement) == SQLITE_ROW)
        {
            char *one = (char *) sqlite3_column_text(sqlstatement,0);
            NSLog(@"one %s",one);
            if(one!=nil)
            {
                count = [[NSString alloc] initWithUTF8String:one];
                count_float=[NSString stringWithFormat:@"%f", [count doubleValue]];
                sqlite3_finalize(sqlstatement);
                //closing the database after the query execution
                sqlite3_close(databaseReference);
                return count_float;
            }
            else{
                sqlite3_finalize(sqlstatement);
                sqlite3_close(databaseReference);
                return nil;
            }
        }
        
        
        //return nil;
    }
    else{
        
        
    }
    
    return nil;
}

-(NSString *)getSalesTaxTotal:(NSString *)MicroMarketID
{
    NSString *count;
    NSString *count_float;
    [self closeanyOpenConnection];
    NSString *update_query = [NSString stringWithFormat:@"Select SUM(SalesTax) as total from VB_Cart where MicroMarketID='%@'",MicroMarketID];
    NSLog(@"Update Query:%@",update_query);
    const char *sqliteQuery = [update_query UTF8String];
    
    
    sqlite3_stmt *sqlstatement = nil;
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
        {
        while (sqlite3_step(sqlstatement) == SQLITE_ROW)
            {
            char *one = (char *) sqlite3_column_text(sqlstatement,0);
            NSLog(@"one %s",one);
            if(one!=nil)
                {
                count = [[NSString alloc] initWithUTF8String:one];
                count_float=[NSString stringWithFormat:@"%f", [count doubleValue]];
                sqlite3_finalize(sqlstatement);
                    //closing the database after the query execution
                sqlite3_close(databaseReference);
                return count_float;
                }
            else{
                sqlite3_finalize(sqlstatement);
                sqlite3_close(databaseReference);
                return nil;
            }
            
            }
        
        
            //return nil;
        }
    else{
        
    }
    
    return nil;
    
}

-(NSString *)getCRVTaxTotal:(NSString *)MicroMarketID
{
    NSString *count;
    NSString *count_float;
    [self closeanyOpenConnection];
    NSString *update_query = [NSString stringWithFormat:@"Select SUM(CRVTax) as total from VB_Cart where MicroMarketID='%@'",MicroMarketID];
    NSLog(@"Update Query:%@",update_query);
    const char *sqliteQuery = [update_query UTF8String];
    
    
    sqlite3_stmt *sqlstatement = nil;
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
        {
        while (sqlite3_step(sqlstatement) == SQLITE_ROW)
            {
            char *one = (char *) sqlite3_column_text(sqlstatement,0);
            NSLog(@"one %s",one);
            if(one!=nil)
                {
                count = [[NSString alloc] initWithUTF8String:one];
                count_float=[NSString stringWithFormat:@"%.2f", [count doubleValue]];
                sqlite3_finalize(sqlstatement);
                    //closing the database after the query execution
                sqlite3_close(databaseReference);
                return count_float;
                }
            else{
                sqlite3_finalize(sqlstatement);
                sqlite3_close(databaseReference);
                return nil;
            }
            }
        
        
            //return nil;
        }
    else{
        
        
    }
    
    return nil;
    
}

-(BOOL)CheckItem:(NSString *)ItemNo andMicroMarketID:(NSString *)MicroMarketID
{
    [self closeanyOpenConnection];
    
    NSString *update_query = [NSString stringWithFormat:@"Select * from VB_Cart where ItemNo='%@' and MicroMarketID='%@'",ItemNo,MicroMarketID];
    NSLog(@"Update Query:%@",update_query);
    const char *sqliteQuery = [update_query UTF8String];
    
    
    sqlite3_stmt *sqlstatement = nil;
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
        {
        
        while (sqlite3_step(sqlstatement) == SQLITE_ROW)
            {
            sqlite3_finalize(sqlstatement);
                //closing the database after the query execution
            sqlite3_close(databaseReference);
            return YES;
            break;
            }
        
        
        return NO;
        }
    
    return NO;
    
}

-(NSString *)getUserAnalyticsBySessionID:(NSString *)sessionID
{
    NSString *count;
    [self closeanyOpenConnection];
    NSString *update_query = [NSString stringWithFormat:@"select Count(*) as Total from analytics_user where session_guid='%@'",sessionID];
    NSLog(@"Update Query:%@",update_query);
    const char *sqliteQuery = [update_query UTF8String];
    
    
    sqlite3_stmt *sqlstatement = nil;
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
            {
        while (sqlite3_step(sqlstatement) == SQLITE_ROW)
                {
            char *one = (char *) sqlite3_column_text(sqlstatement,0);
            NSLog(@"one %s",one);
            if(one!=nil)
                    {
                count = [[NSString alloc] initWithUTF8String:one];
                sqlite3_finalize(sqlstatement);
                //closing the database after the query execution
                sqlite3_close(databaseReference);
                return count;
                    }
            else{
                sqlite3_finalize(sqlstatement);
                sqlite3_close(databaseReference);
                return nil;
            }
                }
        
        
        //return nil;
            }
    else{
        
        
    }
    
    return nil;
    
    
}

-(NSString *)getScreenAnalyticsBySessionID:(NSString *)sessionID andScreenName:(NSString *)screenName
{
    NSString *count;
    [self closeanyOpenConnection];
    NSString *update_query = [NSString stringWithFormat:@"select screen_visits from analytics_screen where session_guid='%@' and screen_name='%@'",sessionID,screenName];
    NSLog(@"Update Query:%@",update_query);
    const char *sqliteQuery = [update_query UTF8String];
    
    
    sqlite3_stmt *sqlstatement = nil;
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
            {
        while (sqlite3_step(sqlstatement) == SQLITE_ROW)
                {
            char *one = (char *) sqlite3_column_text(sqlstatement,0);
            NSLog(@"one %s",one);
            if(one!=nil)
                    {
                count = [[NSString alloc] initWithUTF8String:one];
                sqlite3_finalize(sqlstatement);
                //closing the database after the query execution
                sqlite3_close(databaseReference);
                return count;
                    }
            else{
                sqlite3_finalize(sqlstatement);
                sqlite3_close(databaseReference);
                return nil;
            }
                }
        
        
        //return nil;
            }
    else{
        
        
    }
    
    return nil;
    
    
}


-(NSString *)getProductAnalyticsBySessionID:(NSString *)sessionID andProductID:(NSString *)productID andType:(NSString *)type
{
    NSString *count;
    [self closeanyOpenConnection];
    NSString *update_query = [NSString stringWithFormat:@"select item_qty from analytics_cartproduct where session_guid='%@' and product_id='%@' and type='%@'",sessionID,productID,type];
    NSLog(@"Update Query:%@",update_query);
    const char *sqliteQuery = [update_query UTF8String];
    
    
    sqlite3_stmt *sqlstatement = nil;
    if (sqlite3_prepare_v2(databaseReference, sqliteQuery, -1, &sqlstatement, NULL)==SQLITE_OK )
            {
        while (sqlite3_step(sqlstatement) == SQLITE_ROW)
                {
            char *one = (char *) sqlite3_column_text(sqlstatement,0);
            NSLog(@"one %s",one);
            if(one!=nil)
                    {
                count = [[NSString alloc] initWithUTF8String:one];
                sqlite3_finalize(sqlstatement);
                //closing the database after the query execution
                sqlite3_close(databaseReference);
                return count;
                    }
            else{
                sqlite3_finalize(sqlstatement);
                sqlite3_close(databaseReference);
                return nil;
            }
                }
        
        
        //return nil;
            }
    else{
        
        
    }
    
    return nil;
    
}

-(NSMutableArray *)getProductAnalyticsByProductID:(NSString *)productID andType:(NSString *)type
{
    [self closeanyOpenConnection];
    NSString *update_query = [NSString stringWithFormat:@"select item_qty,session_guid from analytics_cartproduct where product_id='%@' and type='%@' ORDER BY session_guid ASC LIMIT 1",productID,type];
    NSMutableArray *recordSet = [[NSMutableArray alloc]init];
    NSLog(@"Update Query:%@",update_query);
    const char *selectQuery = [update_query UTF8String];
    sqlite3_stmt *sqlstatement = nil;
    
    if (sqlite3_prepare_v2(databaseReference, selectQuery, -1, &sqlstatement , NULL)==SQLITE_OK) {
        
        // declaring a dictionary so that response can be saved in KVC
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        
        while (sqlite3_step(sqlstatement) == SQLITE_ROW)
                {
            char* MicroMarketID = (char*)sqlite3_column_text(sqlstatement, 0);
            NSString *MicroMarketIDstr  = [NSString stringWithUTF8String:MicroMarketID];
            [dict setValue:MicroMarketIDstr  forKey:@"item_qty"];
            
            char* categoryid = (char*)sqlite3_column_text(sqlstatement, 1);
            NSString *categoryidstr  = [NSString stringWithUTF8String:categoryid];
            [dict setValue:categoryidstr  forKey:@"session_guid"];
            
            // saving the record set in a MutableArray
            [recordSet addObject:dict];

            // clearing off the dictionary to make sure that it does not contain any garbage data
            dict = nil;
            
            // re- initalizing the dictionary for next record
            dict = [[NSMutableDictionary alloc]init];
                }
        sqlite3_finalize(sqlstatement);
        sqlite3_close(databaseReference);
        
        // once all the fetching is one clearing off the dict variable for good.
        dict = nil;
        
    }
    
    return  recordSet;
    
}

-(BOOL)deleteSessionID:(NSString *)sessionID
{
    BOOL flag = NO ;
    [self closeanyOpenConnection];
    NSString *sqliteQuery = [NSString stringWithFormat:@"Delete FROM analytics_user where session_guid='%@'",sessionID];
    
    const char *deleteQuery = [sqliteQuery UTF8String];
    sqlite3_stmt *sqlStatement = nil;
    if (sqlite3_prepare_v2(databaseReference, deleteQuery, -1, &sqlStatement, NULL) == SQLITE_OK)
            {
        sqlite3_step(sqlStatement);
        sqlite3_finalize(sqlStatement);
        sqlite3_close(databaseReference);
        flag = YES;
            }
    else
            {
        flag = NO;
            }
    
    return flag;
    
}



@end
