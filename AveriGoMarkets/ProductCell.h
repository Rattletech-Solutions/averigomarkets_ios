//
//  ProductCell.h
//  AveriGo Markets
//
//  Created by LeniYadav on 31/10/18.
//  Copyright © 2018 BTS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <HCSStarRatingView/HCSStarRatingView.h>

@interface ProductCell : UICollectionViewCell

@property(strong,nonatomic) IBOutlet UIImageView *productImage;
@property(strong,nonatomic) IBOutlet UILabel *productName;
@property(strong,nonatomic) IBOutlet UILabel *productQty;
@property(strong,nonatomic) IBOutlet UILabel *productPrice;
@property (weak, nonatomic) IBOutlet UIButton *buyOptionBtn;
@property (weak, nonatomic) IBOutlet UIButton *ratingBtn;
@property (nonatomic, weak) HCSStarRatingView *ratingView;
@property (weak, nonatomic) IBOutlet UIControl *starView;

@end

