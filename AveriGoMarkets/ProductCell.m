//
//  ProductCell.m
//  AveriGo Markets
//
//  Created by LeniYadav on 31/10/18.
//  Copyright © 2018 BTS. All rights reserved.
//

#import "ProductCell.h"

@implementation ProductCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.buyOptionBtn.layer.cornerRadius = 5;
    self.buyOptionBtn.layer.masksToBounds = true;
    self.buyOptionBtn.backgroundColor = [UIColor colorWithRed: 0.0/255.0 green: 164.0/255.0 blue: 215.0/255.0 alpha: 1.0];
    
    self.ratingBtn.layer.cornerRadius = 5;
    self.ratingBtn.layer.masksToBounds = true;
    
    {
          HCSStarRatingView *starRatingView = [[HCSStarRatingView alloc]init];
          
          starRatingView.translatesAutoresizingMaskIntoConstraints = NO;
          
          starRatingView.backgroundColor = [UIColor clearColor];
          
          starRatingView.maximumValue = 5;
          
          starRatingView.minimumValue = 0;
          
          starRatingView.value = 3;
       
          starRatingView.spacing = 5;
          
          starRatingView.starBorderColor = [UIColor grayColor];
          
          starRatingView.emptyStarColor = [UIColor whiteColor];
          
          starRatingView.tintColor = [UIColor colorWithRed: 116.0/255.0 green: 187.0/255.0 blue: 60.0/255.0 alpha: 1.0];
          
          starRatingView.allowsHalfStars = YES;
          
          starRatingView.userInteractionEnabled = NO;
          
          [self.starView addSubview: starRatingView];
          
          self.ratingView = starRatingView;
    
    
    [[self.ratingView.topAnchor constraintEqualToAnchor: self.starView.topAnchor constant: 0.0] setActive: YES];
    [[self.ratingView.leftAnchor constraintEqualToAnchor: self.starView.leftAnchor constant: 20.0] setActive: YES];
    [[self.ratingView.rightAnchor constraintEqualToAnchor: self.starView.rightAnchor constant: -20.0] setActive: YES];
    [[self.ratingView.heightAnchor constraintEqualToConstant: 30] setActive: YES];
      }
}

@end
