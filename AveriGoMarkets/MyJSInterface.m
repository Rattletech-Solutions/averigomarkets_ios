//
//  MyJSInterface.m
//  EasyJSWebViewSample
//
//  Created by Lau Alex on 19/1/13.
//  Copyright (c) 2013 Dukeland. All rights reserved.
//

#import "MyJSInterface.h"
#import "AllConstants.h"
#import "AlertVC.h"
#import "ActivityIndicator.h"

@implementation MyJSInterface

- (void)showPopup:(NSString *)msg :(NSString *)options
{
    bool confirm = NO;
    
    NSString *str=options;
    
    
    NSDictionary *optionDetails=[NSJSONSerialization JSONObjectWithData:[str dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
    
    NSNumber* mapXNum = [optionDetails valueForKey:@"showConfirmationPopup"];
    NSString* timeStampNum = [optionDetails valueForKey:@"TimestampClient"];
  
    NSString* isBackButton = [optionDetails valueForKey:@"btnFlag"];
    
    [[NSUserDefaults standardUserDefaults] setValue:isBackButton forKey:@"isBackButton"];
   
    int mapX = [mapXNum intValue];
    if(timeStampNum != nil){
    //int timeStamp = [timeStampNum intValue];
    [[NSUserDefaults standardUserDefaults]setValue:timeStampNum forKey:@"checkoutstamp"];
        
    }
    
    if([optionDetails valueForKey:@"Status"] != nil){
       NSString* statusStr = [optionDetails valueForKey:@"Status"];
        if([statusStr isEqualToString:@"Success"]){
            NSString* messageStr = [optionDetails valueForKey:@"Message"];
            NSDictionary *dict = [NSDictionary dictionaryWithObject:messageStr forKey:@"popUpMsg"];
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"Payment successful"
             object:self userInfo:dict];
        }else{
                NSString* messageStr = [optionDetails valueForKey:@"Message"];
                NSDictionary *dict = [NSDictionary dictionaryWithObject:messageStr forKey:@"popUpMsg"];
                [[NSNotificationCenter defaultCenter]
                 postNotificationName:@"ShowErrorPopUp"
                 object:self userInfo:dict];
        }
    }
    else if ([msg  isEqual: @"Token expired"])
    {
        NSLog(@"%@ message", msg);
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"Token expired"
         object:self];
        [AlertVC showAlertWithTitleDefault:APP_NAME message:msg controller:self];
    }
    else if ([msg  isEqual: @"Card Deleted Successfully."] || [msg  isEqual: @"Duplicated card."] || [msg  isEqual: @"Card Saved Successfully."] || [msg  rangeOfString: @"We are unable to process"].location != NSNotFound || [msg rangeOfString: @"Oops, something went wrong." ].location != NSNotFound)
    {
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"showPopup"
         object:self];
        [AlertVC showAlertWithTitleDefault:APP_NAME message:msg controller:self];
    }
//    else if ([msg isEqual: @"Thank you for your purchase. This amount will be deducted from your paycheck."])
//    {
//        [[NSNotificationCenter defaultCenter]
//         postNotificationName:@"ShowPayRollPopUp"
//         object:self];
//    }
    else{
        NSLog(@"%@ message", msg);
        if(msg.length != 0 && mapX == 0){
         NSDictionary *dict = [NSDictionary dictionaryWithObject:msg forKey:@"popUpMsg"];
        [[NSNotificationCenter defaultCenter]
             postNotificationName:@"ShowErrorPopUp"
         object:self userInfo:dict];
        }
    }
    
   
    
    NSString *pay = @"pay";
   
    
    NSString *payMethod = [optionDetails valueForKey:@"payMethod"];
    if(payMethod.length > 0){
    pay = payMethod;
    [[NSUserDefaults standardUserDefaults] setValue:pay forKey:@"pay"];
    } else {
    [[NSUserDefaults standardUserDefaults] setValue:pay forKey:@"pay"];
    }
    
    
   
    
    if(mapX  == 1)
    {
        confirm = YES;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSString *htmlString = [NSString stringWithFormat:@"<html><body style='font-family:Montserrat;font-size:14;font-style:normal;text-align: center'> %@ </body></html>",msg];
           [AlertVC showAlertWithHtml:APP_NAME message:htmlString controller:self];
         });
       
    }
    
    if([optionDetails valueForKey:@"showLoader"] != nil){
        NSNumber* showNumber = [optionDetails valueForKey:@"showLoader"];
        int show = [showNumber intValue];
        if(show == 1){
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"showLoader"
             object:self];
        }else{
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"dismissLoader"
             object:self];
        }
    }
    
}

-(NSAttributedString*)convertHtmlPlainText:(NSString*)HTMLString{
    
    NSAttributedString *attrString = [[NSAttributedString alloc] initWithData:[HTMLString dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:NULL error:NULL];
    
    return attrString;
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end

