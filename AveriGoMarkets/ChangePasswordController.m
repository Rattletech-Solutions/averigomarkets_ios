    //
    //  ChangePasswordController.m
    //  AveriGo Markets
    //
    //  Created by LeniYadav on 31/10/18.
    //  Copyright © 2018 BTS. All rights reserved.
    //

#import "ChangePasswordController.h"
#import <STPopup.h>
#import "IQKeyBoardManager/IQKeyboardReturnKeyHandler.h"
#import "Reachability.h"
#import "AlertVC.h"
#import "RealReachability.h"

@interface ChangePasswordController (){
    BOOL isPasswordVisible;
    BOOL isResetPasswordVisible;
}

@end

@implementation ChangePasswordController

{
    IQKeyboardReturnKeyHandler *returnKeyHandler;
   
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self){
        [self customInit];
    }
    return self;
}

- (instancetype)init
{
    if (self = [super init]) {
        self.contentSizeInPopup = CGSizeMake(375, 250);
    }
    return self;
}

-(void)customInit{
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    self.contentSizeInPopup = CGSizeMake(screenWidth - 10, 250);
}


- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"Change Password";
    isPasswordVisible = YES;
    isResetPasswordVisible = YES;
    [self setFieldBorder:_passwordText];
    [self setFieldBorder:_confirmPasswordText];
    
    UIButton *eyeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    eyeBtn.frame = CGRectMake(0, 0, 24, 24);
    [eyeBtn setImage:[UIImage imageWithIcon:@"fa-eye" backgroundColor:[UIColor clearColor] iconColor:[UIColor grayColor] andSize:CGSizeMake(24,24)] forState:UIControlStateNormal];
    [eyeBtn addTarget:self action:@selector(eyeBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    _passwordText.rightViewMode = UITextFieldViewModeAlways;
    _passwordText.rightView = eyeBtn;
    
    UIButton *resetEyeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    resetEyeBtn.frame = CGRectMake(0, 0, 24, 24);
    [resetEyeBtn setImage:[UIImage imageWithIcon:@"fa-eye" backgroundColor:[UIColor clearColor] iconColor:[UIColor grayColor] andSize:CGSizeMake(24,24)] forState:UIControlStateNormal];
    [resetEyeBtn addTarget:self action:@selector(resetEyeBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    _confirmPasswordText.rightViewMode = UITextFieldViewModeAlways;
    _confirmPasswordText.rightView = resetEyeBtn;
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}

-(IBAction)eyeBtnClicked:(id)sender{
    UIButton *eyeBtn = (UIButton*)sender;
    
    if(isPasswordVisible){
        [eyeBtn setImage:[UIImage imageWithIcon:@"fa-eye-slash" backgroundColor:[UIColor clearColor] iconColor:[UIColor grayColor] andSize:CGSizeMake(24,24)] forState:UIControlStateNormal];
        [_passwordText setSecureTextEntry:NO];
        isPasswordVisible = NO;
    }else{
        [eyeBtn setImage:[UIImage imageWithIcon:@"fa-eye" backgroundColor:[UIColor clearColor] iconColor:[UIColor grayColor] andSize:CGSizeMake(24,24)] forState:UIControlStateNormal];
        [_passwordText setSecureTextEntry:YES];
        isPasswordVisible = YES;
    }
}

-(IBAction)resetEyeBtnClicked:(id)sender{
    UIButton *resetEyeBtn = (UIButton*)sender;
    
    if(isResetPasswordVisible){
        [resetEyeBtn setImage:[UIImage imageWithIcon:@"fa-eye-slash" backgroundColor:[UIColor clearColor] iconColor:[UIColor grayColor] andSize:CGSizeMake(24,24)] forState:UIControlStateNormal];
        [_confirmPasswordText setSecureTextEntry:NO];
        isResetPasswordVisible = NO;
    }else{
        [resetEyeBtn setImage:[UIImage imageWithIcon:@"fa-eye" backgroundColor:[UIColor clearColor] iconColor:[UIColor grayColor] andSize:CGSizeMake(24,24)] forState:UIControlStateNormal];
        [_confirmPasswordText setSecureTextEntry:YES];
        isResetPasswordVisible = YES;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if(textField == self.passwordText){
        [textField resignFirstResponder];
        [self.confirmPasswordText becomeFirstResponder];
    }else{
        [textField resignFirstResponder];
    }
    return YES;
}

-(void)setRightView:(JVFloatLabeledTextField *)textField{
    
    CGSize hideShowSize = [@"SHOWX" sizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0f]}];
    UIButton *hideShow = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, hideShowSize.width, textField.frame.size.height)];
    [hideShow.titleLabel setFont:[UIFont systemFontOfSize:14.0f]];
    [hideShow setTitle:@"HIDE" forState:UIControlStateNormal];
    [hideShow setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    textField.rightView = hideShow;
    textField.rightViewMode = UITextFieldViewModeAlways;
   // [hideShow addTarget:self action:@selector(hideShow:) forControlEvents:UIControlEventTouchUpInside];
    
}


-(void)setFieldBorder:(JVFloatLabeledTextField *)textField{
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1;
    NSLog(@"%f",textField.frame.size.height - borderWidth);
    NSLog(@"%f",textField.frame.size.width);
    NSLog(@"%f",textField.frame.size.height);
    border.borderColor = [UIColor lightGrayColor].CGColor;
    border.frame = CGRectMake(0, textField.frame.size.height - borderWidth, textField.frame.size.width + 120, textField.frame.size.height);
    border.borderWidth = borderWidth;
    [textField.layer addSublayer:border];
    textField.layer.masksToBounds = YES;
}

-(IBAction)changePassword:(id)sender {
    
    if(_passwordText.text.length == 0 || _confirmPasswordText.text.length == 0) {
        
        [self showAlert:PLEASE_FILL_REQUIRED_FIELDS] ;
        
    }
    else if( _passwordText.text == _confirmPasswordText.text){
        if([self connected]){
        [self.popupController dismiss];
        [self showAlert:PASSWORD_CHANGED_SUCCESSFULLY];
        NSDictionary *userInfo =
        [NSDictionary dictionaryWithObject:_passwordText.text forKey:@"Password"];
        [[NSNotificationCenter defaultCenter] postNotificationName:
         @"ChangePasswordNotification" object:nil userInfo:userInfo];
        }else{
            [self showAlert:NO_INTERNET_CONNECTION];
        }
        
    } else {
        
        [self showAlert:PASSWORD_DONT_MATCH];
    }
    
}

-(void)showAlert:(NSString *) message {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:APP_NAME message:message preferredStyle:UIAlertControllerStyleAlert];
    NSMutableAttributedString *titleAttr = [[NSMutableAttributedString alloc] initWithString:APP_NAME];
    [titleAttr addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:FONT_MEDIUM size:20]
                      range:NSMakeRange(0, [titleAttr length])];
    
    [alertController setValue:titleAttr forKey:@"attributedTitle"];
    
    NSMutableAttributedString *messageAttr = [[NSMutableAttributedString alloc] initWithString:message];
    [messageAttr addAttribute:NSFontAttributeName
                        value:[UIFont fontWithName:FONT_REGULAR size:14]
                        range:NSMakeRange(0, [messageAttr length])];
    
    
    [alertController setValue:messageAttr forKey:@"attributedMessage"];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alertController addAction:defaultAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}

- (BOOL)connected
{
    ReachabilityStatus status = [GLobalRealReachability currentReachabilityStatus];
    NSLog(@"Initial reachability status:%@",@(status));
    
    return status == 1 || status == 2;
}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
