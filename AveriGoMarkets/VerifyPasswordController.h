//
//  VerifyPasswordControllerViewController.h
//  AveriGo Markets
//
//  Created by Macmini on 11/14/18.
//  Copyright © 2018 BTS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "EMCCountryPickerController.h"
#import "JVFloatLabeledTextField.h"
#import "AllConstants.h"
#import "AlertVC.h"
#import <STPopup.h>

NS_ASSUME_NONNULL_BEGIN

@interface VerifyPasswordController : UIViewController<UITextFieldDelegate,UIGestureRecognizerDelegate,EMCCountryDelegate>

@property(strong,nonatomic) IBOutlet JVFloatLabeledTextField *emailTxt;
@property(strong,nonatomic) IBOutlet JVFloatLabeledTextField *mobileTxt;
@property(strong,nonatomic) IBOutlet UITextField *countryField;
@property(strong,nonatomic) IBOutlet UIButton *sendCodeBtn;
@property(strong,nonatomic) IBOutlet UIImageView *countryImg;
@property(strong,nonatomic) NSString *str;

@end

NS_ASSUME_NONNULL_END
