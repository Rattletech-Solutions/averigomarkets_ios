//
//  ActivityIndicator.h
//  AveriGo Markets
//
//  Created by Rattletech on 08/11/18.
//  Copyright © 2018 BTS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <DGActivityIndicatorView.h>

NS_ASSUME_NONNULL_BEGIN

@interface ActivityIndicator : NSObject

@property (nonatomic) DGActivityIndicatorView * activityIndicatorView;
+ (instancetype)sharedInstance;
- (void)showLoader;
- (void)hideLoader;
- (BOOL)isLoading;
@end

NS_ASSUME_NONNULL_END
