
#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

typedef void(^DidUpdateLocation)(CLLocation *newLocation,CLLocation *oldLocation,NSError *error);

@interface LocationHelper : NSObject<CLLocationManagerDelegate>
{
    CLLocationManager *locationManager;
    DidUpdateLocation blockDidUpdate;
}
-(id)init;
+(LocationHelper *)sharedObject;

-(void)locationPermissionAlert;

-(void)startLocationUpdating;
-(void)stopLocationUpdating;

-(void)startLocationUpdatingWithBlock:(DidUpdateLocation)block;

@end
