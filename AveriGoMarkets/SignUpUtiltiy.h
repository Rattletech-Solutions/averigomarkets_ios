//
//  SignUpUtiltiy.h
//  AveriGo Markets
//
//  Created by Macmini on 11/14/18.
//  Copyright © 2018 BTS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNHelper.h"
#import "AFHTTPSessionManager.h"
#import "SendGrid.h"
#import "AllConstants.h"
#import <STPopup.h>

NS_ASSUME_NONNULL_BEGIN

@interface SignUpUtiltiy : NSObject

+ (instancetype)sharedInstance;

-(void)sendEmailThroughSendGrid:(int)randomNo andEmail:(NSString *)emailTxt andController:(STPopupController *)currentController;

-(void)sendCode:(int)randomNo andMobile:(NSString *)mobileTxt andCountry:(NSString *)countryTxt andEmail:(NSString *)emailTxt andController:(STPopupController *)currentController;





@end

NS_ASSUME_NONNULL_END
