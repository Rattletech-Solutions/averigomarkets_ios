//
//  ReviewAndRatingViewController.h
//  AveriGo Markets
//
//  Created by Macmini on 11/19/19.
//  Copyright © 2019 BTS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol RatingViewDelegate <NSObject>
@optional
- (void)refreshRatingView;

@end

@interface ReviewAndRatingViewController : UIViewController

@property (strong,nonatomic) NSDictionary *purchaseDictionary;

@property (strong,nonatomic) NSString *microMarketID;

@property (strong,nonatomic) NSString *modeString;

@property (strong,nonatomic) NSString *ratingID;

@property (strong,nonatomic) NSString *microMartketString;

@property (nonatomic, weak) id <RatingViewDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
