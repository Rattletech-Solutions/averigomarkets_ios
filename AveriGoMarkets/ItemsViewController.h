    //
    //  ItemsViewController.h
    //  Averigo
    //
    //  Created by BTS on 09/11/16.
    //  Copyright © 2016 BTS. All rights reserved.
    //

#import <AMPopTip/AMPopTip.h>
#import "DataBaseHandler.h"
#import "RealReachability.h"

@interface ItemsViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UIScrollViewDelegate,UIGestureRecognizerDelegate>
{
    NSString *typeSelected;
    UIView *filterView;
    UITableView *filterTable;
    DataBaseHandler *objDBHelper;
}

@property(strong,nonatomic) IBOutlet UISearchBar *search_bar;
@property(strong,nonatomic) IBOutlet UICollectionView *items_view;
@property(strong,nonatomic) IBOutlet UITableView *items_table_view;
@property(strong,nonatomic) IBOutlet UIButton *list_btn;
@property(strong,nonatomic) IBOutlet UIButton *filter_btn;
@property(strong,nonatomic) IBOutlet UILabel *categoryLbl;
@property(strong,nonatomic)  NSMutableArray *itemsArray;
@property(strong,nonatomic)  NSMutableArray *cartArray;
@property(strong,nonatomic)  NSMutableArray *quantityArray;
@property(strong,nonatomic)  NSMutableArray *categoryArray;
@property(strong,nonatomic)  NSMutableArray *filterArray;
@property(strong,nonatomic)  NSMutableArray *categoryIdArray;
@property(strong,nonatomic) IBOutlet UISearchBar *itemsSearch;
@property(strong,nonatomic) IBOutlet UILabel *no_lbl;

@property(strong,nonatomic) NSMutableArray *demoImages;
@property(strong,nonatomic) NSMutableArray *demoTitle;
@property(strong,nonatomic) NSMutableArray *demoDesc;
@property(strong,nonatomic) NSMutableArray *demoPrice;
@property(strong,nonatomic) NSMutableArray *dollarPrice;

@property(strong,nonatomic) NSString *microMarketId;
@property(strong,nonatomic) NSString *categoryId;
@property(strong,nonatomic) NSString *categoryName;
@property(strong,nonatomic) NSString *search_txt;

@property (nonatomic, strong) AMPopTip *popTip;
@property(strong,nonatomic) NSMutableArray  *beacons;
@property(strong,nonatomic)NSString *UDID;

-(IBAction)typeChooseClicked:(id)sender;
-(IBAction)filterClicked:(id)sender;

@end
