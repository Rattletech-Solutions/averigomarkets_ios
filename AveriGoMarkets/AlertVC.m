//
//  AlertVC.m
//  AveriGo Markets
//
//  Created by Rattletech on 28/08/18.
//  Copyright © 2018 BTS. All rights reserved.
//

#import "AlertVC.h"
#import "AllConstants.h"

@implementation AlertVC

+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)msg controller:(id)controller
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    NSMutableAttributedString *titleAttr = [[NSMutableAttributedString alloc] initWithString:title];
    [titleAttr addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:FONT_MEDIUM size:20]
                      range:NSMakeRange(0, [titleAttr length])];
    
    [alertController setValue:titleAttr forKey:@"attributedTitle"];
    
    
    
    NSMutableAttributedString *messageAttr = [[NSMutableAttributedString alloc] initWithString:msg];
    //[messageAttr addAttribute:NSFontAttributeName
                      //  value:[UIFont fontWithName:FONT_REGULAR size:14]
                     //   range:NSMakeRange(0, [messageAttr length])];
    
//    NSMutableAttributedString *messageAttr = [[NSMutableAttributedString alloc] initWithData:[msg dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
//    [messageAttr addAttribute:NSFontAttributeName
//                             value:[UIFont fontWithName:FONT_REGULAR size:14]
//                             range:NSMakeRange(0, [messageAttr length])];
    
    
  //  [alertController setValue:messageAttr forKey:@"attributedMessage"];
    
    id rootViewController = [UIApplication sharedApplication].delegate.window.rootViewController;
    if([rootViewController isKindOfClass:[UINavigationController class]])
    {
        rootViewController = ((UINavigationController *)rootViewController).viewControllers.firstObject;
    }
    if([rootViewController isKindOfClass:[UITabBarController class]])
    {
        rootViewController = ((UITabBarController *)rootViewController).selectedViewController;
    }
    
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                              NSLog(@"You pressed button one");
                                                          }];
    UIAlertAction *secondAction = [UIAlertAction actionWithTitle:@"Confirm"
                                                           style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                               NSLog(@"You pressed button two");
                                                               [[NSNotificationCenter defaultCenter]
                                                                postNotificationName:@"confirmationPopUp"
                                                                object:self];
                                                           }];
    
    [ alertController addAction:firstAction];
    [ alertController addAction:secondAction];
    alertController.preferredAction = secondAction;
    
    [rootViewController presentViewController:alertController animated:YES completion:nil];
    
}

+ (void)showAlertWithTitleDefault:(NSString *)title message:(NSString *)msg controller:(id)controller
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    NSMutableAttributedString *titleAttr = [[NSMutableAttributedString alloc] initWithString:title];
    [titleAttr addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:FONT_MEDIUM size:20]
                      range:NSMakeRange(0, [titleAttr length])];
    
    [alertController setValue:titleAttr forKey:@"attributedTitle"];
    
    NSMutableAttributedString *messageAttr = [[NSMutableAttributedString alloc] initWithString:msg];
    [messageAttr addAttribute:NSFontAttributeName
                        value:[UIFont fontWithName:FONT_REGULAR size:14]
                        range:NSMakeRange(0, [messageAttr length])];
    
    
    [alertController setValue:messageAttr forKey:@"attributedMessage"];
    
    id rootViewController = [UIApplication sharedApplication].delegate.window.rootViewController;
    if([rootViewController isKindOfClass:[UINavigationController class]])
    {
        rootViewController = ((UINavigationController *)rootViewController).viewControllers.firstObject;
    }
    if([rootViewController isKindOfClass:[UITabBarController class]])
    {
        rootViewController = ((UITabBarController *)rootViewController).selectedViewController;
    }
    
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"OK"
                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                  {
                                      NSLog(@"You pressed button one");
                                      [[NSNotificationCenter defaultCenter]
                                       postNotificationName:@"reloadDropDown"
                                       object:self];
                                  }];
    
    [ alertController addAction:firstAction];
    alertController.preferredAction = firstAction;
    [rootViewController presentViewController:alertController animated:YES completion:nil];
    
}

+ (void)showAlertWithTitleForView:(NSString *)title message:(NSString *)msg controller:(id)controller
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    NSMutableAttributedString *titleAttr = [[NSMutableAttributedString alloc] initWithString:title];
    [titleAttr addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:FONT_MEDIUM size:20]
                      range:NSMakeRange(0, [titleAttr length])];
    
    [alertController setValue:titleAttr forKey:@"attributedTitle"];
    
    NSMutableAttributedString *messageAttr = [[NSMutableAttributedString alloc] initWithString:msg];
    [messageAttr addAttribute:NSFontAttributeName
                        value:[UIFont fontWithName:FONT_REGULAR size:14]
                        range:NSMakeRange(0, [messageAttr length])];
    
    
    [alertController setValue:messageAttr forKey:@"attributedMessage"];
    
    id rootViewController = [UIApplication sharedApplication].delegate.window.rootViewController;
    if([rootViewController isKindOfClass:[UINavigationController class]])
    {
        rootViewController = ((UINavigationController *)rootViewController).viewControllers.firstObject;
    }
    if([rootViewController isKindOfClass:[UITabBarController class]])
    {
        rootViewController = ((UITabBarController *)rootViewController).selectedViewController;
    }
    
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"OK"
                                  style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                  {
                                      NSLog(@"You pressed button one");
                                  }];
    
    [ alertController addAction:firstAction];
    alertController.preferredAction = firstAction;
    [rootViewController presentViewController:alertController animated:YES completion:nil];
}

+ (void)showAlertWithHtml:(NSString *)title message:(NSString *)msg controller:(id)controller
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    NSMutableAttributedString *titleAttr = [[NSMutableAttributedString alloc] initWithString:title];
    [titleAttr addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:FONT_MEDIUM size:20]
                      range:NSMakeRange(0, [titleAttr length])];
    
    [alertController setValue:titleAttr forKey:@"attributedTitle"];
    
    
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

    NSMutableAttributedString *messageStr = [[NSMutableAttributedString alloc] initWithData:[msg dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:NULL error:NULL];
      [alertController setValue:messageStr forKey:@"attributedMessage"];
            // do your db stuff here...
            dispatch_async(dispatch_get_main_queue(), ^{
                id rootViewController = [UIApplication sharedApplication].delegate.window.rootViewController;
                if([rootViewController isKindOfClass:[UINavigationController class]])
                {
                    rootViewController = ((UINavigationController *)rootViewController).viewControllers.firstObject;
                }
                if([rootViewController isKindOfClass:[UITabBarController class]])
                {
                    rootViewController = ((UITabBarController *)rootViewController).selectedViewController;
                }
                
                UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                          NSLog(@"You pressed button one");
                                                                      }];
                UIAlertAction *secondAction = [UIAlertAction actionWithTitle:@"Confirm"
                                                                       style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                           NSLog(@"You pressed button two");
                                                                           [[NSNotificationCenter defaultCenter]
                                                                            postNotificationName:@"confirmationPopUp"
                                                                            object:self];
                                                                       }];
                
                [ alertController addAction:firstAction];
                [ alertController addAction:secondAction];
                alertController.preferredAction = secondAction;
                
                [rootViewController presentViewController:alertController animated:YES completion:nil];
            });
    });
    
}




@end
