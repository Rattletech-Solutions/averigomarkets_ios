
#import "LocationHelper.h"
#import "UserDefaultHelper.h"
#import "AllConstants.h"

@implementation LocationHelper

#pragma mark -
#pragma mark - Init

-(id)init
{
    if((self = [super init]))
    {
        //get current location
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    }
    return self;
}

+(LocationHelper *)sharedObject
{
    static LocationHelper *obj = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        obj = [[LocationHelper alloc] init];
    });
    return obj;
}

-(void)locationPermissionAlert
{
    BOOL locationAllowed = [CLLocationManager locationServicesEnabled];
    if (locationAllowed==NO)
    {

        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"POIBeam"
                                      message:@"To re-enable, please go to Settings and turn on Location Service for this app."
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            //do something when click button
        }];
        [alert addAction:okAction];
        UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
        [vc presentViewController:alert animated:YES completion:nil];
        
    }
}

-(void)startLocationUpdating
{
//    if (![CLLocationManager locationServicesEnabled])
//    {
//        [[UserDefaultHelper sharedObject]setLocationDisable:@"1"];
//        [Helper showAlertWithTitle:@"Location Services disabled" Message:@"App requires location services to find your current city weather.Please enable location services in Settings."];
//    }
//    else{
//        [self stopLocationUpdating];
//        if (locationManager==nil) {
//            locationManager = [[CLLocationManager alloc] init];
//            locationManager.delegate = self;
//            locationManager.desiredAccuracy = kCLLocationAccuracyBest;
//        }
//        [locationManager startUpdatingLocation];
//    }
    
        locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    if([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]){
        NSUInteger code = [CLLocationManager authorizationStatus];
        if (code == kCLAuthorizationStatusNotDetermined && ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])) {
            // choose one request      to your business.
//            if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationAlwaysUsageDescription"]){
//                [locationManager requestAlwaysAuthorization];
//            } else
                if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"]) {
                    [locationManager  requestWhenInUseAuthorization];
                } else {
                    NSLog(@"Info.plist does not contain NSLocationAlwaysUsageDescription or NSLocationWhenInUseUsageDescription");
                }
        }
    }
    [locationManager startUpdatingLocation];
    
}

-(void)stopLocationUpdating
{
    [locationManager stopUpdatingLocation];
    locationManager.delegate=nil;
    if (locationManager) {
        locationManager=nil;
    }
}

-(void)startLocationUpdatingWithBlock:(DidUpdateLocation)block
{
    blockDidUpdate=[block copy];
    [self startLocationUpdating];
}

#pragma mark -
#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    switch([error code])
    {
        case kCLErrorLocationUnknown: // location is currently unknown, but CL will keep trying
            break;
            
        case kCLErrorDenied: // CL access has been denied (eg, user declined location use)
            //message = @"Sorry, flook has to know your location in order to work. You'll be able to see some cards but not find them nearby";
            break;
            
        case kCLErrorNetwork: // general, network-related error
            //message = @"Flook can't find you - please check your network connection or that you are not in airplane mode";
            break;
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    CLLocation *currentLocation = newLocation;
    NSLog(@"didUpdateToLocation: %@", currentLocation);
    
    if (currentLocation != nil) {
        NSString  *strForCurrentLongitude= [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        NSString  *strForCurrentLatitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        [[UserDefaultHelper sharedObject]setCurrentLatitude:strForCurrentLatitude];
        [[UserDefaultHelper sharedObject]setCurrentLongitude:strForCurrentLongitude];
        if (blockDidUpdate) {
            blockDidUpdate(currentLocation,oldLocation,nil);
        }
    }else{
        if (blockDidUpdate) {
            blockDidUpdate(currentLocation,oldLocation,[NSError errorWithDomain:@"flamer" code:90000 userInfo:[NSDictionary dictionary]]);
        }
    }
}

- (void)locationManagerDidPauseLocationUpdates:(CLLocationManager *)manager
{
    NSLog(@"Paused Updatting");
}

- (void)locationManagerDidResumeLocationUpdates:(CLLocationManager *)manager
{
    NSLog(@"Resumed Updatting");
}

@end
