//
//  ActivityIndicator.m
//  AveriGo Markets
//
//  Created by Rattletech on 08/11/18.
//  Copyright © 2018 BTS. All rights reserved.
//

#import "ActivityIndicator.h"

@implementation ActivityIndicator

+ (instancetype)sharedInstance{
    static id sharedInstance;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        sharedInstance = [[[self class] alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init{
    if(!(self = [super init])){
        return nil;
    }
    
    _activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallPulseSync tintColor:[UIColor colorWithRed:0.45 green:0.73 blue:0.31 alpha:1.0] size:50];
    return self;
}

- (void)showLoader {
    _activityIndicatorView.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width/2 - 75, [[UIScreen mainScreen] bounds].size.height/2 - 90, 150, 150);
    [[self topViewController].view addSubview:_activityIndicatorView];
    [_activityIndicatorView startAnimating];

}

- (UIViewController*)topViewController {
    return [self topViewControllerWithRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)rootViewController {
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController* tabBarController = (UITabBarController*)rootViewController;
        return [self topViewControllerWithRootViewController:tabBarController.selectedViewController];
    } else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController* navigationController = (UINavigationController*)rootViewController;
        return [self topViewControllerWithRootViewController:navigationController.visibleViewController];
    } else if (rootViewController.presentedViewController) {
        UIViewController* presentedViewController = rootViewController.presentedViewController;
        return [self topViewControllerWithRootViewController:presentedViewController];
    } else {
        return rootViewController;
    }
}
- (void)hideLoader {
    [_activityIndicatorView stopAnimating];
    [_activityIndicatorView setHidden: YES];
}

- (BOOL)isLoading {
    return ![_activityIndicatorView isHidden];
}

@end
