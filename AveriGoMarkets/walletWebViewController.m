
//
//  walletWebViewController.m
//  VendBeam
//
//  Created by BTS on 10/07/18.
//  Copyright © 2018 BTS. All rights reserved.
//

#import "walletWebViewController.h"
#import "EasyJSWebView.h"
#import "MyJSInterface.h"
#import "MBProgressHUD.h"
#import "ProfileViewController.h"
#import "Reachability.h"
#import "BarCodeController.h"
#import "AllConstants.h"
#import "AlertVC.h"
#import "ActivityIndicator.h"
#import "UIImage+FontAwesome.h"
#import "RealReachability.h"

@interface walletWebViewController ()

@end

@implementation walletWebViewController
{
    NSString * Json_String ;
    NSString *jsResult;
    BOOL isLoaded;
    BOOL beaconIsThere;
    BOOL noNetworkDidntLoad;
    ReachabilityStatus currentStatus;
    int BeaconMajor;
    int BeaconMinor;
    BOOL isFirst;
    BOOL isBackBtnFlag;
    UIButton * backButton;
}

- (void)loadView{
    [super loadView];
    isBackBtnFlag = NO;
    NSLog(@"self.view.bounds.size.height %f", self.view.bounds.size.height);
    CGFloat topbarHeight = ([UIApplication sharedApplication].statusBarFrame.size.height +
                            (self.navigationController.navigationBar.frame.size.height ?: 0.0));
    [self.webview setFrame:CGRectStandardize(CGRectMake(0, 5, self.view.bounds.size.width, self.view.bounds.size.height-topbarHeight-75))];
    self.webview.scrollView.delegate = self;
    //[self.checkOutWebView.scrollView setBouncesZoom:false];
    [self.view addSubview:self.webview];
}
-(void)scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view{
    [[scrollView pinchGestureRecognizer] setEnabled:false];
    
    /*for (UIGestureRecognizer *recognizer in scrollView.gestureRecognizers) {
     UITapGestureRecognizer *tap = (UITapGestureRecognizer *)recognizer;
     NSLog(@"%@",[tap class]);
     NSLog(@"AQUI %@",tap);
     NSLog(@"recognizer %@",recognizer);
     [scrollView setMaximumZoomScale:1.0];
     [scrollView setBouncesZoom:false];
     
     
     if([tap class] == nil){
     
     
     if(tap.numberOfTapsRequired == 2 && tap.numberOfTouchesRequired == 1){
     [self.checkOutWebView removeGestureRecognizer:tap];
     }
     
     }
     }*/
    
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    isFirst = YES;
    BeaconMajor = -1;
    BeaconMinor = -1;
    self.tabBarController.tabBar.userInteractionEnabled = NO;
    _webViewLoaded = false;
    _webview.tintColor = [UIColor blackColor];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self InitNavBar];
    //[self loadWebView];
    isLoaded = YES;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear: animated];
    
    _checkFlag = true;
    objDBHelper = [[DataBaseHandler alloc]init];
    
    if (![self connected]) {
        noNetworkDidntLoad = YES;
        self.tabBarController.tabBar.userInteractionEnabled = YES;
        isLoaded = NO;
        [self.noInternetLbl setHidden:NO];
        [self.webview setHidden:YES];
        [AlertVC showAlertWithTitleForView:APP_NAME message:NO_INTERNET_CONNECTION controller:self];
    } else if (!isFirst){
        [self.noInternetLbl setHidden:YES];
        [self.webview setHidden:NO];
        _webViewLoaded = false;
        [self InitNavBar];
        [self loadWebView];
    }
}

-(void)timerCallback {
    if (_webViewLoaded) {
        if (_progressView.progress >= 1) {
            _progressView.hidden = true;
            [_timer invalidate];
        }
        else {
            _progressView.progress += 0.1;
        }
    }
    else {
        _progressView.progress += 0.05;
        if (_progressView.progress >= 0.95) {
            _progressView.progress = 0.95;
        }
    }
}

-(void) loadHtml {
    NSString *headerString = @"<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'></header>";
    
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"errorHtml" ofType:@"html"];
    NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
    [self.webview loadHTMLString:[headerString stringByAppendingString:htmlString] baseURL: [[NSBundle mainBundle] bundleURL]];
    noNetworkDidntLoad = YES;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear: animated];
    
    noNetworkDidntLoad = YES;
    [self setScreenAnalytics];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillTerminate:) name:UIApplicationWillTerminateNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillTerminate:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidComeToForeGround:) name:@"appDidComeToForeGround" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(scanBeaconNotification:)
                                                name:BEACON_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkChanged:)
                                                 name:kRealReachabilityChangedNotification
                                               object:nil];
    self.tabBarController.tabBar.hidden=NO;
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(notify:)
                                                name:LEAVE_FOR_VIEW object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:WEB_VIEW_DID_FINISH
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:SHOW_POPUP
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:CARD_DELETED
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:TOKEN_EXPIRED
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:DID_FAIL_WITH_ERROR
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveConfirmNotification:)
                                                 name:@"confirmationPopUp"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveConfirmNotification:)
                                                 name:@"showLoader"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveConfirmNotification:)
                                                 name:@"dismissLoader"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"ShowErrorPopUp"
                                               object:nil];
}

- (void) scanBeaconNotification:(NSNotification *) notification
{
    beaconIsThere = NO;
    NSDictionary *dict = notification.userInfo;
    _beacons=[dict valueForKey:@"Beacons"];
    NSMutableArray * UDIDS = [NSMutableArray array];
    
//    int minorValue = -1;
//    int majorValue = -1;
//
    for(int i=0;i<_beacons.count;i++)
    {
        CLBeacon *beacon = (CLBeacon*)[self.beacons objectAtIndex:i];
        NSString *BUUID=[NSString stringWithFormat:@"%d-%d",beacon.major.intValue,beacon.minor.intValue];
        [UDIDS addObject:BUUID];
    }
    _UDID= [UDIDS componentsJoinedByString:@","];
    NSString *location_beacon =  [[NSUserDefaults standardUserDefaults]objectForKey:@"LOCATION_BEACON"];
    if(location_beacon != nil){
        if(_beacons.count == 1){
            CLBeacon *beacon = (CLBeacon*)[self.beacons objectAtIndex:0];
            NSString *BUUID=[NSString stringWithFormat:@"%d-%d",beacon.major.intValue,beacon.minor.intValue];
            NSLog(@"location beacon %@",location_beacon);
            NSLog(@"beacon %@",BUUID);
            if(location_beacon == BUUID){
                beaconIsThere = YES;
                BeaconMajor = beacon.major.intValue;
                BeaconMinor = beacon.minor.intValue;
            }else{
                [[NSNotificationCenter defaultCenter] postNotificationName:SCAN_NOTIFICATION object:@"hide"];
            }
        }else if(_beacons.count > 1){
            for(int i = 0; i<_beacons.count;i++){
                CLBeacon *beacon = (CLBeacon*)[self.beacons objectAtIndex:i];
                NSString *BUUID=[NSString stringWithFormat:@"%d-%d",beacon.major.intValue,beacon.minor.intValue];
                NSLog(@"location beacon %@",location_beacon);
                NSLog(@"beacon %@",BUUID);
                if(location_beacon == BUUID){
                    beaconIsThere = YES;
                    BeaconMajor = beacon.major.intValue;
                    BeaconMinor = beacon.minor.intValue;
                }
            }
            if(!beaconIsThere){
                [[NSNotificationCenter defaultCenter] postNotificationName:SCAN_NOTIFICATION object:@"hide"];
                
            }
        } else {
            [[NSNotificationCenter defaultCenter] postNotificationName:SCAN_NOTIFICATION object:@"hide"];
            
        }
    } else {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:SCAN_NOTIFICATION object:@"hide"];
        
    }
    
//    BeaconMajor = majorValue;
//
//    BeaconMinor = minorValue;
    
    if (isFirst) {
        isFirst = NO;
        [self.noInternetLbl setHidden:YES];
        [self.webview setHidden:NO];
        _webViewLoaded = false;
        [self loadWebView];
    }
}

- (void) notify:(NSNotification *) notification {
    if([self connected])
    {
        NSString *interfaceUrl = [NSString stringWithFormat: @"%@", @"javascript:closePopup()"];
        [self.webview evaluateJavaScript:interfaceUrl completionHandler:nil];
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
    }
    //    else
    //        {
    //        [self loadHtml];
    //        }
    
}

-(void)loadWebView {
    [[ActivityIndicator sharedInstance] showLoader];
    
    if([self connected]) {
        [self.noInternetLbl setHidden:YES];
        [self.webview setHidden:NO];
        noNetworkDidntLoad = NO;
        _progressView.progress = 0;
        _timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(timerCallback) userInfo:nil repeats:YES];
        NSString *userName;
        NSString *Secret;
        NSString *version;
        userName =  [[NSUserDefaults standardUserDefaults]objectForKey:@"username"];
        Secret  = @"&Secret=97AE5B1E44D5438872AB1AF53A292";
        version = @"&version=v2&isUpgraded=1";
        NSString *urlString = @"https://vending.averiware.com/index.php?UserName=";
        NSString *concatUrl = [NSString stringWithFormat: @"%@%@%@%@", urlString, userName, Secret, version];
        
        if ((BeaconMinor != -1) && (BeaconMajor != -1)) {
            
            concatUrl = [NSString stringWithFormat: @"%@%@%d%@%d", concatUrl, @"&BeaconMajor=", BeaconMajor, @"&BeaconMinor=", BeaconMinor];
        }
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                
                NSURL *url = [NSURL URLWithString:concatUrl];
                NSURLRequest *req = [NSURLRequest requestWithURL:url];
                MyJSInterface* interface = [MyJSInterface new];
                [self.webview addJavascriptInterfaces:interface WithName:JSINTERFACE];
                [self.webview loadRequest:req];
                [[ActivityIndicator sharedInstance] showLoader];
                
            });
        });
        
    } else {
        noNetworkDidntLoad = YES;
        self.tabBarController.tabBar.userInteractionEnabled = YES;
        isLoaded = NO;
        [[ActivityIndicator sharedInstance] hideLoader];
        [AlertVC showAlertWithTitleForView:APP_NAME message:NO_INTERNET_CONNECTION controller:self];
        [self.noInternetLbl setHidden:NO];
        [self.webview setHidden:YES];
    }
}

-(void) appDidComeToForeGround:(NSNotification *) notification {
    self.tabBarController.selectedIndex=0;
}

- (void) receiveConfirmNotification:(NSNotification *) notification {
    if ([[notification name] isEqualToString:@"confirmationPopUp"])
    {
        [[ActivityIndicator sharedInstance] hideLoader];
        //NSString *interfaceUrl = [NSString stringWithFormat: @"%@", @"javascript:pay()"];
        NSLog(@"jsinterface %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"pay"]);
        //NSString *payStr = [[NSUserDefaults standardUserDefaults] valueForKey:@"pay"];
        NSString *interfaceUrl = [NSString stringWithFormat:@"javascript:%@()",[[NSUserDefaults standardUserDefaults] valueForKey:@"pay"]];
        [self.webview evaluateJavaScript:interfaceUrl completionHandler:nil];
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
    }
    else if ([[notification name] isEqualToString:@"showLoader"]) {
        [[ActivityIndicator sharedInstance] showLoader];
    }
    else if ([[notification name] isEqualToString:@"dismissLoader"]) {
        [[ActivityIndicator sharedInstance] hideLoader];
    }
}

- (void) receiveTestNotification:(NSNotification *) notification
{
    
    if ([[notification name] isEqualToString:WEB_VIEW_DID_FINISH])
    {
        self.tabBarController.tabBar.userInteractionEnabled = YES;
        isLoaded = YES;
        _webViewLoaded = true;
        [self sendJsonThroughInterface];
        [[ActivityIndicator sharedInstance] hideLoader];
    }
    else if ([[notification name] isEqualToString:DID_FAIL_WITH_ERROR])
    {
        self.tabBarController.tabBar.userInteractionEnabled = YES;
        _webViewLoaded = true;
        [[ActivityIndicator sharedInstance] hideLoader];
        [self loadHtml];
    }
    else if ([[notification name] isEqualToString:SHOW_POPUP])
    {
        NSLog(@"Javascript closepopup :%@",self.webview);
        NSString *interfaceUrl = [NSString stringWithFormat: @"%@", @"javascript:closePopup()"];
        [self.webview evaluateJavaScript:interfaceUrl completionHandler:nil];
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
    }
    else if ([[notification name] isEqualToString:TOKEN_EXPIRED])
    {
        [self loadWebView];
        self.tabBarController.selectedIndex=0;
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else if ([[notification name] isEqualToString:CARD_DELETED]) {
        NSString *interfaceUrl = [NSString stringWithFormat: @"%@", @"javascript:closePopup()"];
        [self.webview evaluateJavaScript:interfaceUrl completionHandler:nil];
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
    }
    else if ([[notification name] isEqualToString:@"ShowErrorPopUp"])
    {
        NSDictionary *dict = notification.userInfo;
        NSString *msg=[dict valueForKey:@"popUpMsg"];
        
        NSString *isBackButton = [NSString stringWithFormat: @"%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"isBackButton"]];
        
        if ([msg  isEqual: @"backButton"]) {
            if ([isBackButton isEqualToString: @"0"]) {
                [self.button setImage:[UIImage imageWithIcon:@"fa-user-circle" backgroundColor:[UIColor clearColor] iconColor:[UIColor whiteColor] andSize:CGSizeMake(35,35)] forState:UIControlStateNormal];
                isBackBtnFlag = NO;
                self.title = @"Wallet";
            } else {
                [self.button setImage:[UIImage imageWithIcon:@"fa-chevron-left" backgroundColor:[UIColor clearColor] iconColor:[UIColor whiteColor] andSize:CGSizeMake(35,35)] forState:UIControlStateNormal];
                isBackBtnFlag = YES;
                self.title = @"Add Funds";
            }
            
        } else {
        [self showErrorAlert:msg];
        }
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];

    isFirst = YES;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillTerminateNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SHOW_POPUP object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:BEACON_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"appDidComeToForeGround" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:LEAVE_FOR_VIEW object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:WEB_VIEW_DID_FINISH object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CARD_DELETED object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TOKEN_EXPIRED object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:DID_FAIL_WITH_ERROR object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"confirmationPopUp" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"showLoader" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ShowErrorPopUp" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"dismissLoader" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRealReachabilityChangedNotification object:nil];
}

- (void)willEnterForeground:(NSNotification *) notification
{
    [self.webview reload];
}
-(void)showErrorAlert:(NSString *)message{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:APP_NAME message:message preferredStyle:UIAlertControllerStyleAlert];
    NSMutableAttributedString *titleAttr = [[NSMutableAttributedString alloc] initWithString:APP_NAME];
    [titleAttr addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:FONT_MEDIUM size:20]
                      range:NSMakeRange(0, [titleAttr length])];
    
    [alertController setValue:titleAttr forKey:@"attributedTitle"];
    
    NSMutableAttributedString *messageAttr = [[NSMutableAttributedString alloc] initWithString:message];
    [messageAttr addAttribute:NSFontAttributeName
                        value:[UIFont fontWithName:FONT_REGULAR size:14]
                        range:NSMakeRange(0, [messageAttr length])];
    
    
    [alertController setValue:messageAttr forKey:@"attributedMessage"];
    UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"OK"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action)
                                {
        if (self->isBackBtnFlag != YES) {
                                    NSString *interfaceUrl = [NSString stringWithFormat: @"%@", @"javascript:closePopup()"];
                                    [self.webview evaluateJavaScript:interfaceUrl completionHandler:nil];
                                    [[NSURLCache sharedURLCache] removeAllCachedResponses];
        }
                                }];
    [alertController addAction:yesButton];
    [self presentViewController:alertController animated:YES completion:nil];
    
}
-(void)sendJsonThroughInterface
{
    [self formJsonBody];
    NSLog(@"JSON string :%@",Json_String);
    NSString *interfaceUrl = [NSString stringWithFormat: @"%@%@%@", @"javascript:passData(", Json_String, @")"];
    [self.webview evaluateJavaScript:interfaceUrl completionHandler:nil];
}

-(void)formJsonBody{
    
    NSMutableDictionary *jsonDictionary=[[NSMutableDictionary alloc]init];
    [jsonDictionary setObject:@"1" forKey:@"SupportsConfirmation"];
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary options:0 error:nil];
    Json_String = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSLog(@"%@", Json_String);
}

-(void)InitNavBar
{
    self.navigationItem.title = WALLET;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor colorWithRed:0.98 green:0.96 blue:0.95 alpha:1.0],NSFontAttributeName:[UIFont fontWithName:FONT_REGULAR size:20]}];
    
    self.button  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
    [self.button setImage:[UIImage imageWithIcon:@"fa-user-circle" backgroundColor:[UIColor clearColor] iconColor:[UIColor whiteColor] andSize:CGSizeMake(35,35)] forState:UIControlStateNormal];
    [self.button addTarget:self action:@selector(UserProfileClicked:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *LeftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.button];
    self.navigationItem.leftBarButtonItem = LeftBarButtonItem;
}

-(IBAction)UserProfileClicked:(id)sender
{
    if (isBackBtnFlag) {
             isBackBtnFlag= NO;
             self.title = @"Wallet";
            [self.button setImage:[UIImage imageWithIcon:@"fa-user-circle" backgroundColor:[UIColor clearColor] iconColor:[UIColor whiteColor] andSize:CGSizeMake(35,35)] forState:UIControlStateNormal];
             [[ActivityIndicator sharedInstance] hideLoader];
             NSString *interfaceUrl = [NSString stringWithFormat:@"javascript:goBack()"];
             [self.webview evaluateJavaScript:interfaceUrl completionHandler:nil];
             [[NSURLCache sharedURLCache] removeAllCachedResponses];
       } else {
          ProfileViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:PROFILE_VC];
           controller.modalPresentationStyle = UIModalPresentationFullScreen;
          [self presentViewController:controller animated:YES completion:nil];
       }
}

- (void)networkChanged:(NSNotification *)notification
{
    RealReachability *reachability = (RealReachability *)notification.object;
    ReachabilityStatus status = [reachability currentReachabilityStatus];
    if(status == ReachableViaWiFi && currentStatus == ReachableViaWWAN) {
        [self loadWebView];
    } else if (status != RealStatusNotReachable) {
        NSLog(@"noNetworkDidntLoad:%@",@(noNetworkDidntLoad));
        if(noNetworkDidntLoad){
            [self loadWebView];
            NSLog(@"loadwebview:%@",@(status));
        }
    }else{
        noNetworkDidntLoad = YES;
        self.tabBarController.tabBar.userInteractionEnabled = YES;
        isLoaded = NO;
        [self.noInternetLbl setHidden:NO];
        [self.webview setHidden:YES];
        [AlertVC showAlertWithTitleForView:APP_NAME message:NO_INTERNET_CONNECTION controller:self];
    }
    currentStatus = status;
    NSLog(@"currentStatus:%@",@(status));
    
}

- (BOOL)connected {
    ReachabilityStatus status = [GLobalRealReachability currentReachabilityStatus];
    
    NSLog(@"Initial reachability status:%@",@(status));
    
    return status != RealStatusNotReachable;
}


-(void)setScreenAnalytics {
    
    NSString * SessionGUID = [[NSUserDefaults standardUserDefaults] objectForKey:@"SessionGUID"];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString * checkInTime=[dateFormatter stringFromDate:[NSDate date]];
    NSString * micromarketid = [[NSUserDefaults standardUserDefaults] valueForKey:@"micromarketid"];
    NSString * micromarketname = [[NSUserDefaults standardUserDefaults] valueForKey:@"micromarketname"];
    NSString * userID = [[NSUserDefaults standardUserDefaults] objectForKey:@"username"];
    NSString * analyticsCount = [objDBHelper getScreenAnalyticsBySessionID:SessionGUID andScreenName:@"Wallet"];
    int visitCount = [analyticsCount intValue] +1;
    if(visitCount == 1) {
        [objDBHelper insertScreenAnalytics:userID andsessionGUID:SessionGUID andMircoMarketID:[micromarketid intValue] andMicroMarketName:micromarketname andScreenName:@"Wallet" andscreenVisits:visitCount andCheckInTime:checkInTime andCheckOutTime:@"" andSyncFlag:@"Y"];
    } else {
        [objDBHelper UpdateScreenAnalytics:SessionGUID andVisitCount:visitCount andScreenName:@"Wallet"];
        
    }
}

-(void)updateScreenAnalyticsCheckOutTime {
    
    NSString * SessionGUID = [[NSUserDefaults standardUserDefaults] objectForKey:@"SessionGUID"];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString * checkOutTime=[dateFormatter stringFromDate:[NSDate date]];
    dispatch_async(dispatch_get_main_queue(), ^{
        [objDBHelper UpdateScreenAnalyticsCheckOutTime:SessionGUID andCheckOutTime:checkOutTime andScreenName:@"Wallet"];
    });
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillTerminateNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
}

-(void)viewWillDisappear:(BOOL)animated {
    [self updateScreenAnalyticsCheckOutTime];
    [super viewWillDisappear:true];
}

-(void)appWillTerminate:(NSNotification*)note
{
    [self updateScreenAnalyticsCheckOutTime];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end



