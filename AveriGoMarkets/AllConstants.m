    //
    //  AllConstants.m
    //  Created by Saranya
    //
    //

#import "AllConstants.h"

    //Fonts Helper
NSString *const FONT_REGULAR = @"Montserrat-Regular";
NSString *const FONT_BOLD    = @"Montserrat-Bold";
NSString *const FONT_MEDIUM  = @"Montserrat-Medium";

    // App Constants
NSString *const EMP_STR              = @"";
NSString *const ZERO_POINT_ZERO      = @"0.00";
NSString *const SUCCESS              = @"Success";
NSString *const STATUS               = @"Status";
NSString *const DATEFORMAT_TYPE      = @"YYYY/MM/dd HH:mm:ss";
NSString *const DATEFORMAT_TYPE_TWO  = @"MM/dd/YYYY HH:mm:ss";
NSString *const CONTENT_TYPE         = @"Content-Type";
NSString *const APP_TYPE             = @"application/x-www-form-urlencoded; charset=UTF-8";
NSString *const DONE                 = @"Done";
NSString *const UDID                 = @"UUID";
NSString *const SUPPORT_EMAL         = @"support@averigo.com";
NSString *const SCAN_NOTIFICATION    = @"ScanNotification";
NSString *const BEACON_NOTIFICATION  = @"BeaconNotification";
NSString *const BARCODE_NOTIFICATION = @"BarcodeNotification";
NSString *const CATEGORY_NOTIFICATION = @"CategoryNotification";
NSString *const LEAVE_FOR_VIEW        = @"leaveFromView";
NSString *const WEB_VIEW_DID_FINISH   = @"didFinishNavigation";
NSString *const DID_FAIL_WITH_ERROR   = @"didFailNavigation";
NSString *const JSINTERFACE           = @"JSInterface";
NSString *const CARD_DELETED          = @"Card Deleted Successfully.";
NSString *const TOKEN_EXPIRED         = @"Token expired";
NSString *const SHOW_POPUP            = @"showPopup";
NSString *const SHOW                  = @"show";
NSString *const HIDE                  = @"hide";
NSString *const TITLE_FEATURED        = @"FEATURED";
NSString *const SHOW_LOADER           = @"showLoader";
NSString *const DISMISS_LOADER        = @"dismissLoader";
NSString *const CONFIRMATION_POPUP    = @"confirmationPopUp";
NSString *const PAYROLL_POPUP         = @"ShowPayRollPopUp";
NSString *const PAYMENT_SUCCESS       = @"Payment successful";
NSString *const GOBACK_POPUP          = @"backButton";



    // Cell Identifier's
NSString *const HOME_CELL     = @"HomeCell";
NSString *const HOME_CELL_ID   = @"Home_Cell";
NSString *const SCAN_CELL     = @"ScanCell";
NSString *const PURCHASE_CELL = @"PurchaseCell";
NSString *const ITEMS_CELL    = @"ItemsCell";
NSString *const ORDER_HISTORY_CELL = @"OrderHistoryCell";


    // Navigation Title's
NSString *const PURCHASE_HISTORY = @"Purchase History";
NSString *const SHOPPING_CART    = @"Shopping Cart";
NSString *const CHECK_OUT        = @"Check Out";
NSString *const WALLET           = @"Wallet";
NSString *const SIGN_UP          = @"Sign Up";
NSString *const SCAN_BARCODE     = @"Scan Barcode";
NSString *const SEARCH_TITLE     = @"Search";
NSString *const HELP             = @"Help";
NSString *const FEEDBACK         = @"Feedback";


    // App-Related "Alert" Constants
NSString *const APP_NAME                               = @"AveriGo Markets";
NSString *const FEEDBACK_THANK_YOU                     = @"Thank you for your feedback.";
NSString *const PLEASE_ENTER_FEEDBACK                  = @"Please enter your feedback.";
NSString *const PLEASE_CHECK_YOUR_INTERNET_CONNECTION  = @"Please check your internet connection.";
NSString *const NO_INTERNET_CONNECTION                 = @"The Internet connection appears to be offline";
NSString *const INVALID_EMAIL_ADDRESS                  = @"Invalid Email Address.Sign Up to create an account.";
NSString *const REQUEST_TIMED_OUT                      = @"Request Timed out. Please try again.";
NSString *const ONE_TIME_PASSWORD_SENT                 = @"One-Time Password sent to your registered Email address and Mobile number.";
NSString *const PLEASE_ENTER_VALID_EMAIL               = @"Please enter First Name, Last Name & Email Address";
NSString *const PLEASE_ENTER_VALID_MOBILE              = @"Please enter Valid Email Address & Mobile Number";
NSString *const PLEASE_FILL_EMAIL                      = @"Please fill in Email Address";
NSString *const EMAIL_REG_EX                           = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
NSString *const PLEASE_FILL_REQUIRED_FIELDS            = @"Please enter required fields.";
NSString *const MESSAGE                                = @"Message";
NSString *const PASSWORD_DONT_MATCH                    = @"Passwords don't match.";
NSString *const PASSWORD_CHANGED_SUCCESSFULLY          = @"Password changed successfully";
NSString *const SERVER_BUSY                            = @"Server Busy.";
NSString *const THERE_ARE_NO_ITEMS                     = @"There are no items in cart.";
NSString *const ADD_TO_CART                            = @"Add To Cart";
NSString *const ALERT_PAYROLL_DEDUCTION                = @"Thank you for your purchase. This amount will be deducted from your paycheck.";
NSString *const ERROR_TIMED_OUT                        = @"Timed out due to poor Internet connection. Please try again.";

    // ViewControllers.
NSString *const OPEN_ORDER_VC               = @"OpenOrderViewController";
NSString *const MANAGE_INVENTORY_VC         = @"ManageInventoryController";
NSString *const CREATE_INVENTORY_VC         = @"CreateInventoryViewController";
NSString *const RECONCILE_VC                = @"ReconcileController";
NSString *const RECONCILE_CATEGORY_VC       = @"ReconcileCategoryController";
NSString *const WALLET_WEB_VC               = @"walletWebViewController";
NSString *const CHECKOUT_WEB_VC             = @"CheckOutWebViewController";
NSString *const HELP_WEB_VC                 = @"WebViewController";
NSString *const INTRO_VC                    = @"IntroViewController";
NSString *const FORGOT_PASSWORD_VC          = @"ForgotPasswordController";
NSString *const INITIAL_VC                  = @"InitialViewController";
NSString *const HOME_VC                     = @"HomeViewController";
NSString *const TABBAR_VC                   = @"TabBarViewController";
NSString *const PURCHASE_HISTORY_VC         = @"PurchaseHistoryController";
NSString *const PROFILE_VC                  = @"ProfileViewController";
NSString *const SHOPPING_CART_VC            = @"ShoppingCartViewController";
NSString *const BARCODE_VC                  = @"BarCodeController";
NSString *const ITEMS_VC                    = @"ItemsViewController";
NSString *const CATEGORY_VC                 = @"CategoryViewController";
NSString *const REGISTER_VC                 = @"RegisterViewController";
NSString *const LOGIN_VC                    = @"LoginViewController";


    //Methods
NSString *const MICROMARKET                 = @"VLTRASERV";
NSString *const METHOD_SIGNUP               = @"VB_SignUp_MC";
NSString *const METHOD_SIGNIN               = @"VB_SignIn_MC";
NSString *const METHOD_CATEGORY             = @"VB_CategoriesLists";
NSString *const METHOD_ITEMS                = @"VB_ItemsLists";
NSString *const METHOD_FORGOT               = @"VB_ForgotPassword";
NSString *const METHOD_BARCODE              = @"VB_GetItemDetailUsingBarCode";
NSString *const METHOD_BARCODESCAN          = @"VB_GetItemDetailUsingBarCode_MC";
NSString *const METHOD_COUNTRY              = @"VB_CountryDetails";
NSString *const METHOD_STATE                = @"VB_StateDetails";
NSString *const METHOD_FEEDBACK             = @"VB_SendFeedback";
NSString *const METHOD_PURCHASE             = @"VB_PurchaseHistory";
NSString *const METHOD_CHECKUSER            = @"VB_UserAvailability";
NSString *const METHOD_MICROMARKET          = @"VB_MicroMarketLists_MC";
NSString *const METHOD_SEARCHITEMS          = @"VB_ItemsListsByItemDesc";
NSString *const METHOD_COMPANYDETAILS       = @"VB_CompanyDetails";
NSString *const METHOD_CHECKOUT             = @"VB_CheckOut";
NSString *const METHOD_SAVECARDS            = @"VB_SaveCards";
NSString *const METHOD_GETCARDS             = @"VB_CardList";
NSString *const METHOD_QRCODE               = @"VB_MicroMarketListsByQRCode";
NSString *const METHOD_CHANGEPASSWORD       = @"VB_ChangePassword";
NSString *const METHOD_WAREHOUSE            = @"VB_Warehouse";
NSString *const METHOD_USERAVAILABILITY     = @"VB_UserAvailability_MC";
NSString *const METHOD_MAILSERVER           = @"VB_MailServer";
NSString *const METHOD_MMITEMS              = @"VB_MMItems";
NSString *const METHOD_RECONCILECATEGORYS   = @"VB_CategoriesListsByMarket_V2";
NSString *const METHOD_RECEIVEINVENTORYS    = @"VB_ReceiveInventory";
NSString *const METHOD_RECONCILEINVENTORYS  = @"VB_ReconcileInventory";
NSString *const METHOD_RC                   = @"VB_CategoriesListsByMarket";
NSString *const METHOD_RI                   = @"VB_ReceiveInventory";
NSString *const METHOD_CI                   = @"VB_ReconcileInventory";
NSString *const METHOD_PURCHASE_HISTORY     = @"VB_PurchaseHistory_MC_V4";
NSString *const METHOD_FORGOT_MC            = @"VB_ForgotPassword_MC";
NSString *const METHOD_VALIDATE_CART        = @"VB_ValidateCartItem";
NSString *const METHOD_BESTSELLING          = @"VB_TopSoldItems";
NSString *const METHOD_PURCHASE_RECENT      = @"VB_PurchaseHistory_Recent";
NSString *const METHOD_ANALYTICS            = @"analytics";



    //Prameters
NSString *const PARAM_EMAIL_KEY       = @"EmailId";
NSString *const PARAM_USERNAME        = @"USERNAME";
NSString *const PARAM_USERPASSWORD    = @"USERPASSWORD";
NSString *const PARAM_COMPANYACRO     = @"COMPANYACRO";
NSString *const PARAM_SERVICE_NAME    = @"SERVICE_NAME";
NSString *const PARAM_DEVICE_UDID     = @"DEVICE_UDID";
NSString *const PARAM_DEVICE_DATETIME = @"DEVICE_DATETIME";
NSString *const PARAM_FIRSTNAME       = @"FIRSTNAME";
NSString *const PARAM_LASTNAME        = @"LASTNAME";
NSString *const PARAM_COMPANY_NAME    = @"COMPANY_NAME";
NSString *const PARAM_NICKNAME        = @"NICK_NAME";
NSString *const PARAM_PROFILE_IMAGE   = @"PROFILE_IMAGE";
NSString *const PARAM_EMAIL_ID        = @"EMAIL_ID";
NSString *const PARAM_PASSWORD        = @"PASSWORD";
NSString *const PARAM_PHONENO         = @"PHONE_NO";
NSString *const PARAM_ADDRESS         = @"ADDRESS";
NSString *const PARAM_CITY            = @"CITY";
NSString *const PARAM_ZIP             = @"ZIP";
NSString *const PARAM_STATE           = @"STATE";
NSString *const PARAM_COUNTRY         = @"COUNTRY";
NSString *const PARAM_ENABEL_NEWSLETTER = @"ENABEL_NEWSLETTER";
NSString *const PARAM_ENABLE_SMS      = @"ENABLE_SMS";
NSString *const PARAM_PROCESS_STATUS  = @"PROCESS_STATUS";
NSString *const PARAM_OTP             = @"Otp";
NSString *const PARAM_EMAIL_ID_KEY    = @"EmailID";
NSString *const PARAM_SERVICE_NAME_KEY = @"ServiceName";
NSString *const PARAM_DEVICE_UDID_KEY  = @"DeviceUDID";
NSString *const PARAM_DEVICE_DATE_TIME_KEY = @"DeviceDateTime";
NSString *const PARAM_PASSWORD_KEY     = @"Password";
NSString *const PARAM_PHONENO_KEY      = @"PhoneNo";
NSString *const PARAM_USERNAME_KEY     = @"UserName";
NSString *const PARAM_MICROMARKET_ID   = @"MicroMarketID";
NSString *const PARAM_MICROMRKET_NAME  = @"MicroMarketName";
NSString *const PARAM_BARCODE_ID       =  @"BarCodeID";
NSString *const PARAM_BEACON_MAJOR     = @"BeaconMajor";
NSString *const PARAM_BEACON_MINOR     = @"BeaconMinor";
NSString *const PARAM_PAGE_NO     = @"PageNo";
NSString *const PARAM_MODE_UPDATE     = @"U";
NSString *const PARAM_MODE_INSERT     = @"I";
NSString *const PARAM_MODE_DELETE     = @"D";


