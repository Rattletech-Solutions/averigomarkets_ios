    //
    //  DataBaseHandler.h
    //  Averigo
    //
    //  Created by BTS on 12/01/17.
    //  Copyright © 2017 BTS. All rights reserved.
    //

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface DataBaseHandler : NSObject
{
    sqlite3 *databaseReference;
}

@property(nonatomic,strong)NSString *dbPath;
@property(strong,nonatomic)NSUserDefaults *Defaults;
- (id)init;

    //Copies the database from the Project bundle to the document directory
- (BOOL)copyDatabaseFromBundletoDocumentDirectory;

    // Delete Datease
- (void)deleteDatabaseFromBundletoDocumentDirectory;

    // gets the location of the database present in the document directory
- (NSString*)documentDirectoryDatabaseLocation;

    // creates the database instance and save it document directory
- (void)createDatabaseInstance;

    // checks if any previous db connection is open before firing a new query
    //and if the connection is open then closes it
- (void)closeanyOpenConnection;

-(BOOL)insertCartItems:(NSString *)MicroMarketID andCategoryID:(NSString *)CategoryID andDescription:(NSString *)ItemDescription andItemNo:(NSString *)ItemNo andcrvtax:(NSString *)CRVTAX andSalesTax:(NSString *)SalesTax andDiscountPrice:(NSString *)DiscountPrice andItemName:(NSString *)ItemName andItemImage:(NSString *)ItemImage andItemPrice:(NSString *)ItemPrice andStock:(NSString *)Stock andQuantity:(NSString *)Quantity andOriginalPrice:(NSString *)OriginalPrice andCRVPercent:(NSString *)CRVPercent andSalesPercent:(NSString *)SalesPercent andCRVTAXEnable:(NSString *)CRVTAXEnable andSalesTaxEnable:(NSString *)SalesTaxEnable andDiscountPriceDiff:(NSString *)DiscountPriceDiff andDiscountPriceEnable:(NSString *)DiscountPriceEnable andBarcode:(NSString *)Barcode;

-(BOOL)insertBeacons:(NSString *)BeaconMajor andDistance:(NSString *)Distance andMicroMarketID:(NSString *)MicroMarketID andStateName:(NSString *)StateName andAddress:(NSString *)Address andLongitude:(NSString *)Longitude andZip:(NSString *)Zip andImage:(NSString *)Image andDescription:(NSString *)Description andCountry:(NSString *)Country andLatitude:(NSString *)Latitude andOrderBy:(NSString *)OrderBy andCity:(NSString *)City andSalesTax:(NSString *)SalesTax andCompany:(NSString *)Company andBannerImage:(NSString *)Image andBannerText:(NSString *)Text andisFeatured:(NSString *)isFeatured andcategoryId:(NSString *)categoryId andPayrollStatus:(NSString *)payrollStatus andPayUserId:(NSString *)payUserId andHasBestSelling:(NSString *)HasBestSelling andCreditStatus:(NSString *)CreditStatus andCreditUserId:(NSString *)CreditUserId;

-(BOOL)insertOperatorBeacons:(NSString *)BeaconMajor andBeaconMinor:(NSString *)BeaconMinor andAppMailId:(NSString *)AppRelatedMailId andBannerImage:(NSString *)BannerImage andBannerText:(NSString *)BannerText andBeaconValue:(NSString *)BeaconValue andCategoryId:(NSString *)CategoryId andCompanyName:(NSString *)CompanyName andCountryName:(NSString *)CountryName andOrderBy:(NSString *)OrderBy andDistance:(NSString *)Distance andIsFeatured:(NSString *)IsFeatured andLocationDesc:(NSString *)LocationDesc andLocationId:(NSString *)LocationId andLocationAdd:(NSString *)LocationAdd andLocationCity:(NSString *)LocationCity andLocationImage:(NSString *)LocationImage andLocationZip:(NSString *)LocationZip andMerchantId:(NSString *)MerchantId andSalesTax:(NSString *)SalesTax andTerminalId:(NSString *)TerminalId andWebLink:(NSString *)WebLink andWebLinkUrl:(NSString *)WebLinkUrl;


-(BOOL)insertUserAnalytics:(NSString *)userid andsessionGUID:(NSString *)sessionGuid andFirstName:(NSString *)firstName andLastName:(NSString *)lastName andDeviceUDID:(NSString *)deviceUDID andDeviceType:(NSString *)deviceType andDeviceModel:(NSString *)deviceModel andeDeviceOS:(NSString *)deviceOS andLatitude:(NSString *)latitude andLongitude:(NSString *)longitude andCheckInTime:(NSString *)checkInTime andCheckOutTime:(NSString *)checkOutTime andSyncFlag:(NSString *)syncFlag;


- (BOOL)UpdateUserAnalytics:(NSString *)sessionGuid andCheckOutTime:(NSString *)checkOutTime;

- (BOOL)UpdateScreenAnalyticsMicroMarket:(int)microMarketId andSessionGuid:(NSString *)sessionGuid;

- (BOOL)UpdateUserAnalyticsCart:(int)microMarketId andSessionGuid:(NSString *)sessionGuid;

- (BOOL)UpdateScreenAnalytics:(NSString *)sessionGuid andVisitCount:(int)visitCount andScreenName:(NSString *)screenName;
    
- (BOOL)UpdateProductAnalytics:(NSString *)sessionGuid andQty:(int)qty andProductID:(NSString *)productID andType:(NSString *)type;

- (BOOL)UpdateScreenAnalyticsCheckOutTime:(NSString *)sessionGuid andCheckOutTime:(NSString *)checkOutTime andScreenName:(NSString *)screenName;

-(BOOL)insertScreenAnalytics:(NSString *)userid andsessionGUID:(NSString *)sessionGuid andMircoMarketID:(int)microMakrketID andMicroMarketName:(NSString *)microMarketName andScreenName:(NSString *)screenName andscreenVisits:(int)screenVisits andCheckInTime:(NSString *)checkInTime andCheckOutTime:(NSString *)checkOutTime andSyncFlag:(NSString *)syncFlag;

-(BOOL)insertProductAnalytics:(NSString *)userid andsessionGUID:(NSString *)sessionGuid andMircoMarketID:(int)microMakrketID andMicroMarketName:(NSString *)microMarketName andProductID:(NSString *)productID andProductName:(NSString *)productName andCategoryID:(NSString *)categoryID andUPCCode:(NSString *)upcCode andItemQty:(int)qty andItemPrice:(float)itemPrice andType:(NSString *)type andSyncFlag:(NSString *)syncFlag;

-(BOOL)insertFeedbackAnalytics:(NSString *)recipient andDate:(NSString *)date andTime:(NSString *)time andName:(NSString *)name andUserId:(NSString *)userId andMicroMarketName:(NSString *)microMarketName andMicroMarketId:(NSString *)microMarketId andMessage:(NSString *)message andDeviceManufacturer:(NSString *)deviceManufacturer andDeviceModel:(NSString *)deviceModel andDeviceOS:(NSString *)deviceOS andAppVersion:(NSString *)appVersion;

-(BOOL)deleteSessionID:(NSString *)sessionID;

- (NSMutableArray *)getCartItems:(NSString *)MicroMarketID;

- (NSMutableArray *)getBeacons;

- (NSMutableArray *)getOperatorBeacons;

- (NSMutableArray *)getUserAnalyticsToBeSynced;

- (NSMutableArray *)getScreenAnalyticsToBeSynced;

- (NSMutableArray *)getProductAnalyticsToBeScanned;

- (NSMutableArray *)getFeedbackAnalytics;


-(NSString *)getUserAnalyticsBySessionID:(NSString *)sessionID;

-(NSString *)getScreenAnalyticsBySessionID:(NSString *)sessionID andScreenName:(NSString *)screenName;

-(NSString *)getProductAnalyticsBySessionID:(NSString *)sessionID andProductID:(NSString *)productID andType:(NSString *)type;

-(NSMutableArray *)getProductAnalyticsByProductID:(NSString *)productID andType:(NSString *)type;

-(BOOL)deleteOperatorBeacon;

-(BOOL)deleteBeacon;

-(BOOL)deleteCart;

-(BOOL)deleteUserAnalytics;

-(BOOL)deleteProductAnalytics;

-(BOOL)deleteScreenAnalytics;

-(BOOL)deleteFeedbackAnalytics;

-(BOOL)deleteCartByMicroMarket:(NSString *)MicroMarketID;

-(BOOL)deleteItem:(NSString *)ItemNo andMicroMarketID:(NSString *)MicroMarketID;

- (BOOL)UpdateBeacon:(NSString *)Major andDistance:(NSString *)Distance;

- (BOOL)UpdateOperatorBeacon:(NSString *)Major andDistance:(NSString *)Distance;

-(BOOL)UpdateQuantity:(NSString *)ItemNo andQuantity:(NSString *)Quantity andMicroMarketID:(NSString *)MicroMarketID;

-(BOOL)UpdatePrice:(NSString *)ItemNo andPrice:(NSString *)Price andMicroMarketID:(NSString *)MicroMarketID;

-(BOOL)UpdateDiscountPriceDiff:(NSString *)ItemNo andPrice:(NSString *)Price andMicroMarketID:(NSString *)MicroMarketID andDiscountPriceDiff:(NSString *)DiscountPriceDiff;

-(BOOL)UpdateOriginalPrice:(NSString *)ItemNo andPrice:(NSString *)Price andMicroMarketID:(NSString *)MicroMarketID;

-(BOOL)UpdateScreenAnalytics:(NSString *)syncFlag;

-(BOOL)UpdateUserAnalytics:(NSString *)syncFlag;

-(BOOL)UpdateCartProductAnalytics:(NSString *)syncFlag;


-(BOOL)ReduceQuantity:(NSString *)ItemNo andQuantity:(NSString *)Quantity andMicroMarketID:(NSString *)MicroMarketID;

-(BOOL)ReducePrice:(NSString *)ItemNo andPrice:(NSString *)Price andMicroMarketID:(NSString *)MicroMarketID;

-(BOOL)ReduceDiscountDiff:(NSString *)ItemNo andPrice:(NSString *)Price andMicroMarketID:(NSString *)MicroMarketID andDiscountPriceDiff:(NSString *)DiscountPriceDiff;

- (NSMutableArray *)getCartItemsByItemNo:(NSString *)MicroMarketID andItemNo:(NSString *)ItemNo;



-(BOOL)CheckItem:(NSString *)ItemNo andMicroMarketID:(NSString *)MicroMarketID;

-(NSString *)getcount:(NSString *)MicroMarketID;

-(NSString *)getDiscountTotal:(NSString *)MicroMarketID;

-(NSString *)getSalesTaxTotal:(NSString *)MicroMarketID;

-(NSString *)getCRVTaxTotal:(NSString *)MicroMarketID;

-(NSString *)getBeaconCount;

-(NSString *)getDiscountPriceDiff:(NSString *)ItemNo;

-(NSString *)getSalesTax:(NSString *)ItemNo;

-(BOOL)UpdateSalesTax:(NSString *)SalesTax andItemNo:(NSString *)ItemNo;

-(NSString *)getCRVTax:(NSString *)ItemNo;

-(BOOL)UpdateCRVTax:(NSString *)CRVTax andItemNo:(NSString *)ItemNo;

-(BOOL)UpdateCRVTaxPercent:(NSString *)CRVTaxPercent andItemNo:(NSString *)ItemNo;

-(BOOL)UpdateSalesTaxPercent:(NSString *)SalesTaxPercent andItemNo:(NSString *)ItemNo;

-(BOOL)UpdateNewPrice:(NSString *)ItemNo andPrice:(NSString *)Price andMicroMarketID:(NSString *)MicroMarketID;

-(BOOL)UpdateDiscountPrice:(NSString *)ItemNo andPrice:(NSString *)Price andMicroMarketID:(NSString *)MicroMarketID;

-(NSString *)getTotalItems:(NSString *)MicroMarketID;


@end
