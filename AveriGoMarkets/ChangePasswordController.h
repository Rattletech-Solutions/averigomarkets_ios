    //
    //  ChangePasswordController.h
    //  AveriGo Markets
    //
    //  Created by LeniYadav on 31/10/18.
    //  Copyright © 2018 BTS. All rights reserved.
    //

#import <UIKit/UIKit.h>
#import "UIImage+FontAwesome.h"
#import "JVFloatLabeledTextField.h"
#import "AllConstants.h"

@interface ChangePasswordController : UIViewController<UITextFieldDelegate>

@property(strong,nonatomic) IBOutlet JVFloatLabeledTextField *passwordText;
@property(strong,nonatomic) IBOutlet JVFloatLabeledTextField *confirmPasswordText;

@end
