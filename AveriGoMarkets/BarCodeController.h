//
//  BarCodeController.h
//  Averigo
//
//  Created by BTS on 10/11/16.
//  Copyright © 2016 BTS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreLocation/CoreLocation.h>
#import <QuartzCore/QuartzCore.h>
#import "DataBaseHandler.h"
#import "UIImage+ImageCompress.h"
#import <ZXingObjC/ZXingObjC.h>

@interface BarCodeController : UIViewController<UIGestureRecognizerDelegate,UITableViewDelegate,UITableViewDataSource,ZXCaptureDelegate>
{
    int scancount;
    DataBaseHandler *objDBHelper;
}
@property (strong,nonatomic) UIButton *button;
@property (strong,nonatomic) IBOutlet UIButton *notScanningBtn;

@property (weak, nonatomic) IBOutlet UIView *viewPreview;
@property (nonatomic, weak) IBOutlet UIView *scanRectView;
@property (nonatomic, strong) ZXCapture *capture;
@property (nonatomic, strong) ZXDecodeHints *hints;
@property (nonatomic) BOOL scanning;
@property (nonatomic) BOOL isFirstApplyOrientation;
@property (nonatomic, weak) IBOutlet UILabel *decodedLabel;

@property (nonatomic, strong) AVCaptureSession *captureSession;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *videoPreviewLayer;
@property (nonatomic, strong) AVCapturePhotoOutput *stillImageOutput;
@property (nonatomic, strong) AVAudioPlayer *audioPlayer;
@property (strong,nonatomic) IBOutlet UIButton *scan_btn;
@property(strong,nonatomic) NSString *CountFlag;
@property(strong,nonatomic) IBOutlet UITableView *item_tableview;
@property(strong,nonatomic) IBOutlet UIButton *checkout_btn;
@property(strong,nonatomic) NSMutableArray *demo_title;
@property(strong,nonatomic) NSMutableArray *dollar_price;
@property(strong,nonatomic) NSMutableArray *items_array;
@property(strong,nonatomic) NSArray *temp_array;
@property(strong,nonatomic) IBOutlet UILabel *total_lbl;
@property(strong,nonatomic) NSString *micromarketid;
@property(strong,nonatomic) NSString *serviceName;
@property(strong,nonatomic) NSMutableArray  *beacons;
@property (strong,nonatomic) NSMutableArray *validate_array;
@property(strong,nonatomic)NSString *UDID;
@property BOOL LoadFlag;

@property (nonatomic) BOOL isReading;

-(BOOL)startReading;
-(void)stopReading;
-(void)loadBeepSound;
-(IBAction)onScanClicked:(id)sender;
-(IBAction)onCheckOutClicked:(id)sender;

@end
