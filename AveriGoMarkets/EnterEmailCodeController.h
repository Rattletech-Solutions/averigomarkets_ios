//
//  EnterEmailCodeController.h
//  AveriGo Markets
//
//  Created by Macmini on 11/12/18.
//  Copyright © 2018 BTS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface EnterEmailCodeController : UIViewController<UITextFieldDelegate,UIGestureRecognizerDelegate>

@property(strong,nonatomic) IBOutlet UITextField *field1;
@property(strong,nonatomic) IBOutlet UITextField *field2;
@property(strong,nonatomic) IBOutlet UITextField *field3;
@property(strong,nonatomic) IBOutlet UITextField *field4;
@property(strong,nonatomic) IBOutlet UIButton *verifyBtn;
@property(strong,nonatomic) IBOutlet UIButton *resendBtn;


@end

NS_ASSUME_NONNULL_END
