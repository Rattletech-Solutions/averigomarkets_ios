    //
    //  IntroViewController.m
    //  VendBeam
    //
    //  Created by BTS on 12/09/17.
    //  Copyright © 2017 BTS. All rights reserved.
    //

#import "IntroViewController.h"
#import <EAIntroView/EAIntroView.h>
#import "LocationHelper.h"
#import "InitialViewController.h"
#import "AllConstants.h"
#import "AlertVC.h"

static NSString * const bluetoothDescription = @"AveriGo Markets uses Bluetooth beacons to notify you of, and access, a nearby market. So make sure your phone’s Bluetooth is on.";
static NSString * const locationDescription = @"AveriGo Markets uses your phone’s location to display markets near you. So make sure you give AveriGo Markets access to your phone’s location services.";
static NSString * const notifyDescription = @"AveriGo Markets will continue to listen for beacons and use your location to notify you of, and access, nearby markets. So allow AveriGo Markets to CONTINUE to use your phone’s location services when you are asked to do so.";

@interface IntroViewController ()<EAIntroDelegate,CLLocationManagerDelegate>{
    UIView *rootView;
    EAIntroView *intro;
    CLLocationManager *locationManager;
    EAIntroPage *page1;
    EAIntroPage *page2;
    EAIntroPage *page3;
    EAIntroPage  *pageIndexValue;
    int pageIndexNumber;
    
}

@end

@implementation IntroViewController

- (void)viewDidLoad {
    [super viewDidLoad];
        // Do any additional setup after loading the view.
    rootView = self.view;
    locationManager = [[CLLocationManager alloc] init];
    
    [self showIntroWithCrossDissolve];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}
- (void)showIntroWithCrossDissolve {
    page1 = [EAIntroPage page];
        //    page1.title = @"Bluetooth";
    page1.titleColor = [UIColor colorWithRed:0.17 green:0.70 blue:0.34 alpha:1.0];
    page1.titleFont = [UIFont fontWithName:FONT_BOLD size:25.f];
    page1.titlePositionY = rootView.frame.size.height/3 + 150;
    page1.descPositionY = rootView.frame.size.height/3 + 200;
    page1.desc = bluetoothDescription;
    page1.descFont = [UIFont fontWithName:FONT_REGULAR size:22.f];
    page1.bgColor = [UIColor colorWithRed:0.45 green:0.73 blue:0.31 alpha:1.0];
    page1.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bluetooth.png"]];
    page1.titleIconPositionY = rootView.frame.size.height/3 - 150;
    
    UILabel *blueLabel = [[UILabel alloc]initWithFrame:CGRectMake(rootView.frame.size.width/2 - 150, rootView.frame.size.height/3, 300, 32)];
    blueLabel.text = @"Bluetooth";
    blueLabel.font = [UIFont fontWithName:FONT_BOLD size:30.f];
    blueLabel.textColor = UIColor.whiteColor;
    blueLabel.textAlignment = NSTextAlignmentCenter;
   
    UIButton *button  = [[UIButton alloc] initWithFrame:CGRectMake(rootView.frame.size.width/2 - 150, rootView.frame.size.height - 100, 300, 50)];
    [button.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:18.f]];
    [button setTitle:@"Allow access to Bluetooth >>" forState:UIControlStateNormal];
    [button setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    [button setBackgroundColor:[UIColor colorWithRed:0.00 green:0.51 blue:0.69 alpha:1.0]];
    [button addTarget:self action:@selector(bluetoothAccess:) forControlEvents:UIControlEventTouchUpInside];
    button.layer.cornerRadius = 5;
    button.clipsToBounds = YES;
    page1.subviews = @[blueLabel,button];
    
    
    
    
    page2 = [EAIntroPage page];
        //    page2.title = @"Location";
    page2.titleColor = [UIColor colorWithRed:0.17 green:0.70 blue:0.34 alpha:1.0];
    page2.titleFont = [UIFont fontWithName:FONT_BOLD size:25.f];
    page2.titlePositionY = rootView.frame.size.height/3 + 150;
    page2.descPositionY = rootView.frame.size.height/3 + 200;
    page2.desc = locationDescription;
    page2.descFont = [UIFont fontWithName:FONT_REGULAR size:22.f];
    page2.bgColor = [UIColor colorWithRed:0.45 green:0.73 blue:0.31 alpha:1.0];
    page2.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"locate.png"]];
    page2.titleIconPositionY = rootView.frame.size.height/3 - 150;
    
    UILabel *locLabel = [[UILabel alloc]initWithFrame:CGRectMake(rootView.frame.size.width/2 - 150, rootView.frame.size.height/3, 300, 32)];
    locLabel.text = @"Location";
    locLabel.font = [UIFont fontWithName:FONT_BOLD size:30.f];
    locLabel.textColor = UIColor.whiteColor;
    locLabel.textAlignment = NSTextAlignmentCenter;
    
    
    UIButton *locbutton  = [[UIButton alloc] initWithFrame:CGRectMake(rootView.frame.size.width/2 - 150, rootView.frame.size.height - 100, 300, 50)];
    [locbutton.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:18.f]];
    
    [locbutton setTitle:@"Allow access to Location >>" forState:UIControlStateNormal];
    [locbutton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    [locbutton setBackgroundColor:[UIColor colorWithRed:0.00 green:0.51 blue:0.69 alpha:1.0]];
    [locbutton addTarget:self action:@selector(locationAccess:) forControlEvents:UIControlEventTouchUpInside];
    locbutton.layer.cornerRadius = 5;
    locbutton.clipsToBounds = YES;
    page2.subviews = @[locLabel,locbutton];
    
    
    page3 = [EAIntroPage page];
        //    page3.title = @"Notify";
    page3.titleColor = [UIColor colorWithRed:0.17 green:0.70 blue:0.34 alpha:1.0];
    page3.titleFont = [UIFont fontWithName:FONT_BOLD size:25.f];
    page3.titlePositionY = rootView.frame.size.height/3 + 150;
    page3.descPositionY = rootView.frame.size.height/3 + 200;
    page3.descFont = [UIFont fontWithName:FONT_REGULAR size:22.f];
    page3.desc = notifyDescription;
    page3.bgColor = [UIColor colorWithRed:0.45 green:0.73 blue:0.31 alpha:1.0];
    page3.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"notify.png"]];
    page3.titleIconPositionY = rootView.frame.size.height/3 - 150;
    
    UILabel *notifyLabel = [[UILabel alloc]initWithFrame:CGRectMake(rootView.frame.size.width/2 - 150, rootView.frame.size.height/3, 300, 32)];
    notifyLabel.text = @"Notify";
    notifyLabel.font = [UIFont fontWithName:FONT_BOLD size:30];
    notifyLabel.textColor = UIColor.whiteColor;
    notifyLabel.textAlignment = NSTextAlignmentCenter;
    
    
    UIButton *launchbutton  = [[UIButton alloc] initWithFrame:CGRectMake(rootView.frame.size.width/2 - 150, rootView.frame.size.height - 100, 300, 50)];
    [launchbutton.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:18.f]];
    [launchbutton setTitle:@"Launch App" forState:UIControlStateNormal];
    [launchbutton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    [launchbutton setBackgroundColor:[UIColor colorWithRed:0.00 green:0.51 blue:0.69 alpha:1.0]];
    [launchbutton addTarget:self action:@selector(launchApp:) forControlEvents:UIControlEventTouchUpInside];
    launchbutton.layer.cornerRadius = 5;
    launchbutton.clipsToBounds = YES;
    page3.subviews = @[notifyLabel,launchbutton];
    NSLog(@"rootview bounds %f",rootView.frame.size.width);
    
    intro = [[EAIntroView alloc] initWithFrame:rootView.bounds andPages:@[page1,page2,page3]];
    intro.skipButtonAlignment = EAViewAlignmentCenter;
    intro.skipButtonY = 80.f;
    intro.pageControlY = 42.f;
    intro.skipButton.hidden = YES;
    intro.swipeToExit = NO;
    intro.exclusiveTouch = NO;
    intro.scrollView.scrollEnabled = NO;
    [intro setDelegate:self];
    [intro showInView:rootView animateDuration:0.3];
    
}

- (void)introDidFinish:(EAIntroView *)introView wasSkipped:(BOOL)wasSkipped {
    if(wasSkipped) {
        NSLog(@"Intro skipped");
    } else {
        NSLog(@"Intro finished");
    }
}

-(IBAction)bluetoothAccess:(id)sender
{
        // Bluetooth Detection.
    NSDictionary *options = @{CBCentralManagerOptionShowPowerAlertKey: @NO};
    _centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil options:options];
}

- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    if (central.state == CBManagerStatePoweredOn)
        {
        [intro scrollToPageForIndex:1 animated:YES];
        }
    else if(central.state == CBManagerStatePoweredOff)
        {
        NSString * bleMsg =[NSString stringWithFormat:@"%s\"%s\"%s"," This app requires Bluetooth to be On to work properly. After installation is complete, click on ", "Help"," in the upper right corner for instructions to turn it On."];
            if(pageIndexNumber != 2){
            [self showBluetoothAlert:bleMsg];
            }
        }
    
}


-(void)intro:(EAIntroView *)introView pageAppeared:(EAIntroPage *)page withIndex:(NSUInteger)pageIndex
{
    pageIndexValue = page;
    pageIndexNumber = (int) pageIndex;
}


-(IBAction)locationAccess:(id)sender
{
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager requestWhenInUseAuthorization];
    [locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    
    if (pageIndexValue == page2)
        {
        NSString * gpsMsg =[NSString stringWithFormat:@"%s\"%s\"%s"," This app requires Location Services to be On to work properly. After installation is complete, click on ", "Help"," in the upper right corner for instructions to turn them On."];
            [self showLocationAlert:gpsMsg];
        }
    else{
            //...
    }
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
        //CLLocation *currentLocation = newLocation;
    
    float Latitude=newLocation.coordinate.latitude;
    float Longitude=newLocation.coordinate.longitude;
    [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%f",Latitude] forKey:@"Latitude"];
    [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%f",Longitude] forKey:@"Longitude"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    NSError *error;
    if (!error)
        {
        [[LocationHelper sharedObject]stopLocationUpdating];
        }
    [intro scrollToPageForIndex:2 animated:YES];
}


-(IBAction)launchApp:(id)sender
{
    [[NSUserDefaults standardUserDefaults]setObject:@"Y" forKey:@"IntroViewed"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    intro.alpha = 0;
    [[[UIApplication sharedApplication] delegate] performSelector:@selector(getlocationbeacons)];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        }
    else
        {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
        }
    
    [intro removeFromSuperview];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)showBluetoothAlert:(NSString *)message{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:APP_NAME message:message preferredStyle:UIAlertControllerStyleAlert];
    NSMutableAttributedString *titleAttr = [[NSMutableAttributedString alloc] initWithString:APP_NAME];
    [titleAttr addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:FONT_MEDIUM size:20]
                      range:NSMakeRange(0, [titleAttr length])];
    
    [alertController setValue:titleAttr forKey:@"attributedTitle"];
    
    NSMutableAttributedString *messageAttr = [[NSMutableAttributedString alloc] initWithString:message];
    [messageAttr addAttribute:NSFontAttributeName
                        value:[UIFont fontWithName:FONT_REGULAR size:14]
                        range:NSMakeRange(0, [messageAttr length])];
    
    
    [alertController setValue:messageAttr forKey:@"attributedMessage"];
    UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"OK"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action)
                                {
                                    [intro scrollToPageForIndex:1 animated:YES];
                                }];
    [alertController addAction:yesButton];
    [self presentViewController:alertController animated:YES completion:nil];
    
}

-(void)showLocationAlert:(NSString *)message{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:APP_NAME message:message preferredStyle:UIAlertControllerStyleAlert];
    NSMutableAttributedString *titleAttr = [[NSMutableAttributedString alloc] initWithString:APP_NAME];
    [titleAttr addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:FONT_MEDIUM size:20]
                      range:NSMakeRange(0, [titleAttr length])];
    
    [alertController setValue:titleAttr forKey:@"attributedTitle"];
    
    NSMutableAttributedString *messageAttr = [[NSMutableAttributedString alloc] initWithString:message];
    [messageAttr addAttribute:NSFontAttributeName
                        value:[UIFont fontWithName:FONT_REGULAR size:14]
                        range:NSMakeRange(0, [messageAttr length])];
    
    
    [alertController setValue:messageAttr forKey:@"attributedMessage"];
    UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"OK"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action)
                                {
                                     [intro scrollToPageForIndex:2 animated:YES];
                                }];
    [alertController addAction:yesButton];
    [self presentViewController:alertController animated:YES completion:nil];
    
}





/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
