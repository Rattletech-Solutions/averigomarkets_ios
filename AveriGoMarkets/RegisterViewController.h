//
//  RegisterViewController.h
//  Averigo
//
//  Created by BTS on 08/11/16.
//  Copyright © 2016 BTS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "EMCCountryPickerController.h"
#import "JVFloatLabeledTextField.h"
#import "UIImage+FontAwesome.h"
#import <STPopup.h>

@interface RegisterViewController : UIViewController<UITextFieldDelegate,UIGestureRecognizerDelegate,EMCCountryDelegate>

@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txtFirstName;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txtLastName;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txtEmailAddress;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txtMobileNumber;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txtPassword;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txtReEnterPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnSignUp;
@property (weak, nonatomic) IBOutlet UILabel *lblPersonalInfo;

@property(strong,nonatomic) NSArray *tempArray;
@property(strong,nonatomic) NSMutableArray *userArray;

-(IBAction)onNextClick:(id)sender;

@end
