//
//  HelpController.h
//  AveriGo Markets
//
//  Created by Rattletech on 30/10/18.
//  Copyright © 2018 BTS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AllConstants.h"
#import "WebViewController.h"
#import "BrowserController.h"
#import "DataBaseHandler.h"

NS_ASSUME_NONNULL_BEGIN

@interface HelpController : UIViewController
{
    DataBaseHandler *objDBHelper;
}

@property(strong,nonatomic) IBOutlet UIButton *helpBtn;
@property(strong,nonatomic) IBOutlet UIButton *feedbackBtn;
@property(strong,nonatomic) IBOutlet UILabel *appVersionLbl;
@property(strong,nonatomic) IBOutlet UILabel *manufacturerLbl;
@property(strong,nonatomic) IBOutlet UILabel *modelLbl;
@property(strong,nonatomic) IBOutlet UILabel *osLbl;



@end

NS_ASSUME_NONNULL_END
