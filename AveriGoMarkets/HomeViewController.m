//
//  HomeViewController.m
//  Averigo
//
//  Created by BTS on 02/03/17.
//  Copyright © 2017 BTS. All rights reserved.
//

#import "HomeViewController.h"
#import "ProfileViewController.h"
#import "MBProgressHUD.h"
#import "KGModal.h"
#import "AllConstants.h"
#import "AFNHelper.h"
#import "CategoryViewController.h"
#import "SendGrid.h"
#import "BarCodeController.h"
#import "Reachability.h"
#import <Google/Analytics.h>
#import "GAIDictionaryBuilder.h"
#import "WebViewController.h"
#import "UIImageView+WebCache.h"
#import "Reachability.h"
#import "ItemsViewController.h"
#import "LocationHelper.h"
#import "AlertVC.h"
#import "ActivityIndicator.h"
#import "UIImage+FontAwesome.h"
#import <STPopup.h>
#import "AppDelegate.h"

@interface HomeViewController ()<CLLocationManagerDelegate,STPopupControllerTransitioning>
{
    int count;
    UIView *pop_view;
    UITextView *feedback_txt;
    UIToolbar *toolBar;
    NSString *beacon_str;
    int originalCount;
    BOOL appRecipient;
    MKDropdownMenu *dropdownMenu;
    NSArray *title;
    NSString *featuredBanner;
    NSString *enabledLocFlag;
    NSString *enabledBleFlag;
    BOOL noNetworkDidntLoad;
}

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appRecipient = NO;
    objDBHelper=[[DataBaseHandler alloc]init];
    count=0;
    beacon_str=EMP_STR;
    originalCount = 0;
    _Index=0;
    _BeaconSelectedArray=[[NSMutableArray alloc]init];
    _CompareArray=       [[NSMutableArray alloc]init];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    NSLog(@"%@", [NSString stringWithFormat: @"%@",[[NSUserDefaults standardUserDefaults] valueForKey: @"oldBuildNumber"]]);
    
    // Bluetooth Detection.
    NSDictionary *options = @{CBCentralManagerOptionShowPowerAlertKey: @NO};
    self.bluetoothManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil options:options];
    [self.micromarket_view registerNib:[UINib nibWithNibName:HOME_CELL bundle:nil] forCellWithReuseIdentifier:@"Home_Cell"];
    self.micromarket_view.bounces=NO;
    self.micromarket_view.alwaysBounceVertical=NO;
    self.micromarket_view.scrollsToTop=NO;
    self.micromarket_view.alwaysBounceHorizontal=YES;
    self.micromarket_view.contentSize=CGSizeMake(self.view.frame.size.width, self.view.frame.size.height-40);
    self.micromarket_view.allowsSelection = NO;
    self.micromarket_view.contentMode = UIViewContentModeCenter;
    self.userDetailsArray=[[NSMutableArray alloc]init];
    self.userDetailsArray=[[NSUserDefaults standardUserDefaults] objectForKey:@"userdetailsarray"];
    UIOffset anOffset=UIOffsetZero;
    anOffset = UIOffsetMake(10,0);
    [(HomeCollectionViewLayout *)self.micromarket_view.collectionViewLayout setOffset:anOffset];
    [(HomeCollectionViewLayout *)self.micromarket_view.collectionViewLayout setLayoutType:EBCardCollectionLayoutHorizontal];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"reloadDropDown"
                                               object:nil];
    
    if(self.isMicroMarket && [self.type isEqualToString:@"qrcode"]) {
        self.microMarketArray=[[NSUserDefaults standardUserDefaults]objectForKey:@"micromarketarray"];
        NSDictionary *dict;
        dict=[self.microMarketArray objectAtIndex:0];
        NSLog(@"micromarket array size %lu",(unsigned long)self.microMarketArray.count);
        [[NSUserDefaults standardUserDefaults]setValue:[dict valueForKey:@"LOCATION_ID"] forKey:@"micromarketid"];
        self.type=[[NSUserDefaults standardUserDefaults]objectForKey:@"type"];
        [self.micromarket_view reloadData];
    }
    
    title = @[HELP, FEEDBACK];
    self.micromarket_view.hidden = YES;
    self.noMicroMarketLbl.hidden = NO;
    
    if([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied || ![CLLocationManager locationServicesEnabled] ) {
        [self showEnablePopup];
    }else
    {
    }
    
    NSLog(@"Checout:%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"checkoutstamp"]);
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"checkoutstamp"] != nil){
        [self getCheckoutStatus];
    }
}

- (NSInteger)numberOfComponentsInDropdownMenu:(MKDropdownMenu *)dropdownMenu{
    return 1;
}

- (NSInteger)dropdownMenu:(MKDropdownMenu *)dropdownMenu numberOfRowsInComponent:(NSInteger)component{
    return title.count;
}

- (NSAttributedString *)dropdownMenu:(MKDropdownMenu *)dropdownMenu attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSMutableAttributedString *string =
    [[NSMutableAttributedString alloc] initWithString: title[row]
                                           attributes:@{NSFontAttributeName: [UIFont fontWithName:FONT_REGULAR size:20],
                                                        NSForegroundColorAttributeName: [UIColor whiteColor]}];
    
    return string;
}

- (UIColor *)dropdownMenu:(MKDropdownMenu *)dropdownMenu backgroundColorForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [UIColor colorWithRed:0.17 green:0.70 blue:0.34 alpha:1.0];
}

- (void)dropdownMenu:(MKDropdownMenu *)dropdownMenu didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    [dropdownMenu closeAllComponentsAnimated:NO];
    if(row == 0){
        WebViewController *vc;
        vc=[self.storyboard instantiateViewControllerWithIdentifier:HELP_WEB_VC];
        vc.modalPresentationStyle = UIModalPresentationFullScreen;
        [self presentViewController:vc animated:YES completion:nil];
    }else{
        [dropdownMenu closeAllComponentsAnimated:NO];
        [self AboutUsClicked:nil];
    }
    
}

- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    if (central.state == CBManagerStatePoweredOff)
    {
        [self showEnablePopup];
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"Description %@",error.localizedDescription);
}


- (void)showEnablePopup
{
    NSString * gpsMsg =[NSString stringWithFormat:@"%s\"%s\"%s"," Both Bluetooth and Location Services need to be On to use this app. Click on ", "Help"," in the upper right corner for instructions to turn them On."];
    [AlertVC showAlertWithTitleDefault:APP_NAME message:gpsMsg controller:self];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear: animated];
    
    noNetworkDidntLoad = YES;
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"fromregister"] != nil){
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"fromregister"];
        self.tabBarController.selectedIndex = 3;
    }
    self.tabBarController.tabBar.hidden=NO;
    [self InitNavBar];
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(receiveNotification1:)
                                                name:BEACON_NOTIFICATION object:nil];
    
    if(![self.type isEqualToString:@"qrcode"])
    {
        _loadFlag=YES;
    }
    [self setScreenAnalytics];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillTerminate:) name:UIApplicationWillTerminateNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillTerminate:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkChanged:)
                                                 name:kRealReachabilityChangedNotification
                                               object:nil];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear: animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:BEACON_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRealReachabilityChangedNotification object:nil];
    self.bluetoothManager = [[CBCentralManager alloc] initWithDelegate:nil queue:dispatch_get_main_queue()];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear: animated];
    
    if([self connected]){
        [self getMicroMarkets];
    }else{
        noNetworkDidntLoad = YES;
        self.micromarket_view.hidden = YES;
        self.noMicroMarketLbl.hidden = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:SCAN_NOTIFICATION object:@"hide"];
        [AlertVC showAlertWithTitleForView:APP_NAME message:NO_INTERNET_CONNECTION controller:self];
    }
}

- (void) receiveNotification1:(NSNotification *) notification {
    NSDictionary *dict = notification.userInfo;
    _beacons=[dict valueForKey:@"Beacons"];
    
    NSMutableArray * UDIDS = [NSMutableArray array];
    NSMutableArray * UUIDS = [NSMutableArray array];
    NSMutableArray * Distances = [NSMutableArray array];
    
    [_MajorValues removeAllObjects];
    [_CompareArray removeAllObjects];
    
    NSString *beacon_count=[NSString stringWithFormat:@"%lu",(unsigned long)_beacons.count];
    NSString *data_count=[NSString stringWithFormat:@"%lu",(unsigned long)_beaconCountCheck.count];
    
    if(![beacon_count isEqualToString:data_count])
    {
        _loadFlag=YES;
        _beaconCountCheck=[[NSMutableArray alloc]init];
        _beaconCountCheck=_beacons;
    }
    
    for(int i=0;i<_beacons.count;i++)
    {
        CLBeacon *beacon = (CLBeacon*)[self.beacons objectAtIndex:i];
        float filterFactor = 0.2;
        float filteredAccuracy = (beacon.accuracy * filterFactor) + (beacon.accuracy * (1.0 - filterFactor));
        NSString *UUI=[NSString stringWithFormat:@"'%d-%d'",beacon.major.intValue,beacon.minor.intValue];
        
        NSDictionary *Beacon_Value = @{
                                       @"Uuuid"    :[UUI lowercaseString],
                                       @"Distance" :[NSString stringWithFormat:@"%f",filteredAccuracy],
                                       };
        
        [_MajorValues addObject:Beacon_Value];
        [_CompareArray addObject:[UUI lowercaseString]];
        NSString *BUUID=[NSString stringWithFormat:@"'%d-%d'",beacon.major.intValue,beacon.minor.intValue];
        NSString *UDID=[NSString stringWithFormat:@"'%d-%d'",beacon.major.intValue,beacon.minor.intValue];
        [UUIDS addObject:UDID];
        [UDIDS addObject:BUUID];
        [Distances addObject:[NSString stringWithFormat:@"%f",filteredAccuracy]];
        
    }
    _UDID= [UDIDS componentsJoinedByString:@","];
    
    if(_beacons.count==1) {
        if(_loadFlag) {
            _loadFlag=NO;
            originalCount = (int)_beacons.count;
            [self getMicroMarkets];
        }
    } else {
        if(_loadFlag) {
            _loadFlag=NO;
            [_BeaconSelectedArray removeAllObjects];
            for(int i=0;i<_CompareArray.count;i++)
            {
                [_BeaconSelectedArray addObject:[_CompareArray objectAtIndex:i]];
            }
            originalCount = (int)_beacons.count;
            [self getMicroMarkets];
        } else {
            NSSet *set1 = [NSSet setWithArray:_BeaconSelectedArray];
            NSSet *set2 = [NSSet setWithArray:_CompareArray];
            
            if(_BeaconSelectedArray.count>0) {
                if([set1 isEqualToSet:set2]) {
                    NSLog(@"Both Array are same.update distance");
                    
                    for(int i=0;i<UUIDS.count;i++) {
                        NSString *UDID=[[UUIDS objectAtIndex:i]lowercaseString];
                        NSString *checkudid =[UDID stringByReplacingOccurrencesOfString:@"’" withString:EMP_STR];
                        NSString *Distance=[Distances objectAtIndex:i];
                        if([Distance isEqualToString:@"-1.000000"])
                        {
                            Distance=@"1000.000000";
                        }
                        NSLog(@"Distance:%@",Distance);
                        [objDBHelper UpdateBeacon:checkudid andDistance:Distance];
                    }
                    [self.microMarketArray removeAllObjects];
                    self.microMarketArray = [objDBHelper getBeacons];
                    if(self.microMarketArray.count != 0){
                        NSDictionary *dictobject;
                        dictobject=[self.microMarketArray objectAtIndex:0];
                        NSArray *beaconItems = [[dictobject valueForKey:@"BeaconMajor"] componentsSeparatedByString:@"-"];
                        [[NSUserDefaults standardUserDefaults] setObject:[dictobject valueForKey:@"BeaconMajor"] forKey:@"LOCATION_BEACON"];
                        [[NSUserDefaults standardUserDefaults] setObject:[dictobject valueForKey:@"SALESTAX"] forKey:@"salestax"];
                        [[NSUserDefaults standardUserDefaults]setValue:[dictobject valueForKey:@"LOCATION_ID"] forKey:@"micromarketid"];
                        [[NSUserDefaults standardUserDefaults]setValue:[dictobject valueForKey:@"CATEGORY_ID"] forKey:@"featuredCategoryId"];
                        [[NSUserDefaults standardUserDefaults]setValue:beaconItems[0] forKey:@"beaconMajor"];
                        [[NSUserDefaults standardUserDefaults]setValue:beaconItems[1] forKey:@"beaconMinor"];
                        [[NSUserDefaults standardUserDefaults]setValue:[dictobject valueForKey:@"PayrollStatus"] forKey:@"payrollstatus"];
                        [[NSUserDefaults standardUserDefaults]setValue:[dictobject valueForKey:@"PayUserID"] forKey:@"payuserid"];
                        [[NSUserDefaults standardUserDefaults]setValue:[dictobject valueForKey:@"COMPANYNAME"] forKey:@"serviceName"];
                        [[NSUserDefaults standardUserDefaults] setValue:[dictobject valueForKey:@"CreditStatus"] forKey:@"creditstatus"];
                        [[NSUserDefaults standardUserDefaults] setValue:[dictobject valueForKey:@"CreditUserId"] forKey:@"credituserid"];
                        [[NSUserDefaults standardUserDefaults]setObject:self.microMarketArray forKey:@"micromarketarray"];
                        [[NSUserDefaults standardUserDefaults]setValue:[dictobject valueForKey:@"ISFEATURED"]  forKey:@"featured"];
                        NSString * SessionGUID = [[NSUserDefaults standardUserDefaults] objectForKey:@"SessionGUID"];
                        NSString * micromarketid = [[NSUserDefaults standardUserDefaults] valueForKey:@"micromarketid"];
                        [objDBHelper UpdateScreenAnalyticsMicroMarket:[micromarketid intValue] andSessionGuid:SessionGUID];
                        [objDBHelper UpdateUserAnalyticsCart:[micromarketid intValue] andSessionGuid:SessionGUID];
                        [self.micromarket_view reloadData];
                    }
                    //update distance
                    NSLog(@"Update Distance");
                }
                else
                {
                    _loadFlag=YES;
                }
            }
        }
    }
}


- (void)InitNavBar {
    self.navigationItem.title = APP_NAME;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor colorWithRed:0.98 green:0.96 blue:0.95 alpha:1.0],NSFontAttributeName:[UIFont fontWithName:FONT_MEDIUM size:20]}];
    
    self.button  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
    //FAUserCircle
    [self.button setImage:[UIImage imageWithIcon:@"fa-user-circle" backgroundColor:[UIColor clearColor] iconColor:[UIColor whiteColor] andSize:CGSizeMake(35,35)] forState:UIControlStateNormal];
    [self.button addTarget:self action:@selector(UserProfileClicked:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.button];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"help"] forState:UIControlStateNormal];
    // [button setImage:[UIImage imageWithIcon:@"fa-envelope" backgroundColor:[UIColor clearColor] iconColor:[UIColor whiteColor] andSize:CGSizeMake(35,35)] forState:UIControlStateNormal];
    [button setTintColor:UIColor.whiteColor];
    button.frame = CGRectMake(0, 0, 40, 40);
    [button addTarget:self action:@selector(helpCenter:) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *backButtonView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    [backButtonView addSubview:button];
    
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButtonView];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier;
    NSDictionary *dict;
    
    cellIdentifier = HOME_CELL_ID;
    
    HomeCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(bannerImageClicked)];
    singleTap.numberOfTapsRequired = 1;
    [cell.banner_img setUserInteractionEnabled:YES];
    [cell.banner_img addGestureRecognizer:singleTap];
    
    cell.viewButton.layer.cornerRadius = 5;
    cell.viewButton.clipsToBounds = YES;
    
    [cell.viewButton addTarget:self action:@selector(viewFeaturedButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UITapGestureRecognizer *singleTapBest = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewBestSellingButtonClicked)];
    singleTapBest.numberOfTapsRequired = 1;
    [cell.bottom_view addGestureRecognizer:singleTapBest];
    
    [cell.bottom_view setUserInteractionEnabled:YES];
    //[cell.bestSellingBtn addTarget:self action:@selector(viewBestSellingButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    if ( self.microMarketArray != nil && self.microMarketArray.count != 0 )
    {
        dict=[self.microMarketArray objectAtIndex:indexPath.row];
        if(![[[NSUserDefaults standardUserDefaults]objectForKey:@"type"] isEqualToString:@"qrcode"])
        {
            [[NSUserDefaults standardUserDefaults]setValue:[dict valueForKey:@"LOCATION_DESC"] forKey:@"micromarketname"];
            cell.company_lbl.text=[dict valueForKey:@"LOCATION_DESC"];
            if([dict valueForKey:@"LOC_CITY"] != nil){
                cell.address_lbl.text=[NSString stringWithFormat:@"%@, %@",[dict valueForKey:@"LOC_CITY"],[dict valueForKey:@"STATE_NAME"]];
            }else{
                cell.address_lbl.text=[dict valueForKey:@"LOC_ADD"];
            }
            if ([[dict valueForKey:@"ISFEATURED"]  isEqual: @"Y"])
            {
                NSString *txtBannerDiscount = [dict valueForKey:@"BANNER_TEXT"];
                cell.banner_lbl.text = txtBannerDiscount;
                cell.banner_lbl.textColor = [UIColor colorWithRed:0.17 green:0.70 blue:0.34 alpha:1.0];
                cell.banner_lbl.backgroundColor = [UIColor clearColor];
                [cell.viewButton setHidden:NO];
                cell.viewButton.backgroundColor = [UIColor colorWithRed:0.00 green:0.64 blue:0.84 alpha:1.0];
                cell.bestSellingBtn.backgroundColor =  [UIColor colorWithRed:0.45 green:0.73 blue:0.31 alpha:1.0];
                cell.featured_lbl.text = TITLE_FEATURED;
            }
            else
            {
                cell.banner_lbl.backgroundColor = [UIColor clearColor];
                cell.banner_lbl.textColor = [UIColor clearColor];
                [cell.viewButton setHidden:YES];
                cell.banner_img.userInteractionEnabled = NO;
                cell.featured_lbl.text = EMP_STR;
                cell.bestSellingBtn.backgroundColor = [UIColor colorWithRed:0.00 green:0.64 blue:0.84 alpha:1.0];
                
            }
            
            NSString *encoded = [[dict valueForKey:@"BANNER_IMAGE"] stringByAddingPercentEncodingWithAllowedCharacters: [NSCharacterSet URLQueryAllowedCharacterSet]];
            // NSString *encoded = @"";
            NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"https://%@",encoded]];
            
            [cell.banner_img sd_setImageWithURL:url
                               placeholderImage:[UIImage imageNamed:@"averigo_logo_markets.png"] options:SDWebImageRefreshCached];
        }
        else {
            cell.company_lbl.text=[dict valueForKey:@"LOCATION_DESC"];
            if([dict valueForKey:@"LOC_CITY"] != nil){
                cell.address_lbl.text=[NSString stringWithFormat:@"%@, %@",[dict valueForKey:@"LOC_CITY"],[dict valueForKey:@"STATE_NAME"]];
            }else{
                cell.address_lbl.text=[dict valueForKey:@"LOC_ADD"];
            }
        }
    }
    else
    {
        //...
    }
    return cell;
}

- (void)heightToFit:(UILabel *)label {
    
    CGSize maxSize = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize textSize = [label.text sizeWithFont:label.font constrainedToSize:maxSize lineBreakMode:label.lineBreakMode];
    
    CGRect labelRect = label.frame;
    labelRect.size.height = textSize.height;
    [label setFrame:labelRect];
}

- (void)viewBestSellingButtonClicked {
    [self openCategoryScreen];
    
    ItemsViewController *vc;
    vc = [self.storyboard instantiateViewControllerWithIdentifier: ITEMS_VC];
    vc.categoryId= @"'BESTSELLING'";
    vc.categoryName = @"Best Selling";
    vc.microMarketId=featuredBanner;
    vc.itemsArray=nil;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)viewFeaturedButtonClicked:(UIButton*)sender {
    [self openCategoryScreen];
    
    featuredBanner =[[NSUserDefaults standardUserDefaults]objectForKey:@"micromarketid"];
    ItemsViewController *vc;
    vc = [self.storyboard instantiateViewControllerWithIdentifier: ITEMS_VC];
    vc.categoryId= [NSString stringWithFormat:@"'%@'",[[NSUserDefaults standardUserDefaults]objectForKey:@"featuredCategoryId"]];
    vc.categoryName = @"Featured";
    vc.microMarketId=featuredBanner;
    vc.itemsArray=nil;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)bannerImageClicked {
    [self openCategoryScreen];
    featuredBanner =[[NSUserDefaults standardUserDefaults]objectForKey:@"micromarketid"];
    ItemsViewController *vc;
    vc = [self.storyboard instantiateViewControllerWithIdentifier: ITEMS_VC];
    vc.categoryId= [NSString stringWithFormat:@"'%@'",[[NSUserDefaults standardUserDefaults]objectForKey:@"featuredCategoryId"]];
    vc.microMarketId=featuredBanner;
    vc.itemsArray=nil;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)openCategoryScreen {
    self.microMarketPrefArray=[[NSMutableArray alloc]init];
    NSDictionary *dict;
    dict=[self.microMarketArray objectAtIndex:0];
    [self.microMarketPrefArray addObject:[self.microMarketArray objectAtIndex:0]];
    NSLog(@"count %lu",(unsigned long)self.microMarketPrefArray.count);
    [[NSUserDefaults standardUserDefaults] setObject:[dict valueForKey:@"BeaconMajor"] forKey:@"LOCATION_BEACON"];
    [[NSUserDefaults standardUserDefaults] setObject:[dict valueForKey:@"SALESTAX"] forKey:@"salestax"];
    [[NSUserDefaults standardUserDefaults]setValue:[dict valueForKey:@"LOCATION_ID"] forKey:@"micromarketid"];
    [[NSUserDefaults standardUserDefaults]setObject:self.microMarketPrefArray forKey:@"micromarketarray"];
    [[NSUserDefaults standardUserDefaults]setObject:self.microMarketPrefArray forKey:@"micromarketarray"];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:[NSString stringWithFormat:@"%@ %@ (%@) - FEATURED",[[NSUserDefaults standardUserDefaults]objectForKey:@"firstname"],[[NSUserDefaults standardUserDefaults]objectForKey:@"lastname"],[dict valueForKey:@"LOCATION_DESC"]]];
    [tracker set:kGAIUserId value:[NSString stringWithFormat:@"%@ %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"firstname"],[[NSUserDefaults standardUserDefaults]objectForKey:@"lastname"]]];
    
    NSLog(@" %@", kGAIScreenName );
    
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    GAIDictionaryBuilder *builder = [GAIDictionaryBuilder createScreenView];
    [builder set:@"start" forKey:kGAISessionControl];
    [tracker set:kGAIScreenName value:[NSString stringWithFormat:@"%@ %@ (%@) - FEATURED",[[NSUserDefaults standardUserDefaults]objectForKey:@"firstname"],[[NSUserDefaults standardUserDefaults]objectForKey:@"lastname"],[dict valueForKey:@"LOCATION_DESC"]]];
    [tracker send:[builder build]];
    
}

- (IBAction)UserProfileClicked:(id)sender {
    ProfileViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:PROFILE_VC];
    controller.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:controller animated:YES completion:nil];
}

- (IBAction)AboutUsClicked:(id)sender {
    pop_view = [[UIView alloc] initWithFrame:CGRectMake(0, 0,self.view.frame.size.width-20, 250)];
    pop_view.backgroundColor     = [UIColor whiteColor];
    pop_view.layer.cornerRadius  = 5.0;
    pop_view.layer.masksToBounds = YES;
    UILabel *name_=[[UILabel alloc]initWithFrame:CGRectMake(20,15,pop_view.frame.size.width-40, 40)];
    name_.text=FEEDBACK;
    name_.font=[UIFont fontWithName:FONT_BOLD size:20.f];
    name_.textAlignment=NSTextAlignmentCenter;
    name_.textColor=[UIColor colorWithRed:0.17 green:0.24 blue:0.31 alpha:1.0];
    
    UIButton *close_btn=[[UIButton alloc]initWithFrame:CGRectMake(10, 10, 32, 32)];
    [close_btn setImage:[UIImage imageNamed:@"filter_close_green.png"] forState:UIControlStateNormal];
    [close_btn addTarget:self action:@selector(filterClosed:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIImageView *line_img=[[UIImageView alloc]initWithFrame:CGRectMake(10, 53, pop_view.frame.size.width-20,3 )];
    line_img.backgroundColor=[UIColor colorWithRed:0.17 green:0.70 blue:0.34 alpha:1.0];
    
    UIButton *appRelatedBtn=[[UIButton alloc]initWithFrame:CGRectMake(5 ,80, pop_view.frame.size.width - 10 , 50)];
    [appRelatedBtn setTitle:@"For AveriGo (app-related)" forState:UIControlStateNormal];
    [appRelatedBtn setBackgroundColor:[UIColor colorWithRed:0.22 green:0.46 blue:0.24 alpha:1.0]];
    [appRelatedBtn.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:15.f]];
    [appRelatedBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [appRelatedBtn addTarget:self action:@selector(onAppRelatedClick:) forControlEvents:UIControlEventTouchUpInside];
    appRelatedBtn.layer.cornerRadius=5;
    appRelatedBtn.clipsToBounds=YES;
    
    UIButton *productRelatedBtn=[[UIButton alloc]initWithFrame:CGRectMake(5 , 160, pop_view.frame.size.width - 10 , 50)];
    [productRelatedBtn setTitle:@"For Vending Operator (product-related)" forState:UIControlStateNormal];
    [productRelatedBtn setBackgroundColor:[UIColor colorWithRed:0.22 green:0.46 blue:0.24 alpha:1.0]];
    [productRelatedBtn.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:15.f]];
    [productRelatedBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [productRelatedBtn addTarget:self action:@selector(onProductRelatedClick:) forControlEvents:UIControlEventTouchUpInside];
    productRelatedBtn.layer.cornerRadius=5;
    productRelatedBtn.clipsToBounds=YES;
    
    [pop_view addSubview:close_btn];
    [pop_view addSubview:line_img];
    [pop_view addSubview:name_];
    [pop_view addSubview:appRelatedBtn];
    [pop_view addSubview:productRelatedBtn];
    [[KGModal sharedInstance] showWithContentView:pop_view andAnimated:YES];
    [KGModal sharedInstance].tapOutsideToDismiss=NO;
}

- (void)openMessageBox {
    pop_view = [[UIView alloc] initWithFrame:CGRectMake(0, 0,self.view.frame.size.width-20, 380)];
    pop_view.backgroundColor     = [UIColor whiteColor];//[[UIColor whiteColor] colorWithAlphaComponent:0.20f];
    pop_view.layer.cornerRadius  = 5.0;
    pop_view.layer.masksToBounds = YES;
    UILabel *name_=[[UILabel alloc]initWithFrame:CGRectMake(20,15,pop_view.frame.size.width-40, 40)];
    name_.text=FEEDBACK;
    name_.font=[UIFont fontWithName:FONT_BOLD size:20.f];
    name_.textAlignment=NSTextAlignmentCenter;
    name_.textColor=[UIColor colorWithRed:0.17 green:0.24 blue:0.31 alpha:1.0];
    
    UIButton *close_btn=[[UIButton alloc]initWithFrame:CGRectMake(10, 10, 32, 32)];
    [close_btn setImage:[UIImage imageNamed:@"filter_close_green.png"] forState:UIControlStateNormal];
    [close_btn addTarget:self action:@selector(filterClosed:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIImageView *line_img=[[UIImageView alloc]initWithFrame:CGRectMake(10, 53, pop_view.frame.size.width-20,3 )];
    line_img.backgroundColor=[UIColor colorWithRed:0.17 green:0.70 blue:0.34 alpha:1.0];
    
    
    UILabel *title_lbl=[[UILabel alloc]initWithFrame:CGRectMake(5,60,pop_view.frame.size.width , 40)];
    title_lbl.text=@"Send Feedback";
    title_lbl.font=[UIFont fontWithName:FONT_REGULAR size:17.f];
    title_lbl.textAlignment=NSTextAlignmentLeft;
    title_lbl.textColor=[UIColor colorWithRed:0.17 green:0.24 blue:0.31 alpha:1.0];
    
    
    feedback_txt=[[UITextView alloc]initWithFrame:CGRectMake(5, 110,pop_view.frame.size.width-10, 180)];
    feedback_txt.layer.cornerRadius=5;
    feedback_txt.clipsToBounds=YES;
    feedback_txt.layer.borderColor=[UIColor colorWithRed:0.17 green:0.70 blue:0.34 alpha:1.0].CGColor;
    feedback_txt.layer.borderWidth=3.0f;
    feedback_txt.font=[UIFont fontWithName:FONT_REGULAR size:17.f];
    feedback_txt.textColor=[UIColor colorWithRed:0.17 green:0.24 blue:0.31 alpha:1.0];
    feedback_txt.delegate=self;
    
    toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    toolBar.barStyle = UISearchBarStyleDefault;
    UIBarButtonItem *btnDone = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(btnClickedDone:)];
    [toolBar setItems:[NSArray arrayWithObject:btnDone]];
    
    UIButton *add_btn=[[UIButton alloc]initWithFrame:CGRectMake(pop_view.frame.size.width/2-100 , 300, 200 , 30)];
    [add_btn setTitle:@"Submit" forState:UIControlStateNormal];
    [add_btn setBackgroundColor:[UIColor colorWithRed:0.22 green:0.46 blue:0.24 alpha:1.0]];
    [add_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    // [add_btn addTarget:self action:@selector(onConfirmClick:) forControlEvents:UIControlEventTouchUpInside];
    add_btn.layer.cornerRadius=5;
    add_btn.clipsToBounds=YES;
    
    [pop_view addSubview:title_lbl];
    [pop_view addSubview:close_btn];
    [pop_view addSubview:line_img];
    [pop_view addSubview:feedback_txt];
    [pop_view addSubview:name_];
    [pop_view addSubview:add_btn];
    [[KGModal sharedInstance] showWithContentView:pop_view andAnimated:YES];
    [KGModal sharedInstance].tapOutsideToDismiss=NO;
    
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    [textView setInputAccessoryView:toolBar];
    return YES;
}

- (IBAction)btnClickedDone:(id)sender {
    [feedback_txt resignFirstResponder];
}

- (IBAction)filterClosed:(id)sender {
    [[KGModal sharedInstance]hideAnimated:YES];
    
}

- (IBAction)onAppRelatedClick:(id)sender {
    [[KGModal sharedInstance]hideAnimated:YES];
    appRecipient = YES;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self openMessageBox];
    });
}

- (IBAction)onProductRelatedClick:(id)sender {
    [[KGModal sharedInstance]hideAnimated:YES];
    appRecipient = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self openMessageBox];
    });
}

- (void) receiveTestNotification:(NSNotification *) notification {
    [self InitNavBar];
}

- (void)getMicroMarkets {
    [[ActivityIndicator sharedInstance]showLoader];

    if(_UDID==nil)
    {
        _UDID=EMP_STR;
    }
    NSString *latitude;
    NSString *longitude;
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"Latitude"]!=nil)
    {
        latitude=[[NSUserDefaults standardUserDefaults]objectForKey:@"Latitude"];
        longitude=[[NSUserDefaults standardUserDefaults]objectForKey:@"Longitude"];
    }
    else
    {
        latitude=EMP_STR;
        longitude=EMP_STR;
    }
    NSDictionary *dictobject;
    dictobject=[self.userDetailsArray objectAtIndex:0];
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setObject:MICROMARKET forKey:PARAM_SERVICE_NAME_KEY];
    [dictParam setObject:latitude    forKey:@"Latitude"];
    [dictParam setObject:longitude   forKey:@"Longitude"];
    [dictParam setObject:EMP_STR     forKey:@"BeaconMajor"];
    [dictParam setObject:_UDID    forKey:@"BeaconMinor"];
    [dictParam setObject:[dictobject valueForKey:@"EMAIL_ID"] forKey:PARAM_EMAIL_ID_KEY];
    [[NSUserDefaults standardUserDefaults]setObject:_UDID  forKey:@"minor"];
    
    NSLog(@"Dict:%@",dictParam);
    
    NSString *rootURL =  [[AppDelegate shared] rootUrl];
    
    AFNHelper *afn=[[AFNHelper alloc]init];
    [afn postDataFromPath:METHOD_MICROMARKET getUrl:rootURL withParamData:dictParam withBlock:^(id response, NSError *error)
     {
         if(error)
         {
             
             [[NSNotificationCenter defaultCenter] postNotificationName:SCAN_NOTIFICATION object:HIDE];
             
             self.micromarket_view.hidden = YES;
             self.noMicroMarketLbl.hidden = NO;
             
         }
         else
         {
             NSDictionary *dictionary;
             dictionary=response;
             NSLog(@"response %@",response);
             [objDBHelper deleteBeacon];
             NSString *status=[dictionary valueForKey:STATUS];
             if([status isEqualToString:SUCCESS])
             {
                 self.microMarketArray=[[NSMutableArray alloc]init];
                 self.microMarketPrefArray = [[NSMutableArray alloc]init];
                 self.micromarket_compare_array=[dictionary valueForKey:@"Micro Market List"];
                 if(self.micromarket_compare_array.count>0)
                 {
                     self.canLoad = @"true";
                     self.micromarket_view.hidden = NO;
                     self.noMicroMarketLbl.hidden = YES;
                     [[NSNotificationCenter defaultCenter] postNotificationName:SCAN_NOTIFICATION object:@"home"];
                     // [[NSNotificationCenter defaultCenter] postNotificationName:SCAN_NOTIFICATION object:SHOW];
                     for(int i=0;i<self.micromarket_compare_array.count;i++)
                     {
                         NSDictionary *dictobject;
                         dictobject=[self.micromarket_compare_array objectAtIndex:i];
                         
                         id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                         [tracker set:kGAIScreenName value:[NSString stringWithFormat:@"%@ %@ (%@)",[[NSUserDefaults standardUserDefaults]objectForKey:@"firstname"],[[NSUserDefaults standardUserDefaults]objectForKey:@"lastname"],[dictobject valueForKey:@"LOCATION_DESC"]]];
                         [tracker set:kGAIUserId value:[NSString stringWithFormat:@"%@ %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"firstname"],[[NSUserDefaults standardUserDefaults]objectForKey:@"lastname"]]];
                         [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
                         GAIDictionaryBuilder *builder = [GAIDictionaryBuilder createScreenView];
                         [builder set:@"start" forKey:kGAISessionControl];
                         [tracker set:kGAIScreenName value:[NSString stringWithFormat:@"%@ %@ (%@)",[[NSUserDefaults standardUserDefaults]objectForKey:@"firstname"],[[NSUserDefaults standardUserDefaults]objectForKey:@"lastname"],[dictobject valueForKey:@"LOCATION_DESC"]]];
                         [tracker send:[builder build]];
                         NSLog(@" %@", kGAIScreenName );
                         
                         if(i==0)
                         {
                             [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@-%@",[dictobject valueForKey:@"BEACONMAJOR"],[dictobject valueForKey:@"BEACONMINOR"]] forKey:@"LOCATION_BEACON"];
                             [[NSUserDefaults standardUserDefaults] setObject:[dictobject valueForKey:@"APP_RELATED_MAILID"] forKey:@"apprelatedmailid"];
                             [[NSUserDefaults standardUserDefaults] setObject:[dictobject valueForKey:@"PRODUCT_RELATED_MAILID_LIST"] forKey:@"productrelatedmailid"];
                             [[NSUserDefaults standardUserDefaults]objectForKey:@"LOCATION_BEACON"];
                             [[NSUserDefaults standardUserDefaults] setObject:[dictobject valueForKey:@"SALESTAX"] forKey:@"salestax"];
                             [[NSUserDefaults standardUserDefaults]setValue:[dictobject valueForKey:@"LOCATION_ID"] forKey:@"micromarketid"];
                             [[NSUserDefaults standardUserDefaults]setValue:[dictobject valueForKey:@"CATEGORY_ID"] forKey:@"featuredCategoryId"];
                             [[NSUserDefaults standardUserDefaults]setValue:[dictobject valueForKey:@"BEACONMAJOR"] forKey:@"beaconMajor"];
                             [[NSUserDefaults standardUserDefaults]setValue:[dictobject valueForKey:@"BEACONMINOR"] forKey:@"beaconMinor"];
                             [[NSUserDefaults standardUserDefaults]setValue:[dictobject valueForKey:@"COMPANYNAME"] forKey:@"serviceName"];
                             [[NSUserDefaults standardUserDefaults] setValue:[dictobject valueForKey:@"BANNER_IMAGE_NAME"] forKey:@"featuredItemName"];
                             [[NSUserDefaults standardUserDefaults] setValue:[dictobject valueForKey:@"PAYROLL_STATUS"] forKey:@"payrollstatus"];
                             [[NSUserDefaults standardUserDefaults] setValue:[dictobject valueForKey:@"PAY_USER_ID"] forKey:@"payuserid"];
                             [[NSUserDefaults standardUserDefaults] setValue:[dictobject valueForKey:@"CREDIT_STATUS"] forKey:@"creditstatus"];
                             [[NSUserDefaults standardUserDefaults] setValue:[dictobject valueForKey:@"CREDIT_USER_ID"] forKey:@"credituserid"];
                             [[NSUserDefaults standardUserDefaults]setValue:[dictobject valueForKey:@"ISFEATURED"]  forKey:@"featured"];
                             [[NSUserDefaults standardUserDefaults]setObject:self.micromarket_compare_array forKey:@"micromarketarray"];
                         }
                         
                         NSString *beaconvalue = [NSString stringWithFormat:@"%@-%@",[dictobject valueForKey:@"BEACONMAJOR"],[dictobject valueForKey:@"BEACONMINOR"]];
                         NSString *txtBannerDiscount = [dictobject valueForKey:@"BANNER_TEXT"];
                         
                         [objDBHelper insertBeacons:beaconvalue andDistance:[NSString stringWithFormat:@"%@",[dictobject valueForKey:@"Distance"]] andMicroMarketID:[dictobject valueForKey:@"LOCATION_ID"] andStateName:[dictobject valueForKey:@"STATE_NAME"] andAddress:[dictobject valueForKey:@"LOC_ADD"] andLongitude:[dictobject valueForKey:@"LONGITUDE"] andZip:[dictobject valueForKey:@"LOC_ZIP"] andImage:[dictobject valueForKey:@"LOC_IMAGE"] andDescription:[dictobject valueForKey:@"LOCATION_DESC"] andCountry:[dictobject valueForKey:@"COUNTRY_NAME"] andLatitude:[dictobject valueForKey:@"LATITUDE"] andOrderBy:@"1" andCity:[dictobject valueForKey:@"LOC_CITY"] andSalesTax:[dictobject valueForKey:@"SALESTAX"] andCompany:[dictobject valueForKey:@"COMPANYNAME"] andBannerImage:[dictobject valueForKey:@"BANNER_IMAGE"] andBannerText:txtBannerDiscount andisFeatured:[dictobject valueForKey:@"ISFEATURED"] andcategoryId:[dictobject valueForKey:@"CATEGORY_ID"] andPayrollStatus:[dictobject valueForKey:@"PAYROLL_STATUS"] andPayUserId:[dictobject valueForKey:@"PAY_USER_ID"] andHasBestSelling:@"" andCreditStatus:[dictobject valueForKey:@"CREDIT_STATUS"] andCreditUserId:[dictobject valueForKey:@"CREDIT_USER_ID"]];
                     }
                     [[NSUserDefaults standardUserDefaults]setObject:@"micromarket" forKey:@"type"];
                     self.microMarketArray = [objDBHelper getBeacons];
                     [self.microMarketPrefArray addObject:[self.microMarketArray objectAtIndex:0]];
                     [[NSUserDefaults standardUserDefaults]setObject:self.microMarketPrefArray forKey:@"micromarketarray"];
                     [self.micromarket_view reloadData];
                 }
                 else
                 {
                     self.micromarket_view.hidden = YES;
                     self.noMicroMarketLbl.hidden = NO;
                     [[NSNotificationCenter defaultCenter] postNotificationName:SCAN_NOTIFICATION object:@"qrcode"];
                 }
                 
             }
             else
             {
                 self.micromarket_view.hidden = YES;
                 self.noMicroMarketLbl.hidden = NO;
                 [[NSNotificationCenter defaultCenter] postNotificationName:SCAN_NOTIFICATION object:@"hide"];
                 
             }
             
             noNetworkDidntLoad = NO;
         }
         
         [[ActivityIndicator sharedInstance] hideLoader];
     }];
}

- (void)getCheckoutStatus {
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setObject:@"3E11BA47A393B22F345F5C6FEC86C"  forKey:@"secret"];
    NSString * checoutTime = [NSString stringWithFormat:@"%ld",(long)[[NSUserDefaults standardUserDefaults]integerForKey:@"checkoutstamp"]];
    [dictParam setObject:checoutTime forKey:@"timestamp_client"];
    [dictParam setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"username"]         forKey:@"email_id"];
    NSLog(@"DICTPARAM:%@",dictParam);
    
    
    AFNHelper *afn=[[AFNHelper alloc]init];
    [afn postDataFromPath:@"" getUrl:@"https://vending.averiware.com/interface/public/index.php/transaction/check" withParamData:dictParam withBlock:^(id response, NSError *error)
     {
         if(error)
         {
             
         }
         else
         {
             NSLog(@"response %@",response);
             NSDictionary *dictionary;
             dictionary=response;
             NSString *status=[dictionary valueForKey:STATUS];
             if([status isEqualToString:SUCCESS])
             {
                 [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"checkoutstamp"];
                 NSString *transaction = [dictionary valueForKey:@"TransactionStatus"];
                 NSString *message = [dictionary valueForKey:@"Message"];
                 if([transaction isEqualToString:SUCCESS]){
                     [objDBHelper deleteCartByMicroMarket:[[NSUserDefaults standardUserDefaults]objectForKey:@"lastmicromarketid"]];
                     [self showSuccessAlert:message];
                 }else{
                     [AlertVC showAlertWithTitleDefault:APP_NAME message:message controller:self];
                 }
             }
             else
             {
                 [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"checkoutstamp"];
                 
             }
         }
         
     }];
}

- (void)showSuccessAlert:(NSString *)message {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:APP_NAME message:message preferredStyle:UIAlertControllerStyleAlert];
    NSMutableAttributedString *titleAttr = [[NSMutableAttributedString alloc] initWithString:APP_NAME];
    [titleAttr addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:FONT_MEDIUM size:20]
                      range:NSMakeRange(0, [titleAttr length])];
    
    [alertController setValue:titleAttr forKey:@"attributedTitle"];
    
    NSMutableAttributedString *messageAttr = [[NSMutableAttributedString alloc] initWithString:message];
    [messageAttr addAttribute:NSFontAttributeName
                        value:[UIFont fontWithName:FONT_REGULAR size:14]
                        range:NSMakeRange(0, [messageAttr length])];
    
    
    [alertController setValue:messageAttr forKey:@"attributedMessage"];
    UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"OK"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action)
                                {
                                    self.tabBarController.selectedIndex = 4;
                                }];
    [alertController addAction:yesButton];
    [self presentViewController:alertController animated:YES completion:nil];
    
}

- (IBAction)helpCenter:(id)sende {
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"HelpController"]];
    //popupController.style = STPopupStyleBottomSheet;
    [popupController presentInViewController:self];
}

#pragma mark - STPopupControllerTransitioning

- (NSTimeInterval)popupControllerTransitionDuration:(STPopupControllerTransitioningContext *)context
{
    return context.action == STPopupControllerTransitioningActionPresent ? 0.5 : 0.35;
}

- (void)popupControllerAnimateTransition:(STPopupControllerTransitioningContext *)context completion:(void (^)(void))completion
{
    UIView *containerView = context.containerView;
    if (context.action == STPopupControllerTransitioningActionPresent) {
        containerView.transform = CGAffineTransformMakeTranslation(containerView.superview.bounds.size.width - containerView.frame.origin.x, 0);
        
        [UIView animateWithDuration:[self popupControllerTransitionDuration:context] delay:0 usingSpringWithDamping:1 initialSpringVelocity:1 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            context.containerView.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished) {
            completion();
        }];
    }
    else {
        [UIView animateWithDuration:[self popupControllerTransitionDuration:context] delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            containerView.transform = CGAffineTransformMakeTranslation(- 2 * (containerView.superview.bounds.size.width - containerView.frame.origin.x), 0);
        } completion:^(BOOL finished) {
            containerView.transform = CGAffineTransformIdentity;
            completion();
        }];
    }
}

- (BOOL)connected {
    ReachabilityStatus status = [GLobalRealReachability currentReachabilityStatus];
    NSLog(@"Initial reachability status:%@",@(status));
    
    return status != RealStatusNotReachable;
}

- (void)networkChanged:(NSNotification *)notification {
    RealReachability *reachability = (RealReachability *)notification.object;
    ReachabilityStatus status = [reachability currentReachabilityStatus];
    if (status != RealStatusNotReachable){
        if(noNetworkDidntLoad){
            [self getMicroMarkets];
        }
    }
    NSLog(@"currentStatus:%@",@(status));
}

-(void)setScreenAnalytics {
    NSString * SessionGUID = [[NSUserDefaults standardUserDefaults] objectForKey:@"SessionGUID"];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString * checkInTime=[dateFormatter stringFromDate:[NSDate date]];
    NSString * micromarketid = [[NSUserDefaults standardUserDefaults] valueForKey:@"micromarketid"];
    NSString * micromarketname = [[NSUserDefaults standardUserDefaults] valueForKey:@"micromarketname"];
    NSString * userID = [[NSUserDefaults standardUserDefaults] objectForKey:@"username"];
    NSString * analyticsCount = [objDBHelper getScreenAnalyticsBySessionID:SessionGUID andScreenName:@"Home"];
    int visitCount = [analyticsCount intValue] +1;
    if(visitCount == 1) {
        [objDBHelper insertScreenAnalytics:userID andsessionGUID:SessionGUID andMircoMarketID:[micromarketid intValue] andMicroMarketName:micromarketname andScreenName:@"Home" andscreenVisits:visitCount andCheckInTime:checkInTime andCheckOutTime:@"" andSyncFlag:@"Y"];
    } else {
        [objDBHelper UpdateScreenAnalytics:SessionGUID andVisitCount:visitCount andScreenName:@"Home"];
    }
}

-(void)updateScreenAnalyticsCheckOutTime {
    
    NSString * SessionGUID = [[NSUserDefaults standardUserDefaults] objectForKey:@"SessionGUID"];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString * checkOutTime=[dateFormatter stringFromDate:[NSDate date]];
    [objDBHelper UpdateScreenAnalyticsCheckOutTime:SessionGUID andCheckOutTime:checkOutTime andScreenName:@"Home"];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillTerminateNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
}

-(void)viewWillDisappear:(BOOL)animated {
    [self updateScreenAnalyticsCheckOutTime];
    [super viewWillDisappear:true];
}

-(void)appWillTerminate:(NSNotification*)note
{
    [self updateScreenAnalyticsCheckOutTime];
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end


