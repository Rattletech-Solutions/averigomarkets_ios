//
//  HelpController.m
//  AveriGo Markets
//
//  Created by Rattletech on 30/10/18.
//  Copyright © 2018 BTS. All rights reserved.
//

#import "HelpController.h"
#import <STPopup/STPopup.h>

@interface HelpController ()

@end

@implementation HelpController


-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self){
        [self customInit];
    }
    return self;
}

- (instancetype)init
{
    if (self = [super init]) {
        self.contentSizeInPopup = CGSizeMake(self.view.frame.size.width , self.view.frame.size.height);
    }
    return self;
}

-(void)customInit{
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    self.contentSizeInPopup = CGSizeMake(screenWidth - 25, 260);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Help Center";
    
    self.helpBtn.layer.cornerRadius = 5;
    self.helpBtn.clipsToBounds = YES;
    
    self.feedbackBtn.layer.cornerRadius = 5;
    self.feedbackBtn.clipsToBounds = YES;
    
    NSString *appVersion = [NSString stringWithFormat:@"Version %@ (%@)",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"],[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]];
    NSString *deviceManufacturer = @"Apple Inc.";
    NSString *deviceModel = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"deviceModel"]];
    NSString *deviceOS = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"deviceOS"]];
    
    self.appVersionLbl.text = appVersion;
    self.manufacturerLbl.text = deviceManufacturer;
    self.modelLbl.text = deviceModel;
    self.osLbl.text = deviceOS;

}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear: animated];

    [self setScreenAnalytics];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillTerminate:) name:UIApplicationWillTerminateNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillTerminate:) name:UIApplicationDidEnterBackgroundNotification object:nil];
}

-(IBAction)helpclicked:(id)sender {
        [self.popupController pushViewController:[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"BrowserController"] animated:YES];
}


-(IBAction)feedbackClicked:(id)sender {
    [self.popupController pushViewController:[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"FeedbackCentreController"] animated:YES];
    
}

-(void)appWillTerminate:(NSNotification*)note
{
[self updateScreenAnalyticsCheckOutTime];
}





-(void)viewWillDisappear:(BOOL)animated
{
[self updateScreenAnalyticsCheckOutTime];
[super viewWillDisappear:true];
}



-(void)setScreenAnalytics {
    
    NSString * SessionGUID = [[NSUserDefaults standardUserDefaults] objectForKey:@"SessionGUID"];
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString * checkInTime=[dateFormatter stringFromDate:[NSDate date]];
    
    NSString * micromarketid = [[NSUserDefaults standardUserDefaults] valueForKey:@"micromarketid"];
    
    NSString * micromarketname = [[NSUserDefaults standardUserDefaults] valueForKey:@"micromarketname"];
    
    NSString * userID = [[NSUserDefaults standardUserDefaults] objectForKey:@"username"];
    
    
    
    NSString * analyticsCount = [objDBHelper getScreenAnalyticsBySessionID:SessionGUID andScreenName:@"Help"];
    
    int visitCount = [analyticsCount intValue] +1;
    
    
    
    if(visitCount == 1) {
        
        [objDBHelper insertScreenAnalytics:userID andsessionGUID:SessionGUID andMircoMarketID:[micromarketid intValue] andMicroMarketName:micromarketname andScreenName:@"Help" andscreenVisits:visitCount andCheckInTime:checkInTime andCheckOutTime:@"" andSyncFlag:@"Y"];
        
    } else {
        
        [objDBHelper UpdateScreenAnalytics:SessionGUID andVisitCount:visitCount andScreenName:@"Help"];
        
    }
    
    
    
}



-(void)updateScreenAnalyticsCheckOutTime {
    
    NSString * SessionGUID = [[NSUserDefaults standardUserDefaults] objectForKey:@"SessionGUID"];
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString * checkOutTime=[dateFormatter stringFromDate:[NSDate date]];
    
    [objDBHelper UpdateScreenAnalyticsCheckOutTime:SessionGUID andCheckOutTime:checkOutTime andScreenName:@"Help"];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillTerminateNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    
}


@end
