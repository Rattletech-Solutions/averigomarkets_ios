//
//  SignUpUtiltiy.m
//  AveriGo Markets
//
//  Created by Macmini on 11/14/18.
//  Copyright © 2018 BTS. All rights reserved.
//

#import "SignUpUtiltiy.h"
#import "AppDelegate.h"

@implementation SignUpUtiltiy
{
    int randomNumber;
    STPopupController *openPopUpController;
}

+ (instancetype)sharedInstance{
    static id sharedInstance;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        sharedInstance = [[[self class] alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init{
    if(!(self = [super init])){
        return nil;
    }
    
return self;
}

-(void)sendEmailThroughSendGrid:(int)randomNo andEmail:(NSString *)emailTxt andController:(STPopupController *)currentController{
    openPopUpController = currentController;
    NSString* randomString = [NSString stringWithFormat:@"%i", randomNumber];
    NSString *message = [NSString stringWithFormat: @"%s%s%s%@%s%s%s%s","<div style='font-size: 12pt; font-family: verdana; color: rgb(0, 0, 0); line-height: normal;'> ", "<p>Greetings!</p>", "<p>Here is your verification code: <br />", randomString,"</p>", "<p>Enter it on the AveriGo Markets app to continue the signup process.</p>" ,"<p>Sincerely,<br />", "The AveriGo Team</p></div>"];
    
    NSString *rootURL =  [[AppDelegate shared] rootUrl];
    
    AFNHelper *afn=[[AFNHelper alloc]init];
    [afn getDataFromPath:METHOD_MAILSERVER getUrl:rootURL withParamData:nil withBlock:^(id response, NSError *error) {
        
        if(error) {
            // [self.popupController dismiss];
            NSLog(@"ERROR DESC:%@",error.localizedDescription);
            [self showAlert:SERVER_BUSY];
        } else {
            NSDictionary *dictionary;
            dictionary=response;
            NSDictionary *messageDict = [dictionary valueForKey:MESSAGE];
            NSString *status=[dictionary valueForKey:STATUS];
            
            if([status isEqualToString:SUCCESS]) {
                SendGrid *sendgrid = [SendGrid apiUser:[messageDict valueForKey:@"mail_host_user"] apiKey:[messageDict valueForKey:@"mail_host_pass"]];
                SendGridEmail *email = [[SendGridEmail alloc] init];
                email.to      = emailTxt;
                email.from    = [messageDict valueForKey:@"fromEmail"];
                email.subject = @"AveriGo Markets Email Verification";
                email.html    = message;
                [sendgrid sendWithWeb:email];
            } else {
                //  [self.popupController dismiss];
                [self showAlert:SERVER_BUSY];
            }
        }
    }];
}

-(void)sendCode:(int)randomNo andMobile:(NSString *)mobileTxt andCountry:(NSString *)countryTxt andEmail:(NSString *)emailTxt andController:(STPopupController *)currentController{
    openPopUpController = currentController;
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    randomNumber = arc4random() % 9000 + 1000;
    // Formatting Phone Number
    NSString *formattedPhoneNum = [[mobileTxt componentsSeparatedByCharactersInSet:
                                    [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"]
                                     invertedSet]]
                                   componentsJoinedByString:EMP_STR];
    NSLog(@"formated String %@", formattedPhoneNum);
    NSString *concatPhoneNum = [NSString stringWithFormat: @"%@%@", countryTxt, formattedPhoneNum];
    NSLog(@"%@", concatPhoneNum);
    
    [dictParam setObject:[NSString stringWithFormat:@"%@",concatPhoneNum] forKey:@"Phone"];
    [dictParam setObject:[NSString stringWithFormat:@"%d", randomNumber] forKey:@"code"];
    [dictParam setObject:[NSString stringWithFormat:@"%@", @"R"] forKey:@"flag"];
    NSLog(@"%@", dictParam);
    
    NSString *url = [NSString stringWithFormat:AVERIWARE_TWILIO_URL];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [manager.requestSerializer setValue:APP_TYPE forHTTPHeaderField:CONTENT_TYPE];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    [manager POST:url parameters:dictParam progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
     {
         NSLog(@"%@",responseObject);
         
     }
          failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              NSLog(@"%@",error.localizedDescription);
              
              [[NSUserDefaults standardUserDefaults]setValue:emailTxt forKey:@"popEmail"];
              [[NSUserDefaults standardUserDefaults]setValue:formattedPhoneNum forKey:@"popMobile"];
              [[NSUserDefaults standardUserDefaults]setValue:countryTxt forKey:@"popCountry"];
              [[NSUserDefaults standardUserDefaults]setValue:[NSString stringWithFormat:@"%d", randomNumber] forKey:@"popCode"];
              [[NSUserDefaults standardUserDefaults]setValue:countryTxt forKey:@"popCountry"];
              
              [openPopUpController dismiss];
              [[NSNotificationCenter defaultCenter] postNotificationName:
               @"VerifyCorrectEmailId" object:nil userInfo:nil];
         
          }];
}

-(void)showAlert:(NSString *) message {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:APP_NAME message:message preferredStyle:UIAlertControllerStyleAlert];
    NSMutableAttributedString *titleAttr = [[NSMutableAttributedString alloc] initWithString:APP_NAME];
    [titleAttr addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:FONT_MEDIUM size:20]
                      range:NSMakeRange(0, [titleAttr length])];
    
    [alertController setValue:titleAttr forKey:@"attributedTitle"];
    
    NSMutableAttributedString *messageAttr = [[NSMutableAttributedString alloc] initWithString:message];
    [messageAttr addAttribute:NSFontAttributeName
                        value:[UIFont fontWithName:FONT_REGULAR size:14]
                        range:NSMakeRange(0, [messageAttr length])];
    
    
    [alertController setValue:messageAttr forKey:@"attributedMessage"];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alertController addAction:defaultAction];
    [openPopUpController.topViewController presentViewController:alertController animated:YES completion:nil];
}


@end
