//
//  AFNHelper.h
//  Created by Leni
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AFHTTPClient.h"

typedef void (^RequestCompletionBlock)(id response, NSError *error);

@interface AFNHelper : NSObject<NSURLConnectionDelegate>
{
    NSMutableData *webData;
    
    NSMutableData *_responseData;
    
    //for ASIRequest
    AFHTTPClient *client;
    
    NSString *strReqMethod;
    //blocks
    RequestCompletionBlock dataBlock;
}
@property(nonatomic,copy)NSString *strReqMethod;

- (id) initWithRequestMethod:(NSString *)method;

-(void)getDataFromURL:(NSString *)url withBody:(NSMutableDictionary *)dictBody withBlock:(RequestCompletionBlock)block;

-(void)getDataFromPath:(NSString *)path getUrl:(NSString *)URL withParamData:(NSMutableDictionary *)dictParam withBlock:(RequestCompletionBlock)block;

-(void)postDataFromPath:(NSString *)path getUrl:(NSString *)URL withParamData:(NSMutableDictionary *)dictParam withBlock:(RequestCompletionBlock)block;
-(void)postDataFromPath1:(NSString *)path getUrl:(NSString *)URL withParamData:(NSMutableDictionary *)dictParam withBlock:(RequestCompletionBlock)block;
-(void)postDataFromPath2:(NSString *)path getUrl:(NSString *)URL withParamData:(NSString *)dictParam withBlock:(RequestCompletionBlock)block;

-(void)postFormDataPath:(NSString *)path getUrl:(NSString *)URL withParamData:(NSMutableDictionary *)dictParam withBlock:(RequestCompletionBlock)block;

-(void)getDataFromPath:(NSString *)path withParamDataImage:(NSMutableDictionary *)dictParam andImage:(UIImage *)image andimage_name:(NSString *)image_name withBlock:(RequestCompletionBlock)block;

//-(void)callWebserviceWithMethod:(NSString *)method andBody:(NSString *)body;

@end
