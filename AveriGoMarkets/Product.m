//
//  Product.m
//  VendBeam
//
//  Created by LeniYadav on 20/05/18.
//  Copyright © 2018 BTS. All rights reserved.
//

#import "Product.h"

@implementation Product
-(instancetype)initWithName:(NSString *)name andQty: (NSString *)qty andReconciled: (NSString *)reconciled andMaxium:(int)max {
    if (!(self = [super init]))
        return nil;
    self.Name = name;
    self.Qty = qty;
    self.Reconciled = reconciled;
    self.Maximum = max;
    return self;
}

@end
