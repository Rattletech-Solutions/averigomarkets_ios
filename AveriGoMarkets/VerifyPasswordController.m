//
//  VerifyPasswordControllerViewController.m
//  AveriGo Markets
//
//  Created by Macmini on 11/14/18.
//  Copyright © 2018 BTS. All rights reserved.
//

#import "VerifyPasswordController.h"
#import <STPopup.h>
#import "IQKeyBoardManager/IQKeyboardReturnKeyHandler.h"
#import "AFNHelper.h"
#import "AFHTTPSessionManager.h"
#import "MBProgressHUD.h"
#import "Reachability.h"
#import "ActivityIndicator.h"
#import "SendGrid.h"
#import "countryCode.h"
#import "SignUpUtiltiy.h"
#import "KGModal.h"
#import "RealReachability.h"
#import "AppDelegate.h"

@interface VerifyPasswordController ()

@end

@implementation VerifyPasswordController

{
    IQKeyboardReturnKeyHandler *returnKeyHandler;
    NSString *imagePath;
    NSString *txtPhoneToPass;
    NSString *txtEmail;
    int  randomNo;
    NSString *concatPhoneNum;
    NSString    *registeredEmail;
    NSString    *registeredPhoneNumber;
    NSString    *isEmailRegistered;
    NSString    *isPhoneNumberRegistered;
    NSString    *otp;
    NSString    *udid;
    NSString    *username;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self){
        [self customInit];
    }
    return self;
}

- (instancetype)init
{
    if (self = [super init]) {
        self.contentSizeInPopup = CGSizeMake(375, 250);
    }
    return self;
}

-(void)customInit{
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    self.contentSizeInPopup = CGSizeMake(screenWidth - 10, 250);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Reset Password";
    imagePath = [NSString stringWithFormat:@"EMCCountryPickerController.bundle/%@", @"US" ];
    self.countryImg.image = [UIImage imageNamed:imagePath];
    [self setFieldBorder:self.emailTxt];
    [self setFieldBorder:self.mobileTxt];
    
    UITapGestureRecognizer *singleTapForCountryPicker = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(countryImgClicked)];
    singleTapForCountryPicker.numberOfTapsRequired = 1;
    [self.countryImg setUserInteractionEnabled:YES];
    [self.countryImg addGestureRecognizer:singleTapForCountryPicker];
    
    self.emailTxt.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"forgotUserName"];
    
    self.sendCodeBtn.layer.cornerRadius = 5;
    self.sendCodeBtn.clipsToBounds = YES;
}

-(void)setFieldBorder:(JVFloatLabeledTextField *)textField{
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1;
    NSLog(@"%f",textField.frame.size.height - borderWidth);
    NSLog(@"%f",textField.frame.size.width);
    NSLog(@"%f",textField.frame.size.height);
    border.borderColor = [UIColor lightGrayColor].CGColor;
    border.frame = CGRectMake(0, textField.frame.size.height - borderWidth, textField.frame.size.width + 120, textField.frame.size.height);
    border.borderWidth = borderWidth;
    [textField.layer addSublayer:border];
    textField.layer.masksToBounds = YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == self.countryField)
    {
        [self showCountryPicker];
        return NO;
    }
    else
    {
        return YES;
    }
}

-(void)countryImgClicked
{
    [self showCountryPicker];
}


-(void)showCountryPicker
{
    
    NSString * storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    EMCCountryPickerController * countryPicker = [storyboard instantiateViewControllerWithIdentifier:@"EMCCountryPickerController"];
    [self presentViewController:countryPicker animated:YES completion:nil];
    
    // default values
    countryPicker.showFlags = true;
    countryPicker.countryDelegate = self;
    countryPicker.drawFlagBorder = true;
    countryPicker.flagBorderColor = [UIColor grayColor];
    countryPicker.flagBorderWidth = 0.5f;
    
}

- (void)countryController:(id)sender didSelectCountry:(EMCCountry *)chosenCity
{
    [self dismissViewControllerAnimated:YES completion:nil];
    imagePath = [NSString stringWithFormat:@"EMCCountryPickerController.bundle/%@", chosenCity.countryCode];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"countryCode contains[c] %@ ",chosenCity.countryCode];
    countryCode *code = [[countryCode alloc] init];
    NSArray *filteredCountry = [code.getCoutries filteredArrayUsingPredicate:filter];
    NSLog(@"%@",filteredCountry[0][@"phoneNumber"]);
    
    self.countryField.text = filteredCountry[0][@"phoneNumber"];
    self.countryImg.image = [UIImage imageNamed:imagePath];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.tag  == 30)
    {
        NSString* totalString = [NSString stringWithFormat:@"%@%@",textField.text,string];
        
        if (range.length == 1) {
            // Delete button was hit.. so tell the method to delete the last char.
            textField.text = [self formatPhoneNumber:totalString deleteLastChar:YES];
        } else {
            textField.text = [self formatPhoneNumber:totalString deleteLastChar:NO ];
        }
        return false;
    }
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return NO;
}

-(NSString*) formatPhoneNumber:(NSString*) simpleNumber deleteLastChar:(BOOL)deleteLastChar {
    if(simpleNumber.length==0) return EMP_STR;
    // use regex to remove non-digits(including spaces) so we are left with just the numbers
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[\\s-\\(\\)]" options:NSRegularExpressionCaseInsensitive error:&error];
    simpleNumber = [regex stringByReplacingMatchesInString:simpleNumber options:0 range:NSMakeRange(0, [simpleNumber length]) withTemplate:EMP_STR];
    
    // check if the number is to long
    if(simpleNumber.length>10) {
        // remove last extra chars.
        simpleNumber = [simpleNumber substringToIndex:10];
    }
    
    if(deleteLastChar) {
        // should we delete the last digit?
        simpleNumber = [simpleNumber substringToIndex:[simpleNumber length] - 1];
    }
    
    // 123 456 7890
    // format the number.. if it's less then 7 digits.. then use this regex.
    if(simpleNumber.length<7)
        simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d+)"
                                                               withString:@"$1-$2"
                                                                  options:NSRegularExpressionSearch
                                                                    range:NSMakeRange(0, [simpleNumber length])];
    
    else   // else do this one..
        simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d{3})(\\d+)"
                                                               withString:@"$1-$2-$3"
                                                                  options:NSRegularExpressionSearch
                                                                    range:NSMakeRange(0, [simpleNumber length])];
    return simpleNumber;
}


-(IBAction)sendCode:(id)sender
{
    // Formatting Phone Number
    NSString *formattedPhoneNum = [[self.mobileTxt.text componentsSeparatedByCharactersInSet:
                                    [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"]
                                     invertedSet]]
                                   componentsJoinedByString:EMP_STR];
    NSString *trimmedEmail = [_emailTxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    self.emailTxt.text = trimmedEmail;
    
    if ((self.emailTxt.text.length > 0) && (formattedPhoneNum.length == 10) && (self.countryField.text.length <= 3))
    {
        [self showLoader];
         [self forgotPasswordAPI];
    }
    else
    {
        [AlertVC showAlertWithTitleForView:APP_NAME message:PLEASE_ENTER_VALID_MOBILE controller:self];
        
    }
    
}

-(void)forgotPasswordAPI
{
    if([self connected])
    {
        NSDate *today = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:DATEFORMAT_TYPE];
        NSString *currentTime = [dateFormatter stringFromDate:today];
        
        // Formatting Phone Number
        NSString *formattedPhoneNum = [[_mobileTxt.text componentsSeparatedByCharactersInSet:
                                        [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"]
                                         invertedSet]]
                                       componentsJoinedByString:EMP_STR];
        
        NSString *deviceUDID;
        deviceUDID = [[NSUserDefaults standardUserDefaults] objectForKey:UDID];
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setObject:_emailTxt.text         forKey:PARAM_EMAIL_ID_KEY];
        [dictParam setObject:formattedPhoneNum        forKey:PARAM_PHONENO_KEY];
        [dictParam setObject:deviceUDID               forKey:PARAM_DEVICE_UDID_KEY];
        [dictParam setObject:currentTime              forKey:PARAM_DEVICE_DATE_TIME_KEY];
        
        NSString *rootURL =  [[AppDelegate shared] rootUrl];

        AFNHelper *afn=[[AFNHelper alloc]init];
        [afn postDataFromPath: METHOD_FORGOT_MC getUrl:rootURL withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             if(error)
             {
                 NSLog(@"ERROR DESC:%@",error.localizedDescription);
                 [self hideLoader];
                
                 if(error.code == NSURLErrorTimedOut){
                     [AlertVC showAlertWithTitleForView:APP_NAME message:ERROR_TIMED_OUT controller:self];
                 }else if(error.code == NSURLErrorNotConnectedToInternet){
                     [AlertVC showAlertWithTitleForView:APP_NAME message:NO_INTERNET_CONNECTION controller:self];
                 }else{
                      [AlertVC showAlertWithTitleForView:APP_NAME message:REQUEST_TIMED_OUT controller:self];
                 }
             }
             else {
                 NSDictionary *dictionary;
                 dictionary=response;
                 NSLog(@"response %@",response);
                 
                 NSString *status      =[dictionary valueForKey:STATUS];
                 registeredEmail       = [dictionary valueForKey:@"registeredEmail"];
                 registeredPhoneNumber = [dictionary valueForKey:@"registeredPhone"];
                 isEmailRegistered = [dictionary valueForKey:@"isEmailRegistered"];//Y;
                 isPhoneNumberRegistered =[dictionary valueForKey:@"isPhoneNumberRegistered"];// N;
                 
                 if([status isEqualToString:SUCCESS])
                 {
                     [self sendMailToUser];
                 }
                 else
                 {
                    [self hideLoader];
                     [[NSNotificationCenter defaultCenter] postNotificationName:
                      @"IncorrectEmail" object:nil userInfo:nil];
                     
                 }
                 
             }
             
         }];
    }
    else
    {
        [self hideLoader];
        [AlertVC showAlertWithTitleForView:APP_NAME message:NO_INTERNET_CONNECTION controller:self];
    }
    
}

-(void)sendMailToUser
{
    if([self connected])
    {
        NSDate *today = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:DATEFORMAT_TYPE];
        NSString *currentTime = [dateFormatter stringFromDate:today];
        
        //Validate Email Address.
        NSString *emailRegEx = EMAIL_REG_EX ;
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
        username=registeredEmail;
        
        if ([emailTest evaluateWithObject:username] == YES)
        {
            udid=[[NSUserDefaults standardUserDefaults] objectForKey:UDID];
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
            [dictParam setObject:username         forKey:PARAM_EMAIL_ID_KEY];
            [dictParam setObject:MICROMARKET      forKey:PARAM_SERVICE_NAME_KEY];
            [dictParam setObject:udid             forKey:PARAM_DEVICE_UDID_KEY];
            [dictParam setObject:currentTime      forKey:PARAM_DEVICE_DATE_TIME_KEY];
            NSLog(@"username %@",dictParam);
            
            NSString *rootURL =  [[AppDelegate shared] rootUrl];

            AFNHelper *afn=[[AFNHelper alloc]init];
            [afn postDataFromPath:METHOD_CHANGEPASSWORD getUrl:rootURL withParamData:dictParam withBlock:^(id response, NSError *error)
             {
                 if(error)
                 {
                     if(error.code == NSURLErrorTimedOut){
                         [AlertVC showAlertWithTitleForView:APP_NAME message:ERROR_TIMED_OUT controller:self];
                     }else if(error.code == NSURLErrorNotConnectedToInternet){
                         [AlertVC showAlertWithTitleForView:APP_NAME message:NO_INTERNET_CONNECTION controller:self];
                     }else{
                         [AlertVC showAlertWithTitleForView:APP_NAME message:REQUEST_TIMED_OUT controller:self];
                     }
                 }
                 else
                 {
                     NSDictionary *dictionary;
                     dictionary=response;
                     NSString *status=[dictionary valueForKey:STATUS];
                     
                     if([status isEqualToString:SUCCESS])
                     {
                         otp = [dictionary valueForKey:PARAM_OTP];
                         
                         if ([isEmailRegistered  isEqual: @"Y"] && [isPhoneNumberRegistered  isEqual: @"N"])
                         {
                             NSString *htmlbody = [NSString stringWithFormat: @"%s%@%s%@%s","<div style='font-size: 12pt; font-family: verdana; color: rgb(0, 0, 0); line-height: normal;'> <p>Greetings!</p><p>We received a request to reset your AveriGo Markets account password.</p><p>Enter this One-Time Password on the AveriGo Markets app, and then reset your password:</p><p><b>", otp,"</b> and the registered mobile number is <b>", registeredPhoneNumber, "</b></p><p>(This One-Time Password will only be valid for the next 4 hours)</p><p>Sincerely,<br/>The AveriGo Team</p></div>"];
                             SendGrid *sendgrid = [SendGrid apiUser:[dictionary valueForKey:@"mail_host_user"] apiKey:[dictionary valueForKey:@"mail_host_pass"]];
                             SendGridEmail *email = [[SendGridEmail alloc] init];
                             email.to       = username;
                             email.from     = [dictionary valueForKey:@"fromEmail"];
                             email.subject  = [dictionary valueForKey:@"subject"];
                             email.html     = htmlbody;
                             email.text     = htmlbody;
                             [sendgrid sendWithWeb:email];
                             //self.username_field.text = registeredEmail;
                             [self hideLoader];
                             //[AlertVC showAlertWithTitleForView:APP_NAME message:ONE_TIME_PASSWORD_SENT controller:self];
                             [[NSNotificationCenter defaultCenter] postNotificationName:
                              @"ResetPassword" object:nil userInfo:nil];
                             //[self.popupController dismiss];
                         }
                         else if ([isEmailRegistered  isEqual: @"N"] && [isPhoneNumberRegistered  isEqual: @"Y"])
                         {
                             [self sendSMSCode];
                             [self hideLoader];
                         }
                         else
                         {
                             otp = [dictionary valueForKey:PARAM_OTP];
                             NSString *htmlbody = [NSString stringWithFormat: @"%s%@%s","<div style='font-size: 12pt; font-family: verdana; color: rgb(0, 0, 0); line-height: normal;'> <p>Greetings!</p><p>We received a request to reset your AveriGo Markets account password.</p><p>Enter this One-Time Password on the AveriGo Markets app, and then reset your password:</p><p><b>", otp,"</b></p><p>(This One-Time Password will only be valid for the next 4 hours)</p><p>Sincerely,<br/>The AveriGo Team</p></div>"];
                             SendGrid *sendgrid = [SendGrid apiUser:[dictionary valueForKey:@"mail_host_user"] apiKey:[dictionary valueForKey:@"mail_host_pass"]];
                             SendGridEmail *email = [[SendGridEmail alloc] init];
                             email.to       = registeredEmail;
                             email.from     = [dictionary valueForKey:@"fromEmail"];
                             email.subject  = [dictionary valueForKey:@"subject"];
                             email.html     = htmlbody;
                             email.text     = htmlbody;
                             [sendgrid sendWithWeb:email];
                             //self.username_field.text = registeredEmail;
                             NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
                             // Formatting Phone Number
                             NSString *formattedPhoneNum = [[_mobileTxt.text componentsSeparatedByCharactersInSet:
                                                             [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"]
                                                              invertedSet]]
                                                            componentsJoinedByString:EMP_STR];
                             NSLog(@"formated String %@", formattedPhoneNum);
                             
                             NSString *concatPhoneNum = [NSString stringWithFormat: @"%@%@", self.countryField.text, formattedPhoneNum];
                             NSLog(@"%@", concatPhoneNum);
                             NSString *concatPhoneEmail = [NSString stringWithFormat: @"%@", otp];
                             [dictParam setObject:[NSString stringWithFormat:@"%@",concatPhoneNum] forKey:@"Phone"];
                             [dictParam setObject:[NSString stringWithFormat:@"%@", concatPhoneEmail] forKey:@"code"];
                             [dictParam setObject:[NSString stringWithFormat:@"%@", @"F"] forKey:@"flag"];
                             
                             NSString *url = [NSString stringWithFormat:AVERIWARE_TWILIO_URL];
                             AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
                             [manager.requestSerializer setValue:APP_TYPE forHTTPHeaderField:CONTENT_TYPE];
                             manager.requestSerializer = [AFHTTPRequestSerializer serializer];
                             NSLog(@"Parameters:%@", dictParam);
                             
                             [manager POST:url parameters:dictParam progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
                              {
                                  NSLog(@"%@",responseObject);
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
                              {
                                  NSLog(@"%@",error.localizedDescription);
                                  [self hideLoader];
                                  [[NSNotificationCenter defaultCenter] postNotificationName:
                                   @"ResetPassword" object:nil userInfo:nil];
                                  //[AlertVC showAlertWithTitleForView:APP_NAME message:ONE_TIME_PASSWORD_SENT controller:self];
                                  //[self.popupController dismiss];
                              }];
                             
                         }
                         
                     }
                     else
                     {
                         NSString *message=[dictionary valueForKey:MESSAGE];
                         [AlertVC showAlertWithTitleForView:APP_NAME message:message controller:self];
                         //self.username_field.text = emailField.text;
                     }
                     
                 }
                 
             }];
        }
        else
        {
            [AlertVC showAlertWithTitleForView:APP_NAME message:PLEASE_FILL_EMAIL  controller:self];
        }
    }
    else
    {
        [AlertVC showAlertWithTitleForView:APP_NAME message:NO_INTERNET_CONNECTION  controller:self];
    }
    
}

-(void)sendSMSCode
{
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    // Formatting Phone Number
    NSString *formattedPhoneNum = [[_mobileTxt.text componentsSeparatedByCharactersInSet:
                                    [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"]
                                     invertedSet]]
                                   componentsJoinedByString:EMP_STR];
    NSString *concatPhoneNum = [NSString stringWithFormat: @"%@%@", _countryField.text, formattedPhoneNum];
    
    NSString *concatPhoneEmail = [NSString stringWithFormat: @"%@%s%@", otp, " and the registered email address is " ,registeredEmail];
    [dictParam setObject:[NSString stringWithFormat:@"%@",concatPhoneNum] forKey:@"Phone"];
    [dictParam setObject:[NSString stringWithFormat:@"%@", concatPhoneEmail] forKey:@"code"];
    [dictParam setObject:[NSString stringWithFormat:@"%@", @"F"] forKey:@"flag"];
    
    NSString *url = [NSString stringWithFormat:AVERIWARE_TWILIO_URL];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [manager.requestSerializer setValue:APP_TYPE forHTTPHeaderField:CONTENT_TYPE];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    [manager POST:url parameters:dictParam progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
     {
         NSLog(@"%@",responseObject);
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
     {
         NSLog(@"%@",error.localizedDescription);
         [self hideLoader];
         //[AlertVC showAlertWithTitleForView:APP_NAME message:ONE_TIME_PASSWORD_SENT controller:self];
         [[NSNotificationCenter defaultCenter] postNotificationName:
          @"ResetPassword" object:nil userInfo:nil];
         //[self.popupController dismiss];
     }];
}

- (BOOL)connected
{
    ReachabilityStatus status = [GLobalRealReachability currentReachabilityStatus];
    NSLog(@"Initial reachability status:%@",@(status));
    
    return status == 1 || status == 2;
}

-(void)hideLoader{
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [[ActivityIndicator sharedInstance] hideLoader];
        });
    });
}

-(void)showLoader{
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [[ActivityIndicator sharedInstance] showLoader];
        });
    });
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
