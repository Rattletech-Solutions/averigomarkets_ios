//
//  ShoppingCartViewController.h
//  Averigo
//
//  Created by BTS on 22/11/16.
//  Copyright © 2016 BTS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreLocation/CoreLocation.h>
#import <QuartzCore/QuartzCore.h>
#import "DataBaseHandler.h"
#import "UIImage+ImageCompress.h"
#import <ZXingObjC/ZXingObjC.h>


@interface ShoppingCartViewController : UIViewController<UIGestureRecognizerDelegate,UITableViewDelegate,UITableViewDataSource,ZXCaptureDelegate>
{
    int scancount;
    DataBaseHandler *objDBHelper;
}

@property (strong,nonatomic) IBOutlet UITableView *cart_tableview;
@property (strong,nonatomic) NSMutableArray *demo_title;
@property (strong,nonatomic) NSMutableArray *dollar_price;
@property (strong,nonatomic) NSMutableArray *demo_images;
@property (strong,nonatomic) NSMutableArray *validate_array;
@property (strong,nonatomic) IBOutlet UIButton *checkout_btn;
@property (strong,nonatomic) IBOutlet UIButton *scan_btn;
@property (strong,nonatomic)  IBOutlet UIButton *add_btn;
@property (strong,nonatomic)  IBOutlet UITextField *barcode_txt;
@property (strong,nonatomic) NSMutableArray *cart_array;

@property (strong,nonatomic) IBOutlet UIView *scan_preview;
@property (nonatomic, strong)  UIView *scanRectView;
@property (nonatomic, strong) ZXCapture *capture;
@property (nonatomic) BOOL scanning;
@property (nonatomic) BOOL isFirstApplyOrientation;

@property (nonatomic,strong) AVCaptureSession *captureSession;
@property (nonatomic,strong) AVCaptureVideoPreviewLayer *videoPreviewLayer;
@property (nonatomic,strong) AVAudioPlayer *audioPlayer;
@property (nonatomic,strong) AVCapturePhotoOutput *stillImageOutput;
@property (strong,nonatomic) IBOutlet UILabel *total_lbl;
@property (strong,nonatomic) NSArray *temp_array;
@property (strong,nonatomic) NSString *micromarketid;
@property (strong,nonatomic)  NSMutableArray  *beacons;
@property (strong,nonatomic)  NSString *UDID;


@property (nonatomic,strong) NSLayoutConstraint *CollectionTopConstraint;
@property (nonatomic,strong) NSLayoutConstraint *DescconstraintHeight;
@property (nonatomic,strong) NSLayoutConstraint *DescconstraintWidth;
@property (nonatomic,strong) NSLayoutConstraint *CollectionconstraintHeight;
@property (nonatomic,strong) NSLayoutConstraint *LabelWidthConstraint;


@property (nonatomic) BOOL isReading;
@property (nonatomic) BOOL fromItemView;

-(IBAction)onOpenScan:(id)sender;
-(IBAction)onAddClicked:(id)sender;
@end
