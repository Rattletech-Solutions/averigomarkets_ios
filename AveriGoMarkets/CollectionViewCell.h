//
//  CollectionViewCell.h
//  Averigo
//
//  Created by BTS on 09/11/16.
//  Copyright © 2016 BTS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCell : UICollectionViewCell

@property(strong,nonatomic) IBOutlet UIImageView *cell_img;
@property(strong,nonatomic) IBOutlet UILabel *cell_title;

@end
