//
//  InitialViewController.m
//  Averigo
//
//  Created by BTS on 24/05/17.
//  Copyright © 2017 BTS. All rights reserved.
//

#import "InitialViewController.h"
#import "TabBarViewController.h"
#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "IntroViewController.h"
#import "UserDefaultHelper.h"

@interface InitialViewController ()

@end

@implementation InitialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear: animated];

    NSString *Intro_Viewed=[[UserDefaultHelper sharedObject]verifyStringValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"IntroViewed"]];
    NSString * Signed_In = [[NSUserDefaults standardUserDefaults]objectForKey:@"signedin"];

    if([Intro_Viewed isEqualToString:@"Y"])
    {
        if(Signed_In == nil)
        {

                LoginViewController *vc;
                vc=[self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:vc animated:YES];

        }

       else
        {

            if([[NSUserDefaults standardUserDefaults]objectForKey:@"otpscreen"]!=nil){
                if([[[NSUserDefaults standardUserDefaults]objectForKey:@"otpscreen"] isEqual:@"true"]){
                    LoginViewController *vc;
                    vc=[self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                    [self.navigationController pushViewController:vc animated:YES];
                }else{
                    TabBarViewController *vc;
                    vc=[self.storyboard instantiateViewControllerWithIdentifier:@"TabBarViewController"];
                    vc.navigationItem.hidesBackButton=NO;
                    vc.load=@"yes";
                    [[NSUserDefaults standardUserDefaults]setObject:@"micromarket" forKey:@"type"];
                    [self.navigationController pushViewController:vc animated:YES];
                }
            }else{
            TabBarViewController *vc;
            vc=[self.storyboard instantiateViewControllerWithIdentifier:@"TabBarViewController"];
            vc.navigationItem.hidesBackButton=NO;
            vc.load=@"yes";
            [[NSUserDefaults standardUserDefaults]setObject:@"micromarket" forKey:@"type"];
            [self.navigationController pushViewController:vc animated:YES];
            }
        }

    }
    else
    {
        IntroViewController *vc;
        vc = [self.storyboard instantiateViewControllerWithIdentifier:INTRO_VC];
        vc.modalPresentationStyle = UIModalPresentationFullScreen;
        [self presentViewController:vc animated:YES completion:nil];

    }
     
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
