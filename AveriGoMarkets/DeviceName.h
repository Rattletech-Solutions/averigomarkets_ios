//
//  DeviceName.h
//  VendBeam
//
//  Created by BTS on 24/08/18.
//  Copyright © 2018 BTS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeviceName : NSObject
- (NSString*) deviceModel;
@end
