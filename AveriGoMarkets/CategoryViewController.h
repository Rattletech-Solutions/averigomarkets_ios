    //
    //  CategoryViewController.h
    //  Averigo
    //
    //  Created by BTS on 09/11/16.
    //  Copyright © 2016 BTS. All rights reserved.
    //

#import <UIKit/UIKit.h>
#import "DataBaseHandler.h"
#import "UIImage+FontAwesome.h"

@interface CategoryViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,UISearchBarDelegate,UIGestureRecognizerDelegate>
{
    DataBaseHandler *objDBHelper;
}

@property(strong,nonatomic) IBOutlet UICollectionView *category_view;
@property(strong,nonatomic) NSMutableArray *demoImages;
@property(strong,nonatomic) NSMutableArray *demoTitle;
@property(strong,nonatomic) NSArray *categoryArray;
@property(strong,nonatomic) NSMutableArray *itemsArray;
@property(strong,nonatomic) IBOutlet UILabel *no_lbl;
@property(strong,nonatomic) NSMutableArray *cartArray;
@property(strong,nonatomic) IBOutlet UIButton *button;
@property(strong,nonatomic) NSString *fromMicroMarket;
@property(strong,nonatomic) NSMutableArray *filterArray;
@property(strong,nonatomic) NSString *microMarketId;
@property(strong,nonatomic) NSMutableArray  *beacons;
@property(strong,nonatomic) NSString *UDID;

@end
