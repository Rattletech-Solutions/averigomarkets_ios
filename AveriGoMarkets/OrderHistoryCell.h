//
//  OrderHistoryCell.h
//  AveriGo Markets
//
//  Created by Rattletech on 29/10/18.
//  Copyright © 2018 BTS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImage+FontAwesome.h"
#import "ProductCell.h"
#import "UIImageView+WebCache.h"

NS_ASSUME_NONNULL_BEGIN

@protocol OrderHistoryCellDelegate <NSObject>
@optional
- (void)redirectToBuyOptionWebPage: (NSDictionary *)dict ;
- (void)redirectToReviewAndRatingView: (NSDictionary *)dict ;

@end

@interface OrderHistoryCell : UITableViewCell<UICollectionViewDelegate,UICollectionViewDataSource>
@property(strong,nonatomic) IBOutlet UIView *view;
@property(strong,nonatomic) IBOutlet UILabel *itemname;
@property(strong,nonatomic) IBOutlet UILabel *itemprice;
@property(strong,nonatomic) IBOutlet UILabel *orderno;
@property(strong,nonatomic) IBOutlet UILabel *ordertotal;
@property(strong,nonatomic) IBOutlet UILabel *quantity;
@property(strong,nonatomic) IBOutlet UILabel *orderdate;
@property(strong,nonatomic) IBOutlet UILabel *ordertime;
@property(strong,nonatomic) IBOutlet UILabel *location;
@property(strong,nonatomic) IBOutlet UILabel *paymentMethod;
@property(strong,nonatomic) IBOutlet UIView *foregroundView;
@property(strong,nonatomic) IBOutlet UIImageView *locationImageView;
@property(strong,nonatomic) IBOutlet UICollectionView *collectionview;
@property(strong,nonatomic) IBOutlet UIPageControl *pageControl;
@property(strong,nonatomic) IBOutlet NSString *microMarkettring;
@property(strong,nonatomic)NSMutableArray *items;
@property(strong,nonatomic) NSString *indexpath;
@property (nonatomic, weak) id <OrderHistoryCellDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
