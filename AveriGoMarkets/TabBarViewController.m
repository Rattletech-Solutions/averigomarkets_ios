//
//  TabBarViewController.m
//  Averigo
//
//  Created by BTS on 27/02/17.
//  Copyright © 2017 BTS. All rights reserved.
//

#import "TabBarViewController.h"
#import "PurchaseHistoryController.h"
#import "BarCodeController.h"
#import "HomeViewController.h"
#import "LoginViewController.h"
#import "walletWebViewController.h"
#import "CheckOutWebViewController.h"
#import "AllConstants.h"
#import "AFNHelper.h"
#import <sys/utsname.h>

#define IS_IPHONE        (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_IPHONE_4      (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 480.0)
#define IS_IPHONE_5      (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 568.0)
#define IS_IPHONE_6      (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 667.0)
#define IS_IPHONE_6PLUS  (IS_IPHONE && [[UIScreen mainScreen] nativeScale] == 3.0f)
#define IS_IPHONE_6_PLUS (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 736.0)
#define IS_IPHONE_X      (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 812.0)

#define IS_IPHONE_XS      (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 812.0)
#define IS_IPHONE_X_MAX      (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 896.0)
#define IS_RETINA        ([[UIScreen mainScreen] scale] >= 2.0) // 3.0 for iPhone X, 2.0 for others

#define IS_IPAD_DEVICE   [(NSString*)[UIDevice currentDevice].model hasPrefix:@"iPad"]



@interface TabBarViewController ()
{
    UITabBar *tabBar;
    //UIImageView * imageview;
    int i;
}

@end

@implementation TabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    objDBHelper = [[DataBaseHandler alloc]init];
    [self syncAnalytics];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    self.navigationItem.hidesBackButton = YES;
    [[UITabBarItem appearance] setTitleTextAttributes:@{
                                                        
                                                        NSFontAttributeName:[UIFont fontWithName:FONT_MEDIUM size:12]
                                                        
                                                        } forState:UIControlStateNormal];
    [UITabBar appearance].layer.borderWidth = 0.0f;
    [UITabBar appearance].clipsToBounds = true;
    self.navigationItem.title=SCAN_BARCODE;
    [self setDelegate:self];
    UITabBarController *tabBarController = (UITabBarController *)self;
    tabBar = tabBarController.tabBar;
    tabBar.barTintColor = [UIColor colorWithRed:0.45 green:0.73 blue:0.31 alpha:1.0];
    tabBar.unselectedItemTintColor = [UIColor lightTextColor];
    i=0;
    
}


-(NSString*)deviceName
{
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *machineName = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    
    
    NSDictionary *commonNamesDictionary =
    @{
      @"i386":     @"i386 Simulator",
      @"x86_64":   @"x86_64 Simulator",
      
      @"iPhone1,1":    @"iPhone",
      @"iPhone1,2":    @"iPhone 3G",
      @"iPhone2,1":    @"iPhone 3GS",
      @"iPhone3,1":    @"iPhone 4",
      @"iPhone3,2":    @"iPhone 4",
      @"iPhone3,3":    @"iPhone 4",
      @"iPhone4,1":    @"iPhone 4S",
      @"iPhone5,1":    @"iPhone 5",
      @"iPhone5,2":    @"iPhone 5",
      @"iPhone5,3":    @"iPhone 5c",
      @"iPhone5,4":    @"iPhone 5c",
      @"iPhone6,1":    @"iPhone 5s",
      @"iPhone6,2":    @"iPhone 5s",
      
      @"iPhone7,1":    @"iPhone 6+",
      @"iPhone7,2":    @"iPhone 6",
      
      @"iPhone8,1":    @"iPhone 6S",
      @"iPhone8,2":    @"iPhone 6S+",
      @"iPhone8,4":    @"iPhone SE",
      @"iPhone9,1":    @"iPhone 7",
      @"iPhone9,2":    @"iPhone 7+",
      @"iPhone9,3":    @"iPhone 7",
      @"iPhone9,4":    @"iPhone 7+",
      @"iPhone10,1":    @"iPhone 8",
      @"iPhone10,2":    @"iPhone 8+",
      
      @"iPhone10,3":    @"iPhone X",
      @"iPhone11,2":    @"iPhone XS",
      @"iPhone11,4":    @"iPhone XS Max",
      @"iPhone11,8":    @"iPhone XR",
      
      @"iPad1,1":  @"iPad",
      @"iPad2,1":  @"iPad 2",
      @"iPad2,2":  @"iPad 2",
      @"iPad2,3":  @"iPad 2",
      @"iPad2,4":  @"iPad 2",
      @"iPad2,5":  @"iPad Mini 1G",
      @"iPad2,6":  @"iPad Mini 1G",
      @"iPad2,7":  @"iPad Mini 1G",
      @"iPad3,1":  @"iPad 3",
      @"iPad3,2":  @"iPad 3",
      @"iPad3,3":  @"iPad 3",
      @"iPad3,4":  @"iPad 4",
      @"iPad3,5":  @"iPad 4",
      @"iPad3,6":  @"iPad 4",
      
      @"iPad4,1":  @"iPad Air",
      @"iPad4,2":  @"iPad Air",
      @"iPad4,3":  @"iPad Air",
      
      @"iPad5,3":  @"iPad Air 2",
      @"iPad5,4":  @"iPad Air 2",
      
      @"iPad4,4":  @"iPad Mini 2G",
      @"iPad4,5":  @"iPad Mini 2G",
      @"iPad4,6":  @"iPad Mini 2G",
      
      @"iPad4,7":  @"iPad Mini 3G",
      @"iPad4,8":  @"iPad Mini 3G",
      @"iPad4,9":  @"iPad Mini 3G",
      
      @"iPod1,1":  @"iPod 1st Gen",
      @"iPod2,1":  @"iPod 2nd Gen",
      @"iPod3,1":  @"iPod 3rd Gen",
      @"iPod4,1":  @"iPod 4th Gen",
      @"iPod5,1":  @"iPod 5th Gen",
      @"iPod7,1":  @"iPod 6th Gen",
      };
    
    NSString *deviceName = commonNamesDictionary[machineName];
    if (deviceName == nil) {
        deviceName = machineName;
    }
    return deviceName;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear: animated];
    [self createcustombutton];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(receiveNotification:)
                                                name:SCAN_NOTIFICATION object:nil];
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear: animated];
}

- (void) receiveNotification:(NSNotification *) notification {
    NSString *type_string = [notification object];
    i=0;
    NSLog(@"TYPE STRING:%@",type_string);
    
    _imageView.backgroundColor = [UIColor colorWithRed:0.45 green:0.73 blue:0.31 alpha:1.0];
    UIView *tabItem = self.tabBar.subviews[3];
    tabItem.alpha = 1.0;
    tabItem.userInteractionEnabled= YES;
    UIView *shopItem = self.tabBar.subviews[2];
    shopItem.userInteractionEnabled = YES;
    //[tabBar setHidden: NO];
    NSLog(@"screen height %f",UIScreen.mainScreen.nativeBounds.size.height);
    if([type_string isEqualToString: SHOW] && [self.load isEqualToString: @"yes"])
    {
        [self.scan_btn setHidden:NO];
        if(IS_IPHONE_X) {
            _imageView.frame = CGRectMake(self.view.frame.size.width/2-37, self.view.frame.size.height-110, 75, 75);
        }else if (IS_IPHONE_X_MAX){
            _imageView.frame = CGRectMake(self.view.frame.size.width/2-37, self.view.frame.size.height - 100, 75, 75);
        }
        else if (IS_IPAD_DEVICE){
            _imageView.frame = CGRectMake(self.view.frame.size.width/2 - 63, self.view.frame.size.height - 90, 75, 75);
        }else {
            _imageView.frame = CGRectMake(self.view.frame.size.width/2-37, self.view.frame.size.height-75, 75, 75);
        }
        for (UITabBarItem *tbi in tabBar.items)
        {
            i=i+1;
            if(i==1)
            {
                [tbi setEnabled:YES];
            }
            else if(i==2)
            {
                [tbi setEnabled:YES];
            }
        }
    }
    else if([type_string isEqualToString:@"signout"])
    {
        LoginViewController *vc;
        vc=[self.storyboard instantiateViewControllerWithIdentifier: LOGIN_VC];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else if([type_string isEqualToString:@"home"])
    {
        for (UITabBarItem *tbi in tabBar.items)
        {
            i=i+1;
            if(i==1)
            {
                [tbi setEnabled:YES];
            }
            else if(i==2)
            {
                [tbi setEnabled:YES];
            }
        }
        self.ismicromarket=YES;
        self.load=@"yes";
        [self.scan_btn setAlpha:1.0];
        UIView *shopItem = self.tabBar.subviews[2];
        //shopItem.backgroundColor = [UIColor colorWithRed:0.45 green:0.73 blue:0.31 alpha:1.0];
        shopItem.alpha = 1.0;
        [shopItem setHidden:NO];
        shopItem.userInteractionEnabled = YES;
    }
    else if([type_string isEqualToString:@"qrcode"])
    {
        for (UITabBarItem *tbi in tabBar.items)
        {
            i=i+1;
            if(i==1)
            {
                [tbi setEnabled:YES];
            }
            else if(i==2)
            {
                [tbi setEnabled:YES];
            }
        }
        self.load=@"no";
        //        [self.scan_btn setHidden:YES];
        //            UIView *tabItem = self.tabBar.subviews[3];
        //            tabItem.alpha = 0.0;
        //            tabItem.userInteractionEnabled= NO;
    }
    else if([type_string isEqualToString:@"profile"])
    {
        self.ismicromarket=YES;
    }
    else if([type_string isEqualToString:@"checkout"])
    {
        [tabBar setHidden: YES];
        [self.scan_btn setHidden:YES];
        _imageView.backgroundColor = [UIColor clearColor];
    }
    else if([type_string isEqualToString:@"Shop"])
    {
        UITabBarController *tabBarController = (UITabBarController *)self;
        tabBar = tabBarController.tabBar;
        tabBar.barTintColor = [UIColor colorWithRed:0.45 green:0.73 blue:0.31 alpha:1.0];
        tabBar.unselectedItemTintColor = [UIColor lightTextColor];
        tabBar.tintColor = [UIColor whiteColor];
        tabBar.userInteractionEnabled = YES;
        [self createcustombutton];
    }
    else if([type_string isEqualToString:@"hide"]){
        //[self.scan_btn setHidden:YES];
        [self.scan_btn setAlpha:0.4];
        UIView *tabItem = self.tabBar.subviews[3];
        tabItem.alpha = 0.4;
        tabItem.userInteractionEnabled= NO;
        UIView *shopItem = self.tabBar.subviews[2];
        shopItem.alpha = 0.4;
        //[shopItem setHidden:YES];
        shopItem.userInteractionEnabled = NO;
    }
}

- (void)createcustombutton {
    _imageView = [[UIImageView alloc]init];
    if(IS_IPHONE_X) {
        _imageView.frame = CGRectMake(self.view.frame.size.width/2-37, self.view.frame.size.height-110, 75, 75);
    }else if (IS_IPHONE_X_MAX){
        _imageView.frame = CGRectMake(self.view.frame.size.width/2-37, self.view.frame.size.height - 100, 75, 75);
    }else if (IS_IPAD_DEVICE){
        _imageView.frame = CGRectMake(self.view.frame.size.width/2 - 63, self.view.frame.size.height - 80, 75, 75);
    }
    else {
        _imageView.frame = CGRectMake(self.view.frame.size.width/2-37, self.view.frame.size.height-75, 75, 75);
    }
    _imageView.backgroundColor = [UIColor colorWithRed:0.45 green:0.73 blue:0.31 alpha:1.0];
    _imageView.layer.cornerRadius = _imageView.frame.size.height/2;
    _imageView.clipsToBounds = YES;
    _imageView.tag = 999;
    [self.view addSubview:_imageView];
    [self.view bringSubviewToFront:self.tabBar];
    
}

-(IBAction)onscanclicked:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"BarCode" object:nil];
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
    if ([tabBarController selectedIndex] == 0  || [tabBarController selectedIndex] == 1   || [tabBarController selectedIndex] == 2 || [tabBarController selectedIndex] == 3)
    {
        if ([viewController isKindOfClass:[UINavigationController class]])
        {
            [(UINavigationController *)viewController popToRootViewControllerAnimated:NO];
        }
        else
        {
            // More stuffs.
        }
    }
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
    // Important! We validate that the selected tab is not the current tab, to avoid misplacing views
    if (tabBarController.selectedViewController == viewController) {
        if([tabBarController selectedIndex] == 1 || [tabBarController selectedIndex] == 0){
            return YES;
        }
        return NO;
    }
    
    // Find the selected view's index
    NSUInteger controllerIndex = 0;
    for (UIViewController *vc in tabBarController.viewControllers) {
        if (vc == viewController) {
            controllerIndex = [tabBarController.viewControllers indexOfObject:vc];
            
        }
    }
    
    CGFloat screenWidth = self.view.frame.size.width;
    
    // Note: We must invert the views according to the direction of the scrolling ( FROM Left TO right or FROM right TO left )
    UIView * fromView = tabBarController.selectedViewController.view;
    UIView * toView = viewController.view;
    
    [fromView.superview addSubview:toView];
    CGRect fromViewInitialFrame = fromView.frame;
    CGRect fromViewNewframe = fromView.frame;
    
    CGRect toViewInitialFrame = toView.frame;
    
    if ( controllerIndex > tabBarController.selectedIndex ) {
        // FROM left TO right ( tab0 to tab1 or tab2 )
        
        // The final frame for the current view. It will be displaced to the left
        fromViewNewframe.origin.x = -screenWidth;
        // The initial frame for the new view. It will be displaced to the left
        toViewInitialFrame.origin.x = screenWidth;
        toView.frame = toViewInitialFrame;
        
    } else {
        // FROM right TO left ( tab2 to tab1 or tab0 )
        
        // The final frame for the current view. It will be displaced to the right
        fromViewNewframe.origin.x = screenWidth;
        // The initial frame for the new view. It will be displaced to the right
        toViewInitialFrame.origin.x = -screenWidth;
        toView.frame = toViewInitialFrame;
    }
    
    [UIView animateWithDuration:0.1 animations:^{
        // The new view will be placed where the initial view was placed
        toView.frame = fromViewInitialFrame;
        // The initial view will be place outside the screen bounds
        fromView.frame = fromViewNewframe;
        
        tabBarController.selectedIndex = controllerIndex;
        
        // To prevent user interaction during the animation
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        
    } completion:^(BOOL finished) {
        
        // Before removing the initial view, we adjust its frame to avoid visual lags
        fromView.frame = CGRectMake(0, 0, fromView.frame.size.width, fromView.frame.size.height);
        [fromView removeFromSuperview];
        
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    }];
    
    return NO;
    
    // return viewController != tabBarController.selectedViewController;
}


-(void)removeView {
    UIView *removeView;
    while((removeView = [self.view viewWithTag:999]) != nil) {
        [removeView removeFromSuperview];
    }
}


-(void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item {
    
    [self removeView];
    [self createcustombutton];
    NSInteger index = [self.tabBar.items indexOfObject:item];
    [self animationWithIndex:index];
}

- (void)animationWithIndex:(NSInteger) index {
    NSMutableArray *tabBarBtnArr = [NSMutableArray array];
    for (UIView *tabBarBtn in self.tabBar.subviews) {
        if ([tabBarBtn isKindOfClass:NSClassFromString(@"UITabBarButton")]) {
            [tabBarBtnArr addObject:tabBarBtn];
        }
    }
    UIButton *btn=tabBarBtnArr[index];
    [UIView animateWithDuration:0.1 animations:^{
        btn.transform=CGAffineTransformMakeScale(1.1, 1.1);
        
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.1 animations:^{
            btn.transform=CGAffineTransformIdentity;
            //btn.transform = CGAffineTransformMakeRotation(-2*M_PI);
            
        }];
    }];
    
}

-(void)syncAnalytics {
    
    NSError *error = nil;
    
    NSData *feedbackAnalyticsData = [NSJSONSerialization dataWithJSONObject:[objDBHelper getFeedbackAnalytics] options:NSJSONWritingPrettyPrinted error:&error];
    
    NSString *feedbackAnalyticsString = [[NSString alloc] initWithData:feedbackAnalyticsData encoding:NSUTF8StringEncoding];
    
    NSMutableDictionary *feedbackAnalyticsdict=[NSJSONSerialization JSONObjectWithData:[feedbackAnalyticsString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
    
    NSLog(@"productAnalyticsString:%@",feedbackAnalyticsString);
    
    
    
    NSData *productAnalyticsData = [NSJSONSerialization dataWithJSONObject:[objDBHelper getProductAnalyticsToBeScanned] options:NSJSONWritingPrettyPrinted error:&error];
    
    NSString *productAnalyticsString = [[NSString alloc] initWithData:productAnalyticsData encoding:NSUTF8StringEncoding];
    
    NSMutableDictionary *productAnalyticsdict=[NSJSONSerialization JSONObjectWithData:[productAnalyticsString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
    
    NSLog(@"productAnalyticsString:%@",productAnalyticsString);
    
    
    
    NSData *screenAnalyticsData = [NSJSONSerialization dataWithJSONObject:[objDBHelper getScreenAnalyticsToBeSynced] options:NSJSONWritingPrettyPrinted error:&error];
    
    NSString *screenAnalyticsString = [[NSString alloc] initWithData:screenAnalyticsData encoding:NSUTF8StringEncoding];
    
    NSMutableDictionary *screenAnalyticsdict=[NSJSONSerialization JSONObjectWithData:[screenAnalyticsString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
    
    NSLog(@"screenAnalyticsString:%@",screenAnalyticsString);
    
    
    
    NSData *userAnalyticsData = [NSJSONSerialization dataWithJSONObject:[objDBHelper getUserAnalyticsToBeSynced] options:NSJSONWritingPrettyPrinted error:&error];
    
    NSString *userAnalyticsString = [[NSString alloc] initWithData:userAnalyticsData encoding:NSUTF8StringEncoding];
    
    NSMutableDictionary *userAnalyticsdict=[NSJSONSerialization JSONObjectWithData:[userAnalyticsString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
    
    NSLog(@"userAnalyticsString:%@",userAnalyticsString);
    
    
    
    AFNHelper *afn = [[AFNHelper alloc]init];
    
    
    
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    
    [dictParam setObject:@"F2A56C1C9295F43DB2CB13847728A"  forKey:@"secret"];
    
    [dictParam setObject:userAnalyticsdict     forKey:@"analytics_user"];
    
    [dictParam setObject:screenAnalyticsdict   forKey:@"analytics_screen"];
    
    [dictParam setObject:productAnalyticsdict  forKey:@"analytics_cartproduct"];
    
    [dictParam setObject:feedbackAnalyticsdict forKey:@"analytics_feedback"];
    
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:dictParam options:0 error:nil];
    NSString *Json_String = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSLog(@"dictParam:%@",Json_String);
    
    [afn postDataFromPath1:METHOD_ANALYTICS getUrl:ANALYTICS_URL withParamData:dictParam withBlock:^(id response, NSError *error)
     
     {
         
         NSLog(@"RESPONSE:%@",response);
         
         [objDBHelper UpdateUserAnalytics:@"N"];
         
         [objDBHelper UpdateScreenAnalytics:@"N"];
         
         [objDBHelper UpdateCartProductAnalytics:@"N"];
         
         [objDBHelper deleteFeedbackAnalytics];
         
         [self userAnalytics];
     }];
}

-(void)userAnalytics {
    NSString * SessionGUID = [[NSUserDefaults standardUserDefaults] objectForKey:@"SessionGUID"];
    NSString *deviceModel = [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceModel"];
    NSString *deviceOS = [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceOS"];
    NSString * udid=[[NSUserDefaults standardUserDefaults] objectForKey:UDID];
    NSString * firstName=[[NSUserDefaults standardUserDefaults] objectForKey:@"firstname"];
    NSString * lastName=[[NSUserDefaults standardUserDefaults] objectForKey:@"lastname"];
    NSString * userID = [[NSUserDefaults standardUserDefaults] objectForKey:@"username"];
    NSString * latitude  = [[NSUserDefaults standardUserDefaults]objectForKey:@"Latitude"];
    NSString * longitude = [[NSUserDefaults standardUserDefaults]objectForKey:@"Longitude"];
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString * checkInTime=[dateFormatter stringFromDate:[NSDate date]];
    NSString * analyticsCount = [objDBHelper getUserAnalyticsBySessionID:SessionGUID];
    if([analyticsCount isEqual: @"0"]) {
        
        [objDBHelper insertUserAnalytics:userID andsessionGUID:SessionGUID andFirstName:firstName andLastName:lastName andDeviceUDID:udid andDeviceType:@"iPhone" andDeviceModel:deviceModel andeDeviceOS:deviceOS andLatitude:latitude andLongitude:longitude andCheckInTime:checkInTime andCheckOutTime:@"" andSyncFlag:@"Y"];
    }
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

