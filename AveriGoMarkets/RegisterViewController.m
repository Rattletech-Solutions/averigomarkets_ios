//
//  RegisterViewController.m
//  Averigo
//
//  Created by BTS on 08/11/16.
//  Copyright © 2016 BTS. All rights reserved.
//

#import "RegisterViewController.h"
#import "AllConstants.h"
#import "AFNHelper.h"
#import "AFHTTPSessionManager.h"
#import "MBProgressHUD.h"
#import "Reachability.h"
#import "AFURLRequestSerialization.h"
#import "AFHTTPSessionManager.h"
#import "KGModal.h"
#import "SendGrid.h"
#import "EMCCountryPickerController.h"
#import "countryCode.h"
#import "TabBarViewController.h"
#import "AlertVC.h"
#import "ActivityIndicator.h"
#import "IQKeyBoardManager/IQKeyboardReturnKeyHandler.h"
#import "RealReachability.h"
#import "LoginViewController.h"
#import "AppDelegate.h"

@interface RegisterViewController ()
{
    NSString *firstName;
    NSString *lastName;
    NSString *companyname;
    NSString *nickName;
    NSString *emailId;
    NSString *password;
    NSString *rPassword;
    NSString *concatPhoneNum;
    NSString *txtPhoneToPass;
    UIView      *popView;
    NSString    *txtEmail;
    NSString    *txtPopUpPhoneNum;
    UITextField *phoneNumField;
    UITextField *countryField;
    UITextField * emailField;
    UITextField *fieldOne;
    UITextField *fieldTwo;
    UITextField *fieldThree;
    UITextField *fieldFour;
    UIImageView *countryImg;
    NSString *imagePath;
    UIButton *viewBtn;
    UIButton *verifyBtn;
    UIButton *resendCodeBtn;
    NSString *ifNews;
    NSString *ifSms;
    int  randomNo;
    IQKeyboardReturnKeyHandler *returnKeyHandler;
    STPopupController *openPopUpController;
    BOOL isPasswordVisible;
    BOOL isResetPasswordVisible;
}

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    [self InitNavBar];
    [self setUpUI ];
    
    isPasswordVisible = YES;
    isResetPasswordVisible = YES;
    imagePath = [NSString stringWithFormat:@"EMCCountryPickerController.bundle/%@", @"US"];
    countryImg = [[UIImageView alloc]
                  initWithImage:[UIImage imageNamed:imagePath]];
    
    UITapGestureRecognizer *singleTapForCountryPicker = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(countryImgClicked)];
    singleTapForCountryPicker.numberOfTapsRequired = 1;
    [countryImg setUserInteractionEnabled:YES];
    [countryImg addGestureRecognizer:singleTapForCountryPicker];
    
    
    UITapGestureRecognizer *sendCodeTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(sendCode:)];
    sendCodeTap.numberOfTapsRequired = 1;
    [viewBtn addGestureRecognizer:sendCodeTap];
    [viewBtn setUserInteractionEnabled:YES];
    
    UITapGestureRecognizer *verifyCodeTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(verifyCode:)];
    verifyCodeTap.numberOfTapsRequired = 1;
    [verifyBtn addGestureRecognizer:verifyCodeTap];
    [verifyBtn setUserInteractionEnabled:YES];
    
    UITapGestureRecognizer *reSendCodeTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(sendCode:)];
    reSendCodeTap.numberOfTapsRequired = 1;
    [resendCodeBtn addGestureRecognizer:reSendCodeTap];
    [resendCodeBtn setUserInteractionEnabled:YES];
    
    ifNews=@"N";
    ifSms=@"N";
    
    [self setFieldBorder:_txtFirstName];
    [self setFieldBorder:_txtLastName];
    [self setFieldBorder:_txtEmailAddress];
    [self setFieldBorder:_txtMobileNumber];
    [self setFieldBorder:_txtPassword];
    [self setFieldBorder:_txtReEnterPassword];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"otpscreen"]!=nil){
        if([[[NSUserDefaults standardUserDefaults]objectForKey:@"otpscreen"] isEqual:@"true"]){
            self.txtFirstName.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"otpfirstname"];
            self.txtLastName.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"otplastname"];
            [[NSNotificationCenter defaultCenter] postNotificationName:
             @"VerifyCorrectEmailId" object:nil userInfo:nil];
        }
    }
         });
    
    //returnKeyHandler = [[IQKeyboardReturnKeyHandler alloc] initWithViewController:self];
    //[]
    //[returnKeyHandler setLastTextFieldReturnKeyType:UIReturnKeyDone];
    
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(CorrectEmail:)
                                                     name:@"VerifyCorrectEmailId" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(closePopUp:)
                                                 name:@"EmailIdVerified" object:nil];
    [[NSUserDefaults standardUserDefaults] setObject:@"true" forKey:@"registerscreen"];
}

- (void)viewDidDisappear:(BOOL)animated
{
   // [[NSNotificationCenter defaultCenter] removeObserver:self];
}


-(void)closePopUp:(NSNotification *) notification {
    //if(openPopUpController != nil){
    //[openPopUpController dismiss];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.8 * NSEC_PER_SEC), dispatch_get_main_queue(),
                       ^{
                           self.txtEmailAddress.text  = [[NSUserDefaults standardUserDefaults] valueForKey:@"popEmail"] ;
                           self.txtMobileNumber.text = [NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"popCountry"],[[NSUserDefaults standardUserDefaults] valueForKey:@"popMobile"]];
                           [self.txtPassword becomeFirstResponder];
                           [self.txtMobileNumber setEnabled:NO];
                           //[self showAlert:@"Email Address & Phone Number Verified Successfuly."];
                           [AlertVC showAlertWithTitleForView:APP_NAME message:@"Email Address & Phone Number Verified Successfully." controller:self];
                          
    });
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2.0 * NSEC_PER_SEC), dispatch_get_main_queue(),
                   ^{
                       [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"popEmail"];
                       [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"popCountry"];
                       [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"popMobile"];
                   });
   
   
    //}
}

- (void) CorrectEmail:(NSNotification *) notification {
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"EnterEmailCodeController"]];
    popupController.style = STPopupStyleBottomSheet;
    //[popupController setHidesCloseButton:YES];
    openPopUpController = popupController;
    [popupController presentInViewController:self];
}


-(void)setFieldBorder :(JVFloatLabeledTextField *)textField{
    
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1;
    NSLog(@"%f",textField.frame.size.height - borderWidth);
    NSLog(@"%f",textField.frame.size.width);
    NSLog(@"%f",textField.frame.size.height);
    border.borderColor = [UIColor lightGrayColor].CGColor;
    border.frame = CGRectMake(0, textField.frame.size.height - borderWidth, textField.frame.size.width + 120, textField.frame.size.height);
    border.borderWidth = borderWidth;
    [textField.layer addSublayer:border];
    textField.layer.masksToBounds = YES;
    
}

-(void)InitNavBar
{
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor colorWithRed:0.98 green:0.96 blue:0.95 alpha:1.0],NSFontAttributeName:[UIFont fontWithName:FONT_REGULAR size:20]}];
    
    UIImage *buttonImage = [UIImage imageWithIcon:@"fa-chevron-left" backgroundColor:[UIColor clearColor] iconColor:[UIColor whiteColor] andSize:CGSizeMake(30,30)];
    UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button1 setImage:buttonImage forState:UIControlStateNormal];
    button1.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    [button1 addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button1];
    self.navigationItem.leftBarButtonItem = customBarItem;
    
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
}


-(void)countryImgClicked
{
    [self showCountryPicker];
}


-(IBAction)back:(id)sender
{
    [[NSUserDefaults standardUserDefaults]setObject:@"false" forKey:@"otpscreen"];
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)showconfirmpopup
{
    NSDictionary *dictobject;
    dictobject=[self.tempArray objectAtIndex:0];
    
    popView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,self.view.frame.size.width-40, 300)];
    popView.backgroundColor     = [UIColor whiteColor];
    popView.layer.cornerRadius  = 5.0;
    popView.layer.masksToBounds = YES;
    UILabel *title_lbl=[[UILabel alloc]initWithFrame:CGRectMake(0,20,popView.frame.size.width, 40)];
    title_lbl.text=@"Verify your Email & Mobile number";
    title_lbl.font=[UIFont fontWithName:FONT_MEDIUM size:17.f];
    title_lbl.textAlignment=NSTextAlignmentCenter;
    title_lbl.textColor=[UIColor blackColor];
    
    emailField= [[UITextField alloc]initWithFrame:CGRectMake(20, 100, self.view.frame.size.width/2 + 120 , 30)];
    [self SetTextFieldBorder:emailField];
    emailField.placeholder = @"Email Address";
    [emailField setKeyboardType:UIKeyboardTypeEmailAddress];
    emailField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    emailField.autocorrectionType = UITextAutocorrectionTypeNo;
    emailField.font=[UIFont fontWithName:FONT_MEDIUM size:16.f];
    emailField.textAlignment = NSTextAlignmentLeft;
    emailField.delegate = self;
    emailField.tintColor = [UIColor lightGrayColor];
    
    
    phoneNumField= [[UITextField alloc]initWithFrame:CGRectMake(120, 160, popView.frame.size.width/2 + 30, 30)];
    [self SetTextFieldBorder:phoneNumField];
    phoneNumField.placeholder = @"Mobile Number";
    [phoneNumField setKeyboardType:UIKeyboardTypeASCIICapableNumberPad];
    phoneNumField.font=[UIFont fontWithName:FONT_MEDIUM size:16.f];
    phoneNumField.textAlignment = NSTextAlignmentLeft;
    phoneNumField.tintColor = [UIColor lightGrayColor];
    phoneNumField.delegate = self;
    phoneNumField.tag = 30;
    
    countryField = [[UITextField alloc]initWithFrame:CGRectMake(80, 160, 60, 30)];
    countryField.font=[UIFont fontWithName:FONT_MEDIUM size:16.f];
    countryField.delegate = self;
    countryField.text = @"+1";
    countryImg.frame = CGRectMake(20 ,165 , 45, 20);
    imagePath = [NSString stringWithFormat:@"EMCCountryPickerController.bundle/%@", @"US" ];
    countryImg.image = [UIImage imageNamed:imagePath];
    
    viewBtn=[[UIButton alloc]initWithFrame:CGRectMake(popView.frame.size.width/2 - 80 ,230 , 160, 45)];
    [viewBtn setTitle:@"Send Code" forState:UIControlStateNormal];
    [viewBtn.titleLabel setFont:[UIFont fontWithName:FONT_MEDIUM size:16.0]];
    [viewBtn addTarget:self action:@selector(sendCode:) forControlEvents:UIControlEventTouchUpInside];
    viewBtn.layer.cornerRadius=5.0;
    [viewBtn setBackgroundColor:[UIColor colorWithRed:0.00 green:0.64 blue:0.84 alpha:1.0]];
    [viewBtn setTintColor:[UIColor whiteColor]];
    
    [popView addSubview:viewBtn];
    [popView addSubview:title_lbl];
    [popView addSubview:emailField];
    [popView addSubview:phoneNumField];
    [popView addSubview:countryField];
    [popView addSubview:countryImg];
    
    [[KGModal sharedInstance] showWithContentView:popView andAnimated:YES];
    [[KGModal sharedInstance] tapOutsideToDismiss];
    
}

- (void)countryController:(id)sender didSelectCountry:(EMCCountry *)chosenCity
{
    [self dismissViewControllerAnimated:YES completion:nil];
    imagePath = [NSString stringWithFormat:@"EMCCountryPickerController.bundle/%@", chosenCity.countryCode];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"countryCode contains[c] %@ ",chosenCity.countryCode];
    countryCode * code = [[countryCode alloc] init];
    NSArray *filteredCountry = [code.getCoutries filteredArrayUsingPredicate:filter];
    NSLog(@"%@",filteredCountry[0][@"phoneNumber"]);
    
    countryField.text = filteredCountry[0][@"phoneNumber"];
    countryImg.image = [UIImage imageNamed:imagePath];
    [[KGModal sharedInstance] showWithContentView:popView andAnimated:YES];
}

-(void)showCountryPicker
{
    [[KGModal sharedInstance]hideAnimated:YES];
    
    NSString * storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    EMCCountryPickerController * countryPicker = [storyboard instantiateViewControllerWithIdentifier:@"EMCCountryPickerController"];
    [self presentViewController:countryPicker animated:YES completion:nil];
    
    // default values
    countryPicker.showFlags = true;
    countryPicker.countryDelegate = self;
    countryPicker.drawFlagBorder = true;
    countryPicker.flagBorderColor = [UIColor grayColor];
    countryPicker.flagBorderWidth = 0.5f;
    
}


-(void)sendSMSCode
{
    [[KGModal sharedInstance]hideAnimated:YES];
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    randomNo = arc4random() % 9000 + 1000;
    // Formatting Phone Number
    NSString *formattedPhoneNum = [[phoneNumField.text componentsSeparatedByCharactersInSet:
                                    [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"]
                                     invertedSet]]
                                   componentsJoinedByString:EMP_STR];
    NSLog(@"formated String %@", formattedPhoneNum);
    concatPhoneNum = [NSString stringWithFormat: @"%@%@", countryField.text, formattedPhoneNum];
    NSLog(@"%@", concatPhoneNum);
    
    [dictParam setObject:[NSString stringWithFormat:@"%@",concatPhoneNum] forKey:@"Phone"];
    [dictParam setObject:[NSString stringWithFormat:@"%d", randomNo] forKey:@"code"];
    [dictParam setObject:[NSString stringWithFormat:@"%@", @"R"] forKey:@"flag"];
    NSLog(@"%@", dictParam);
    
    NSString *url = [NSString stringWithFormat:AVERIWARE_TWILIO_URL];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [manager.requestSerializer setValue:APP_TYPE forHTTPHeaderField:CONTENT_TYPE];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    [manager POST:url parameters:dictParam progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
     {
         NSLog(@"%@",responseObject);
         
     }
          failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              NSLog(@"%@",error.localizedDescription);
              [self showPopUp ];
              
          }];
    
}


-(void)sendEmailThroughSendGrid
{
    
    NSString* randomString = [NSString stringWithFormat:@"%i", randomNo];
    NSString *message = [NSString stringWithFormat: @"%s%s%s%@%s%s%s%s","<div style='font-size: 12pt; font-family: verdana; color: rgb(0, 0, 0); line-height: normal;'> ", "<p>Greetings!</p>", "<p>Here is your verification code: <br />", randomString,"</p>", "<p>Enter it on the AveriGo Markets app to continue the signup process.</p>" ,"<p>Sincerely,<br />", "The AveriGo Team</p></div>"];
    
    NSString *rootURL =  [[AppDelegate shared] rootUrl];

    AFNHelper *afn=[[AFNHelper alloc]init];
    [afn getDataFromPath:METHOD_MAILSERVER getUrl:rootURL withParamData:nil withBlock:^(id response, NSError *error) {
        
        if(error)
        {
            [[KGModal sharedInstance]hideAnimated:YES];
            NSLog(@"ERROR DESC:%@",error.localizedDescription);
            [self showAlert:SERVER_BUSY];
        }
        else
        {
            NSDictionary *dictionary;
            dictionary=response;
            NSDictionary *messageDict = [dictionary valueForKey:MESSAGE];
            NSString *status=[dictionary valueForKey:STATUS];
            
            if([status isEqualToString:SUCCESS])
            {
                SendGrid *sendgrid = [SendGrid apiUser:[messageDict valueForKey:@"mail_host_user"] apiKey:[messageDict valueForKey:@"mail_host_pass"]];
                SendGridEmail *email = [[SendGridEmail alloc] init];
                email.to      = txtEmail;
                email.from    = [messageDict valueForKey:@"fromEmail"];
                email.subject = @"AveriGo Markets Email Verification";
                email.html    = message;
                [sendgrid sendWithWeb:email];
            }
            else
            {
                [[KGModal sharedInstance]hideAnimated:YES];
                [self showAlert:SERVER_BUSY];
            }
            
        }
        
    }];
    
}


-(void)showAlert:(NSString *) message {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:APP_NAME message:message preferredStyle:UIAlertControllerStyleAlert];
    NSMutableAttributedString *titleAttr = [[NSMutableAttributedString alloc] initWithString:APP_NAME];
    [titleAttr addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:FONT_MEDIUM size:20]
                      range:NSMakeRange(0, [titleAttr length])];
    
    [alertController setValue:titleAttr forKey:@"attributedTitle"];
    
    NSMutableAttributedString *messageAttr = [[NSMutableAttributedString alloc] initWithString:message];
    [messageAttr addAttribute:NSFontAttributeName
                        value:[UIFont fontWithName:FONT_REGULAR size:14]
                        range:NSMakeRange(0, [messageAttr length])];
    
    
    [alertController setValue:messageAttr forKey:@"attributedMessage"];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alertController addAction:defaultAction];
    [self presentViewController:alertController animated:YES completion:nil];
}


- (void)checkForUserAvailability {
    if([self connected])
    {
        // Vlidate Emal Address.
        NSString *emailRegEx = EMAIL_REG_EX;
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
        
        // Formatting Phone Number
        NSString *formattedPhoneNum = [[phoneNumField.text componentsSeparatedByCharactersInSet:
                                        [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"]
                                         invertedSet]]
                                       componentsJoinedByString:EMP_STR];
        NSLog(@"formated String %@", formattedPhoneNum);
        
        if ([emailTest evaluateWithObject:emailField.text] == YES)
        {
            txtEmail = self.txtEmailAddress.text ;
            txtPhoneToPass= self.txtMobileNumber.text;
            
            [[ActivityIndicator sharedInstance] showLoader];

            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
            [dictParam setObject:txtPhoneToPass     forKey:PARAM_PHONENO_KEY];
            [dictParam setObject:txtEmail           forKey:PARAM_EMAIL_ID_KEY];
            
            NSString *rootURL =  [[AppDelegate shared] rootUrl];

            AFNHelper *afn=[[AFNHelper alloc]init];
            [afn postDataFromPath:METHOD_USERAVAILABILITY getUrl:rootURL withParamData:dictParam withBlock:^(id response, NSError *error)
             {
                 if(error)
                 {
                     NSLog(@"ERROR DESC:%@",error.localizedDescription);
                     [[ActivityIndicator sharedInstance] hideLoader];
                     //[self showAlert:NO_INTERNET_CONNECTION];

                 }
                 else
                 {
                     NSDictionary *dictionary;
                     dictionary=response;
                     NSString *status=[dictionary valueForKey:STATUS];
                     if([status isEqualToString:SUCCESS])
                     {
                         [self sendSMSCode];
                         [self sendEmailThroughSendGrid];
                     }
                     else
                     {
                         [[KGModal sharedInstance]hideAnimated:YES];
                         NSString *message=[dictionary valueForKey:MESSAGE];
                         [self showAlert:message];

                     }
                 }
                [[ActivityIndicator sharedInstance] hideLoader];
             }];
        }
        
        else
        {
            [[KGModal sharedInstance]hideAnimated:YES];
            [self showAlert:PLEASE_FILL_EMAIL];

        }
    } else {
        [[KGModal sharedInstance]hideAnimated:YES];
        [self showAlert:NO_INTERNET_CONNECTION];
    }
}

- (IBAction)sendCode:(id)sender {
    // Formatting Phone Number
    NSString *formattedPhoneNum = [[phoneNumField.text componentsSeparatedByCharactersInSet:
                                    [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"]
                                     invertedSet]]
                                   componentsJoinedByString:EMP_STR];
    if ((emailField.text.length > 0) && (formattedPhoneNum.length == 10) && (countryField.text.length <= 3))
    {
        [self checkForUserAvailability];
    } else {
        [[KGModal sharedInstance]hideAnimated:YES];
        [self showAlert:PLEASE_ENTER_VALID_EMAIL];

    }
}

- (IBAction)verifyCode:(id)sender {
    
    if (fieldOne.text.length == 1 && fieldTwo.text.length == 1 && fieldThree.text.length == 1 && fieldFour.text.length == 1) {
        NSString *concatPopUpValue = [NSString stringWithFormat: @"%@%@%@%@", fieldOne.text, fieldTwo.text, fieldThree.text, fieldFour.text ];
        
        int intOfRandomNumber = concatPopUpValue.intValue;
        
        if ( randomNo == intOfRandomNumber )
        {
            self.txtEmailAddress.text  = emailField.text ;
            self.txtMobileNumber.text = phoneNumField.text;
            [[KGModal sharedInstance]hideAnimated:YES];
            [self showAlert:@"Email Address & Phone Number Verified Successfully."];

            [self.txtLastName resignFirstResponder];
            
        } else {
            [[KGModal sharedInstance]hideAnimated:YES];
            [self showAlert:@"Incorrect Verification Code. Please try again."];
        }
    } else {
        [[KGModal sharedInstance]hideAnimated:YES];
        [self showAlert:@"Please Enter Valid Value"];
    }
}

- (void)SetTextFieldBorder :(UITextField *)textField {
    
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1.5;
    border.borderColor = [UIColor blackColor].CGColor;
    border.frame = CGRectMake(0, textField.frame.size.height - borderWidth, textField.frame.size.width, textField.frame.size.height);
    border.borderWidth = borderWidth;
    
    [textField.layer addSublayer:border];
    textField.layer.masksToBounds = YES;
    textField.font =[UIFont fontWithName:FONT_MEDIUM size:15.f];
    textField.textAlignment=NSTextAlignmentCenter;
    textField.textColor=[UIColor blackColor];
}

-(void)showPopUp
{
    NSDictionary *dictobject;
    dictobject=[self.tempArray objectAtIndex:0];
    popView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,self.view.frame.size.width-40, 300)];
    popView.backgroundColor     = [UIColor whiteColor];
    popView.layer.cornerRadius  = 5.0;
    popView.layer.masksToBounds = YES;
    UILabel *title_lbl=[[UILabel alloc]initWithFrame:CGRectMake(60, 20, popView.frame.size.width/2 + 60, 40)];
    
    title_lbl.text=@"Enter Your Verification Code";
    title_lbl.font=[UIFont fontWithName:FONT_MEDIUM size:17.f];
    title_lbl.textAlignment=NSTextAlignmentCenter;
    title_lbl.textColor=[UIColor blackColor];
    
    fieldOne= [[UITextField alloc]initWithFrame:CGRectMake(60, 110,popView.frame.size.width/10, 30)];
    [self SetTextFieldBorder:fieldOne];
    [fieldOne setKeyboardType:UIKeyboardTypeNumberPad];
    fieldOne.tag = 20;
    fieldOne.delegate = self;
    
    fieldTwo= [[UITextField alloc]initWithFrame:CGRectMake(130, 110, popView.frame.size.width/10, 30)];
    [self SetTextFieldBorder:fieldTwo];
    [fieldTwo setKeyboardType:UIKeyboardTypeNumberPad];
    fieldTwo.tag = 21;
    fieldTwo.delegate = self;
    
    fieldThree= [[UITextField alloc]initWithFrame:CGRectMake(200, 110, popView.frame.size.width/10, 30)];
    [self SetTextFieldBorder:fieldThree];
    [fieldThree setKeyboardType:UIKeyboardTypeNumberPad];
    fieldThree.tag = 22;
    fieldThree.delegate = self;
    
    fieldFour= [[UITextField alloc]initWithFrame:CGRectMake(270, 110, popView.frame.size.width/10, 30)];
    [self SetTextFieldBorder:fieldFour];
    [fieldFour setKeyboardType:UIKeyboardTypeNumberPad];
    fieldFour.tag = 23;
    fieldFour.delegate = self;
    
    verifyBtn=[[UIButton alloc]initWithFrame:CGRectMake(popView.frame.size.width/2 - 80 ,180 , 160, 45)];
    [verifyBtn setTitle:@"Verify Code" forState:UIControlStateNormal];
    [verifyBtn.titleLabel setFont:[UIFont fontWithName:FONT_MEDIUM size:16.0]];
    [verifyBtn addTarget:self action:@selector(verifyCode:) forControlEvents:UIControlEventTouchUpInside];
    verifyBtn.layer.cornerRadius=5.0;
    [verifyBtn setBackgroundColor:[UIColor colorWithRed:0.22 green:0.46 blue:0.24 alpha:1.0]];
    [verifyBtn setTintColor:[UIColor whiteColor]];
    
    UILabel *getCode_lbl=[[UILabel alloc]initWithFrame:CGRectMake(60,240,self.view.frame.size.width/2, 40)];
    getCode_lbl.text=@"Didn't get the code?";
    getCode_lbl.font=[UIFont fontWithName:FONT_MEDIUM size:16.f];
    getCode_lbl.textAlignment=NSTextAlignmentLeft;
    getCode_lbl.textColor=[UIColor blackColor];
    
    resendCodeBtn=[[UIButton alloc]initWithFrame:CGRectMake(popView.frame.size.width - 170 ,238 , 160, 45)];
    [resendCodeBtn setTitle:@"Resend Code" forState:UIControlStateNormal];
    [resendCodeBtn.titleLabel setFont:[UIFont fontWithName:FONT_MEDIUM size:16.0]];
    [resendCodeBtn addTarget:self action:@selector(sendCode:) forControlEvents:UIControlEventTouchUpInside];
    [resendCodeBtn setTitleColor:[UIColor colorWithRed:0 green:0.46 blue:0.69 alpha:1.0] forState:UIControlStateNormal ];
    
    
    [popView addSubview:verifyBtn];
    [popView addSubview:title_lbl];
    [popView addSubview:resendCodeBtn];
    [popView addSubview:getCode_lbl];
    [popView addSubview:fieldOne];
    [popView addSubview:fieldTwo];
    [popView addSubview:fieldThree];
    [popView addSubview:fieldFour];
    [[KGModal sharedInstance] showWithContentView:popView andAnimated:YES];
    [[KGModal sharedInstance] tapOutsideToDismiss];
    [fieldOne becomeFirstResponder];
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.tag >= 20 && textField.tag <= 23)
    {
        if(textField.tag >= 20 && textField.tag <= 23 && string.length > 0){
            [textField setText:string];
            NSInteger nextText = textField.tag + 1;
            UIResponder* nextResponder = [textField.superview viewWithTag:nextText];
            if (! nextResponder)
            {
                nextResponder = [textField.superview viewWithTag:1];
            }
            else
            {
                [nextResponder becomeFirstResponder];
                UITextField *nextTextfield = [textField.superview viewWithTag:nextText];
                [nextTextfield setText:EMP_STR];
                return NO;
            }
            return NO;
        }
        else if (textField.tag >= 20 && textField.tag <= 23  && string.length <= 0)
        {
            NSInteger beforeText = textField.tag - 1;
            UIResponder* previousResponder = [textField.superview viewWithTag:beforeText];
            
            if (previousResponder == nil)
            {
                previousResponder = [textField.superview viewWithTag:1];
            }
            textField.text = EMP_STR;
            [previousResponder becomeFirstResponder];
            return NO;
        }
        return YES;
    }else if(textField.tag == 100 || textField.tag == 101){
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
        }
        
        return YES;
    }
    
    if (textField.tag  == 30)
    {
        NSString* totalString = [NSString stringWithFormat:@"%@%@",textField.text,string];
        
        if (range.length == 1) {
            // Delete button was hit.. so tell the method to delete the last char.
            textField.text = [self formatPhoneNumber:totalString deleteLastChar:YES];
        } else {
            textField.text = [self formatPhoneNumber:totalString deleteLastChar:NO ];
        }
        return false;
    }
    return YES;
}

-(NSString*) formatPhoneNumber:(NSString*) simpleNumber deleteLastChar:(BOOL)deleteLastChar {
    if(simpleNumber.length==0) return EMP_STR;
    // use regex to remove non-digits(including spaces) so we are left with just the numbers
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[\\s-\\(\\)]" options:NSRegularExpressionCaseInsensitive error:&error];
    simpleNumber = [regex stringByReplacingMatchesInString:simpleNumber options:0 range:NSMakeRange(0, [simpleNumber length]) withTemplate:EMP_STR];
    
    // check if the number is to long
    if(simpleNumber.length>10) {
        // remove last extra chars.
        simpleNumber = [simpleNumber substringToIndex:10];
    }
    
    if(deleteLastChar) {
        // should we delete the last digit?
        simpleNumber = [simpleNumber substringToIndex:[simpleNumber length] - 1];
    }
    
    // 123 456 7890
    // format the number.. if it's less then 7 digits.. then use this regex.
    if(simpleNumber.length<7)
        simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d+)"
                                                               withString:@"$1-$2"
                                                                  options:NSRegularExpressionSearch
                                                                    range:NSMakeRange(0, [simpleNumber length])];
    
    else   // else do this one..
        simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d{3})(\\d+)"
                                                               withString:@"$1-$2-$3"
                                                                  options:NSRegularExpressionSearch
                                                                    range:NSMakeRange(0, [simpleNumber length])];
    return simpleNumber;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
        if (textField == self.txtFirstName)
        {
            [textField resignFirstResponder];
            [self.txtLastName becomeFirstResponder];
            return NO;
        }
        else if (textField == self.txtLastName)
        {
            [self.txtLastName resignFirstResponder];
            [self.txtPassword becomeFirstResponder];
            return NO;
        }
        else if (textField == self.txtEmailAddress)
        {
            [textField resignFirstResponder];
            return NO;
        }
        else if (textField == self.txtReEnterPassword)
        {
            [textField resignFirstResponder];
            return NO;
        }
        else if (textField == emailField)
        {
            [textField resignFirstResponder];
            [phoneNumField becomeFirstResponder];
            return NO;
        }
        else if (textField == phoneNumField)
        {
            [textField resignFirstResponder];
            return YES;
        }
        else if (textField == self.txtPassword)
        {
            [self.txtReEnterPassword becomeFirstResponder];
            return NO;
        }
    
    return YES;
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    //    if(textField==self.txtPassword||textField==self.txtReEnterPassword || textField.text != nil)
    //    {
    //        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    //        [self.view endEditing:YES];
    //    }
    
}


- (void)keyboardToolBar:(UITextField*)textfield
{
    //KeyBoard Toolbar..
    UIToolbar* keyboardToolbar = [[UIToolbar alloc] init];
    [keyboardToolbar sizeToFit];
    UIBarButtonItem *flexBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                      target:nil action:nil];
    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                      target:self.view action:@selector(endEditing:)];
    
    keyboardToolbar.items = @[flexBarButton, doneBarButton];
    textfield.inputAccessoryView  = keyboardToolbar;
    
}

-(void) endEditing
{
    [phoneNumField resignFirstResponder];
    [fieldOne resignFirstResponder];
    [fieldTwo resignFirstResponder];
    [fieldThree resignFirstResponder];
    [fieldFour resignFirstResponder];
}

-(void) showVerifyEmailPopUp{
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"VerifyEmailController"]];
    popupController.style = STPopupStyleBottomSheet;
    openPopUpController = popupController;
    [popupController presentInViewController:self];
}




- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField.tag == 102)
    {
        if (self.txtEmailAddress.text.length == 0)
        {
            [[NSUserDefaults standardUserDefaults]setObject:self.txtFirstName.text forKey:@"otpfirstname"];
            [[NSUserDefaults standardUserDefaults]setObject:self.txtLastName.text forKey:@"otplastname"];
            
            [self showVerifyEmailPopUp];
            return NO;
            //return YES;
        }
        else
        {
            return NO;
        }
    } else if (textField.tag == 103)
    {
        
        if (self.txtEmailAddress.text.length == 0 || self.txtFirstName.text.length == 0 || self.txtLastName.text.length == 0) {
            [self showPopUpForMobileField];
            return NO;
        } else
        {
            return YES;
        }
    } else if (textField == countryField) {
        [self showCountryPicker];
        return NO;
    } else if (textField.tag == 104) {
        if (self.txtEmailAddress.text.length == 0 || self.txtFirstName.text.length == 0 || self.txtLastName.text.length == 0) {
            [self showPopUpForMobileField];
            return NO;
        } else {
            return YES;
        }
    } else if (textField.tag == 105) {
        if (self.txtEmailAddress.text.length == 0 || self.txtFirstName.text.length == 0 || self.txtLastName.text.length == 0) {
            [self showPopUpForMobileField];
            return NO;
        } else {
            return YES;
        }
    } else {
        return YES;
    }
}

- (void) showPopUpForMobileField {
    [AlertVC showAlertWithTitleForView:APP_NAME message:PLEASE_ENTER_VALID_EMAIL controller:self];
}


- (IBAction)onNextClick:(id)sender {
    if(self.txtFirstName.text.length!=0&&self.txtLastName.text.length!=0&&self.txtEmailAddress.text.length!=0&&self.txtMobileNumber.text.length!=0&&self.txtPassword.text.length!=0&&self.txtReEnterPassword.text.length!=0)
    {
        if([self NSStringIsValidEmail:self.txtEmailAddress.text])
        {
            if([self.txtPassword.text isEqualToString:self.txtReEnterPassword.text])
            {
                firstName=self.txtFirstName.text;
                lastName=self.txtLastName.text;
                emailId=self.txtEmailAddress.text;
                password=self.txtPassword.text;
                txtPhoneToPass= self.txtMobileNumber.text;
                [self checkuser];
            }
            else
            {
                [[KGModal sharedInstance]hideAnimated:YES];
                [self showAlert:PASSWORD_DONT_MATCH];
            }
        }
        else
        {
            [[KGModal sharedInstance]hideAnimated:YES];
            [self showAlert:PLEASE_ENTER_VALID_EMAIL];
        }

    }
    else
    {
        [[KGModal sharedInstance]hideAnimated:YES];
        [self showAlert:PLEASE_FILL_REQUIRED_FIELDS];
    }
    
}

- (BOOL)NSStringIsValidEmail:(NSString *)inputText {
    NSString *emailRegex = EMAIL_REG_EX;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    NSRange aRange;
    if([emailTest evaluateWithObject:inputText]) {
        aRange = [inputText rangeOfString:@"." options:NSBackwardsSearch range:NSMakeRange(0, [inputText length])];
        int indexOfDot = aRange.location;
        if(aRange.location != NSNotFound) {
            NSString *topLevelDomain = [inputText substringFromIndex:indexOfDot];
            topLevelDomain = [topLevelDomain lowercaseString];
            NSSet *TLD;
            TLD = [NSSet setWithObjects:@".in",@".aero", @".asia", @".biz", @".cat", @".com", @".coop", @".edu", @".gov", @".info", @".int", @".jobs", @".mil", @".mobi", @".museum", @".name", @".net", @".org", @".pro", @".tel", @".travel", @".ac", @".ad", @".ae", @".af", @".ag", @".ai", @".al", @".am", @".an", @".ao", @".aq", @".ar", @".as", @".at", @".au", @".aw", @".ax", @".az", @".ba", @".bb", @".bd", @".be", @".bf", @".bg", @".bh", @".bi", @".bj", @".bm", @".bn", @".bo", @".br", @".bs", @".bt", @".bv", @".bw", @".by", @".bz", @".ca", @".cc", @".cd", @".cf", @".cg", @".ch", @".ci", @".ck", @".cl", @".cm", @".cn", @".co", @".cr", @".cu", @".cv", @".cx", @".cy", @".cz", @".de", @".dj", @".dk", @".dm", @".do", @".dz", @".ec", @".ee", @".eg", @".er", @".es", @".et", @".eu", @".fi", @".fj", @".fk", @".fm", @".fo", @".fr", @".ga", @".gb", @".gd", @".ge", @".gf", @".gg", @".gh", @".gi", @".gl", @".gm", @".gn", @".gp", @".gq", @".gr", @".gs", @".gt", @".gu", @".gw", @".gy", @".hk", @".hm", @".hn", @".hr", @".ht", @".hu", @".id", @".ie", @" No", @".il", @".im", @".in", @".io", @".iq", @".ir", @".is", @".it", @".je", @".jm", @".jo", @".jp", @".ke", @".kg", @".kh", @".ki", @".km", @".kn", @".kp", @".kr", @".kw", @".ky", @".kz", @".la", @".lb", @".lc", @".li", @".lk", @".lr", @".ls", @".lt", @".lu", @".lv", @".ly", @".ma", @".mc", @".md", @".me", @".mg", @".mh", @".mk", @".ml", @".mm", @".mn", @".mo", @".mp", @".mq", @".mr", @".ms", @".mt", @".mu", @".mv", @".mw", @".mx", @".my", @".mz", @".na", @".nc", @".ne", @".nf", @".ng", @".ni", @".nl", @".no", @".np", @".nr", @".nu", @".nz", @".om", @".pa", @".pe", @".pf", @".pg", @".ph", @".pk", @".pl", @".pm", @".pn", @".pr", @".ps", @".pt", @".pw", @".py", @".qa", @".re", @".ro", @".rs", @".ru", @".rw", @".sa", @".sb", @".sc", @".sd", @".se", @".sg", @".sh", @".si", @".sj", @".sk", @".sl", @".sm", @".sn", @".so", @".sr", @".st", @".su", @".sv", @".sy", @".sz", @".tc", @".td", @".tf", @".tg", @".th", @".tj", @".tk", @".tl", @".tm", @".tn", @".to", @".tp", @".tr", @".tt", @".tv", @".tw", @".tz", @".ua", @".ug", @".uk", @".us", @".uy", @".uz", @".va", @".vc", @".ve", @".vg", @".vi", @".vn", @".vu", @".wf", @".ws", @".ye", @".yt", @".za", @".zm", @".zw", nil];
            if(topLevelDomain != nil && ([TLD containsObject:topLevelDomain]))
            {
                return TRUE;
            }
        }
    }
    return FALSE;
}


- (void)checkuser {
    if([self connected])
    {
        // Current Date
        NSDate *today = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:DATEFORMAT_TYPE];
        NSString *currentTime = [dateFormatter stringFromDate:today];
        
        // Formatting Phone Number
        NSString *formattedPhoneNum = [[phoneNumField.text componentsSeparatedByCharactersInSet:
                                        [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"]
                                         invertedSet]]
                                       componentsJoinedByString:EMP_STR];
        
        [[ActivityIndicator sharedInstance] showLoader];

        NSString *DeviceUDID;
        NSString *DeviceDateTime;
        NSString *Subscription;
        NSString *sms;
        
        DeviceUDID=[[NSUserDefaults standardUserDefaults] objectForKey:UDID];
        DeviceDateTime=currentTime;
        Subscription=ifNews;
        sms=ifSms;
        
        firstName=self.txtFirstName.text;
        lastName=self.txtLastName.text;
        emailId=self.txtEmailAddress.text;
        password=self.txtPassword.text;
        txtPhoneToPass= self.txtMobileNumber.text;
        
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setObject:MICROMARKET            forKey:PARAM_COMPANYACRO];
        [dictParam setObject:MICROMARKET            forKey:PARAM_SERVICE_NAME];
        [dictParam setObject:DeviceUDID             forKey:PARAM_DEVICE_UDID];
        [dictParam setObject:DeviceDateTime         forKey:PARAM_DEVICE_DATETIME];
        [dictParam setObject:firstName              forKey:PARAM_FIRSTNAME];
        [dictParam setObject:lastName               forKey:PARAM_LASTNAME];
        [dictParam setObject:EMP_STR                forKey:PARAM_COMPANY_NAME];
        [dictParam setObject:EMP_STR                forKey:PARAM_NICKNAME];
        [dictParam setObject:EMP_STR                forKey:PARAM_PROFILE_IMAGE];
        [dictParam setObject:emailId                forKey:PARAM_EMAIL_ID];
        [dictParam setObject:password               forKey:PARAM_PASSWORD];
        [dictParam setObject:txtPhoneToPass         forKey:PARAM_PHONENO];
        [dictParam setObject:EMP_STR                forKey:PARAM_ADDRESS];
        [dictParam setObject:EMP_STR                forKey:PARAM_CITY];
        [dictParam setObject:EMP_STR                forKey:PARAM_ZIP];
        [dictParam setObject:EMP_STR                forKey:PARAM_STATE];
        [dictParam setObject:@"US"                  forKey:PARAM_COUNTRY];
        [dictParam setObject:Subscription           forKey:PARAM_ENABEL_NEWSLETTER];
        [dictParam setObject:sms                    forKey:PARAM_ENABLE_SMS];
        [dictParam setObject:@"IN"                  forKey:PARAM_PROCESS_STATUS];
        
        NSString *rootURL =  [[AppDelegate shared] rootUrl];

        AFNHelper *afn=[[AFNHelper alloc]init];
        [afn postDataFromPath1:METHOD_SIGNUP getUrl:rootURL withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [[ActivityIndicator sharedInstance] hideLoader];
                 });
             });
             if(error)
             {
                 [self showAlert:REQUEST_TIMED_OUT];
             }
             else
             {
                 NSDictionary *dictionary;
                 dictionary=response;
                 self.userArray=[[NSMutableArray alloc]init];
                 NSString *status=[dictionary valueForKey:STATUS];
                 self.userArray=[dictionary valueForKey:@"User"];
                 if([status isEqualToString:SUCCESS])
                 {
                     
                     NSDictionary *dict;
                     dict=[self.userArray objectAtIndex:0];
                     SendGrid *sendgrid = [SendGrid apiUser:[dict valueForKey:@"mail_host_user"] apiKey:[dict valueForKey:@"mail_host_pass"]];
                     SendGridEmail *email = [[SendGridEmail alloc] init];
                     email.to      = emailId;
                     email.from    = [dict valueForKey:@"fromEmail"];
                     email.subject = [dict valueForKey:@"subject"];
                     email.html = [dict valueForKey:@"Msg"];
                     email.text = [dict valueForKey:@"Msg"];
                     [email addBcc:SUPPORT_EMAL];
                     [sendgrid sendWithWeb:email];
        
                     [[NSUserDefaults standardUserDefaults] setObject:self.userArray forKey:@"userdetailsarray"];
                     [[NSUserDefaults standardUserDefaults]setObject:[dict valueForKey:@"EMAIL_ID"] forKey:@"username"];
                     [[NSUserDefaults standardUserDefaults]setObject:[dict valueForKey:@"PASSWORD"] forKey:@"password"];
                     [[NSUserDefaults standardUserDefaults]setObject:[dict valueForKey:@"FIRST_NAME"] forKey:@"firstname"];
                     [[NSUserDefaults standardUserDefaults]setObject:[dict valueForKey:@"LAST_NAME"] forKey:@"lastname"];
                     [[NSUserDefaults standardUserDefaults]setObject:[dict valueForKey:@"PROFILE_IMAGE"]forKey:@"imagename"];
                     [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"registerscreen"];
                     [[NSUserDefaults standardUserDefaults]setObject:@"micromarket" forKey:@"type"];
                     [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"signedin"];
                     [[NSUserDefaults standardUserDefaults]setObject:@"YES" forKey:@"fromregister"];
                     
                     LoginViewController *vc;
                     vc=[self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                     [self.navigationController pushViewController:vc animated:YES];
//                     TabBarViewController *vc;
//                     vc=[self.storyboard instantiateViewControllerWithIdentifier:@"TabBarViewController"];
//                     vc.navigationItem.hidesBackButton=YES;
//                     vc.load=@"yes";
//                     [self.navigationController pushViewController:vc animated:YES];
                     [AlertVC showAlertWithTitleForView:APP_NAME message:@"Signup Successful." controller:self];
                    
                 } else {
                    
                     NSString *message=[dictionary valueForKey:MESSAGE];
                     [self showAlert:message];
                 }
             }
         }];
    } else {
        //[[KGModal sharedInstance]hideAnimated:YES];
        [self showAlert:NO_INTERNET_CONNECTION];

    }
}

- (void)setUpUI {
    // Textfield Properties.
    _txtFirstName.font = [UIFont fontWithName:FONT_REGULAR size:15.0];
    _txtFirstName.tag = 100;
    _txtLastName.font = [UIFont fontWithName:FONT_REGULAR size:15.0];
    _txtLastName.tag = 101;
    _txtMobileNumber.font = [UIFont fontWithName:FONT_REGULAR size:15.0];
    //_txtMobileNumber.tag = 103;
    _txtPassword.font = [UIFont fontWithName:FONT_REGULAR size:15.0];
    ///_txtPassword.tag = 104;
    _txtReEnterPassword.font = [UIFont fontWithName:FONT_REGULAR size:15.0];
    //_txtReEnterPassword.tag = 105;
    _txtEmailAddress.font = [UIFont fontWithName:FONT_REGULAR size:15.0];
    //_txtEmailAddress.tag = 102;
    _lblPersonalInfo.font = [UIFont fontWithName:FONT_BOLD size:17.0];
    [_btnSignUp.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:17.0]];
    
    // Navigation Item Properties.
    self.navigationItem.title=SIGN_UP;
    //    [self.navigationController.navigationBar setTitleTextAttributes:
    //     @{NSForegroundColorAttributeName:[UIColor colorWithRed:0.98 green:0.96 blue:0.95 alpha:1.0],NSFontAttributeName:[UIFont fontWithName:FONT_BOLD size:20]}];
    self.btnSignUp.layer.cornerRadius=5;
    self.btnSignUp.clipsToBounds=YES;
    
    UIButton *eyeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    eyeBtn.frame = CGRectMake(0, 0, 24, 24);
    [eyeBtn setImage:[UIImage imageWithIcon:@"fa-eye" backgroundColor:[UIColor clearColor] iconColor:[UIColor grayColor] andSize:CGSizeMake(24,24)] forState:UIControlStateNormal];
    [eyeBtn addTarget:self action:@selector(eyeBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
   // eyeBtn.imageEdgeInsets = UIEdgeInsetsMake(0, -16, 0, 0);
    self.txtPassword.rightViewMode = UITextFieldViewModeAlways;
    self.txtPassword.rightView = eyeBtn;
    
    UIButton *resetEyeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    resetEyeBtn.frame = CGRectMake(0, 0, 24, 24);
    //[resetEyeBtn setImage:[UIImage imageNamed:@"password_visible"] forState:UIControlStateNormal];
    [resetEyeBtn setImage:[UIImage imageWithIcon:@"fa-eye" backgroundColor:[UIColor clearColor] iconColor:[UIColor grayColor] andSize:CGSizeMake(24,24)] forState:UIControlStateNormal];
    [resetEyeBtn addTarget:self action:@selector(resetEyeBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
   // resetEyeBtn.imageEdgeInsets = UIEdgeInsetsMake(0, -16, 0, 0);
    self.txtReEnterPassword.rightViewMode = UITextFieldViewModeAlways;
    self.txtReEnterPassword.rightView = resetEyeBtn;
    
    
}

- (IBAction)eyeBtnClicked:(id)sender {
    UIButton *eyeBtn = (UIButton*)sender;
    
    if(isPasswordVisible){
        [eyeBtn setImage:[UIImage imageWithIcon:@"fa-eye-slash" backgroundColor:[UIColor clearColor] iconColor:[UIColor grayColor] andSize:CGSizeMake(24,24)] forState:UIControlStateNormal];
        [self.txtPassword setSecureTextEntry:NO];
        isPasswordVisible = NO;
    }else{
        [eyeBtn setImage:[UIImage imageWithIcon:@"fa-eye" backgroundColor:[UIColor clearColor] iconColor:[UIColor grayColor] andSize:CGSizeMake(24,24)] forState:UIControlStateNormal];
        [self.txtPassword setSecureTextEntry:YES];
        isPasswordVisible = YES;
    }
}

- (IBAction)resetEyeBtnClicked:(id)sender {
    UIButton *resetEyeBtn = (UIButton*)sender;
    
    if(isResetPasswordVisible) {
        [resetEyeBtn setImage:[UIImage imageWithIcon:@"fa-eye-slash" backgroundColor:[UIColor clearColor] iconColor:[UIColor grayColor] andSize:CGSizeMake(24,24)] forState:UIControlStateNormal];
        [self.txtReEnterPassword setSecureTextEntry:NO];
        isResetPasswordVisible = NO;
    } else {
        [resetEyeBtn setImage:[UIImage imageWithIcon:@"fa-eye" backgroundColor:[UIColor clearColor] iconColor:[UIColor grayColor] andSize:CGSizeMake(24,24)] forState:UIControlStateNormal];
        [self.txtReEnterPassword setSecureTextEntry:YES];
        isResetPasswordVisible = YES;
    }
}

- (BOOL)connected {
    ReachabilityStatus status = [GLobalRealReachability currentReachabilityStatus];
    NSLog(@"Initial reachability status:%@",@(status));
    
    return status == 1 || status == 2;
}

- (IBAction)closeBtnClicked:(id)sender {
    //[self dismissViewControllerAnimated:YES completion:nil];
    [[NSUserDefaults standardUserDefaults]setObject:@"false" forKey:@"otpscreen"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"registerscreen"];
   // [[NSUserDefaults standardUserDefaults] setObject:@"false" forKey:@"registerscreen"];
    [self.navigationController popViewControllerAnimated:YES];
    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end





