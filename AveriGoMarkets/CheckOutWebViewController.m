
    //
    //  CheckOutWebViewController.m
    //  VendBeam
    //
    //  Created by BTS on 12/07/18.
    //  Copyright © 2018 BTS. All rights reserved.
    //

#import "CheckOutWebViewController.h"
#import "AllConstants.h"
#import "MBProgressHUD.h"
#import "EasyJSWebView.h"
#import "MyJSInterface.h"
#import "CategoryViewController.h"
#import "AlertVC.h"
#import "ActivityIndicator.h"
#import "Reachability.h"
#import "RealReachability.h"

@interface CheckOutWebViewController ()
{
    NSString *total_;
    NSString *jsResult;
    NSString * Json_String ;
    BOOL beaconIsThere;
    BOOL noNetworkDidntLoad;
    ReachabilityStatus currentStatus;
    int BeaconMajor;
    int BeaconMinor;
    BOOL isFirst;
    BOOL isBackBtnFlag;
}
@end

@implementation CheckOutWebViewController

- (void)loadView {
    [super loadView];
    //NSLog(@"self.view.bounds.size.height %f", self.view.bounds.size.height);
    CGFloat topbarHeight = ([UIApplication sharedApplication].statusBarFrame.size.height +
                            (self.navigationController.navigationBar.frame.size.height ?: 0.0));
    [self.checkOutWebView setFrame:CGRectStandardize(CGRectMake(0, 5, self.view.bounds.size.width, self.view.bounds.size.height-topbarHeight))];
    self.checkOutWebView.scrollView.delegate = self;
    //[self.checkOutWebView.scrollView setBouncesZoom:false];
    [self.view addSubview:self.checkOutWebView];
}

- (void)scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view {
    [[scrollView pinchGestureRecognizer] setEnabled:false];
    
    /*for (UIGestureRecognizer *recognizer in scrollView.gestureRecognizers) {
     UITapGestureRecognizer *tap = (UITapGestureRecognizer *)recognizer;
     NSLog(@"%@",[tap class]);
     NSLog(@"AQUI %@",tap);
     NSLog(@"recognizer %@",recognizer);
     [scrollView setMaximumZoomScale:1.0];
     [scrollView setBouncesZoom:false];
     
     
     if([tap class] == nil){
     
     
     if(tap.numberOfTapsRequired == 2 && tap.numberOfTouchesRequired == 1){
     [self.checkOutWebView removeGestureRecognizer:tap];
     }
     
     }
     }*/
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    isBackBtnFlag = NO;
    isFirst = YES;
    BeaconMajor = -1;
    BeaconMinor = -1;
    _progressView.progress = 0;
    _webViewLoaded = false;
    _checkOutWebView.tintColor = [UIColor blackColor];
    _timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(timerCallback) userInfo:nil repeats:YES];
    
  //  [[NSNotificationCenter defaultCenter] postNotificationName:SCAN_NOTIFICATION object:@"checkout"];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self InitNavBar];
    
    self.micromarketid=[[NSUserDefaults standardUserDefaults]objectForKey:@"micromarketid"];
    [[NSUserDefaults standardUserDefaults]setObject:self.micromarketid forKey:@"lastmicromarketid"];
    objDBHelper = [[DataBaseHandler alloc]init];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear: animated];
    
    noNetworkDidntLoad = YES;
    
    [self.tabBarController.tabBar setHidden: YES];

    //[[NSNotificationCenter defaultCenter] postNotificationName:SCAN_NOTIFICATION object:@"checkout"];
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(scanBeaconNotification:)
                                                name:BEACON_NOTIFICATION object:nil];
    
    [self setScreenAnalytics];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillTerminate:) name:UIApplicationWillTerminateNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillTerminate:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotificationCheckOut:)
                                                 name:WEB_VIEW_DID_FINISH
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
    selector:@selector(receiveTestNotificationCheckOut:)
        name:GOBACK_POPUP
      object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotificationCheckOut:)
                                                 name:SHOW_POPUP
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotificationCheckOut:)
                                                 name:@"changeActivity"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotificationCheckOut:)
                                                 name:PAYMENT_SUCCESS
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotificationCheckOut:)
                                                 name:TOKEN_EXPIRED
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveConfirmNotification:)
                                                 name:CONFIRMATION_POPUP
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotificationCheckOut:)
                                                 name:PAYROLL_POPUP
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotificationCheckOut:)
                                                 name:DID_FAIL_WITH_ERROR
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotificationCheckOut:)
                                                 name:@"ShowErrorPopUp"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveConfirmNotification:)
                                                 name:SHOW_LOADER
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveConfirmNotification:)
                                                 name:DISMISS_LOADER
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkChanged:)
                                                 name:kRealReachabilityChangedNotification
                                               object:nil];
    
    for (id subview in self.tabBarController.view.subviews) {
        
        if ([subview isMemberOfClass:[UIImageView class]]) {
            [subview setHidden: YES];
        }
    }
    /*self.tabBarController.tabBar.userInteractionEnabled = NO;
    
    self.tabBarController.tabBar.layer.zPosition = -1;*/
}


- (void) scanBeaconNotification:(NSNotification *) notification {
    beaconIsThere = NO;
    NSDictionary *dict = notification.userInfo;
    _beacons=[dict valueForKey:@"Beacons"];
    NSMutableArray * UDIDS = [NSMutableArray array];
    
    for(int i=0;i<_beacons.count;i++)
    {
        CLBeacon *beacon = (CLBeacon*)[self.beacons objectAtIndex:i];
        NSString *BUUID=[NSString stringWithFormat:@"%d-%d",beacon.major.intValue,beacon.minor.intValue];
        [UDIDS addObject:BUUID];
    }
    _UDID= [UDIDS componentsJoinedByString:@","];
    NSString *location_beacon =  [[NSUserDefaults standardUserDefaults]objectForKey:@"LOCATION_BEACON"];
    if(location_beacon != nil){
        if(_beacons.count == 1){
            CLBeacon *beacon = (CLBeacon*)[self.beacons objectAtIndex:0];
            NSString *BUUID=[NSString stringWithFormat:@"%d-%d",beacon.major.intValue,beacon.minor.intValue];
            //NSLog(@"location beacon %@",location_beacon);
            //NSLog(@"beacon %@",BUUID);
            if(location_beacon == BUUID){
                beaconIsThere = YES;
                BeaconMajor = beacon.major.intValue;
                BeaconMinor = beacon.minor.intValue;
            }else{
                [self.navigationController popToRootViewControllerAnimated:YES];
                self.tabBarController.selectedIndex=0;
            }
        } else if(_beacons.count > 1) {
            for(int i = 0; i<_beacons.count;i++){
                CLBeacon *beacon = (CLBeacon*)[self.beacons objectAtIndex:i];
                NSString *BUUID=[NSString stringWithFormat:@"%d-%d",beacon.major.intValue,beacon.minor.intValue];
                //NSLog(@"location beacon %@",location_beacon);
                //NSLog(@"beacon %@",BUUID);
                if(location_beacon == BUUID){
                    beaconIsThere = YES;
                    BeaconMajor = beacon.major.intValue;
                    BeaconMinor = beacon.minor.intValue;
                }
            }
            if(!beaconIsThere) {
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
        } else {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    } else {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    if (isFirst) {
        isFirst = NO;
        NSString *urlString = @"https://vending.averiware.com/payment.php?UserName=";
        [self loadRequestFromString:urlString];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear: animated];

    for (id subview in self.tabBarController.view.subviews) {
        
        if ([subview isMemberOfClass:[UIImageView class]]) {
            [subview setHidden: NO];
        }
    }
    
    [self.tabBarController.tabBar setHidden: NO];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillTerminateNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:BEACON_NOTIFICATION object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ShowErrorPopUp" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:PAYROLL_POPUP object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"changeActivity" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:PAYMENT_SUCCESS object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SHOW_POPUP object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:WEB_VIEW_DID_FINISH object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CARD_DELETED object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TOKEN_EXPIRED object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:DID_FAIL_WITH_ERROR object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"confirmationPopUp" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"showLoader" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"dismissLoader" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRealReachabilityChangedNotification object:nil];
    [self updateScreenAnalyticsCheckOutTime];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear: animated];

    isFirst = YES;

    [[NSNotificationCenter defaultCenter] postNotificationName:SCAN_NOTIFICATION object: SHOW];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear: animated];

    //[[NSNotificationCenter defaultCenter] postNotificationName:SCAN_NOTIFICATION object:@"checkout"];
}

- (void)timerCallback {
    if (_webViewLoaded) {
        if (_progressView.progress >= 1) {
            _progressView.hidden = true;
            [_timer invalidate];
        }
        else {
            _progressView.progress += 0.1;
        }
    }
    else {
        _progressView.progress += 0.05;
        if (_progressView.progress >= 0.95) {
            _progressView.progress = 0.95;
        }
    }
}

- (void)InitNavBar {
    self.navigationItem.title = CHECK_OUT;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor colorWithRed:0.98 green:0.96 blue:0.95 alpha:1.0],NSFontAttributeName:[UIFont fontWithName:FONT_REGULAR size:20]}];
    
    UIImage *buttonImage = [UIImage imageWithIcon:@"fa-chevron-left" backgroundColor:[UIColor clearColor] iconColor:[UIColor whiteColor] andSize:CGSizeMake(30,30)];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    [button addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = customBarItem;
}

- (IBAction)back:(id)sender {
    if (isBackBtnFlag) {
          isBackBtnFlag= NO;
          self.title = @"Check Out";
          [[ActivityIndicator sharedInstance] hideLoader];
          NSString *interfaceUrl = [NSString stringWithFormat:@"javascript:goBack()"];
          [_checkOutWebView evaluateJavaScript:interfaceUrl completionHandler:nil];
          [[NSURLCache sharedURLCache] removeAllCachedResponses];
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:SCAN_NOTIFICATION object:SHOW];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void) receiveConfirmNotification:(NSNotification *) notification {
    if([self connected]) {
         noNetworkDidntLoad = NO;
    if ([[notification name] isEqualToString:CONFIRMATION_POPUP]) {
        [[ActivityIndicator sharedInstance] hideLoader];
        //NSString *interfaceUrl = [NSString stringWithFormat: @"%@", @"javascript:pay()"];
        NSLog(@"jsinterface %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"pay"]);
        NSString *interfaceUrl = [NSString stringWithFormat:@"javascript:%@()",[[NSUserDefaults standardUserDefaults] valueForKey:@"pay"]];
        [_checkOutWebView evaluateJavaScript:interfaceUrl completionHandler:nil];
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
        }
    else if ([[notification name] isEqualToString:SHOW_LOADER]) {
        [[ActivityIndicator sharedInstance] showLoader];
    }
    else if ([[notification name] isEqualToString:DISMISS_LOADER]) {
        [[ActivityIndicator sharedInstance] hideLoader];
    }
    }
    else {
         noNetworkDidntLoad = YES;
            [AlertVC showAlertWithTitleForView:APP_NAME message:NO_INTERNET_CONNECTION controller:self];
    }
}

- (void) receiveTestNotificationCheckOut:(NSNotification *) notification {
    if ([[notification name] isEqualToString:WEB_VIEW_DID_FINISH]) {
            [_retry invalidate];
        if(!_webViewLoaded){
        _webViewLoaded = true;
        [self sendJsonThroughInterface];
        }
        [[ActivityIndicator sharedInstance] hideLoader];
        } else if ([[notification name] isEqualToString:DID_FAIL_WITH_ERROR])
        {
            [_retry invalidate];
        if(!_webViewLoaded){
        _webViewLoaded = true;
        [self loadHtml];
        }
        [[ActivityIndicator sharedInstance] hideLoader];
       
        } else if ([[notification name] isEqualToString:SHOW_POPUP])
        {
        NSString *interfaceUrl = [NSString stringWithFormat: @"%@", @"javascript:closePopup()"];
        [_checkOutWebView evaluateJavaScript:interfaceUrl completionHandler:nil];
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
        } else if ([[notification name] isEqualToString:PAYMENT_SUCCESS])
        {
            NSDictionary *dict = notification.userInfo;
            NSString *msg=[dict valueForKey:@"popUpMsg"];
        // NSString * successMsg =[NSString stringWithFormat:@"%s\"%s\"%s"," Thank you for your purchase. This charge will show up on your statement as ", "AveriGo Markets","."];
            [self showAlert:msg];
        } else if ([[notification name] isEqualToString:TOKEN_EXPIRED])
        {
        NSString *urlString = @"https://vending.averiware.com/payment.php?UserName=";
        [self loadRequestFromString:urlString];
        self.tabBarController.selectedIndex=0;
        [self.navigationController popToRootViewControllerAnimated:YES];
        }
    else if ([[notification name] isEqualToString:PAYROLL_POPUP])
    {
        NSString * successMsg =[NSString stringWithFormat:@"%@", ALERT_PAYROLL_DEDUCTION];
        [self showAlert:successMsg];
    }
    else if ([[notification name] isEqualToString:@"ShowErrorPopUp"])
    {
        NSDictionary *dict = notification.userInfo;
        NSString *msg=[dict valueForKey:@"popUpMsg"];
        
        NSString *isBackButton = [NSString stringWithFormat: @"%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"isBackButton"]];
        
        if ([msg  isEqual: @"backButton"]) {
            if ([isBackButton isEqualToString: @"0"]) {
                isBackBtnFlag = NO;
                self.title = @"Check Out";
            } else {
                isBackBtnFlag = YES;
                self.title = @"Add Funds";
            }
            
        } else {
        [self showErrorAlert:msg];
        }
    }
    
}

- (void)formJsonBody {
    
    float price=[_total_price floatValue];
    _crvtax=[objDBHelper getCRVTaxTotal:self.micromarketid];
    
        // ---Value From DB for Purchase History ---
    self.order_array=[[NSMutableArray alloc]init];
    self.order_array=[objDBHelper getCartItems:self.micromarketid];
    
    NSMutableDictionary *checkout=[[NSMutableDictionary alloc]init];
    NSMutableDictionary *order;
    NSMutableArray *orderArray = [[NSMutableArray alloc]init];
    for(int i =0;i<[self.order_array count];i++)
        {
        NSDictionary *dictobject;
        dictobject=[self.order_array objectAtIndex:i];
        order=[[NSMutableDictionary alloc]init];
        
        [order setObject:[NSString stringWithFormat:@"%@", [dictobject valueForKey:@"DiscountPrice"]] forKey:@"ItemPrice"];
        [order setObject:[NSString stringWithFormat:@"%@", [dictobject valueForKey:@"ItemNo"]] forKey:@"ItemID"];
        [order setObject:[NSString stringWithFormat:@"%@", [dictobject valueForKey:@"Quantity"]] forKey:@"Quantity"];
        [order setObject:[dictobject valueForKey:@"ItemName"] forKey:@"ItemName"];
        [order setObject:[dictobject valueForKey:@"SalesTaxEnable"] forKey:@"Taxable"];
        [order setObject:[dictobject valueForKey:@"CategoryID"] forKey:@"CategoryID"];
        [order setObject:[[NSUserDefaults standardUserDefaults]valueForKey:@"featured"] forKey:@"Featured"];
        [order setObject:[NSString stringWithFormat:@"%@", [dictobject valueForKey:@"Barcode"]] forKey:@"UPC"];

        NSNumberFormatter *taxFormatter = [[NSNumberFormatter alloc] init];
        taxFormatter.numberStyle = NSNumberFormatterDecimalStyle;
        //[taxFormatter setMinimumFractionDigits:4];
        [taxFormatter setMaximumFractionDigits:4];
            
        NSString *itemSalesTax = [objDBHelper getSalesTax: [dictobject valueForKey:@"ItemNo"]];
        float itemSalestaxValue = [taxFormatter numberFromString:itemSalesTax].floatValue;
        NSString *roundedTotalItemSalesTaxValue = [taxFormatter stringFromNumber:[NSNumber numberWithFloat:itemSalestaxValue]];
        //NSLog(@"totalitemsalesTaxvalue %@", roundedTotalItemSalesTaxValue);
            
        NSString *itemCRVTax = [objDBHelper getCRVTax: [dictobject valueForKey:@"ItemNo"]];
        float itemCRVTaxValue = [taxFormatter numberFromString:itemCRVTax].floatValue;
        NSString *roundedTotalItemCRVTaxValue = [taxFormatter stringFromNumber:[NSNumber numberWithFloat:itemCRVTaxValue]];
        //NSLog(@"totalitemCRVTaxvalue %@", roundedTotalItemCRVTaxValue);
            
        NSMutableDictionary *extraData = [[NSMutableDictionary alloc] init];
        [extraData setObject:[NSString stringWithFormat:@"%@ (%@)",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"],[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]] forKey:@"AppVersion"];
        [extraData setObject:@"iOS" forKey:@"DeviceOS"];

            
        [order setObject:roundedTotalItemSalesTaxValue forKey:@"Tax"];
        [order setObject:[dictobject valueForKey:@"CRVTAXEnable"] forKey:@"CRVEnabled"];
        [order setObject:roundedTotalItemCRVTaxValue forKey:@"CRV"];
            [order setObject:extraData forKey:@"ExtraData"];
        [orderArray addObject:order];
        }
    
    [checkout setObject:orderArray forKey:EMP_STR];
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:DateFormatCheckout];
    NSString *currentTime = [dateFormatter stringFromDate:today];
    
        // --- SalesTax Calculation ---
    _salestax=[objDBHelper getSalesTaxTotal:self.micromarketid];
    float tax;
    tax = [_salestax doubleValue];
    
        // --- Formatter To RoundOff SalesTax ---
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
    float taxValue = [numberFormatter numberFromString:_salestax].floatValue;
    float totalTaxValue = floorf(taxValue * 100 + 0.5) / 100;
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setMaximumFractionDigits:2];
    NSString *roundedSalesTax = [formatter stringFromNumber:[NSNumber numberWithFloat:totalTaxValue]];
    
    if ([roundedSalesTax  isEqual: @"0"] ) {
        roundedSalesTax = ZERO_POINT_ZERO;
        } else if (roundedSalesTax.length == 3) {
        roundedSalesTax = [roundedSalesTax stringByAppendingString:@"0"];
        } else {
        roundedSalesTax = roundedSalesTax;
        }
    
        // --- CRV Tax & TotalPrice Calculation ---
    float crvtax=[_crvtax floatValue];
    float total_price = price+totalTaxValue+crvtax;
    total_=[NSString stringWithFormat:@"%.02f",total_price];
    
        // Adding Values to Dictionary of Transactions, PurchaseHistory, BeaconDetails....
    NSString *beaconMajor;
    NSString *beaconMinor;
    
    beaconMajor = [[NSUserDefaults standardUserDefaults]objectForKey:@"beaconMajor"];
    beaconMinor = [[NSUserDefaults standardUserDefaults]objectForKey:@"beaconMinor"];
    
    NSTimeZone* currentTimeZone = [NSTimeZone localTimeZone];
    
    NSMutableDictionary *TransactionDetails = [[NSMutableDictionary alloc] init];
    [TransactionDetails setObject:_total_price forKey:@"Subtotal"];
    [TransactionDetails setObject:_discountTotal forKey:@"TotalDiscount"];
    [TransactionDetails setObject:_crvtax forKey:@"CRV"];
    [TransactionDetails setObject:roundedSalesTax forKey:@"SalesTax"];
    [TransactionDetails setObject:total_ forKey:@"Total"];
    [TransactionDetails setObject:currentTime forKey:@"Timestamp"];
    [TransactionDetails setObject:currentTimeZone.name forKey:@"Timezone"];
    
    NSMutableDictionary *beaconDetails = [[NSMutableDictionary alloc] init];
    [beaconDetails setObject:[NSString stringWithFormat:@"%@", beaconMajor] forKey:@"BeaconMajor"];
    [beaconDetails setObject:[NSString stringWithFormat:@"%@", beaconMinor] forKey:@"BeaconMinor"];
    
    NSMutableDictionary *payrollDetails = [[NSMutableDictionary alloc] init];
    [payrollDetails setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"payrollstatus"] forKey:@"PayrollStatus"];
    [payrollDetails setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"payuserid"] forKey:@"PayUserId"];
    [payrollDetails setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"creditstatus"] forKey:@"CreditStatus"];
    [payrollDetails setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"credituserid"] forKey:@"CreditUserId"];
    [payrollDetails setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"serviceName"] forKey:@"ServiceName"];
    
    NSMutableDictionary *historyDetails = [[NSMutableDictionary alloc] init];
    [historyDetails setObject:beaconDetails forKey:@"Beacon"];
    [historyDetails setObject:TransactionDetails forKey:@"Transaction"];
    [historyDetails setObject:orderArray forKey:@"PurchaseDetails"];
    [historyDetails setObject:payrollDetails forKey:@"PayRoll"];
    [historyDetails setValue:[NSNumber numberWithBool:YES] forKey:@"SupportsConfirmation"];
    [historyDetails setValue:[NSNumber numberWithBool:YES] forKey:@"SupportsHTML"];

    NSMutableDictionary *resul = [[NSMutableDictionary alloc] init];
    [resul setObject:historyDetails forKey:EMP_STR];
    //NSLog(@"JSON string :%@",historyDetails);
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:historyDetails options:0 error:nil];
    Json_String = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    //NSLog(@"%@", Json_String);
}

- (void)cancelWeb {
    //NSLog(@"didn't finish loading within 20 sec");
    // do anything error
    if([_checkOutWebView isLoading]){
        [_checkOutWebView stopLoading];
    }
    _webViewLoaded = false;
    NSString *urlString = @"https://vending.averiware.com/payment.php?UserName=";
    [self loadRequestFromString:urlString];

}
    // Load From Request..
- (void)loadRequestFromString:(NSString*)urlString {
    [[ActivityIndicator sharedInstance] showLoader];
    if ([self connected])
    {
        MyJSInterface* interface = [MyJSInterface new];
        [_checkOutWebView addJavascriptInterfaces:interface WithName:JSINTERFACE];
        NSString *userName;
        NSString *Secret;
        NSString *version;
        userName =  [[NSUserDefaults standardUserDefaults]objectForKey:@"username"];
        Secret  = @"&Secret=97AE5B1E44D5438872AB1AF53A292";
        version = @"&version=v2&isUpgraded=1";
        NSString *concatUrl = [NSString stringWithFormat: @"%@%@%@%@", urlString, userName, Secret, version];
        
        if ((BeaconMinor != -1) && (BeaconMajor != -1)) {
            
            concatUrl = [NSString stringWithFormat: @"%@%@%d%@%d", concatUrl, @"&BeaconMajor=", BeaconMajor, @"&BeaconMinor=", BeaconMinor];
        }
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                
                NSURL *url = [NSURL URLWithString:concatUrl];
                //NSURLRequest *req = [NSURLRequest requestWithURL:url];
                NSURLRequest* req = [NSURLRequest requestWithURL:url];
                self->_retry = [NSTimer scheduledTimerWithTimeInterval:20 target:self selector:@selector(cancelWeb) userInfo:nil repeats:NO];
                //NSLog(@"_checkOutWebView loadRequest:req");
                
                [self->_checkOutWebView loadRequest:req];
                
            });
        });
    }
    else
    {
        [[ActivityIndicator sharedInstance] hideLoader];
        ReachabilityStatus status = [GLobalRealReachability currentReachabilityStatus];
        NSLog(@"Initial reachability status:%@",@(status));
        [AlertVC showAlertWithTitleForView:APP_NAME message:NO_INTERNET_CONNECTION controller:self];
        [self loadHtml];
    }
    
}

- (void) loadHtml {
    NSString *headerString = @"<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'></header>";

    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"errorHtml" ofType:@"html"];
    NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
    [_checkOutWebView loadHTMLString:[headerString stringByAppendingString:htmlString] baseURL: [[NSBundle mainBundle] bundleURL]];
    noNetworkDidntLoad = YES;
}

- (void)sendJsonThroughInterface {
    [self formJsonBody];
    //NSLog(@"JSON string :%@",Json_String);
    printf("json %s", [Json_String UTF8String]);
    NSString *interfaceUrl = [NSString stringWithFormat: @"%@%@%@", @"javascript:passData(", Json_String, @")"];
    [_checkOutWebView evaluateJavaScript:interfaceUrl completionHandler:nil];
}

- (void)networkChanged:(NSNotification *)notification {
    RealReachability *reachability = (RealReachability *)notification.object;
    ReachabilityStatus status = [reachability currentReachabilityStatus];
    /*if(status == RealStatusNotReachable){
        [[ActivityIndicator sharedInstance] hideLoader];
    }*/
     //NSLog(@"[[ActivityIndicator sharedInstance] isLoading]:%@",@(![[ActivityIndicator sharedInstance] isLoading]));
    if(![[ActivityIndicator sharedInstance] isLoading] && status != RealStatusNotReachable) {
        //NSLog(@"isloading yes:%@",@(status));
    
        if(status == ReachableViaWiFi && currentStatus == ReachableViaWWAN) {
            [self cancelWeb];
        }else if (status != RealStatusNotReachable) {
            if(noNetworkDidntLoad){
                [self cancelWeb];
            }
        }
    }
    currentStatus = status;
    //NSLog(@"currentStatus:%@",@(status));
}

- (BOOL)connected {
    ReachabilityStatus status = [GLobalRealReachability currentReachabilityStatus];
    //NSLog(@"Initial reachability status:%@",@(status));
    
    return status != RealStatusNotReachable;
}

- (void)showAlert:(NSString *)message {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:APP_NAME message:message preferredStyle:UIAlertControllerStyleAlert];
    NSMutableAttributedString *titleAttr = [[NSMutableAttributedString alloc] initWithString:APP_NAME];
    [titleAttr addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:FONT_MEDIUM size:20]
                      range:NSMakeRange(0, [titleAttr length])];
    
    [alertController setValue:titleAttr forKey:@"attributedTitle"];
    
    NSMutableAttributedString *messageAttr = [[NSMutableAttributedString alloc] initWithString:message];
    [messageAttr addAttribute:NSFontAttributeName
                        value:[UIFont fontWithName:FONT_REGULAR size:14]
                        range:NSMakeRange(0, [messageAttr length])];
    
    
    [alertController setValue:messageAttr forKey:@"attributedMessage"];
    UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"OK"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action)
                                {
                                    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"checkoutstamp"];
                                    
                                    [objDBHelper deleteCartByMicroMarket:self.micromarketid];
                                    
                                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"purchased"];

                                    [[NSNotificationCenter defaultCenter] postNotificationName:SCAN_NOTIFICATION object:SHOW];
                                    self.tabBarController.selectedIndex=4;
                                    [self.navigationController popToRootViewControllerAnimated:YES];
                                }];
    [alertController addAction:yesButton];
    [self presentViewController:alertController animated:YES completion:nil];
    
}

- (void)showErrorAlert:(NSString *)message {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:APP_NAME message:message preferredStyle:UIAlertControllerStyleAlert];
    NSMutableAttributedString *titleAttr = [[NSMutableAttributedString alloc] initWithString:APP_NAME];
    [titleAttr addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:FONT_MEDIUM size:20]
                      range:NSMakeRange(0, [titleAttr length])];
    
    [alertController setValue:titleAttr forKey:@"attributedTitle"];
    
    NSMutableAttributedString *messageAttr = [[NSMutableAttributedString alloc] initWithString:message];
    [messageAttr addAttribute:NSFontAttributeName
                        value:[UIFont fontWithName:FONT_REGULAR size:14]
                        range:NSMakeRange(0, [messageAttr length])];
    
    
    [alertController setValue:messageAttr forKey:@"attributedMessage"];
    UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"OK"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action)
                                {
        if (self->isBackBtnFlag != YES) {

                                    NSString *interfaceUrl = [NSString stringWithFormat: @"%@", @"javascript:closePopup()"];
                                    [_checkOutWebView evaluateJavaScript:interfaceUrl completionHandler:nil];
                                    [[NSURLCache sharedURLCache] removeAllCachedResponses];
        }
                                }];
    [alertController addAction:yesButton];
    [self presentViewController:alertController animated:YES completion:nil];
    
}

- (void)setScreenAnalytics {
    
    NSString * SessionGUID = [[NSUserDefaults standardUserDefaults] objectForKey:@"SessionGUID"];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString * checkInTime=[dateFormatter stringFromDate:[NSDate date]];
    NSString * micromarketid = [[NSUserDefaults standardUserDefaults] valueForKey:@"micromarketid"];
    NSString * micromarketname = [[NSUserDefaults standardUserDefaults] valueForKey:@"micromarketname"];
    NSString * userID = [[NSUserDefaults standardUserDefaults] objectForKey:@"username"];
    NSString * analyticsCount = [objDBHelper getScreenAnalyticsBySessionID:SessionGUID andScreenName:@"CheckOut"];
    int visitCount = [analyticsCount intValue] +1;
    if(visitCount == 1) {
    [objDBHelper insertScreenAnalytics:userID andsessionGUID:SessionGUID andMircoMarketID:[micromarketid intValue] andMicroMarketName:micromarketname andScreenName:@"CheckOut" andscreenVisits:visitCount andCheckInTime:checkInTime andCheckOutTime:@"" andSyncFlag:@"Y"];
    } else {
    [objDBHelper UpdateScreenAnalytics:SessionGUID andVisitCount:visitCount andScreenName:@"CheckOut"];
    }
}

- (void)updateScreenAnalyticsCheckOutTime {
    
    NSString * SessionGUID = [[NSUserDefaults standardUserDefaults] objectForKey:@"SessionGUID"];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString * checkOutTime=[dateFormatter stringFromDate:[NSDate date]];
    [objDBHelper UpdateScreenAnalyticsCheckOutTime:SessionGUID andCheckOutTime:checkOutTime andScreenName:@"CheckOut"];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillTerminateNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
}

- (void)appWillTerminate:(NSNotification*)note {
    [self updateScreenAnalyticsCheckOutTime];
}

@end


