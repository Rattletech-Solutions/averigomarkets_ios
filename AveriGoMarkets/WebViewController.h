    //
    //  WebViewController.h
    //  AveriGo Markets
    //
    //  Created by BTS on 14/03/18.
    //  Copyright © 2018 BTS. All rights reserved.
    //

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>


@interface WebViewController : UIViewController<WKUIDelegate>

@property(strong,nonatomic) IBOutlet UIButton *closeBtn;
@property (weak, nonatomic) IBOutlet WKWebView *inAppView;
@property (weak, nonatomic) IBOutlet UILabel *title_lbl;
@property (weak,nonatomic) NSString *loadStr;
@property (weak,nonatomic) NSString *titleStr;

@end
