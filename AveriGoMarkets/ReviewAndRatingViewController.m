//
//  RateAndReviewViewController.m
//  AveriGo Markets
//
//  Created by Macmini on 11/19/19.
//  Copyright © 2019 BTS. All rights reserved.
//

#import "ReviewAndRatingViewController.h"
#import "AllConstants.h"
#import <HCSStarRatingView/HCSStarRatingView.h>
#import <UITextView+Placeholder/UITextView+Placeholder.h>
#import "Reachability.h"
#import "RealReachability.h"
#import "AlertVC.h"
#import "AppDelegate.h"
#import "AFNHelper.h"
#import "AFHTTPSessionManager.h"
#import "MBProgressHUD.h"
#import "ActivityIndicator.h"

@interface ReviewAndRatingViewController ()

@property(nonatomic, weak) UILabel *titleLabel;

@property (nonatomic, weak) HCSStarRatingView *ratingView;

@property (nonatomic, weak) UITextView *descriptionTextView;

@property(nonatomic, weak) UIButton *submitButton;

@property(nonatomic, weak) UIButton *deleteButton;

@property(nonatomic, strong) UIButton *backButton;

@end

@implementation ReviewAndRatingViewController

@synthesize delegate;

//MARK : Life cycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setupViews];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
    self.navigationItem.title = @"Rate/Review";

    [self.navigationController.navigationBar setTitleTextAttributes:
        @{NSForegroundColorAttributeName:[UIColor colorWithRed:0.98 green:0.96 blue:0.95 alpha:1.0],NSFontAttributeName:[UIFont fontWithName:FONT_REGULAR size:20]}];
    
    self.navigationController.navigationBar.tintColor  = [UIColor  whiteColor];
    
    self.backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
    
    NSString *ratingID = [NSString stringWithFormat: @"%@", [self.purchaseDictionary valueForKey:@"RATING_ID"]];
    
    NSString *ratingValue = [NSString stringWithFormat: @"%@", [self.purchaseDictionary valueForKey:@"RATING_VALUE"]];
    
    if (![ratingID isEqualToString: @"0"] && ![ratingID isKindOfClass: NULL] && ![ratingID isEqualToString: @""]) {
    self.ratingView.value = [ratingValue floatValue];
    
    self.descriptionTextView.text = self.purchaseDictionary[@"RATING_FEEDBACK"];
    
    self.deleteButton.hidden = NO;
        
    self.modeString = PARAM_MODE_UPDATE;
        
    self.ratingID = ratingID;
        
    [_submitButton setTitle:@"Update" forState:UIControlStateNormal];
        
    } else {
        self.ratingView.value = 0;
        
        self.descriptionTextView.text = @"";
        
        self.deleteButton.hidden = YES;
        
        self.ratingID = @"0";
        
        [_submitButton setTitle:@"Submit" forState:UIControlStateNormal];
    }
}

- (BOOL)connected {
    ReachabilityStatus status = [GLobalRealReachability currentReachabilityStatus];
    NSLog(@"Initial reachability status:%@",@(status));
    
    return status == 1 || status == 2;
}

-(void)hideLoader {
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [[ActivityIndicator sharedInstance] hideLoader];
        });
    });
}

-(void)showLoader {
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [[ActivityIndicator sharedInstance] showLoader];
        });
    });
}

- (void)submitRatingsAPI {
    
    [self showLoader];
    
    NSMutableArray *parmArray = [[NSMutableArray alloc] init];
    
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setObject: SECRET_KEY forKey:@"Secret"];
    [dictParam setObject: self.ratingID  forKey:@"Id"];
    [dictParam setObject: [NSString stringWithFormat: @"%.2f", self.ratingView.value]  forKey:@"RatingValue"];
    [dictParam setObject: self.descriptionTextView.text forKey:@"RatingFeedback"];
    [dictParam setObject: self.microMartketString  forKey:@"Micromarket"];
    [dictParam setObject: self.purchaseDictionary[@"ITEM_DESC"] forKey:@"ProductName"];
    [dictParam setObject: self.purchaseDictionary[@"BAR_CODE"] forKey:@"UPC"];
    [dictParam setObject: [[NSUserDefaults standardUserDefaults]objectForKey:@"username"] forKey:@"UserName"];
    [dictParam setObject: self.modeString forKey:@"Mode"];

    [parmArray addObject: dictParam];
    
    NSLog(@"%@", dictParam);
    
     if([self connected]) {
        NSString *rootURL =  @"http://vending.averiware.com:8080/saveratingreview.php";//[[AppDelegate shared] rootUrl];
         
         AFNHelper *afn = [[AFNHelper alloc] init];
         
         [afn postFormDataPath: @"" getUrl: rootURL withParamData: dictParam withBlock:^(id response, NSError *error) {
             if(error)
                 {
                     self.submitButton.userInteractionEnabled = YES;

                     NSLog(@"%@",error);
                     NSLog(@"ERROR DESC:%@",error.localizedDescription);
                                     [self hideLoader];
                                    
                                     if(error.code == NSURLErrorTimedOut){
                                         [AlertVC showAlertWithTitleForView: APP_NAME message: ERROR_TIMED_OUT controller: self];
                                     }else if(error.code == NSURLErrorNotConnectedToInternet){
                                         [AlertVC showAlertWithTitleForView: APP_NAME message: NO_INTERNET_CONNECTION controller: self];
                                     }else{
                                          [AlertVC showAlertWithTitleForView: APP_NAME message: REQUEST_TIMED_OUT controller: self];
                                     }
                 }
             else
                 {
                     self.submitButton.userInteractionEnabled = YES;
                     self.deleteButton.userInteractionEnabled = YES;

                     [self hideLoader];

                     NSDictionary *dictionary;
                     dictionary = response;
                     NSLog(@"%@",response);
                                          
                     
                     NSString * message =  [NSString stringWithFormat: @"%@", response[@"message"]];
                     
                     UIAlertController *alertController = [UIAlertController alertControllerWithTitle:APP_NAME message:message preferredStyle:UIAlertControllerStyleAlert];
                     NSMutableAttributedString *titleAttr = [[NSMutableAttributedString alloc] initWithString:APP_NAME];
                     [titleAttr addAttribute:NSFontAttributeName
                                       value:[UIFont fontWithName:FONT_MEDIUM size:20]
                                       range:NSMakeRange(0, [titleAttr length])];
                     
                     [alertController setValue:titleAttr forKey: @"attributedTitle"];
                     
                     NSMutableAttributedString *messageAttr = [[NSMutableAttributedString alloc] initWithString:message];
                     [messageAttr addAttribute:NSFontAttributeName
                                         value:[UIFont fontWithName:FONT_REGULAR size:14]
                                         range:NSMakeRange(0, [messageAttr length])];
                     
                     
                     [alertController setValue:messageAttr forKey: @"attributedMessage"];
                     UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"OK"
                                                                         style:UIAlertActionStyleDefault
                                                                       handler:^(UIAlertAction * action)
                                                 {
                          dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                      [[NSUserDefaults standardUserDefaults] setBool: YES forKey: @"refreshView"];
                         
                                                      [[NSUserDefaults standardUserDefaults] synchronize];
                         
                                                      [self redirectToPreviousView];
                         });

                                                 }];
                     [alertController addAction:yesButton];
                     
                     [self presentViewController:alertController animated:YES completion:nil];
                 }
         }];
         
    } else {
        [self hideLoader];
        [AlertVC showAlertWithTitleForView:APP_NAME message: NO_INTERNET_CONNECTION controller: self];
    }
}

-(void)redirectToPreviousView {
    
    if (([[NSUserDefaults standardUserDefaults] boolForKey:@"refreshView"] == YES)) {
        //code to be executed on the main queue after delay
        [self.navigationController popToRootViewControllerAnimated: YES];
    } else {
        [self redirectToPreviousView];
    }
}

-(void)submitBtnClicked:(id)sender {
    
    if (self.ratingView.value != 0) {
        self.submitButton.userInteractionEnabled = NO;
        self.deleteButton.userInteractionEnabled = NO;
    [self submitRatingsAPI];
    } else {
        [AlertVC showAlertWithTitleForView:APP_NAME message: @"Please rate the product" controller: self];
    }
}

-(void)deleteBtnClicked:(id)sender {
    
    self.deleteButton.userInteractionEnabled = NO;

    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle: APP_NAME
                                 message: @"Are you sure you want to delete your rating/review?"
                                 preferredStyle: UIAlertControllerStyleAlert];

    //Add Buttons

    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle: @"Yes"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                    self.modeString = PARAM_MODE_DELETE;
        self.deleteButton.userInteractionEnabled = YES;

                                    [self submitRatingsAPI];
                                }];

    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle: @"Cancel"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle no, thanks button
        self.deleteButton.userInteractionEnabled = YES;

                               }];

    //Add your buttons to alert controller

    [alert addAction:yesButton];
    [alert addAction:noButton];

    [self presentViewController:alert animated:YES completion:nil];
}

//MARK: - Views setup

- (void)setupViews {
    
//    {
//        UILabel *titleLabel = [[UILabel alloc] init];
//
//        titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
//
//        titleLabel.numberOfLines = 0;
//
//        titleLabel.textColor = [UIColor blackColor];
//
//        titleLabel.font = [UIFont fontWithName: FONT_BOLD size: 20.f];
//
//        titleLabel.text = @"Rate";
//
//        titleLabel.textAlignment = NSTextAlignmentCenter;
//
//        [self.view addSubview: titleLabel];
//
//        self.titleLabel = titleLabel;
//    }
//
    {
           HCSStarRatingView *starRatingView = [[HCSStarRatingView alloc]init];
           
           starRatingView.translatesAutoresizingMaskIntoConstraints = NO;
           
           starRatingView.backgroundColor = [UIColor clearColor];
           
           starRatingView.maximumValue = 5;
           
           starRatingView.minimumValue = 0;
           
           starRatingView.value = 0;
        
           starRatingView.spacing = 20;
           
           starRatingView.starBorderColor = [UIColor grayColor];
           
           starRatingView.emptyStarColor = [UIColor whiteColor];
           
           starRatingView.tintColor = [UIColor colorWithRed: 116.0/255.0 green: 187.0/255.0 blue: 60.0/255.0 alpha: 1.0];
           
           starRatingView.allowsHalfStars = NO;
           
           starRatingView.userInteractionEnabled = YES;
           
           [self.view addSubview: starRatingView];
           
           self.ratingView = starRatingView;
           
       }
    
    {
        UITextView *textView = [UITextView new];
           
        textView.translatesAutoresizingMaskIntoConstraints = NO;
           
        textView.backgroundColor = [UIColor clearColor];
           
        textView.textColor = [UIColor blackColor];
        
        textView.tintColor = [UIColor blackColor];
           
        textView.clipsToBounds = YES;
           
        textView.font = [UIFont fontWithName: FONT_REGULAR size: 16.f];
                          
        textView.placeholder = @"Tell us what you like or don't like about this product";
        
        textView.placeholderColor = [UIColor grayColor];
               
        textView.showsVerticalScrollIndicator = NO;
        
        textView.layer.cornerRadius = 15;
        
        textView.layer.masksToBounds = YES;
        
        textView.layer.borderColor = [UIColor blackColor].CGColor;
        
        textView.layer.borderWidth = 1;
        
        textView.textContainerInset = UIEdgeInsetsMake(15, 15, 15, 15);
        
        [self.view addSubview: textView];
        
        self.descriptionTextView = textView;
    }
    
    {
        UIButton *button = [UIButton buttonWithType: UIButtonTypeCustom];
           
        button.translatesAutoresizingMaskIntoConstraints = NO;
           
        button.clipsToBounds = YES;

        button.backgroundColor = [UIColor colorWithRed: 0.0/255.0 green: 164.0/255.0 blue: 215.0/255.0 alpha: 1.0];

        [button setTitle: @"Submit" forState: UIControlStateNormal];
           
        [button setTitleColor: [UIColor whiteColor] forState: UIControlStateNormal];
           
        [button setTitleColor: [[UIColor whiteColor] colorWithAlphaComponent: 0.3] forState: UIControlStateHighlighted];

        [button.titleLabel setFont: [UIFont fontWithName: FONT_REGULAR size: 16.f]];
        
        button.layer.cornerRadius = 5;
        
        button.layer.masksToBounds = YES;
                
        button.clipsToBounds = YES;

        [button addTarget:self action:@selector(submitBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.view addSubview: button];
        
        self.submitButton = button;
    }
    
    {
        UIButton *button = [UIButton buttonWithType: UIButtonTypeCustom];
           
        button.translatesAutoresizingMaskIntoConstraints = NO;
           
        button.clipsToBounds = YES;

        button.backgroundColor = [UIColor redColor];

        [button setTitle: @"Delete" forState: UIControlStateNormal];
           
        [button setTitleColor: [UIColor whiteColor] forState: UIControlStateNormal];
           
        [button setTitleColor: [[UIColor whiteColor] colorWithAlphaComponent: 0.3] forState: UIControlStateHighlighted];

        [button.titleLabel setFont: [UIFont fontWithName: FONT_REGULAR size: 16.f]];
                
        button.layer.cornerRadius = 5;
        
        button.layer.masksToBounds = YES;
        
        button.clipsToBounds = YES;
        
        [button addTarget:self action:@selector(deleteBtnClicked:) forControlEvents:UIControlEventTouchUpInside];

        [self.view addSubview: button];
        
        self.deleteButton = button;
    }

[self setupConstraints];
    
}

- (void)setupConstraints {
    
//    [[self.titleLabel.leftAnchor constraintEqualToAnchor: self.view.safeAreaLayoutGuide.leftAnchor constant: 20.0] setActive: YES];
//    [[self.titleLabel.rightAnchor constraintEqualToAnchor: self.view.safeAreaLayoutGuide.rightAnchor constant: -20.0] setActive: YES];
//    [[self.titleLabel.topAnchor constraintEqualToAnchor: self.view.safeAreaLayoutGuide.topAnchor constant: 30.0] setActive: YES];

    [[self.ratingView.centerXAnchor constraintEqualToAnchor: self.ratingView.superview.centerXAnchor] setActive: YES];
    [[self.ratingView.topAnchor constraintEqualToAnchor: self.view.safeAreaLayoutGuide.topAnchor constant: 30.0] setActive: YES];
    [[self.ratingView.heightAnchor constraintEqualToConstant: 40.0] setActive: YES];
    
    [[self.descriptionTextView.topAnchor constraintEqualToAnchor: self.ratingView.bottomAnchor constant: 40.0] setActive: YES];
    [[self.descriptionTextView.leftAnchor constraintEqualToAnchor: self.view.safeAreaLayoutGuide.leftAnchor constant: 40.0] setActive: YES];
    [[self.descriptionTextView.rightAnchor constraintEqualToAnchor: self.view.safeAreaLayoutGuide.rightAnchor constant: -40.0] setActive: YES];
    [[self.descriptionTextView.heightAnchor constraintEqualToConstant: 200.0] setActive: YES];
    
    [[self.submitButton.topAnchor constraintEqualToAnchor: self.descriptionTextView.bottomAnchor constant: 40.0] setActive: YES];
    [[self.submitButton.leftAnchor constraintEqualToAnchor: self.view.safeAreaLayoutGuide.leftAnchor constant: 40.0] setActive: YES];
    [[self.submitButton.rightAnchor constraintEqualToAnchor: self.view.safeAreaLayoutGuide.rightAnchor constant: -40.0] setActive: YES];
    [[self.submitButton.heightAnchor constraintEqualToConstant: 40.0] setActive: YES];
    
    [[self.deleteButton.topAnchor constraintEqualToAnchor: self.submitButton.bottomAnchor constant: 10.0] setActive: YES];
    [[self.deleteButton.leftAnchor constraintEqualToAnchor: self.view.safeAreaLayoutGuide.leftAnchor constant: 40.0] setActive: YES];
    [[self.deleteButton.rightAnchor constraintEqualToAnchor: self.view.safeAreaLayoutGuide.rightAnchor constant: -40.0] setActive: YES];
    [[self.deleteButton.heightAnchor constraintEqualToConstant: 40.0] setActive: YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

