    //
    //  IntroViewController.h
    //  VendBeam
    //
    //  Created by BTS on 12/09/17.
    //  Copyright © 2017 BTS. All rights reserved.
    //

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import <CoreLocation/CoreLocation.h>

@interface IntroViewController : UIViewController<CBCentralManagerDelegate,CBPeripheralDelegate>

@property(strong,nonatomic)CBCentralManager *centralManager;

@end
