//
//  HomeCell.h
//  Averigo
//
//  Created by R VIKRAM on 23/03/17.
//  Copyright © 2017 BTS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeCell : UICollectionViewCell


@property(strong,nonatomic) IBOutlet UILabel *company_lbl;
@property(strong,nonatomic) IBOutlet UILabel *address_lbl;
@property(strong,nonatomic) IBOutlet UIImageView *banner_img;
@property(strong,nonatomic) IBOutlet UILabel *banner_lbl;
@property(strong,nonatomic) IBOutlet UIView *top_view;
@property(strong,nonatomic) IBOutlet UIView *bottom_view;
@property(strong,nonatomic) IBOutlet UIButton *viewButton;
@property(strong,nonatomic) IBOutlet UIButton *bestSellingBtn;
@property (strong, nonatomic) IBOutlet UILabel *featured_lbl;

@end
