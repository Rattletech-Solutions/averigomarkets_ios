//
//  FeedbackCentreController.h
//  AveriGo Markets
//
//  Created by LeniYadav on 30/10/18.
//  Copyright © 2018 BTS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedbackCentreController : UIViewController

@property(strong,nonatomic) IBOutlet UIButton *helpBtn;
@property(strong,nonatomic) IBOutlet UIButton *feedbackBtn;

@end
