//
//  CartCell.h
//  Averigo
//
//  Created by BTS on 22/11/16.
//  Copyright © 2016 BTS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CartCell : UITableViewCell

@property(strong,nonatomic) IBOutlet UIImageView *item_img;
@property(strong,nonatomic) IBOutlet UILabel *title_lbl;
@property(strong,nonatomic) IBOutlet UILabel *price_lbl;
@property(strong,nonatomic) IBOutlet UILabel *quantity_lbl;
@property(strong,nonatomic) IBOutlet UIButton *add_btn;
@property(strong,nonatomic) IBOutlet UIButton *minus_btn;
@property(strong,nonatomic) IBOutlet UIButton *delete_btn;

@end
