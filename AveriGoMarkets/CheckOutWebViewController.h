    //
    //  CheckOutWebViewController.h
    //  VendBeam
    //
    //  Created by BTS on 12/07/18.
    //  Copyright © 2018 BTS. All rights reserved.
    //

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <CoreLocation/CoreLocation.h>
#import <WebKit/WebKit.h>
#import "MyJSInterface.h"
#import "EasyJSWebView.h"
#import "DataBaseHandler.h"
#import "UIImage+FontAwesome.h"

@interface CheckOutWebViewController : UIViewController<WKNavigationDelegate,UITabBarDelegate, WKUIDelegate, UIScrollViewDelegate>

{
    DataBaseHandler *objDBHelper;
}

@property (strong, nonatomic) IBOutlet EasyJSWebView *checkOutWebView;
@property (strong, nonatomic)IBOutlet UIProgressView* progressView;
@property(strong,nonatomic) UIScrollView *content_scroll;
@property(strong,nonatomic) UILabel *total_lbl;
@property(strong,nonatomic) UILabel *select_lbl;
@property(strong,nonatomic) NSString *total_price;
@property(strong,nonatomic) NSString *discountTotal;
@property(strong,nonatomic) NSString *micromarketid;
@property(strong,nonatomic) NSMutableArray *order_array;
@property(strong,nonatomic) NSString *salestax;
@property(strong,nonatomic) NSString *crvtax;
@property(strong,nonatomic) NSString *serialno;
@property(strong,nonatomic) NSString *token;
@property(strong,nonatomic) NSString *custId;
@property(strong,nonatomic) NSString *cardNo;
@property(strong,nonatomic) NSMutableArray *card_array;
@property(strong,nonatomic) NSMutableDictionary *payment;
@property(strong,nonatomic) NSMutableArray *paymentArray;
@property(strong,nonatomic) NSMutableArray *check_array;
@property (strong, nonatomic) NSString *urlFromJSON;
@property (strong,nonatomic) NSTimer *timer;
@property (strong,nonatomic) NSTimer *retry;
@property (strong,nonatomic)  NSMutableArray  *beacons;
@property (strong,nonatomic)  NSString *UDID;

@property (nonatomic) BOOL fromShoppingCart;
@property (nonatomic) BOOL fromBarcodeTab;
@property BOOL webViewLoaded;



@end
