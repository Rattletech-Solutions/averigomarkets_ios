//
//  ForgotPasswordController.m
//  VendBeam
//
//  Created by BTS on 28/08/17.
//  Copyright © 2017 BTS. All rights reserved.
//

#import "ForgotPasswordController.h"
#import "AFNHelper.h"
#import "AllConstants.h"
#import "MBProgressHUD.h"
#import "Reachability.h"
#import "SendGrid.h"
#import "Base64.h"
#import "FBEncryptorAES.h"
#import "NSString+AESCrypt.h"
#import "TabBarViewController.h"
#import "AlertVC.h"
#import "ActivityIndicator.h"
#import "IQKeyBoardManager/IQKeyboardReturnKeyHandler.h"
#import "RealReachability.h"
#import "AppDelegate.h"

@interface ForgotPasswordController ()

{
    NSString *udid;
    NSString *password;
    NSString *currentTime;
    int count;
    IQKeyboardReturnKeyHandler *returnKeyHandler;

}

@end

@implementation ForgotPasswordController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.getOtpBtn.layer.cornerRadius = 5;
    self.getOtpBtn.clipsToBounds = YES;
    self.verifyOtpBtn.layer.cornerRadius = 5;
    self.verifyOtpBtn.clipsToBounds = YES;
    
    self.submitBtn.layer.cornerRadius = 5;
    self.submitBtn.clipsToBounds = YES;
    
    self.otpView.hidden = YES;
    self.resetView.hidden = NO;
    
    UIToolbar* keyboardToolbar = [[UIToolbar alloc] init];
    [keyboardToolbar sizeToFit];
    UIBarButtonItem *flexBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                      target:nil action:nil];
    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                      target:self.view action:@selector(endEditing:)];
    keyboardToolbar.items = @[flexBarButton, doneBarButton];
    self.otpTxt.inputAccessoryView = keyboardToolbar;
    
   // returnKeyHandler = [[IQKeyboardReturnKeyHandler alloc] initWithViewController:self];
    //[returnKeyHandler setLastTextFieldReturnKeyType:UIReturnKeyDone];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if(textField == self.passTxt){
    [textField resignFirstResponder];
        [self.confirmTxt becomeFirstResponder];
    }else{
      [textField resignFirstResponder];
    }
    return YES;
}

-(IBAction)btnCloseClicked:(id)sender
{
    [[ActivityIndicator sharedInstance] hideLoader];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)submitClicked:(id)sender
{
    
    if([self connected])
    {
        NSDate *today = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:DATEFORMAT_TYPE];
        currentTime = [dateFormatter stringFromDate:today];
        if (self.passTxt.text.length != 0)
        {
            
            if([self.confirmTxt.text isEqualToString:self.passTxt.text])
            {
                udid=[[NSUserDefaults standardUserDefaults] objectForKey:UDID];
                [[ActivityIndicator sharedInstance] showLoader];

                NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
                [dictParam setObject:self.otp             forKey:PARAM_OTP];
                [dictParam setObject:self.emailid         forKey:PARAM_EMAIL_ID_KEY];
                [dictParam setObject:MICROMARKET            forKey:PARAM_SERVICE_NAME_KEY];
                [dictParam setObject:udid                   forKey:PARAM_DEVICE_UDID_KEY];
                [dictParam setObject:currentTime            forKey:PARAM_DEVICE_DATE_TIME_KEY];
                [dictParam setObject:self.confirmTxt.text forKey:@"Password"];
                NSLog(@"username %@",dictParam);
                
                NSString *rootURL =  [[AppDelegate shared] rootUrl];

                AFNHelper *afn=[[AFNHelper alloc]init];
                [afn postDataFromPath:METHOD_CHANGEPASSWORD getUrl:rootURL withParamData:dictParam withBlock:^(id response, NSError *error)
                 {
                     if(error)
                     {
                         NSLog(@"ERROR DESC:%@",error.localizedDescription);
                         [[ActivityIndicator sharedInstance] hideLoader];

                     }
                     else
                     {
                         NSDictionary *dictionary;
                         dictionary=response;
                         NSString *status=[dictionary valueForKey:STATUS];
                         NSLog(@"dictionary %@",dictionary);
                         if([status isEqualToString:SUCCESS])
                         {
                             [self loginClicked];
                         }
                         else
                         {
                             [[ActivityIndicator sharedInstance] hideLoader];

                             NSString *message=@"Invalid User";
                             [self showAlert:message];
                         }
                         
                     }
                     dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
                         // Do something...
                         dispatch_async(dispatch_get_main_queue(), ^{
                             [[ActivityIndicator sharedInstance] hideLoader];
                         });
                     });
                     
                     
                 }];
            }
            else
            {
                [self showAlert:PASSWORD_DONT_MATCH];
            }
        }
        else
        {
            [self showAlert: @"Please enter password."];
        }
    }
    else
    {
        [self showAlert:NO_INTERNET_CONNECTION];
    }
    
}

-(void)showAlert:(NSString *) message {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:APP_NAME message:message preferredStyle:UIAlertControllerStyleAlert];
    NSMutableAttributedString *titleAttr = [[NSMutableAttributedString alloc] initWithString:APP_NAME];
    [titleAttr addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:FONT_MEDIUM size:20]
                      range:NSMakeRange(0, [titleAttr length])];
    
    [alertController setValue:titleAttr forKey:@"attributedTitle"];
    
    NSMutableAttributedString *messageAttr = [[NSMutableAttributedString alloc] initWithString:message];
    [messageAttr addAttribute:NSFontAttributeName
                        value:[UIFont fontWithName:FONT_REGULAR size:14]
                        range:NSMakeRange(0, [messageAttr length])];
    
    
    [alertController setValue:messageAttr forKey:@"attributedMessage"];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alertController addAction:defaultAction];
    [self presentViewController:alertController animated:YES completion:nil];
}


-(void)loginClicked
{
    password=self.confirmTxt.text;
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:DateFormat];
    NSString *currentTime = [dateFormatter stringFromDate:today];
    udid=[[NSUserDefaults standardUserDefaults] objectForKey:UDID];
    //[HUD showAnimated:YES];
    [[ActivityIndicator sharedInstance] showLoader];

    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setObject:self.emailid         forKey:PARAM_EMAIL_ID_KEY];
    [dictParam setObject:password               forKey:PARAM_PASSWORD_KEY];
    [dictParam setObject:MICROMARKET            forKey:PARAM_SERVICE_NAME_KEY];
    [dictParam setObject:udid                   forKey:PARAM_DEVICE_UDID_KEY];
    [dictParam setObject:currentTime            forKey:PARAM_DEVICE_DATE_TIME_KEY];
    NSLog(@"DICTPARAM:%@",dictParam);
    
    NSString *rootURL =  [[AppDelegate shared] rootUrl];

    AFNHelper *afn=[[AFNHelper alloc]init];
    [afn postDataFromPath:METHOD_SIGNIN getUrl:rootURL withParamData:dictParam withBlock:^(id response, NSError *error)
     {
         if(error)
         {
             [[ActivityIndicator sharedInstance] hideLoader];
             NSLog(@"ERROR DESC:%@",error.localizedDescription);
             [self showAlert:error.localizedDescription];
         }
         else
         {
             NSLog(@"response %@",response);
             NSDictionary *dictionary;
             dictionary=response;
             self.userdetails_array=[[NSMutableArray alloc]init];
             NSString *status=[dictionary valueForKey:STATUS];
             self.userdetails_array=[dictionary valueForKey:@"User"];
             NSDictionary *dictobject;
             dictobject=[self.userdetails_array objectAtIndex:0];
             if([status isEqualToString:SUCCESS])
             {
                 [[ActivityIndicator sharedInstance] hideLoader];

                 [[NSUserDefaults standardUserDefaults]setObject:@"signedin" forKey:@"signedin"];
                 [[NSUserDefaults standardUserDefaults]setObject:@"remember" forKey:@"remember"];
                 [[NSUserDefaults standardUserDefaults] setObject:self.userdetails_array forKey:@"userdetailsarray"];
                 [[NSUserDefaults standardUserDefaults]setObject:[dictobject valueForKey:@"EMAIL_ID"] forKey:@"username"];
                 [[NSUserDefaults standardUserDefaults]setObject:password forKey:@"password"];
                 [[NSUserDefaults standardUserDefaults]setObject:[dictobject valueForKey:@"FIRST_NAME"] forKey:@"firstname"];
                 [[NSUserDefaults standardUserDefaults]setObject:[dictobject valueForKey:@"LAST_NAME"] forKey:@"lastname"];
                 [[NSUserDefaults standardUserDefaults]setObject:[dictobject valueForKey:@"PROFILE_IMAGE"]forKey:@"imagename"];
                 [self dismissViewControllerAnimated:true completion:nil];
                 
             }
             else
             {
                 [[ActivityIndicator sharedInstance] hideLoader];
                 NSString *message=[dictionary valueForKey:MESSAGE];
                 [self showAlert:message];
             }
         }
         dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
             // Do something...
             dispatch_async(dispatch_get_main_queue(), ^{
                 [[ActivityIndicator sharedInstance] hideLoader];
                 //[HUD hideAnimated:YES];
             });
         });
         
         
     }];
    
}

- (BOOL)connected
{
    ReachabilityStatus status = [GLobalRealReachability currentReachabilityStatus];
    NSLog(@"Initial reachability status:%@",@(status));
    
    return status == 1 || status == 2;
}




/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

