//
//  BrowserController.m
//  AveriGo Markets
//
//  Created by Rattletech on 30/10/18.
//  Copyright © 2018 BTS. All rights reserved.
//

#import "BrowserController.h"
#import <STPopup/STPopup.h>
#import "ActivityIndicator.h"

@interface BrowserController ()

@end

@implementation BrowserController

- (instancetype)init
{
    if (self = [super init]) {
        self.contentSizeInPopup = CGSizeMake(self.view.frame.size.width , self.view.frame.size.height);
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"Help";
    self.inAppView.backgroundColor = [UIColor clearColor];
    self.inAppView.layer.cornerRadius = 5;
    self.inAppView.clipsToBounds = true;
    [self loadRequestFromString:@"http://www.averigo.com/markets/help"];//
}

-(void)viewWillDisappear:(BOOL)animated
{
     [[ActivityIndicator sharedInstance] hideLoader];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadRequestFromString:(NSString*)urlString
{
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [self.inAppView loadRequest:urlRequest];
}

-(IBAction)btnCloseClicked:(id)sender
{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [[ActivityIndicator sharedInstance] showLoader];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    //if (webView.isLoading)
    //  return;
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [[ActivityIndicator sharedInstance] hideLoader];
        });
    });
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [[ActivityIndicator sharedInstance] hideLoader];
        });
    });
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
