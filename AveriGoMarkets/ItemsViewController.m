    //
    //  ItemsViewController.m
    //  Averigo
    //
    //  Created by BTS on 09/11/16.
    //  Copyright © 2016 BTS. All rights reserved.
    //

#import "ItemsViewController.h"
#import "BarCodeController.h"
#import "ShoppingCartViewController.h"
#import "ItemsCell.h"
#import "ItemsTableCell.h"
#import "FilterCell.h"
#import <QuartzCore/QuartzCore.h>
#import "AFNHelper.h"
#import "AllConstants.h"
#import "MBProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "CategoryViewController.h"
#import "AlertVC.h"
#import "ActivityIndicator.h"
#import "AppDelegate.h"

@interface ItemsViewController ()
{
    int total_count;
    UILabel *count_lbl;
    NSString *search_txt;
    NSString *firstcategoryid;
    BOOL beaconIsThere;
    NSString *serviceName;
    UITextField *searchField;
    BOOL isCartClicked;
    BOOL noNetworkDidntLoad;
    
}

@end

@implementation ItemsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
        // Do any additional setup after loading the view.
    objDBHelper = [[DataBaseHandler alloc]init];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    self.navigationItem.title=@"Items";
    total_count=0;

    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor colorWithRed:0.98 green:0.96 blue:0.95 alpha:1.0],NSFontAttributeName:[UIFont fontWithName:FONT_REGULAR size:20]}];
    [self InitNavBar];
    [self.items_view registerNib:[UINib nibWithNibName:ITEMS_CELL bundle:nil] forCellWithReuseIdentifier:@"Items_Cell"];
    [self.items_table_view registerNib:[UINib nibWithNibName:@"ItemsTableCell" bundle:nil] forCellReuseIdentifier:@"ItemsTable_Cell"];
    typeSelected=@"list";
    [self.filter_btn setImage:[UIImage imageWithIcon:@"fa-filter" backgroundColor:[UIColor clearColor] iconColor:[UIColor darkGrayColor] andSize:CGSizeMake(30,30)] forState:UIControlStateNormal];
    [self.list_btn setImage:[UIImage imageWithIcon:@"fa-list-ul" backgroundColor:[UIColor clearColor] iconColor:[UIColor darkGrayColor] andSize:CGSizeMake(30,30)] forState:UIControlStateNormal];
    [UIView transitionWithView:self.items_view
                      duration:0.3
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        
                        [self.items_view setHidden:YES];
                        [self.items_table_view setHidden:NO];
                    }
                    completion:NULL];
    self.items_table_view.separatorStyle=UITableViewCellSeparatorStyleNone;
    self.quantityArray=[[NSMutableArray alloc]init];
    
    if([self connected]){
        noNetworkDidntLoad = NO;
    if(self.itemsArray==nil)
    {
        if([self.categoryId isEqualToString:@"'BESTSELLING'"]){
            [self getTopSelling];
        }else{
        [self getitems];
        }
    }
    self.categoryLbl.text = self.categoryName;
    
    [self getCategories];
    }else{
        [AlertVC showAlertWithTitleForView:APP_NAME message:NO_INTERNET_CONNECTION controller:self];
        noNetworkDidntLoad = YES;
    }
    
    firstcategoryid=self.categoryId;
    self.filterArray=[[NSMutableArray alloc]init];
    serviceName = [[NSUserDefaults standardUserDefaults]objectForKey:@"serviceName"];
   
}

- (void) showCategoryNotification:(NSNotification *)notification {
    [self.navigationController popToRootViewControllerAnimated:YES];
    self.tabBarController.selectedIndex=1;
}

- (void) scanBeaconNotification:(NSNotification *) notification {
    beaconIsThere = NO;
    NSDictionary *dict = notification.userInfo;
    _beacons=[dict valueForKey:@"Beacons"];
    NSMutableArray * UDIDS = [NSMutableArray array];
    
    for(int i=0;i<_beacons.count;i++)
        {
        CLBeacon *beacon = (CLBeacon*)[self.beacons objectAtIndex:i];
        NSString *BUUID=[NSString stringWithFormat:@"%d-%d",beacon.major.intValue,beacon.minor.intValue];
        [UDIDS addObject:BUUID];
        }
    _UDID= [UDIDS componentsJoinedByString:@","];
    NSString *location_beacon =  [[NSUserDefaults standardUserDefaults]objectForKey:@"LOCATION_BEACON"];
    if(location_beacon != nil){
        if(_beacons.count == 1){
            CLBeacon *beacon = (CLBeacon*)[self.beacons objectAtIndex:0];
            NSString *BUUID=[NSString stringWithFormat:@"%d-%d",beacon.major.intValue,beacon.minor.intValue];
            NSLog(@"location beacon %@",location_beacon);
            NSLog(@"beacon %@",BUUID);
            if(location_beacon == BUUID){
                
            }else{
                [self.navigationController popToRootViewControllerAnimated:YES];
                self.tabBarController.selectedIndex=0;
            }
        }else if(_beacons.count > 1){
            for(int i = 0; i<_beacons.count;i++){
                CLBeacon *beacon = (CLBeacon*)[self.beacons objectAtIndex:i];
                NSString *BUUID=[NSString stringWithFormat:@"%d-%d",beacon.major.intValue,beacon.minor.intValue];
                NSLog(@"location beacon %@",location_beacon);
                NSLog(@"beacon %@",BUUID);
                if(location_beacon == BUUID){
                    beaconIsThere = YES;
                }
            }
            if(!beaconIsThere){
                [self.navigationController popToRootViewControllerAnimated:YES];
                self.tabBarController.selectedIndex=0;
            }
        }else{
            [self.navigationController popToRootViewControllerAnimated:YES];
            self.tabBarController.selectedIndex=0;
        }
    }else{
        [self.navigationController popToRootViewControllerAnimated:YES];
        self.tabBarController.selectedIndex=0;
    }
    
}

-(void)InitNavBar {
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor colorWithRed:0.98 green:0.96 blue:0.95 alpha:1.0],NSFontAttributeName:[UIFont fontWithName:FONT_REGULAR size:20]}];
    
    UISearchBar *searchBar = [[UISearchBar alloc] initWithFrame:CGRectZero];
    searchBar.delegate=self;
    searchBar.backgroundColor = [UIColor clearColor];
    searchBar.tintColor=[UIColor colorWithRed:0.00 green:0.62 blue:0.83 alpha:1.0];
    searchBar.backgroundImage = [[UIImage alloc] init];
    searchBar.backgroundColor = [UIColor clearColor];
    [searchBar sizeToFit];
    
    searchField = [searchBar valueForKey:@"searchField"];
    
    // To change background color
    searchField.backgroundColor = [UIColor whiteColor];
    
    // To change text color
    searchField.textColor = [UIColor blackColor];
    searchField.font = [UIFont fontWithName:FONT_REGULAR size:17];
    
    // To change placeholder text color
    searchField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search Items"];
    UILabel *placeholderLabel = [searchField valueForKey:@"placeholderLabel"];
    placeholderLabel.textColor = [UIColor grayColor];
    
    //UISearchDisplayController *searchDisplayController= [[UISearchDisplayController alloc] initWithSearchBar:searchBar
                                                                                      //    contentsController:self];
    //[searchDisplayController.searchBar setBackgroundImage:[self imageFromColor:[UIColor clearColor]]];
   // self.searchDisplayController.searchResultsDelegate = self;
    //self.searchDisplayController.searchResultsDataSource = self;
    //self.searchDisplayController.delegate = self;
    
    UIView* titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds)-150, 44)];
    titleView.backgroundColor = UIColor.clearColor;
    searchBar.frame = titleView.bounds;
    [titleView addSubview:searchBar];
    self.navigationItem.titleView = titleView;
    
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageWithIcon:@"fa-cart-plus" backgroundColor:[UIColor clearColor] iconColor:[UIColor whiteColor] andSize:CGSizeMake(35,35)] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(FilterClicked:)forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 32, 32)];
    
    count_lbl = [[UILabel alloc]initWithFrame:CGRectMake(20, -3, 25, 25)];
    [count_lbl setFont:[UIFont fontWithName:FONT_REGULAR size:13]];
    count_lbl.text=[NSString stringWithFormat:@"%d",total_count];
    count_lbl.textAlignment = NSTextAlignmentCenter;
    [count_lbl setTextColor:[UIColor colorWithRed:0.22 green:0.50 blue:0.24 alpha:1.0]];
    [count_lbl setBackgroundColor:[UIColor yellowColor]];
    count_lbl.layer.cornerRadius=12.5;
    count_lbl.clipsToBounds=YES;
    [button addSubview:count_lbl];
    self.cartArray=[[NSMutableArray alloc]init];
    self.microMarketId=[[NSUserDefaults standardUserDefaults]objectForKey:@"micromarketid"];
    self.cartArray=[objDBHelper getCartItems:self.microMarketId];
    NSString *count=[objDBHelper getTotalItems:self.microMarketId];
    NSLog(@"%@", self.microMarketId);
    if(count!=nil)
        {
        count_lbl.text=count;
        }
    else{
        count_lbl.text=@"0";
    }
    
    UIBarButtonItem *cart = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem=cart;
    
    UIImage *buttonImage = [UIImage imageWithIcon:@"fa-chevron-left" backgroundColor:[UIColor clearColor] iconColor:[UIColor whiteColor] andSize:CGSizeMake(30,30)];
    UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button1 setImage:buttonImage forState:UIControlStateNormal];
    button1.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    [button1 addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button1];
    self.navigationItem.leftBarButtonItem = customBarItem;
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
}

- (UIImage *)imageFromColor:(UIColor *)color {
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldBeRequiredToFailByGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewDidAppear:(BOOL)animated {
    
    isCartClicked = NO;
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(scanBeaconNotification:)
                                                name:BEACON_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(showCategoryNotification:)
                                            name:CATEGORY_NOTIFICATION object:nil];
    self.microMarketId=[[NSUserDefaults standardUserDefaults]objectForKey:@"micromarketid"];
    self.cartArray=[[NSMutableArray alloc]init];
    self.cartArray=[objDBHelper getCartItems:self.microMarketId];
    NSString *count=[objDBHelper getTotalItems:self.microMarketId];
    if(count!=nil)
        {
        count_lbl.text=count;
        }
    else{
        count_lbl.text=@"0";
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:SCAN_NOTIFICATION object:SHOW];
    [super viewDidAppear:YES];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear: animated];
    
    noNetworkDidntLoad = YES;
    self.tabBarController.tabBar.hidden=NO;
    [self setScreenAnalytics];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillTerminate:) name:UIApplicationWillTerminateNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillTerminate:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkChanged:)
                                                 name:kRealReachabilityChangedNotification
                                               object:nil];
}

-(void)viewDidDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:BEACON_NOTIFICATION object:nil];
    if (!isCartClicked) {
        isCartClicked = NO;
        [self.navigationController popViewControllerAnimated:YES ];
    }
  [[NSNotificationCenter defaultCenter] removeObserver:self name:kRealReachabilityChangedNotification object:nil];
    [super viewDidDisappear:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.itemsArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Items_Cell";
    NSDictionary *dictobject;
    dictobject=[self.itemsArray objectAtIndex:indexPath.row];
    
    ItemsCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.cell_title.text=[dictobject valueForKey:@"ITEM_DESC"];
    
    NSString *encoded = [[dictobject valueForKey:@"ITEM_IMAGE"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"https://%@",encoded]];;
    NSLog(@"image %@",url);
    
    // [cell.cell_img sd_setImageWithURL:url
    // placeholderImage:[UIImage imageNamed:@"category_placeholder6.png"]];
    [cell.cell_img sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"cart_placeholder.png"]];
    cell.addToCart_btn.layer.cornerRadius=5;
    cell.addToCart_btn.clipsToBounds=YES;
    cell.addToCart_btn.tag=indexPath.row+1;
    if(![[dictobject valueForKey:@"STOCK"] isEqualToString:@"Y"])
        {
        [cell.addToCart_btn setTitle:@"OutOfStock" forState:UIControlStateNormal];
        [cell.addToCart_btn setEnabled:NO];
        }
    else{
        [cell.addToCart_btn setTitle:@"Add To Cart" forState:UIControlStateNormal];
        [cell.addToCart_btn setEnabled:YES];
        [cell.addToCart_btn addTarget:self action:@selector(onAddCartClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    cell.layer.borderWidth=1.0f;
    cell.layer.borderColor=[UIColor colorWithRed:0.22 green:0.50 blue:0.24 alpha:1.0].CGColor;
    if([[dictobject valueForKey:@"DISCOUNT_APPLICABLE"] isEqualToString:@"Y"])
        {
        cell.discount_lbl.text=[NSString stringWithFormat:@"$%@",[dictobject valueForKey:@"ITEM_PRICE"]];
        cell.discount_lbl.hidden=NO;
        NSString *trimmedText = [[dictobject valueForKey:@"ORIGINAL_PRICE"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"$%@",trimmedText]];
        [attributeString addAttribute:NSStrikethroughStyleAttributeName
                                value:@2
                                range:NSMakeRange(0, [attributeString length])];
        [attributeString addAttribute:NSStrikethroughColorAttributeName
                                    value:[UIColor colorWithRed:0.72 green:0.69 blue:0.69 alpha:1.0]
                                    range:NSMakeRange(0, [attributeString length])];
        [attributeString addAttribute:NSForegroundColorAttributeName
                                    value:[UIColor colorWithRed:0.72 green:0.69 blue:0.69 alpha:1.0]
                                    range:NSMakeRange(0, [attributeString length])];
        NSLog(@"attribute string %@",attributeString);
            cell.cell_price.attributedText= attributeString;
        }
    else
        {
        cell.discount_lbl.hidden=YES;
        NSString *trimmedText = [[dictobject valueForKey:@"ITEM_PRICE"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        cell.cell_price.text=[NSString stringWithFormat:@"$%@",trimmedText];
        }
    cell.info_btn.tag=indexPath.row+1;
    [cell.info_btn addTarget:self action:@selector(infoClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    CALayer *layer = ((ItemsCell *) cell).layer;
    layer.cornerRadius = 10;
    layer.borderColor = [UIColor colorWithRed:0.26 green:0.26 blue:0.26 alpha:0.5].CGColor;
    layer.borderWidth = 1.0;
    layer.masksToBounds = YES;
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGRect screenRect = [self.view bounds];
    CGFloat screenWidth = screenRect.size.width;
        // CGFloat screenHeight = screenRect.size.height;
    float cellWidth  = screenWidth / 2.2;
        //float cellHeight = screenHeight/3;
    CGSize size = CGSizeMake(cellWidth,240);
    return size;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(10,10,10,10);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(tableView==self.items_table_view) {
        return 200;
        }
    return 50;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(tableView==filterTable) {
        return 50;
        }
    
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view;
    if(tableView==filterTable)
        {
        view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, filterTable.frame.size.width, 50)];
        view.backgroundColor =[UIColor darkGrayColor];
        
        UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,10,filterTable.frame.size.width,25)];
        headerLabel.textAlignment = NSTextAlignmentCenter;
        headerLabel.text = @"Filter";
        headerLabel.font = [UIFont fontWithName:FONT_BOLD size:20];
        
        headerLabel.textColor=[UIColor whiteColor];
        
        
        [view addSubview:headerLabel];
        return view;
        }
    else{
        return nil;
    }
    
    return view;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(tableView==filterTable)
        {
        return [self.categoryArray count];
        }
    else{
        return [self.itemsArray count];

    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FilterCell *cell;
    if(!(tableView==filterTable))
        {
        NSDictionary *dictobject;
        dictobject=[self.itemsArray objectAtIndex:indexPath.row];
        static NSString *CellIdentifier = @"ItemsTable_Cell";
        ItemsTableCell *cell;
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.cell_title.text=[dictobject valueForKey:@"ITEM_DESC"];
        cell.cell_price.text = @"";
        if([[dictobject valueForKey:@"DISCOUNT_APPLICABLE"] isEqualToString:@"Y"])
            {
            cell.discount_lbl.text=[NSString stringWithFormat:@"$%@",[dictobject valueForKey:@"ITEM_PRICE"]];
            cell.discount_lbl.hidden=NO;
            NSString *trimmedText = [[dictobject valueForKey:@"ORIGINAL_PRICE"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"$%@",trimmedText]];
            [attributeString addAttribute:NSStrikethroughStyleAttributeName
                                    value:@2
                                    range:NSMakeRange(0, [attributeString length])];
            [attributeString addAttribute:NSStrikethroughColorAttributeName
                                    value:[UIColor colorWithRed:0.72 green:0.69 blue:0.69 alpha:1.0]
                                    range:NSMakeRange(0, [attributeString length])];
            [attributeString addAttribute:NSForegroundColorAttributeName
                                    value:[UIColor colorWithRed:0.72 green:0.69 blue:0.69 alpha:1.0]
                                    range:NSMakeRange(0, [attributeString length])];
            NSLog(@"attribute string %@",attributeString);
            cell.cell_price.attributedText = attributeString;
            
            }
        else
            {
            cell.discount_lbl.hidden=YES;
            NSString *trimmedTexts = [[dictobject valueForKey:@"ITEM_PRICE"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            cell.cell_price.text=[NSString stringWithFormat:@"$%@",trimmedTexts];
            }
        NSString *encoded = [[dictobject valueForKey:@"ITEM_IMAGE"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"https://%@",encoded]];;
        NSLog(@"image %@",url);
        
        // [cell.cell_img sd_setImageWithURL:url
        //  placeholderImage:[UIImage imageNamed:@"category_placeholder6.png"]];
        [cell.cell_img sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"cart_placeholder.png"]];
        cell.info_btn.tag=indexPath.row+1;
        [cell.info_btn addTarget:self action:@selector(infoClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.addToCart_btn.layer.cornerRadius=5;
        cell.addToCart_btn.clipsToBounds=YES;
        cell.addToCart_btn.tag=indexPath.row+1;
        if(![[dictobject valueForKey:@"STOCK"] isEqualToString:@"Y"])
            {
            [cell.addToCart_btn setTitle:@"Out Of Stock" forState:UIControlStateNormal];
            [cell.addToCart_btn setEnabled:NO];
            }
        else{
            [cell.addToCart_btn setTitle:@"Add To Cart" forState:UIControlStateNormal];
            [cell.addToCart_btn setEnabled:YES];
            [cell.addToCart_btn addTarget:self action:@selector(onAddCartClicked:) forControlEvents:UIControlEventTouchUpInside];
        }
        return cell;
        }
    else
        {
        NSDictionary *dictobject;
        dictobject=[self.categoryArray objectAtIndex:indexPath.row];
        static NSString *CellIdentifier = @"Filter_Cell";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell.filter_lbl.text=[dictobject valueForKey:@"CATEGORY_DESC"];
        cell.select_btn.tag=indexPath.row+1;
        [cell.select_btn addTarget:self action:@selector(selectClicked:) forControlEvents:UIControlEventTouchUpInside];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        NSLog(@"filter array %@",[self.filterArray objectAtIndex:indexPath.row]);
        
        if([[self.filterArray objectAtIndex:indexPath.row] isEqualToString:@"n"])
            {
            [cell.select_btn setBackgroundImage:[UIImage imageWithIcon:@"fa-square" backgroundColor:[UIColor clearColor] iconColor:[UIColor colorWithRed:0.00 green:0.62 blue:0.83 alpha:1.0] andSize:CGSizeMake(35,35)] forState:UIControlStateNormal];
            }
        else
            {
                [cell.select_btn setBackgroundImage:[UIImage imageWithIcon:@"fa-check-square" backgroundColor:[UIColor clearColor] iconColor:[UIColor colorWithRed:0.00 green:0.62 blue:0.83 alpha:1.0] andSize:CGSizeMake(35,35)] forState:UIControlStateNormal];
            }
        return cell;
        }
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if(tableView==filterTable)
        {
        return 60.0f;
        }
    return 0.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if(tableView==filterTable)
        {
        UIView *sampleView = [[UIView alloc] init];
        sampleView.frame = CGRectMake(0, 0,250, 60);
        sampleView.backgroundColor = [UIColor whiteColor];
        
        UIButton *done_btn=[[UIButton alloc]initWithFrame:CGRectMake(10,10,250-20,50)];
        done_btn.layer.cornerRadius=5;
        done_btn.clipsToBounds=YES;
        [done_btn setBackgroundColor:[UIColor colorWithRed:0.00 green:0.64 blue:0.84 alpha:1.0]];
        [done_btn setTitle:@"Apply" forState:UIControlStateNormal];
        [done_btn.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:20]];
        [done_btn addTarget:self action:@selector(filterDoneClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        [sampleView addSubview:done_btn];
        return sampleView;
        }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView==filterTable)
        {
        FilterCell *cell;
        NSDictionary *dictobject;
        
        if (self.categoryArray != nil)
            {
            dictobject=[self.categoryArray objectAtIndex:indexPath.row];
            cell = [filterTable cellForRowAtIndexPath:indexPath];
            
            if([[self.filterArray objectAtIndex:indexPath.row] isEqualToString:@"n"])
                {
                [cell.select_btn setBackgroundImage:[UIImage imageWithIcon:@"fa-check-square" backgroundColor:[UIColor clearColor] iconColor:[UIColor colorWithRed:0.00 green:0.62 blue:0.83 alpha:1.0] andSize:CGSizeMake(35,35)] forState:UIControlStateNormal];
                [self.filterArray replaceObjectAtIndex:indexPath.row withObject:@"y"];
                }
            else
                {
                [cell.select_btn setBackgroundImage:[UIImage imageWithIcon:@"fa-square" backgroundColor:[UIColor clearColor] iconColor:[UIColor colorWithRed:0.00 green:0.62 blue:0.83 alpha:1.0] andSize:CGSizeMake(35,35)] forState:UIControlStateNormal];
                [self.filterArray replaceObjectAtIndex:indexPath.row withObject:@"n"];
                }
            }
        }
}

-(IBAction)onAddCartClicked:(id)sender
{
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    self.cartArray=[[NSMutableArray alloc]init];
    NSLog(@"%ld",(long)[sender tag]);
    NSDictionary *dictobject;
    dictobject=[self.itemsArray objectAtIndex:[sender tag]-1];
    NSLog(@"itemsArray:%@",self.itemsArray);
    
    NSString *crvtax;
    NSString *salestax;
    NSString *discountPrice;
    NSString * SessionGUID = [[NSUserDefaults standardUserDefaults] objectForKey:@"SessionGUID"];
    NSString * micromarketid = [[NSUserDefaults standardUserDefaults] valueForKey:@"micromarketid"];
    NSString * micromarketname = [[NSUserDefaults standardUserDefaults] valueForKey:@"micromarketname"];
    NSString * userID = [[NSUserDefaults standardUserDefaults] objectForKey:@"username"];
    float price=[[dictobject valueForKey:@"ITEM_PRICE"]floatValue];
    NSString * categoryID = [dictobject valueForKey:@"CATEGORY_ID"];
    NSString * productID = [dictobject valueForKey:@"ITEM_NO"];
    NSString * productName = [dictobject valueForKey:@"ITEM_DESC"];
    
    NSString * analyticsCount = [objDBHelper getProductAnalyticsBySessionID:SessionGUID andProductID:productID andType:@"Product_Added"];
    int productCount = [analyticsCount intValue] +1;
    
    if(productCount == 1) {
        [objDBHelper insertProductAnalytics:userID andsessionGUID:SessionGUID andMircoMarketID:[micromarketid intValue] andMicroMarketName:micromarketname andProductID:productID andProductName:productName andCategoryID:categoryID andUPCCode:@"" andItemQty:1 andItemPrice:price andType:@"Product_Added" andSyncFlag:@"Y"];
    }
    
    if(![objDBHelper CheckItem:[dictobject valueForKey:@"ITEM_NO"] andMicroMarketID:self.microMarketId])
    {
        
        //SALES TAX CALCULATION
        if([[dictobject valueForKey:@"TAXABLE"] isEqualToString:@"Y"])
        {
            NSNumberFormatter *fmt = [[NSNumberFormatter alloc] init];
            [fmt setPositiveFormat:@"0.##"];
            [fmt setMinimumFractionDigits:2];
            float price=[[dictobject valueForKey:@"ITEM_PRICE"]floatValue];
            float tax = [[dictobject valueForKey:@"SALESTAX"] floatValue];
            double total_taxprice=(price/100)*tax;
            salestax=[NSString stringWithFormat:@"%f",total_taxprice];
            NSLog(@"salestax %@",salestax);
        }
        else{
            salestax=@"0.00";
        }
        
        //CRV TAX CALCULATION
        if([[dictobject valueForKey:@"CRV_ENABLE"] isEqualToString:@"Y"])
        {
            float tax = [[dictobject valueForKey:@"CRVTAX"] floatValue];
            crvtax=[NSString stringWithFormat:@"%f",tax];
            NSLog(@"crvtax %@",crvtax);
            
        }
        else{
            crvtax=@"0.00";
        }
        
        //DISCOUNT PRICE CALCULATION
        if([[dictobject valueForKey:@"DISCOUNT_APPLICABLE"] isEqualToString:@"Y"]){
            float originalPrice = [[dictobject valueForKey:@"ORIGINAL_PRICE"] floatValue];
            float itemPrice = [[dictobject valueForKey:@"ITEM_PRICE"] floatValue];
            float discount;
            if(originalPrice > itemPrice){
                discount = originalPrice - itemPrice;
            }else{
                discount = itemPrice - originalPrice;
            }
            discountPrice=[NSString stringWithFormat:@"%f",discount];
        }else{
            discountPrice = @"0.00";
        }
        
        [objDBHelper insertCartItems:self.microMarketId andCategoryID:[dictobject valueForKey:@"CATEGORY_ID"] andDescription:[dictobject valueForKey:@"ITEM_DESC_LONG"] andItemNo:[dictobject valueForKey:@"ITEM_NO"] andcrvtax:crvtax andSalesTax:salestax andDiscountPrice:[dictobject valueForKey:@"DISCOUNT_PRICE"] andItemName:[dictobject valueForKey:@"ITEM_DESC"] andItemImage:[dictobject valueForKey:@"ITEM_IMAGE"] andItemPrice:[dictobject valueForKey:@"ITEM_PRICE"] andStock:[dictobject valueForKey:@"STOCK"] andQuantity:@"1" andOriginalPrice:[dictobject valueForKey:@"ORIGINAL_PRICE"] andCRVPercent:[dictobject valueForKey:@"CRVTAX"] andSalesPercent:[dictobject valueForKey:@"SALESTAX"] andCRVTAXEnable:[dictobject valueForKey:@"CRV_ENABLE"] andSalesTaxEnable:[dictobject valueForKey:@"TAXABLE"] andDiscountPriceDiff:discountPrice andDiscountPriceEnable:[dictobject valueForKey:@"DISCOUNT_APPLICABLE"] andBarcode:[dictobject valueForKey:@"BAR_CODE"]];
        
    }
    else{
        
        //SALES TAX CALCULATION
        if([[dictobject valueForKey:@"TAXABLE"] isEqualToString:@"Y"])
        {
            float price=[[dictobject valueForKey:@"ITEM_PRICE"] floatValue];
            float tax = [[dictobject valueForKey:@"SALESTAX"] floatValue];
            double total_taxprice=(price/100)*tax;
            NSString *salestaxfromdb=[objDBHelper getSalesTax:[dictobject valueForKey:@"ITEM_NO"]];
            double taxtotal=[salestaxfromdb floatValue]+total_taxprice;
            salestax=[NSString stringWithFormat:@"%f",taxtotal];
            NSLog(@"salestax %@",salestax);
        }
        else
        {
            salestax=@"0.00";
        }
        //CRV TAX CALCULATION
        if([[dictobject valueForKey:@"CRV_ENABLE"] isEqualToString:@"Y"])
        {
            float tax = [[dictobject valueForKey:@"CRVTAX"] floatValue];
            NSString *crvtaxfromdb=[objDBHelper getCRVTax:[dictobject valueForKey:@"ITEM_NO"]];
            float taxtotal=[crvtaxfromdb floatValue]+tax;
            crvtax=[NSString stringWithFormat:@"%f",taxtotal];
            NSLog(@"crvtax %@",crvtax);
            
        }
        else{
            crvtax=@"0.00";
        }
        //DISCOUNT PRICE CALCULATION
        if([[dictobject valueForKey:@"DISCOUNT_APPLICABLE"] isEqualToString:@"Y"]){
            float originalPrice = [[dictobject valueForKey:@"ORIGINAL_PRICE"] floatValue];
            float itemPrice = [[dictobject valueForKey:@"ITEM_PRICE"] floatValue];
            float discount;
            if(originalPrice > itemPrice){
                discount = originalPrice - itemPrice;
            }else{
                discount = itemPrice - originalPrice;
            }
            discountPrice=[NSString stringWithFormat:@"%f",discount];
        }else{
            discountPrice = @"0.00";
        }
        [objDBHelper UpdateQuantity:[dictobject valueForKey:@"ITEM_NO"] andQuantity:@"3" andMicroMarketID:self.microMarketId];
        [objDBHelper UpdatePrice:[dictobject valueForKey:@"ITEM_NO"] andPrice:[dictobject valueForKey:@"ITEM_PRICE"] andMicroMarketID:self.microMarketId];
        NSString *discountPriceDiff = [objDBHelper getDiscountPriceDiff:[dictobject valueForKey:@"ITEM_NO"]];
        [objDBHelper UpdateDiscountPriceDiff:[dictobject valueForKey:@"ITEM_NO"] andPrice:discountPrice andMicroMarketID:self.microMarketId andDiscountPriceDiff:discountPriceDiff];
        [objDBHelper UpdateSalesTax:salestax andItemNo:[dictobject valueForKey:@"ITEM_NO"]];
        [objDBHelper UpdateCRVTax:crvtax andItemNo:[dictobject valueForKey:@"ITEM_NO"]];
    }
    self.cartArray=[objDBHelper getCartItems:self.microMarketId];
    count_lbl.text=[objDBHelper getTotalItems:self.microMarketId];
    [AlertVC showAlertWithTitleForView:APP_NAME message:@"Added to cart" controller:self];
    
}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self.popTip hide];
}

-(IBAction)infoClicked:(UIButton *)sender
{
    [self.popTip hide];
    CGRect buttonRect = [sender.superview convertRect:sender.frame toView:self.view];
    NSDictionary *dictobject;
    dictobject=[self.itemsArray objectAtIndex:sender.tag-1];
    AMPopTip *appearance = [AMPopTip appearance];
    appearance.textAlignment = NSTextAlignmentLeft;
    appearance.popoverColor = [UIColor colorWithRed:0.22 green:0.50 blue:0.24 alpha:1.0];
    self.popTip = [AMPopTip popTip];
    [self.popTip showText:[dictobject valueForKey:@"ITEM_DESC"] direction:AMPopTipDirectionLeft maxWidth:200 inView:self.view fromFrame:buttonRect];
}

-(IBAction)selectClicked:(id)sender
{
    UIButton *random = (UIButton *)sender;
    NSDictionary *dictobject;
    dictobject=[self.categoryArray objectAtIndex:[sender tag]-1];
    if([[self.filterArray objectAtIndex:[sender tag]-1] isEqualToString:@"n"])
        {
        [self.filterArray replaceObjectAtIndex:[sender tag]-1 withObject:@"y"];
        
        [random setBackgroundImage:[UIImage imageWithIcon:@"fa-check-square" backgroundColor:[UIColor clearColor] iconColor:[UIColor colorWithRed:0.00 green:0.62 blue:0.83 alpha:1.0] andSize:CGSizeMake(35,35)] forState:UIControlStateNormal];
        }else
            {
            [self.filterArray replaceObjectAtIndex:[sender tag]-1 withObject:@"n"];
            [random setBackgroundImage:[UIImage imageWithIcon:@"fa-square" backgroundColor:[UIColor clearColor] iconColor:[UIColor colorWithRed:0.00 green:0.62 blue:0.83 alpha:1.0] andSize:CGSizeMake(35,35)] forState:UIControlStateNormal];
            }
    
}

-(IBAction)clearButtonClicked:(id)sender
{
    for(int i=0;i<self.filterArray.count;i++)
        {
        [self.filterArray replaceObjectAtIndex:i withObject:@"n"];
        }
    self.categoryId=firstcategoryid;
    [filterTable reloadData];
    [self performSelector: @selector(filterClosed:) withObject:self afterDelay: 0.0];
    if(![firstcategoryid isEqualToString:EMP_STR])
    {
    if([self.categoryId isEqualToString:@"'BESTSELLING'"]){
    [self getTopSelling];
    }else{
    [self getitems];
    }
    }
    [self.categoryLbl setHidden:NO];

}


-(IBAction)ScanClicked:(id)sender
{
    BarCodeController *vc;
    vc = [self.storyboard instantiateViewControllerWithIdentifier: BARCODE_VC];
    vc.micromarketid=self.microMarketId;
    [self.navigationController pushViewController:vc animated:YES];
}

-(IBAction)FilterClicked:(id)sender
{
    if(self.cartArray.count>0)
        {
        isCartClicked = YES;
        ShoppingCartViewController *vc;
        vc=[self.storyboard instantiateViewControllerWithIdentifier:SHOPPING_CART_VC];
        vc.cart_array=self.cartArray;
        vc.micromarketid=self.microMarketId;
        NSLog(@"micromarketid %@",self.microMarketId);
        NSLog(@"cart array %d",self.cartArray.count);
        [self.navigationController pushViewController:vc animated:YES];
        }
    else
        {
        [AlertVC showAlertWithTitleForView:APP_NAME message:THERE_ARE_NO_ITEMS controller:self];
        }
}


-(IBAction)filterClicked:(id)sender
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [filterView.layer addAnimation:transition forKey:nil];
    filterView.frame=CGRectMake(self.view.frame.size.width-250,10, 250, self.view.frame.size.height-80);
}

-(IBAction)typeChooseClicked:(id)sender
{
    if(self.itemsArray.count==0)
        {
        [self.no_lbl setHidden:NO];
        [self.items_view setHidden:YES];
        [self.items_table_view setHidden:YES];
        }else{
            if([typeSelected isEqualToString:@"list"])
                {
                [UIView transitionWithView:self.items_table_view
                                  duration:0.6
                                   options:UIViewAnimationOptionTransitionCrossDissolve
                                animations:^{
                                    
                                    [self.items_table_view setHidden:YES];
                                    [self.items_view setHidden:NO];
                                }
                                completion:NULL];
                
                [self.list_btn setImage:[UIImage imageWithIcon:@"fa-th-large" backgroundColor:[UIColor clearColor] iconColor:[UIColor darkGrayColor] andSize:CGSizeMake(30,30)] forState:UIControlStateNormal];
                typeSelected=@"grid";
                
                }
            else{
                [UIView transitionWithView:self.items_view
                                  duration:0.6
                                   options:UIViewAnimationOptionTransitionCrossDissolve
                                animations:^{
                                    
                                    [self.items_view setHidden:YES];
                                    [self.items_table_view setHidden:NO];
                                }
                                completion:NULL];
                
                [self.list_btn setImage:[UIImage imageWithIcon:@"fa-list-ul" backgroundColor:[UIColor clearColor] iconColor:[UIColor darkGrayColor] andSize:CGSizeMake(30,30)] forState:UIControlStateNormal];
                typeSelected=@"list";
            }
        }
}

-(void)createFilterView
{
    filterView=[[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width, 50, 250, self.view.frame.size.height-80)];
    [filterView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:filterView];
    
    UIButton *close_btn=[[UIButton alloc]initWithFrame:CGRectMake(5, 20, 32, 32)];
    [close_btn setBackgroundColor:[UIColor whiteColor]];
    [close_btn setImage:[UIImage imageWithIcon:@"fa-times" backgroundColor:[UIColor clearColor] iconColor:[UIColor colorWithRed:0.93 green:0.07 blue:0.07 alpha:1.0] andSize:CGSizeMake(30,30)] forState:UIControlStateNormal];
    [close_btn addTarget:self action:@selector(filterClosed:) forControlEvents:UIControlEventTouchUpInside];
    [filterView addSubview:close_btn];
    
    UIButton *clear_btn = [[UIButton alloc]initWithFrame:CGRectMake(250-90, 20, 80, 30)];
    clear_btn.layer.cornerRadius=5;
    clear_btn.clipsToBounds=YES;
    [clear_btn setBackgroundColor:[UIColor colorWithRed:0.45 green:0.73 blue:0.31 alpha:1.0]];
    [clear_btn setTitle:@"Clear" forState:UIControlStateNormal];
    [clear_btn.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:14]];
    [clear_btn addTarget:self action:@selector(clearButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [filterView addSubview:clear_btn];
    
    filterTable=[[UITableView alloc]initWithFrame:CGRectMake(0,70,filterView.frame.size.width, self.view.frame.size.height-190)];
    filterTable.delegate=self;
    filterTable.dataSource=self;
    [filterView addSubview:filterTable];
    [filterTable registerNib:[UINib nibWithNibName:@"FilterCell" bundle:nil] forCellReuseIdentifier:@"Filter_Cell"];
}

-(IBAction)filterDoneClicked:(id)sender
{
    [self.categoryLbl setHidden:YES];
    self.categoryIdArray=[[NSMutableArray alloc]init];
    for(int i=0;i<self.filterArray.count;i++)
        {
        if([[self.filterArray objectAtIndex:i] isEqualToString:@"y"])
            {
            NSDictionary *dictobject;
            dictobject=[self.categoryArray objectAtIndex:i];
            [self.categoryIdArray addObject:[dictobject valueForKey:@"CATEGORY_ID"]];
            }
        }
    if(self.categoryIdArray.count!=0)
        {
        if(self.categoryIdArray.count==1)
            {
                self.categoryId=[NSString stringWithFormat:@"'%@'",[self.categoryIdArray objectAtIndex:0]];
            }
        else{
            for(int i=0;i<self.categoryIdArray.count;i++)
                {
                if(i==0)
                    {
                    self.categoryId=[NSString stringWithFormat:@"'%@'",[self.categoryIdArray objectAtIndex:i]];
                    }
                else{
                    self.categoryId=[NSString stringWithFormat:@"%@,'%@'",self.categoryId,[self.categoryIdArray objectAtIndex:i]];
                }
                }
        }
        [self performSelector: @selector(filterClosed:) withObject:self afterDelay: 0.0];
            if([self.categoryId isEqualToString:@"'BESTSELLING'"]){
                [self getTopSelling];
            }else{
                [self getitems];
            }
        }
    else
        {
        [self performSelector: @selector(filterClosed:) withObject:self afterDelay: 0.0];
        [self.categoryLbl setHidden:NO];
        self.categoryId=EMP_STR;
        }
    
}

-(IBAction)filterClosed:(id)sender
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [filterView.layer addAnimation:transition forKey:nil];
    filterView.frame=CGRectMake(self.view.frame.size.width, 10, 250, self.view.frame.size.height-80);

}

-(void)getitems
{
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    NSString *serviceName;
    serviceName = [[NSUserDefaults standardUserDefaults]objectForKey:@"serviceName"];
    if (serviceName != nil && self.microMarketId != nil)
        {
        [dictParam setObject:self.microMarketId forKey:@"MicroMarketID"];
        [dictParam setObject:serviceName forKey:PARAM_SERVICE_NAME_KEY];
        [dictParam setObject:self.categoryId forKey:@"CategoryID"];
        [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"username"] forKey:PARAM_EMAIL_KEY];
            
        NSString *rootURL =  [[AppDelegate shared] rootUrl];

        AFNHelper *afn=[[AFNHelper alloc]init];
        [afn postDataFromPath:METHOD_ITEMS getUrl:rootURL withParamData:dictParam withBlock:^(id response, NSError *error)
         {
         if(error)
             {
             NSLog(@"ERROR DESC:%@",error.localizedDescription);
             }
         else
             {
             NSLog(@"response %@",response);
             NSDictionary *dictionary;
             dictionary=response;
             NSString *status=[dictionary valueForKey:STATUS];
             self.itemsArray=[[NSMutableArray alloc]init];
             self.itemsArray=[dictionary valueForKey:@"Items"];
             if([status isEqualToString:SUCCESS])
            {
                [self.no_lbl setHidden:YES];
                [self.items_view reloadData];
                [self.items_table_view reloadData];
                if([typeSelected isEqualToString:@"list"])
                {
                    [self.items_view setHidden:YES];
                    [self.items_table_view setHidden:NO];
                }else{
                    [self.items_view setHidden:NO];
                    [self.items_table_view setHidden:YES];
                }
            }
             if(self.itemsArray.count==0)
                 {
                 [self.no_lbl setHidden:NO];
                 [self.items_view setHidden:YES];
                 [self.items_table_view setHidden:YES];
                 }
             }
        // [self getCategories];
         }];
       
        }
    else {
            // ...
    }
    
}

-(void)getTopSelling{
    
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    NSString *serviceName;
    serviceName = [[NSUserDefaults standardUserDefaults]objectForKey:@"serviceName"];
    if (serviceName != nil && self.microMarketId != nil)
    {
        [dictParam setObject:self.microMarketId forKey:@"MicroMarketID"];
        [dictParam setObject:serviceName forKey:PARAM_SERVICE_NAME_KEY];
        [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"username"] forKey:PARAM_EMAIL_KEY];
         NSLog(@"response %@",dictParam);
        NSString *rootURL =  [[AppDelegate shared] rootUrl];

        AFNHelper *afn=[[AFNHelper alloc]init];
        [afn postDataFromPath:METHOD_BESTSELLING getUrl:rootURL withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             if(error)
             {
                 NSLog(@"ERROR DESC:%@",error.localizedDescription);
             }
             else
             {
                 NSLog(@"response %@",response);
                 NSDictionary *dictionary;
                 dictionary=response;
                 NSString *status=[dictionary valueForKey:STATUS];
                 self.itemsArray=[[NSMutableArray alloc]init];
                 self.itemsArray=[dictionary valueForKey:@"Items"];
                 if([status isEqualToString:SUCCESS])
                 {
                     [self.no_lbl setHidden:YES];
                     [self.items_view reloadData];
                     [self.items_table_view reloadData];
                     if([typeSelected isEqualToString:@"list"])
                     {
                         [self.items_view setHidden:YES];
                         [self.items_table_view setHidden:NO];
                     }else{
                         [self.items_view setHidden:NO];
                         [self.items_table_view setHidden:YES];
                     }
                 }
                 if(self.itemsArray.count==0)
                 {
                     [self.no_lbl setHidden:NO];
                     [self.items_view setHidden:YES];
                     [self.items_table_view setHidden:YES];
                 }
             }
             // [self getCategories];
         }];
        
    }
    else {
        // ...
    }
}

-(void)getCategories
{
   // self.microMarketId=[[NSUserDefaults standardUserDefaults]objectForKey:@"micromarketId"];
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    
    NSString *beaconMajor;
    NSString *beaconMinor;
    NSString *serviceName;
    
    beaconMajor = [[NSUserDefaults standardUserDefaults]objectForKey:@"beaconMajor"];
    beaconMinor = [[NSUserDefaults standardUserDefaults]objectForKey:@"beaconMinor"];
    serviceName = [[NSUserDefaults standardUserDefaults]objectForKey:@"serviceName"];
    
    if (serviceName != nil )
        {
        [dictParam setObject:serviceName forKey:@"ServiceName"];
        [dictParam setObject:beaconMajor forKey:@"BeaconMajor"];
        [dictParam setObject:beaconMinor forKey:@"BeaconMinor"];
        [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"username"] forKey:PARAM_EMAIL_KEY];
            
        NSLog(@"items dict %@",dictParam);
        
        NSString *rootURL =  [[AppDelegate shared] rootUrl];

        AFNHelper *afn=[[AFNHelper alloc]init];
        [afn postDataFromPath:METHOD_RECONCILECATEGORYS getUrl:rootURL withParamData:dictParam withBlock:^(id response, NSError *error)
         {
         if(error)
             {
             NSLog(@"ERROR DESC:%@",error.localizedDescription);
             }
         else
             {
             NSDictionary *dictionary;
             NSMutableArray *categoryTempArray;
             categoryTempArray = [[NSMutableArray alloc]init];
             dictionary=response;
             NSLog(@"category response %@",response);
             NSString *status=[dictionary valueForKey:STATUS];
             self.categoryArray=[[NSMutableArray alloc]init];
             categoryTempArray = [dictionary valueForKey:@"Categories"];
            for(int i=0;i<categoryTempArray.count;i++){
                NSDictionary *categoryDict;
                categoryDict = [categoryTempArray objectAtIndex:i];
                if(![[categoryDict valueForKey:@"CATEGORY_ID"]  isEqual: @"BESTSELLING"]){
                    [self.categoryArray addObject:categoryDict];
                }
            }
             if([status isEqualToString:SUCCESS])
                 {
                 for(int i=0;i<self.categoryArray.count;i++)
                     {
                         [self.filterArray addObject:@"n"];
                     }
                        [self createFilterView];
                 }
             else
                 {
                 NSString *message=[dictionary valueForKey:MESSAGE];
                 [AlertVC showAlertWithTitleForView:APP_NAME message:message controller:self];
                 }
             }
         
         }];
        
        }
    
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    search_txt = searchBar.text;
    [self searchItems];
    for(int i=0;i<self.filterArray.count;i++)
        {
        [self.filterArray replaceObjectAtIndex:i withObject:@"n"];
        }
    [filterTable reloadData];
}


-(void) searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    
    [searchBar setShowsCancelButton:YES animated:YES];
    
    UIToolbar *Toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    [Toolbar sizeToFit];
    
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [barItems addObject:flexSpace];
    
    UIBarButtonItem *btnCancel = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked)];
    [barItems addObject:btnCancel];
    
    [Toolbar setItems:barItems animated:YES];
    searchField.inputAccessoryView = Toolbar;
    
}

-(void)doneClicked
{
    [searchField resignFirstResponder];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}


- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    [searchBar setShowsCancelButton:NO animated:YES];
}


-(void)searchItems
{
    [[ActivityIndicator sharedInstance] showLoader];
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    
    if (serviceName != nil && self.microMarketId != nil )
        {
        [dictParam setObject:self.microMarketId  forKey:@"MicroMarketID"];
        [dictParam setObject:serviceName         forKey:PARAM_SERVICE_NAME_KEY];
        [dictParam setObject:search_txt          forKey:@"ItemDesc"];
        [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"username"] forKey:PARAM_EMAIL_KEY];
        
        NSString *rootURL =  [[AppDelegate shared] rootUrl];

        AFNHelper *afn=[[AFNHelper alloc]init];
        [afn postDataFromPath:METHOD_SEARCHITEMS getUrl:rootURL withParamData:dictParam withBlock:^(id response, NSError *error)
         {

         if(error)
             {
             [[ActivityIndicator sharedInstance] hideLoader];

             NSLog(@"ERROR DESC:%@",error.localizedDescription);
             }
         else
             {
             NSDictionary *dictionary;
             dictionary=response;
             NSString *status=[dictionary valueForKey:STATUS];
             
             if([status isEqualToString:SUCCESS])
                 {
                 [[ActivityIndicator sharedInstance] hideLoader];
                 self.itemsArray=[[NSMutableArray alloc]init];
                 self.itemsArray=[dictionary valueForKey:@"Items"];
                 [self.items_table_view reloadData];
                 [self.items_view reloadData];
                 [self.no_lbl setHidden:YES];
                 [self.items_table_view setHidden:NO];
                 [self.items_view setHidden:YES];
                 }
             else
                 {
                 [[ActivityIndicator sharedInstance] hideLoader];
                 NSString *message=[dictionary valueForKey:MESSAGE];
                 [AlertVC showAlertWithTitleForView:APP_NAME message:message controller:self];
                 }
             if(self.itemsArray.count==0)
                 {
                 [[ActivityIndicator sharedInstance] showLoader];

                 [self.no_lbl setHidden:NO];
                 [self.items_view setHidden:YES];
                 [self.items_table_view setHidden:YES];
                 }
             }
                [[ActivityIndicator sharedInstance] hideLoader];
         }];
        
        }
    else {
        
            ///....
    }
    
}

-(void)setScreenAnalytics {
    NSString * SessionGUID = [[NSUserDefaults standardUserDefaults] objectForKey:@"SessionGUID"];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString * checkInTime=[dateFormatter stringFromDate:[NSDate date]];
    NSString * micromarketid = [[NSUserDefaults standardUserDefaults] valueForKey:@"micromarketid"];
    NSString * micromarketname = [[NSUserDefaults standardUserDefaults] valueForKey:@"micromarketname"];
    NSString * userID = [[NSUserDefaults standardUserDefaults] objectForKey:@"username"];

    NSString * analyticsCount = [objDBHelper getScreenAnalyticsBySessionID:SessionGUID andScreenName:@"ProductList"];
    int visitCount = [analyticsCount intValue] +1;

    if(visitCount == 1) {
        [objDBHelper insertScreenAnalytics:userID andsessionGUID:SessionGUID andMircoMarketID:[micromarketid intValue] andMicroMarketName:micromarketname andScreenName:@"ProductList" andscreenVisits:visitCount andCheckInTime:checkInTime andCheckOutTime:@"" andSyncFlag:@"Y"];
    } else {
        [objDBHelper UpdateScreenAnalytics:SessionGUID andVisitCount:visitCount andScreenName:@"ProductList"];
    }

}


-(void)updateScreenAnalyticsCheckOutTime {
    NSString * SessionGUID = [[NSUserDefaults standardUserDefaults] objectForKey:@"SessionGUID"];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString * checkOutTime=[dateFormatter stringFromDate:[NSDate date]];
    [objDBHelper UpdateScreenAnalyticsCheckOutTime:SessionGUID andCheckOutTime:checkOutTime andScreenName:@"ProductList"];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillTerminateNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
}

-(void)appWillTerminate:(NSNotification*)note
{
    [self updateScreenAnalyticsCheckOutTime];
}

-(void)viewWillDisappear:(BOOL)animated {
    [self updateScreenAnalyticsCheckOutTime];
    [super viewWillDisappear:true];
}

- (BOOL)connected
{
    ReachabilityStatus status = [GLobalRealReachability currentReachabilityStatus];
    NSLog(@"Initial reachability status:%@",@(status));
    
    return status == 1 || status == 2;
}

- (void)networkChanged:(NSNotification *)notification
{
    RealReachability *reachability = (RealReachability *)notification.object;
    ReachabilityStatus status = [reachability currentReachabilityStatus];
    if (status == 1 || status == 2){
        if(noNetworkDidntLoad){
            if(self.itemsArray==nil)
            {
                if([self.categoryId isEqualToString:@"'BESTSELLING'"]){
                    [self getTopSelling];
                }else{
                    [self getitems];
                }
            }
            self.categoryLbl.text = self.categoryName;
            
            [self getCategories];
        }
    }
    NSLog(@"currentStatus:%@",@(status));
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
