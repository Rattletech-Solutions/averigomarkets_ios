//
//  ItemsCell.h
//  Averigo
//
//  Created by BTS on 09/11/16.
//  Copyright © 2016 BTS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemsCell : UICollectionViewCell

@property(strong,nonatomic) IBOutlet UIImageView *cell_img;
@property(strong,nonatomic) IBOutlet UILabel     *cell_title;
@property(strong,nonatomic) IBOutlet UILabel     *cell_desc;
@property(strong,nonatomic) IBOutlet UILabel     *cell_price;
@property(strong,nonatomic) IBOutlet UIButton *addToCart_btn;
@property(strong,nonatomic) IBOutlet UIButton *info_btn;
@property(strong,nonatomic) IBOutlet UILabel *discount_lbl;

@end
