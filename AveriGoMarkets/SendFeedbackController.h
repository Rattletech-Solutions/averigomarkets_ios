//
//  SendFeedbackController.h
//  AveriGo Markets
//
//  Created by LeniYadav on 30/10/18.
//  Copyright © 2018 BTS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AllConstants.h"
#import "KGModal.h"
#import "AlertVC.h"
#import "ActivityIndicator.h"
#import "DataBaseHandler.h"

@interface SendFeedbackController : UIViewController
{
    DataBaseHandler *objDBHelper;
}
@property (strong,nonatomic) IBOutlet UITextView *textView;
@property (strong,nonatomic) IBOutlet UIButton *submitBtn;
@property (strong,nonatomic) IBOutlet UIButton *attachImageBtn;
@property (strong,nonatomic) IBOutlet UILabel *attachmentLbl;

@end
