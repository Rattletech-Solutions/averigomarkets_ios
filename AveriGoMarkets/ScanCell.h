//
//  ScanCell.h
//  Averigo
//
//  Created by BTS on 14/12/16.
//  Copyright © 2016 BTS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScanCell : UITableViewCell

@property(strong,nonatomic) IBOutlet UILabel *title_lbl;
@property(strong,nonatomic) IBOutlet UILabel *price_lbl;
@property(strong,nonatomic) IBOutlet UILabel *quantity_lbl;
@property(strong,nonatomic) IBOutlet UIButton *delete_btn;
@property(strong,nonatomic) IBOutlet UIButton *add_btn;
@property(strong,nonatomic) IBOutlet UIButton *minus_btn;

@end
