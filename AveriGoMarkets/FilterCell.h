//
//  FilterCell.h
//  Averigo
//
//  Created by BTS on 24/11/16.
//  Copyright © 2016 BTS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterCell : UITableViewCell

@property(strong,nonatomic) IBOutlet UIButton *select_btn;
@property(strong,nonatomic) IBOutlet UILabel *filter_lbl;

@end
