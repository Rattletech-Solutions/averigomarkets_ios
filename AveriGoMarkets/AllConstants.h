//
//  AllConstants.h

#import <UIKit/UIKit.h>


//#define API_URL @"http://services.averiware.com:8080/AveriwareRestWebService/rest/"
#define TWILIO_URL @ "http://pastforward.in/AverigoTest/twilio-php-master/smsvalidation.php"
#define AVERIWARE_TWILIO_URL @ "https://vending.averiware.com/twilio-php-master/smsvalidation.php"
#define ANALYTICS_URL @"https://vending.averiware.com/interface/public/index.php/"
#define ANALYTICS_URL_DEMO @"http://vending.averiware.com:8080/interface/public/index.php/"
#define SECRET_KEY @"97AE5B1E44D5438872AB1AF53A292"
#define STRIPE_TEST_PUBLIC_KEY @"pk_live_1Czt3u1Mej063FWun1rSNJY7"//@"pk_live_TjSgbWeAo3WYVZMtK3BkLdFt"//@"pk_test_iVnmki1iC7IAwIuVvG8xIm8E"




//spectrumqa
#define WIDTH_IPHONE_4 480
#define IS_IPHONE_4 ( [ [ UIScreen mainScreen ] bounds ].size.height == WIDTH_IPHONE_4 )
#define IS_IPHONE5 ([[UIScreen mainScreen] bounds].size.height == 568)
#define WIDTH_IPHONE_6 667
#define IS_IPHONE_6 ( [ [ UIScreen mainScreen ] bounds ].size.height == WIDTH_IPHONE_6 )
#define WIDTH_IPHONE_6_Plus 736
#define IS_IPHONE_6_Plus ( [ [ UIScreen mainScreen ] bounds ].size.height == WIDTH_IPHONE_6_Plus )
#define IPAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

#ifdef DEBUG
#   define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
#   define DLog(...)
#endif




//Api Methods
#define POST_METHOD             @"POST"
#define GET_METHOD              @"GET"
#define PUT_METHOD              @"PUT"

//DateFormat
#define DateFormat              @"yyyy/MM/dd HH:mm:ss"//"2013-09-13 14:02:49"
#define DateFormatCheckout      @"yyyy-MM-dd HH:mm:ss"

//Colors
#define BLUE_BG_COLOR [UIColor colorWithRed:0.059 green:0.349 blue:0.455 alpha:1]
#define SANDAL_COLOR [UIColor colorWithRed:0.98 green:0.50 blue:0.39 alpha:1.0];

    //Fonts Helper
extern NSString *const FONT_REGULAR;
extern NSString *const FONT_BOLD;
extern NSString *const FONT_MEDIUM;

    // App Constants
extern NSString *const EMP_STR;
extern NSString *const ZERO_POINT_ZERO;
extern NSString *const SUCCESS;
extern NSString *const STATUS;
extern NSString *const DATEFORMAT_TYPE;
extern NSString *const DATEFORMAT_TYPE_TWO;
extern NSString *const CONTENT_TYPE;
extern NSString *const APP_TYPE;
extern NSString *const DONE;
extern NSString *const UDID;
extern NSString *const PASSWORD_DONT_MATCH;
extern NSString *const PASSWORD_CHANGED_SUCCESSFULLY;
extern NSString *const SERVER_BUSY;
extern NSString *const SUPPORT_EMAL;
extern NSString *const SCAN_NOTIFICATION;
extern NSString *const BEACON_NOTIFICATION;
extern NSString *const BARCODE_NOTIFICATION;
extern NSString *const CATEGORY_NOTIFICATION;
extern NSString *const LEAVE_FOR_VIEW;
extern NSString *const WEB_VIEW_DID_FINISH;
extern NSString *const DID_FAIL_WITH_ERROR;
extern NSString *const JSINTERFACE;
extern NSString *const CARD_DELETED;
extern NSString *const TOKEN_EXPIRED;
extern NSString *const SHOW_POPUP;
extern NSString *const SHOW;
extern NSString *const HIDE;
extern NSString *const TITLE_FEATURED;
extern NSString *const SHOW_LOADER;
extern NSString *const DISMISS_LOADER;
extern NSString *const CONFIRMATION_POPUP;
extern NSString *const PAYROLL_POPUP;
extern NSString *const PAYMENT_SUCCESS;



    // Cell Identifier's
extern NSString *const HOME_CELL;
extern NSString *const HOME_CELL_ID;
extern NSString *const SCAN_CELL;
extern NSString *const PURCHASE_CELL;
extern NSString *const ITEMS_CELL;
extern NSString *const ORDER_HISTORY_CELL;

    // Navigation Title's
extern NSString *const PURCHASE_HISTORY;
extern NSString *const SHOPPING_CART;
extern NSString *const CHECK_OUT;
extern NSString *const WALLET;
extern NSString *const SIGN_UP;
extern NSString *const SCAN_BARCODE;
extern NSString *const SEARCH_TITLE;
extern NSString *const HELP;
extern NSString *const FEEDBACK;


    // App-Related "Alert" Constants
extern NSString *const APP_NAME;
extern NSString *const FEEDBACK_THANK_YOU;
extern NSString *const PLEASE_ENTER_FEEDBACK;
extern NSString *const PLEASE_CHECK_YOUR_INTERNET_CONNECTION;
extern NSString *const NO_INTERNET_CONNECTION;
extern NSString *const INVALID_EMAIL_ADDRESS;
extern NSString *const REQUEST_TIMED_OUT;
extern NSString *const ONE_TIME_PASSWORD_SENT;
extern NSString *const PLEASE_ENTER_VALID_EMAIL;
extern NSString *const PLEASE_ENTER_VALID_MOBILE;
extern NSString *const PLEASE_FILL_EMAIL;
extern NSString *const EMAIL_REG_EX;
extern NSString *const PLEASE_FILL_REQUIRED_FIELDS;
extern NSString *const MESSAGE;
extern NSString *const THERE_ARE_NO_ITEMS;
extern NSString *const ADD_TO_CART;
extern NSString *const ALERT_PAYROLL_DEDUCTION;
extern NSString *const ERROR_TIMED_OUT;

    // ViewControllers
extern NSString *const OPEN_ORDER_VC;
extern NSString *const MANAGE_INVENTORY_VC;
extern NSString *const CREATE_INVENTORY_VC;
extern NSString *const RECONCILE_VC;
extern NSString *const RECONCILE_CATEGORY_VC;
extern NSString *const WALLET_WEB_VC;
extern NSString *const CHECKOUT_WEB_VC;
extern NSString *const HELP_WEB_VC;
extern NSString *const INTRO_VC;
extern NSString *const FORGOT_PASSWORD_VC;
extern NSString *const INITIAL_VC;
extern NSString *const HOME_VC;
extern NSString *const TABBAR_VC;
extern NSString *const PURCHASE_HISTORY_VC;
extern NSString *const PROFILE_VC;
extern NSString *const SHOPPING_CART_VC;
extern NSString *const BARCODE_VC;
extern NSString *const ITEMS_VC;
extern NSString *const CATEGORY_VC;
extern NSString *const REGISTER_VC;
extern NSString *const LOGIN_VC;

    //Methods or Paths for Webservice
extern NSString *const MICROMARKET;
extern NSString *const METHOD_SIGNUP;
extern NSString *const METHOD_SIGNIN;
extern NSString *const METHOD_CATEGORY;
extern NSString *const METHOD_ITEMS;
extern NSString *const METHOD_FORGOT;
extern NSString *const METHOD_BARCODE;
extern NSString *const METHOD_COUNTRY;
extern NSString *const METHOD_STATE;
extern NSString *const METHOD_FEEDBACK;
extern NSString *const METHOD_PURCHASE;
extern NSString *const METHOD_CHECKUSER;
extern NSString *const METHOD_MICROMARKET;
extern NSString *const METHOD_SEARCHITEMS;
extern NSString *const METHOD_COMPANYDETAILS;
extern NSString *const METHOD_CHECKOUT;
extern NSString *const METHOD_SAVECARDS;
extern NSString *const METHOD_GETCARDS;
extern NSString *const METHOD_QRCODE;
extern NSString *const METHOD_CHANGEPASSWORD;
extern NSString *const METHOD_WAREHOUSE;
extern NSString *const METHOD_USERAVAILABILITY;
extern NSString *const METHOD_MAILSERVER;
extern NSString *const METHOD_MMITEMS;
extern NSString *const METHOD_RC;
extern NSString *const METHOD_RI;
extern NSString *const METHOD_CI;
extern NSString *const METHOD_RECONCILECATEGORYS;
extern NSString *const METHOD_RECEIVEINVENTORYS;
extern NSString *const METHOD_RECONCILEINVENTORYS;
extern NSString *const METHOD_PURCHASE_HISTORY;
extern NSString *const METHOD_BARCODESCAN;
extern NSString *const METHOD_FORGOT_MC;
extern NSString *const METHOD_VALIDATE_CART;
extern NSString *const METHOD_BESTSELLING;
extern NSString *const METHOD_ANALYTICS;
extern NSString *const METHOD_PURCHASE_RECENT;



    //Prameters
extern NSString *const PARAM_USERNAME;
extern NSString *const PARAM_USERPASSWORD;
extern NSString *const PARAM_COMPANYACRO;
extern NSString *const PARAM_SERVICE_NAME;
extern NSString *const PARAM_DEVICE_UDID;
extern NSString *const PARAM_DEVICE_DATETIME;
extern NSString *const PARAM_FIRSTNAME;
extern NSString *const PARAM_LASTNAME;
extern NSString *const PARAM_COMPANY_NAME;
extern NSString *const PARAM_NICKNAME;
extern NSString *const PARAM_PROFILE_IMAGE;
extern NSString *const PARAM_EMAIL_ID;
extern NSString *const PARAM_PASSWORD;
extern NSString *const PARAM_PHONENO;
extern NSString *const PARAM_ADDRESS;
extern NSString *const PARAM_CITY;
extern NSString *const PARAM_ZIP;
extern NSString *const PARAM_STATE;
extern NSString *const PARAM_COUNTRY;
extern NSString *const PARAM_ENABEL_NEWSLETTER;
extern NSString *const PARAM_ENABLE_SMS;
extern NSString *const PARAM_PROCESS_STATUS;
extern NSString *const PARAM_OTP;
extern NSString *const PARAM_DEVICE_DATE_TIME_KEY;
extern NSString *const PARAM_EMAIL_ID_KEY;
extern NSString *const PARAM_SERVICE_NAME_KEY;
extern NSString *const PARAM_DEVICE_UDID_KEY;
extern NSString *const PARAM_PASSWORD_KEY;
extern NSString *const PARAM_PHONENO_KEY;
extern NSString *const PARAM_USERNAME_KEY;
extern NSString *const PARAM_MICROMARKET_ID;
extern NSString *const PARAM_MICROMARKET_NAME;
extern NSString *const PARAM_BARCODE_ID;
extern NSString *const PARAM_EMAIL_KEY;
extern NSString *const PARAM_BEACON_MAJOR;
extern NSString *const PARAM_BEACON_MINOR;
extern NSString *const PARAM_PAGE_NO;
extern NSString *const GOBACK_POPUP;
extern NSString *const PARAM_MODE_UPDATE;
extern NSString *const PARAM_MODE_INSERT;
extern NSString *const PARAM_MODE_DELETE;



