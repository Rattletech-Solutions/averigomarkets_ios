//
//  VerifyEmailController.m
//  AveriGo Markets
//
//  Created by Macmini on 11/12/18.
//  Copyright © 2018 BTS. All rights reserved.
//

#import "VerifyEmailController.h"
#import <STPopup.h>
#import "IQKeyBoardManager/IQKeyboardReturnKeyHandler.h"
#import "AFNHelper.h"
#import "AFHTTPSessionManager.h"
#import "MBProgressHUD.h"
#import "Reachability.h"
#import "ActivityIndicator.h"
#import "SendGrid.h"
#import "countryCode.h"
#import "SignUpUtiltiy.h"
#import "RealReachability.h"
#import "AppDelegate.h"

@interface VerifyEmailController ()

@end

@implementation VerifyEmailController

{
     IQKeyboardReturnKeyHandler *returnKeyHandler;
     NSString *imagePath;
     NSString *txtPhoneToPass;
     NSString *txtEmail;
     int  randomNo;
     NSString *concatPhoneNum;
     SignUpUtiltiy *signUtils;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self){
        [self customInit];
    }
    return self;
}

- (instancetype)init
{
    if (self = [super init]) {
        self.contentSizeInPopup = CGSizeMake(375, 250);
    }
    return self;
}

- (void)customInit {
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    self.contentSizeInPopup = CGSizeMake(screenWidth - 10, 250);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *titleStr = @"Verify Email & Mobile number";
    CGSize size = [titleStr sizeWithAttributes:
                   @{NSFontAttributeName: [UIFont systemFontOfSize:13.0f]}];
    //UIFont *font =
    [self.title sizeWithAttributes: @{NSFontAttributeName: [UIFont systemFontOfSize:10.0f]}];
    self.title = @"Verify Email & Mobile number";

    
    imagePath = [NSString stringWithFormat:@"EMCCountryPickerController.bundle/%@", @"US" ];
    self.countryImg.image = [UIImage imageNamed:imagePath];
    [self setFieldBorder:self.emailTxt];
    [self setFieldBorder:self.mobileTxt];
    
    UITapGestureRecognizer *singleTapForCountryPicker = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(countryImgClicked)];
    singleTapForCountryPicker.numberOfTapsRequired = 1;
    [self.countryImg setUserInteractionEnabled:YES];
    [self.countryImg addGestureRecognizer:singleTapForCountryPicker];
   // returnKeyHandler = [[IQKeyboardReturnKeyHandler alloc] initWithViewController:self];
   // [returnKeyHandler setLastTextFieldReturnKeyType:UIReturnKeyDone];
    
    self.sendCodeBtn.layer.cornerRadius = 5;
    self.sendCodeBtn.clipsToBounds = YES;
    
}

- (void)viewWillAppear:(BOOL)animated {
   // [[NSNotificationCenter defaultCenter] addObserver:self
                                            // selector:@selector(closeController:)
                                             //    name:@"closecontroller" object:nil];
}

//- (void) closeController:(NSNotification *) notification {
//     [self dismissViewControllerAnimated:YES completion:nil];
//}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear: animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setFieldBorder:(JVFloatLabeledTextField *)textField {
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1;
    NSLog(@"%f",textField.frame.size.height - borderWidth);
    NSLog(@"%f",textField.frame.size.width);
    NSLog(@"%f",textField.frame.size.height);
    border.borderColor = [UIColor lightGrayColor].CGColor;
    border.frame = CGRectMake(0, textField.frame.size.height - borderWidth, textField.frame.size.width + 120, textField.frame.size.height);
    border.borderWidth = borderWidth;
    [textField.layer addSublayer:border];
    textField.layer.masksToBounds = YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (textField == self.countryField) {
        [self showCountryPicker];
        return NO;
    } else {
        return YES;
    }
}

- (void)countryImgClicked {
    [self showCountryPicker];
}

- (void)showCountryPicker {
    //[[KGModal sharedInstance]hideAnimated:YES];
    
    NSString * storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    EMCCountryPickerController * countryPicker = [storyboard instantiateViewControllerWithIdentifier:@"EMCCountryPickerController"];
    [self presentViewController:countryPicker animated:YES completion:nil];
    
    // default values
    countryPicker.showFlags = true;
    countryPicker.countryDelegate = self;
    countryPicker.drawFlagBorder = true;
    countryPicker.flagBorderColor = [UIColor grayColor];
    countryPicker.flagBorderWidth = 0.5f;
}

- (void)countryController:(id)sender didSelectCountry:(EMCCountry *)chosenCity {
    [self dismissViewControllerAnimated:YES completion:nil];
    imagePath = [NSString stringWithFormat:@"EMCCountryPickerController.bundle/%@", chosenCity.countryCode];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"countryCode contains[c] %@ ",chosenCity.countryCode];
    countryCode *code = [[countryCode alloc] init];
    NSArray *filteredCountry = [code.getCoutries filteredArrayUsingPredicate:filter];
    NSLog(@"%@",filteredCountry[0][@"phoneNumber"]);
    
    self.countryField.text = filteredCountry[0][@"phoneNumber"];
    self.countryImg.image = [UIImage imageNamed:imagePath];
   // [[KGModal sharedInstance] showWithContentView:popView andAnimated:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.tag  == 30)
    {
        NSString* totalString = [NSString stringWithFormat:@"%@%@",textField.text,string];
        
        if (range.length == 1) {
            // Delete button was hit.. so tell the method to delete the last char.
            textField.text = [self formatPhoneNumber:totalString deleteLastChar:YES];
        } else {
            textField.text = [self formatPhoneNumber:totalString deleteLastChar:NO ];
        }
        return false;
    }
    return YES;
}

- (NSString*) formatPhoneNumber:(NSString*) simpleNumber deleteLastChar:(BOOL)deleteLastChar {
    if(simpleNumber.length==0) return EMP_STR;
    // use regex to remove non-digits(including spaces) so we are left with just the numbers
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[\\s-\\(\\)]" options:NSRegularExpressionCaseInsensitive error:&error];
    simpleNumber = [regex stringByReplacingMatchesInString:simpleNumber options:0 range:NSMakeRange(0, [simpleNumber length]) withTemplate:EMP_STR];
    
    // check if the number is to long
    if(simpleNumber.length>10) {
        // remove last extra chars.
        simpleNumber = [simpleNumber substringToIndex:10];
    }
    
    if(deleteLastChar) {
        // should we delete the last digit?
        simpleNumber = [simpleNumber substringToIndex:[simpleNumber length] - 1];
    }
    
    // 123 456 7890
    // format the number.. if it's less then 7 digits.. then use this regex.
    if(simpleNumber.length<7)
        simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d+)"
                                                               withString:@"$1-$2"
                                                                  options:NSRegularExpressionSearch
                                                                    range:NSMakeRange(0, [simpleNumber length])];
    
    else   // else do this one..
        simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d{3})(\\d+)"
                                                               withString:@"$1-$2-$3"
                                                                  options:NSRegularExpressionSearch
                                                                    range:NSMakeRange(0, [simpleNumber length])];
    return simpleNumber;
}

- (IBAction)sendCode:(id)sender {
    // Formatting Phone Number
    NSString *formattedPhoneNum = [[self.mobileTxt.text componentsSeparatedByCharactersInSet:
                                    [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"]
                                     invertedSet]]
                                   componentsJoinedByString:EMP_STR];
    NSString *trimmedEmail = [_emailTxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    self.emailTxt.text = trimmedEmail;
    
    if ((self.emailTxt.hasText) && (formattedPhoneNum.length == 10) && (self.countryField.text.length <= 3))
    {
        [self checkForUserAvailability];
    } else {
         //[self.popupController dismiss];
        //[[KGModal sharedInstance]hideAnimated:YES];
        [self showAlert:PLEASE_ENTER_VALID_MOBILE];
    }
}

- (void)checkForUserAvailability {
    if([self connected]) {
        // Vlidate Emal Address.
        NSString *emailRegEx = EMAIL_REG_EX;
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
        
        // Formatting Phone Number
        NSString *formattedPhoneNum = [[self.mobileTxt.text componentsSeparatedByCharactersInSet:
                                        [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"]
                                         invertedSet]]
                                       componentsJoinedByString:EMP_STR];
        NSLog(@"formated String %@", formattedPhoneNum);
        
        if ([emailTest evaluateWithObject:self.emailTxt.text] == YES) {
            txtEmail = self.emailTxt.text ;
            txtPhoneToPass= formattedPhoneNum;
            
            [[ActivityIndicator sharedInstance] showLoader];
            
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
            [dictParam setObject:txtPhoneToPass     forKey:PARAM_PHONENO_KEY];
            [dictParam setObject:txtEmail           forKey:PARAM_EMAIL_ID_KEY];
            
            NSString *rootURL =  [[AppDelegate shared] rootUrl];

            AFNHelper *afn=[[AFNHelper alloc]init];
            [afn postDataFromPath:METHOD_USERAVAILABILITY getUrl:rootURL withParamData:dictParam withBlock:^(id response, NSError *error)
             {
                 if(error) {
                     NSLog(@"ERROR DESC:%@",error.localizedDescription);
                     [[ActivityIndicator sharedInstance] hideLoader];
                     [self showAlert:NO_INTERNET_CONNECTION];
                     if(error.code == NSURLErrorTimedOut) {
                         [self showAlert:ERROR_TIMED_OUT];
                     } else if(error.code == NSURLErrorNotConnectedToInternet) {
                        [self showAlert:NO_INTERNET_CONNECTION];
                     } else {
                         [self showAlert:REQUEST_TIMED_OUT];
                     }
                     
                 } else {
                     NSDictionary *dictionary;
                     dictionary=response;
                     NSString *status=[dictionary valueForKey:STATUS];
                     if([status isEqualToString:SUCCESS]) {
                         [self sendSMSCode];
                         [self sendEmailThroughSendGrid];
                     } else {
                        //[self.popupController dismiss];
                         //[[KGModal sharedInstance]hideAnimated:YES];
                         NSString *message=[dictionary valueForKey:MESSAGE];
                         [self showAlert:message];
                     }
                 }
                 dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [[ActivityIndicator sharedInstance] hideLoader];
                     });
                 });
             }];
        } else {
           //[self.popupController dismiss];
            //[[KGModal sharedInstance]hideAnimated:YES];
            [self showAlert:PLEASE_FILL_EMAIL];
        }
    } else {
        // [self.popupController dismiss];
        //[[KGModal sharedInstance]hideAnimated:YES];
        [self showAlert: NO_INTERNET_CONNECTION];
    }
}

-(void)showAlert:(NSString *) message {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:APP_NAME message:message preferredStyle:UIAlertControllerStyleAlert];
    NSMutableAttributedString *titleAttr = [[NSMutableAttributedString alloc] initWithString:APP_NAME];
    [titleAttr addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:FONT_MEDIUM size:20]
                      range:NSMakeRange(0, [titleAttr length])];
    
    [alertController setValue:titleAttr forKey:@"attributedTitle"];
    
    NSMutableAttributedString *messageAttr = [[NSMutableAttributedString alloc] initWithString:message];
    [messageAttr addAttribute:NSFontAttributeName
                        value:[UIFont fontWithName:FONT_REGULAR size:14]
                        range:NSMakeRange(0, [messageAttr length])];
    
    
    [alertController setValue:messageAttr forKey:@"attributedMessage"];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alertController addAction:defaultAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)sendSMSCode {
    NSLog(@"otp : %d",randomNo);
    [[SignUpUtiltiy sharedInstance] sendCode:randomNo andMobile:self.mobileTxt.text andCountry:self.countryField.text andEmail:self.emailTxt.text andController:self.popupController];
}

-(void)sendEmailThroughSendGrid {
    NSLog(@"otp : %d",randomNo);
    [[SignUpUtiltiy sharedInstance] sendEmailThroughSendGrid:randomNo andEmail:txtEmail andController:self.popupController];
    
}

- (BOOL)connected {
    ReachabilityStatus status = [GLobalRealReachability currentReachabilityStatus];
    NSLog(@"Initial reachability status:%@",@(status));
    
    return status == 1 || status == 2;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
