//
//  AlertVC.h
//  AveriGo Markets
//
//  Created by Rattletech on 28/08/18.
//  Copyright © 2018 BTS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface AlertVC : NSObject
+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)msg controller:(id)controller;
+ (void)showAlertWithTitleDefault:(NSString *)title message:(NSString *)msg controller:(id)controller;
+ (void)showAlertWithTitleForView:(NSString *)title message:(NSString *)msg controller:(id)controller;
+ (void)showAlertWithHtml:(NSString *)title message:(NSString *)msg controller:(id)controller;

@end
