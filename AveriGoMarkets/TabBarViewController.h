    //
    //  TabBarViewController.h
    //  Averigo
    //
    //  Created by BTS on 27/02/17.
    //  Copyright © 2017 BTS. All rights reserved.
    //

#import <UIKit/UIKit.h>
#import "DataBaseHandler.h"


@interface TabBarViewController : UITabBarController <UITabBarControllerDelegate,UITabBarDelegate>
{
DataBaseHandler *objDBHelper;
}

@property(strong,nonatomic) IBOutlet UITabBar *tabbar;
@property(strong,nonatomic) UIButton *scan_btn;
@property(strong,nonatomic) UIImageView *imageView;
@property(nonatomic,assign) BOOL ismicromarket;
@property(strong,nonatomic) NSString *load;
@end
