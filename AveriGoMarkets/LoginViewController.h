//
//  LoginViewController.h
//  Averigo
//
//  Created by BTS on 08/11/16.
//  Copyright © 2016 BTS. All rights reserved.
//


#import <QuartzCore/QuartzCore.h>
#import <MessageUI/MessageUI.h>
#import "DataBaseHandler.h"
#import "JVFloatLabeledTextField.h"


@interface LoginViewController : UIViewController<UITextFieldDelegate,MFMailComposeViewControllerDelegate>
{
    int i_count;
    DataBaseHandler *objDBHelper;
}

@property(strong,nonatomic) IBOutlet JVFloatLabeledTextField *username_field;
@property(strong,nonatomic) IBOutlet JVFloatLabeledTextField *password_field;
@property(strong,nonatomic) IBOutlet UILabel *rememberMeLbl;
@property(strong,nonatomic) IBOutlet UIButton *login_btn;
@property(strong,nonatomic) IBOutlet UIButton *check_btn;
@property(strong,nonatomic) IBOutlet UIButton *forgot_btn;
@property(strong,nonatomic) IBOutlet UIButton *create_btn;
@property(strong,nonatomic) IBOutlet UIButton *help_btn;
@property(strong,nonatomic) IBOutlet UIButton *privacyPolicy_btn;
@property(strong,nonatomic) IBOutlet UIButton *terms_btn;
@property(strong,nonatomic) NSMutableArray *userdetails_array;
@property(strong,nonatomic) NSMutableArray *companydetails_array;
@property (strong, nonatomic) IBOutlet UIButton *btnHelp;
@property(strong,nonatomic) NSArray *temp_array;

-(IBAction)loginClicked:(id)sender;
-(IBAction)forgotClicked:(id)sender;
-(IBAction)createClicked:(id)sender;
-(IBAction)rememberClicked:(id)sender;

@end
