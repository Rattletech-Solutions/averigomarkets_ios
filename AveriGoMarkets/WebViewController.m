    //
    //  WebViewController.m
    //  AveriGo Markets
    //
    //  Created by BTS on 14/03/18.
    //  Copyright © 2018 BTS. All rights reserved.
    //

#import "WebViewController.h"
#import "MBProgressHUD.h"
#import <STPopup.h>
#import "ActivityIndicator.h"

@interface WebViewController ()

@end

@implementation WebViewController

- (instancetype)init
{
    if (self = [super init]) {
        self.contentSizeInPopup = CGSizeMake(300, 400);
        self.landscapeContentSizeInPopup = CGSizeMake(400, 200);
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
        // Do any additional setup after loading the view.
    self.inAppView.backgroundColor = [UIColor whiteColor];
    self.inAppView.layer.cornerRadius = 5;
    self.inAppView.clipsToBounds = true;
    
    self.title_lbl.text = self.titleStr;
    
    [self loadRequestFromString:self.loadStr];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}

- (void)loadRequestFromString:(NSString*)urlString
{
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [self.inAppView loadRequest:urlRequest];
}

-(IBAction)btnCloseClicked:(id)sender
{
     [[ActivityIndicator sharedInstance] hideLoader];
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
