//
//  SendFeedbackController.m
//  AveriGo Markets
//
//  Created by LeniYadav on 30/10/18.
//  Copyright © 2018 BTS. All rights reserved.
//

#import "SendFeedbackController.h"
#import <STPopup.h>
#import "SendGrid.h"
#import "Reachability.h"
#import "RealReachability.h"

@interface SendFeedbackController ()
{
    UIImage *attachment;
    NSMutableArray *imageArray;
}

@end

@implementation SendFeedbackController

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self){
        [self customInit];
    }
    return self;
}

-(void)customInit{
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    self.contentSizeInPopup = CGSizeMake(screenWidth - 25, 400);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Message";
    UIColor *borderColor = [UIColor darkGrayColor];
    _textView.layer.borderColor = borderColor.CGColor;
    _textView.layer.borderWidth = 1.0;
    _textView.layer.cornerRadius = 5.0;
    
    self.submitBtn.layer.cornerRadius = 5;
    self.submitBtn.clipsToBounds = YES;
    
    self.attachImageBtn.layer.cornerRadius = 5;
    self.attachImageBtn.clipsToBounds = YES;
    
    objDBHelper=[[DataBaseHandler alloc]init];
    
    [_attachmentLbl setHidden:YES];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear: animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(imageSelected:) name:@"imageSelected" object:nil];
}

-(void) imageSelected:(NSNotification *) notification
{
    NSDictionary* userInfo = notification.userInfo;
    imageArray = [[NSMutableArray alloc] init];
    imageArray = [userInfo objectForKey:@"image"];
    if(imageArray.count > 0){
        [self.attachmentLbl setHidden:NO];
    }
}

-(IBAction)attachImageClicked:(id)sender{
     [self.popupController pushViewController:[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ChoosePhotoViewController"] animated:YES];
}

-(IBAction)submitClicked:(id)sender {
    NSString * FeedbackType = [[NSUserDefaults standardUserDefaults]valueForKey:@"FeedbackType"];
    NSLog(@"FT%@",FeedbackType);
    NSMutableArray * userDetailsArray=[[NSMutableArray alloc]init];
    userDetailsArray=[[NSUserDefaults standardUserDefaults] objectForKey:@"userdetailsarray"];
    
    if(_textView.text.length!=0)
    {
        NSDictionary *dict;
        dict=[userDetailsArray objectAtIndex:0];
        NSString *username_str = EMP_STR;
        NSString *marketname_str = EMP_STR;
        if([[NSUserDefaults standardUserDefaults]objectForKey:@"lastname"] != NULL){
            username_str = [NSString stringWithFormat:@"%@ %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"firstname"],[[NSUserDefaults standardUserDefaults]objectForKey:@"lastname"]];
        }else{
            username_str = [[NSUserDefaults standardUserDefaults]objectForKey:@"firstname"];
        }
        if([[NSUserDefaults standardUserDefaults]objectForKey:@"micromarketname"] != NULL){
            marketname_str = [[NSUserDefaults standardUserDefaults]objectForKey:@"micromarketname"];
            
        }
        NSString *feedback_str = _textView.text ;
        NSString *recipient_str;
        if([self connected]){
        SendGrid *sendgrid = [SendGrid apiUser:[dict valueForKey:@"mail_host_user"] apiKey:[dict valueForKey:@"mail_host_pass"]];
        SendGridEmail *email = [[SendGridEmail alloc] init];
        if([FeedbackType isEqualToString:@"A"])
        {
            if([[NSUserDefaults standardUserDefaults]objectForKey:@"apprelatedmailid"]!=nil){
                recipient_str = [[NSUserDefaults standardUserDefaults]objectForKey:@"apprelatedmailid"];
                email.to = [[NSUserDefaults standardUserDefaults]objectForKey:@"apprelatedmailid"];
                // email.to = @"leni@rattletech.com";
            }else
            {
                recipient_str = SUPPORT_EMAL;
                email.to = SUPPORT_EMAL;
               //email.to = @"leni@rattletech.com";
            }
        }
        else
        {
            if([[NSUserDefaults standardUserDefaults]objectForKey:@"productrelatedmailid"]!=nil){
                NSString *termString = [[NSUserDefaults standardUserDefaults]objectForKey:@"productrelatedmailid"];
                recipient_str = termString;
                NSArray *terms  = [termString componentsSeparatedByString:@","];
                NSMutableArray *mutableArray = [terms mutableCopy];
                [email setTos: mutableArray];
                [email addBcc: SUPPORT_EMAL];
               // email.to = @"leni@rattletech.com";
                [email addFilter:@"bcc" parameterName:@"enable" parameterValue:@"1"];
                [email addFilter:@"bcc" parameterName:@"email" parameterValue: SUPPORT_EMAL];
                if(attachment != nil){
                 [email attachImage:attachment];
                }

            }else
            {
                recipient_str = SUPPORT_EMAL;
                email.to = SUPPORT_EMAL;
               // email.to = @"leni@rattletech.com";
            }
        }
            if(imageArray != nil && imageArray.count > 0){
                for(int i=0;i<imageArray.count;i++){
                    [email attachImage:[imageArray objectAtIndex:i]];
                }
            }
        email.from     = SUPPORT_EMAL;
        NSString *appVersion = [NSString stringWithFormat:@"AveriGo Markets feedback – iOS app Version %@",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
        email.subject  = appVersion;
        NSString *emailAddress = [[NSUserDefaults standardUserDefaults]objectForKey:@"username"];
        NSString *deviceManufacturer = @"Apple Inc.";
        NSString *deviceModel = [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceModel"];
        NSString *deviceOS = [[NSUserDefaults standardUserDefaults]objectForKey:@"deviceOS"];
        
        email.html = [NSString stringWithFormat:@"<html><body><p><b>Name: </b>%@<br><b>Email Address: </b>%@<br><b>Device Manufacturer: </b>%@<br><b>Device Model: </b>%@<br><b>Device OS: </b>%@<br><b>Micro Market Name: </b>%@<br><b>Message: </b>%@</p></body></html>",username_str,emailAddress,deviceManufacturer,deviceModel,deviceOS,marketname_str,feedback_str];
        [sendgrid sendWithWeb:email];
        [[KGModal sharedInstance]hideAnimated:YES];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        NSLog(@"%@", [formatter stringFromDate:[NSDate date]]);
        NSString *feedbackDate = [formatter stringFromDate:[NSDate date]];
         
        NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
            [timeFormatter setDateFormat:@"HH:mm:ss"];
        NSLog(@"%@", [timeFormatter stringFromDate:[NSDate date]]);
        NSString *feedbackTime = [timeFormatter stringFromDate:[NSDate date]];
        
        [objDBHelper insertFeedbackAnalytics:recipient_str andDate:feedbackDate andTime:feedbackTime andName:username_str andUserId:emailAddress andMicroMarketName:marketname_str andMicroMarketId:[[NSUserDefaults standardUserDefaults]objectForKey:@"micromarketid"] andMessage:feedback_str andDeviceManufacturer:deviceManufacturer andDeviceModel:deviceModel andDeviceOS:deviceOS andAppVersion:[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
            
        [AlertVC showAlertWithTitleDefault:APP_NAME message:FEEDBACK_THANK_YOU controller:self];
        [self.popupController dismiss];
        }else{
          [AlertVC showAlertWithTitleForView:APP_NAME message:NO_INTERNET_CONNECTION controller:self];
        }
    }
    else
    {
        [AlertVC showAlertWithTitleForView:APP_NAME message:PLEASE_ENTER_FEEDBACK controller:self];
    }
    

}


- (BOOL)connected
{
    ReachabilityStatus status = [GLobalRealReachability currentReachabilityStatus];
    NSLog(@"Initial reachability status:%@",@(status));
    
    return status == 1 || status == 2;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
