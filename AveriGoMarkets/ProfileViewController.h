    //
    //  ProfileViewController.h
    //  Averigo
    //
    //  Created by BTS on 23/11/16.
    //  Copyright © 2016 BTS. All rights reserved.
    //

#import <UIKit/UIKit.h>
#import "JVFloatLabeledTextField.h"
#import "DataBaseHandler.h"
#import <STPopup.h>

@interface ProfileViewController : UIViewController<UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIPickerViewDelegate>
{
    int width,height;
    DataBaseHandler *objDBHelper;
    UINavigationItem *newItem;
}

@property(strong,nonatomic) IBOutlet JVFloatLabeledTextField *firstname;
@property(strong,nonatomic) IBOutlet JVFloatLabeledTextField *lastname;
@property(strong,nonatomic) IBOutlet JVFloatLabeledTextField *emailid;
@property(strong,nonatomic) IBOutlet JVFloatLabeledTextField *password;
@property(strong,nonatomic) IBOutlet JVFloatLabeledTextField *number;
@property(strong,nonatomic) IBOutlet UIButton *edit_btn;
@property(strong,nonatomic) IBOutlet UIButton *submitBtn;

@property(strong,nonatomic) UIButton *button;
@property(strong,nonatomic) NSMutableArray *userdetails_array;
@property(strong,nonatomic) IBOutlet UIView *personal_view;

@property(strong,nonatomic) IBOutlet UIButton *signout_btn;
@property(strong,nonatomic) IBOutlet UIButton *close_btn;



@end
