//
//  HomeCell.h
//  HomeBeam
//
//  Created by BTS on 8/11/16.
//  Copyright © 2016 BTS. All rights reserved.
//


#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, EBCardCollectionLayoutType) {
    EBCardCollectionLayoutHorizontal,
    EBCardCollectionLayoutVertical
};

@interface HomeCollectionViewLayout : UICollectionViewLayout

@property (readonly) NSInteger currentPage;
@property (nonatomic, assign) UIOffset offset;
@property (nonatomic, strong) NSDictionary *layoutInfo;
@property (assign) EBCardCollectionLayoutType layoutType;

- (CGRect)contentFrameForCardAtIndexPath:(NSIndexPath *)indexPath;
@end
