    //
    //  PurchaseHistoryController.m
    //  Averigo
    //
    //  Created by BTS on 24/11/16.
    //  Copyright © 2016 BTS. All rights reserved.
    //

#import "PurchaseHistoryController.h"
#import "PurchaseCell.h"
#import "AFNHelper.h"
#import "AllConstants.h"
#import "MBProgressHUD.h"
#import "ProfileViewController.h"
#import "KGModal.h"
#import "ActivityIndicator.h"
#import "ShoppingCartViewController.h"
#import "BarCodeController.h"
#import "UIImage+FontAwesome.h"
#import "RealReachability.h"
#import "ItemsViewController.h"
#import "WebViewController.h"
#import "ReviewAndRatingViewController.h"
#import "AppDelegate.h"
#import "AllConstants.h"

@interface PurchaseHistoryController ()
{
    UIRefreshControl *refreshController;
    UIView *pop_view;
    UITextView *feedback_txt;
    UIToolbar *toolBar;
    NSString *title;
    NSString *price;
    NSString *quantity;
    NSMutableArray *section_array;
    NSMutableArray *recentPurchaseArray;
    int numsection;
    int currentPageNumber;
    UILabel *count_lbl;
    int total_count;
    UINavigationItem *newItem;
    BOOL beaconIsThere;
    BOOL noNetworkDidntLoad;
    int loadRecentPurchase;
    BOOL loading;
    BOOL isRefresh;
    
}

@end

@implementation PurchaseHistoryController

- (void)viewDidLoad {
    
    [super viewDidLoad];
        // Do any additional setup after loading the view.
    self.recentPurchaseBtn.backgroundColor = [UIColor colorWithRed: 0.0/255.0 green: 164.0/255.0 blue: 215.0/255.0 alpha: 1.0];
    objDBHelper = [[DataBaseHandler alloc]init];
    self.history_array=[[NSMutableArray alloc]init];
    self.items_array=[[NSMutableArray alloc]init];
    total_count=0;
    loadRecentPurchase = 0;
    currentPageNumber = 1;
    self->isRefresh = NO;
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self InitNavBar];
    [self.history_table registerNib:[UINib nibWithNibName:ORDER_HISTORY_CELL bundle:nil] forCellReuseIdentifier:@"OrderHistoryCell"];
    self.navigationItem.title=PURCHASE_HISTORY;
    self.history_table.separatorStyle=UITableViewCellSeparatorStyleNone;
    
    refreshController = [[UIRefreshControl alloc] init];
    [refreshController addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    [self.history_table addSubview:refreshController];
    numsection=0;
    
    self.recentPurchaseBtn.layer.cornerRadius = 5;
    self.recentPurchaseBtn.clipsToBounds = YES;
    [self.recentPurchaseBtn setHidden:YES];
    self.recentPurchaseBtnHeightConstraint.constant = 0;
    self.recentPurchaseBtnTopConstraint.constant = 0;
    self.recentPurchaseBtnBottom.constant = 0;
    [self adjustSize];
    
    if([self connected]) {
        noNetworkDidntLoad = NO;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.history_array removeAllObjects];
//            [self.history_table reloadData];
            [self getHistory];
        });
        
    } else {
        noNetworkDidntLoad = YES;
        [AlertVC showAlertWithTitleForView:APP_NAME message:NO_INTERNET_CONNECTION controller:self];
    }
}

- (void)InitNavBar {
    self.navigationItem.title = PURCHASE_HISTORY;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName: [UIColor colorWithRed:0.98 green:0.96 blue:0.95 alpha:1.0],NSFontAttributeName: [UIFont fontWithName: FONT_REGULAR size: 20]}];
    
    self.button  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
    [self.button setImage: [UIImage imageWithIcon: @"fa-user-circle" backgroundColor: [UIColor clearColor] iconColor: [UIColor whiteColor] andSize: CGSizeMake(35,35)] forState: UIControlStateNormal];
    [self.button addTarget:self action:@selector(UserProfileClicked:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *LeftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.button];
    
    LeftBarButtonItem.title = @"";
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    [self.navigationItem setBackBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil]];

    [self.navigationItem.backBarButtonItem setTitle: @""];
    
    self.navigationItem.leftBarButtonItem = LeftBarButtonItem;
}

- (IBAction)handleRefresh:(id)sender {
    numsection=0;
    currentPageNumber = 0;
    [_history_array removeAllObjects];
    [_items_array removeAllObjects];
    [refreshController endRefreshing];
    [self.history_table reloadData];
}

- (void)viewDidAppear:(BOOL)animated {
 
    [super viewDidAppear: animated];

    self.micromarketid=[[NSUserDefaults standardUserDefaults]objectForKey:@"micromarketid"];
    NSLog(@"micromarketid %@",self.micromarketid);
   
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkChanged:)
                                                 name:kRealReachabilityChangedNotification
                                               object:nil];
    
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear: animated];

    [[ActivityIndicator sharedInstance] hideLoader];
     [[NSNotificationCenter defaultCenter] removeObserver:self name:kRealReachabilityChangedNotification object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear: animated];
    
    BOOL isPurchased = [[NSUserDefaults standardUserDefaults] valueForKey:@"purchased"];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"refreshView"] == YES) {
        
        isRefresh = [[NSUserDefaults standardUserDefaults] valueForKey:@"refreshView"];

        [[NSUserDefaults standardUserDefaults] setBool: NO forKey: @"refreshView"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        
               if([self connected]) {
                   //dispatch_async(dispatch_get_main_queue(), ^{
                   
                       self->currentPageNumber = 1;
                   
                       [self.history_array removeAllObjects];
                                      
                       [self.history_table reloadData];
                   
                       [self getHistory];
                  // });
                   
               } else {
                   
                   [[NSUserDefaults standardUserDefaults] setBool: NO forKey: @"refreshView"];
                   
                   [[NSUserDefaults standardUserDefaults] synchronize];
                   
                   noNetworkDidntLoad = YES;

                   [AlertVC showAlertWithTitleForView: APP_NAME message: NO_INTERNET_CONNECTION controller: self];
               }
    }
    
    if (isPurchased) {
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey: @"purchased"];
        if([self connected]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.history_array removeAllObjects];
                [self.history_table reloadData];
                [self getHistory];
            });
            
        } else {
            noNetworkDidntLoad = YES;

            [AlertVC showAlertWithTitleForView:APP_NAME message:NO_INTERNET_CONNECTION controller:self];
        }
    } else if (![self connected]) {
        noNetworkDidntLoad = YES;
        self.no_lbl.text = @"No Internet Connection";
        [self.no_lbl setHidden: NO];
        [self.recentPurchaseBtn setHidden: YES];
        self.recentPurchaseBtnHeightConstraint.constant = 0;
        self.recentPurchaseBtnTopConstraint.constant = 0;
        self.recentPurchaseBtnBottom.constant = 0;
        [self.history_table setHidden: YES];
        [AlertVC showAlertWithTitleForView:APP_NAME message:NO_INTERNET_CONNECTION controller:self];
    } else if ([self connected] && noNetworkDidntLoad) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.recentPurchaseBtn setHidden: NO];
            self.recentPurchaseBtnHeightConstraint.constant = 30;
            self.recentPurchaseBtnTopConstraint.constant = 8;
            self.recentPurchaseBtnBottom.constant = 8;
            [self.history_array removeAllObjects];
            [self.history_table reloadData];
            [self getHistory];
        });
    }
    
   self.tabBarController.tabBar.hidden=NO;
   [self setScreenAnalytics];
   [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillTerminate:) name:UIApplicationWillTerminateNotification object:nil];
   [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillTerminate:) name:UIApplicationDidEnterBackgroundNotification object:nil];
   [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidComeToForeGround:) name:@"appDidComeToForeGround" object:nil];
   [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(scanBeaconNotification:)
                                                name:BEACON_NOTIFICATION object:nil];
}

- (void)networkChanged:(NSNotification *)notification {
    NSLog(@"nETWROK CHANGED");
    RealReachability *reachability = (RealReachability *)notification.object;
    ReachabilityStatus status = [reachability currentReachabilityStatus];
    if (status != RealStatusNotReachable){
        if(noNetworkDidntLoad) {
            [self.recentPurchaseBtn setHidden: NO];
            self.recentPurchaseBtnHeightConstraint.constant = 30;
            self.recentPurchaseBtnTopConstraint.constant = 8;
            self.recentPurchaseBtnBottom.constant = 8;
            [self getHistory];
        }
    } else if (![self connected]) {
        noNetworkDidntLoad = YES;
        self.no_lbl.text = @"No Internet Connection";
        [self.no_lbl setHidden: NO];
        [self.recentPurchaseBtn setHidden: YES];
        self.recentPurchaseBtnHeightConstraint.constant = 0;
        self.recentPurchaseBtnTopConstraint.constant = 0;
        self.recentPurchaseBtnBottom.constant = 0;
        [self.history_table setHidden: YES];
        [AlertVC showAlertWithTitleForView: APP_NAME message: NO_INTERNET_CONNECTION controller: self];
    }
    
    NSLog(@"currentStatus:%@",@(status));
}

- (BOOL)connected {
    ReachabilityStatus status = [GLobalRealReachability currentReachabilityStatus];
    NSLog(@"Initial reachability status:%@",@(status));
    
    return status != RealStatusNotReachable;
}

- (void) scanBeaconNotification:(NSNotification *) notification {
    if ([self connected]) {
        
        NSDictionary *dict = notification.userInfo;
        _beacons=[dict valueForKey:@"Beacons"];
        NSMutableArray * UDIDS = [NSMutableArray array];
        
        for(int i=0;i<_beacons.count;i++)
        {
            CLBeacon *beacon = (CLBeacon*)[self.beacons objectAtIndex:i];
            NSString *BUUID=[NSString stringWithFormat:@"%d-%d",beacon.major.intValue,beacon.minor.intValue];
            [UDIDS addObject:BUUID];
        }
        _UDID= [UDIDS componentsJoinedByString:@","];
        NSString *location_beacon =  [[NSUserDefaults standardUserDefaults]objectForKey:@"LOCATION_BEACON"];
        NSLog(@"location_beacon:%@",location_beacon);
        
        if(_beacons.count == 0) {
            beaconIsThere = NO;
        } else {
            [[NSNotificationCenter defaultCenter] postNotificationName:SCAN_NOTIFICATION object:@"show"];
        }
        
        if(location_beacon != nil){
            if(_beacons.count == 1){
                CLBeacon *beacon = (CLBeacon*)[self.beacons objectAtIndex:0];
                NSString *BUUID=[NSString stringWithFormat:@"%d-%d",beacon.major.intValue,beacon.minor.intValue];
                NSLog(@"location beacon %@",location_beacon);
                NSLog(@"beacon %@",BUUID);
                if([location_beacon isEqualToString:BUUID] && loadRecentPurchase == 0 && [self connected] ){
                    loadRecentPurchase = 1;
                    [self getPurchaseRecent];
                }else{
                    //[[NSNotificationCenter defaultCenter] postNotificationName:SCAN_NOTIFICATION object:@"hide"];
                    //[self hidePurchaseBtn];
                }
            } else if(_beacons.count > 1) {
                for(int i = 0; i<_beacons.count;i++){
                    CLBeacon *beacon = (CLBeacon*)[self.beacons objectAtIndex:i];
                    NSString *BUUID=[NSString stringWithFormat:@"%d-%d",beacon.major.intValue,beacon.minor.intValue];
                    NSLog(@"location beacon %@",location_beacon);
                    NSLog(@"beacon %@",BUUID);
                    if(location_beacon == BUUID && loadRecentPurchase == 0){
                        beaconIsThere = YES;
                        loadRecentPurchase = 1;
                        [self getPurchaseRecent];
                    }
                }
                if(!beaconIsThere){
                    [[NSNotificationCenter defaultCenter] postNotificationName:SCAN_NOTIFICATION object:@"hide"];
                    [self hidePurchaseBtn];
                }
            } else {
                [[NSNotificationCenter defaultCenter] postNotificationName:SCAN_NOTIFICATION object:@"hide"];
                [self hidePurchaseBtn];
            }
        } else {
            [[NSNotificationCenter defaultCenter] postNotificationName:SCAN_NOTIFICATION object:@"hide"];
            [self hidePurchaseBtn];
        }
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:SCAN_NOTIFICATION object:@"hide"];
    }
}

- (void)hidePurchaseBtn {
    if(!self.recentPurchaseBtn.isHidden){
        [self.recentPurchaseBtn setHidden:YES];
        self.recentPurchaseBtnHeightConstraint.constant = 0;
        self.recentPurchaseBtnTopConstraint.constant = 0;
        self.recentPurchaseBtnBottom.constant = 0;
        [self adjustSize];
        loadRecentPurchase = 0;
    }
}

- (void) appDidComeToForeGround:(NSNotification *) notification {
    self.tabBarController.selectedIndex=0;
}

- (IBAction)recentPurchaseClicked:(id)sender {

    [[NSUserDefaults standardUserDefaults] setObject:recentPurchaseArray forKey:@"recentpurchase"];
    
    self.tabBarController.selectedIndex = 1;
    
    double delayInSeconds = 0.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"showrecentpurchases" object:nil];
    });
}

- (IBAction)UserProfileClicked:(id)sender {
    ProfileViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:PROFILE_VC];
    controller.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:controller animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;    //count of section
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 231;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.history_array.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"OrderHistoryCell";
    
    OrderHistoryCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    CALayer *layer = ((OrderHistoryCell *) cell).foregroundView.layer;
    layer.cornerRadius = 10;
    layer.masksToBounds = YES;
    layer.borderColor = [UIColor colorWithRed:0.26 green:0.26 blue:0.26 alpha:0.5].CGColor;
    layer.borderWidth = 1.0;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSDictionary * dictobject = [self.history_array objectAtIndex:indexPath.row];
    cell.orderno.text = [NSString stringWithFormat:@"%@",[dictobject valueForKey:@"INVOICE_NO"]];
    NSString * invoiceDate = [NSString stringWithFormat:@"%@",[dictobject valueForKey:@"INVOICE_DATE"]];
    NSString* orderDate = [invoiceDate componentsSeparatedByString:@" "][0];
    NSString* orderTime = [NSString stringWithFormat:@"%@ %@",[invoiceDate componentsSeparatedByString:@" "][1], [invoiceDate componentsSeparatedByString:@" "][2]] ;
    cell.orderdate.text = orderDate;
    cell.ordertime.text = orderTime;
    cell.location.text = [NSString stringWithFormat:@"%@",[dictobject valueForKey:@"LOCATION"]];
    cell.location.numberOfLines = 2;
    cell.microMarkettring = dictobject[@"LOCATION"];
    cell.ordertotal.text = [NSString stringWithFormat:@"$%@",[dictobject valueForKey:@"TOTAL_AMOUNT"]];
    cell.items = [[self.history_array objectAtIndex:indexPath.row] valueForKey:@"ITEMS"];
    NSString* paymentMethodStr = [NSString stringWithFormat:@"%@",[dictobject valueForKey:@"PAYMENT_METHOD"]];
    cell.indexpath = [NSString stringWithFormat:@"%ld", (long)indexPath.row];
    if([paymentMethodStr hasPrefix:@"xxxxFunds"]) {
        cell.paymentMethod.text = @"Stored Funds";
    } else if ([paymentMethodStr hasPrefix:@"xxx"]) {
        cell.paymentMethod.text = [NSString stringWithFormat:@"Card # %@",paymentMethodStr];
    } else {
        cell.paymentMethod.text = paymentMethodStr;
    }
    
    cell.delegate = self;
    
    [cell.collectionview reloadData];
    return cell;
    
}

-(void)redirectToBuyOptionWebPage:(NSDictionary *)dict {
    NSString * url = [dict valueForKey:@"BUTTON_URL"];
    NSString * buttonText = [dict valueForKey:@"BUTTON_TEXT"];
    if ([url isKindOfClass: [NSString class]]) {
        NSLog(@"%@", url);
        WebViewController *vc;
        vc=[self.storyboard instantiateViewControllerWithIdentifier:HELP_WEB_VC];
        vc.loadStr = url;
        vc.titleStr = buttonText;
        vc.modalPresentationStyle = UIModalPresentationFullScreen;
        [self presentViewController:vc animated:YES completion:nil];
    }
}

-(void)redirectToReviewAndRatingView:(NSDictionary *)dict {
    
    NSString *indexStr = [[NSUserDefaults standardUserDefaults] valueForKey: @"currentIndex"];
        
    ReviewAndRatingViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier: @"ReviewAndRatingViewController"];
    
    vc.microMartketString = indexStr;
    
    vc.purchaseDictionary = dict;
    
    vc.modeString = PARAM_MODE_INSERT;
        
    [self.navigationController pushViewController: vc animated: YES];
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (!loading) {
        NSInteger currentOffset = scrollView.contentOffset.y;
        NSInteger maximumOffset = scrollView.contentSize.height- scrollView.frame.size.height;
        NSLog(@"MOF:%ld",maximumOffset - currentOffset);
        if (maximumOffset - currentOffset <= 500) {
        loading = true;
        currentPageNumber ++;
        [self getHistory];
        }
    }
}

- (void)adjustSize {
    
    CGRect newFrame = self.history_table.frame;
    if(self.recentPurchaseBtn.isHidden){
    newFrame.origin.y = 0;
    newFrame.size.height = self.view.frame.size.height;
    }else{
    newFrame.origin.y = 50;
    newFrame.size.height = self.view.frame.size.height - 50;
    }
    [self.history_table setFrame:newFrame];
}

- (void)getHistory {
    [[ActivityIndicator sharedInstance]showLoader];
    
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"username"]  forKey:PARAM_EMAIL_ID_KEY];
        [dictParam setObject:[NSString stringWithFormat:@"%d",currentPageNumber]  forKey:PARAM_PAGE_NO];
    
    NSString * URL =  [NSString stringWithFormat:@"%@%@%@%@%d%@%@",@"http://vending.averiware.com:8080/PurchaseHistory.php?", @"EmailID=", [[NSUserDefaults standardUserDefaults]objectForKey:@"username"], @"&PageNo=", currentPageNumber, @"&Secret=", SECRET_KEY];
    
        AFNHelper *afn=[[AFNHelper alloc]init];
    [afn getDataFromPath: @"" getUrl:URL withParamData:nil withBlock:^(id response, NSError *error) {
    if(error)
        {
            if(self.history_array != nil && self.history_array.count > 0){
                self.history_array=[[NSMutableArray alloc]init];
                [self.history_table reloadData];
            }
            if(error.code == NSURLErrorTimedOut){
                [AlertVC showAlertWithTitleForView:APP_NAME message:ERROR_TIMED_OUT controller:self];
            }else if(error.code == NSURLErrorNotConnectedToInternet){
                [AlertVC showAlertWithTitleForView:APP_NAME message:NO_INTERNET_CONNECTION controller:self];
            }
        }
    else
        {
        NSDictionary *dictionary;
        dictionary=response;
            NSLog(@"response %@",response);
            self->_status=[dictionary valueForKey:STATUS];
            self->_message=[dictionary valueForKey:MESSAGE];
        [self.history_array addObjectsFromArray:[dictionary valueForKey:@"MC Purchase History"]];
        [self.items_array addObjectsFromArray:[self.history_array valueForKey:@"ITEMS"]];
        NSLog(@"Dict:%@",dictionary);
        
            
        if([self.history_array count]!=0)
            {
            [self.history_table setHidden:NO];
            [self.no_lbl setHidden:YES];
            [[ActivityIndicator sharedInstance]hideLoader];
            [self.history_table reloadData];
            }
        else{
            [self.history_table setHidden:YES];
            self.no_lbl.text = @"No Puschases Made";
            [[ActivityIndicator sharedInstance]hideLoader];
            [self.no_lbl setHidden:NO];
        }
            
            if([self->_status isEqualToString: SUCCESS])
            {
                if (self->isRefresh == YES) {
                    
                    NSInteger historyDataIndex = [[NSUserDefaults standardUserDefaults] integerForKey: @"indexpath"];
                    
                    if (historyDataIndex <= (self.history_array.count-1)) {
                        
                        self->isRefresh = NO;
                                                
                        [self.history_table reloadData];
                        
                        [[ActivityIndicator sharedInstance]hideLoader];

                        NSIndexPath *indexPath = [NSIndexPath indexPathForRow: historyDataIndex inSection:0];
                        
                        [self.history_table scrollToRowAtIndexPath:indexPath
                                             atScrollPosition:UITableViewScrollPositionTop
                                                     animated:NO];
                        
                    } else {
                        self->currentPageNumber = self->currentPageNumber + 1;
                        
                        [self getHistory];
                    }
                    
                } else {
            [self.history_table reloadData];
                    [[ActivityIndicator sharedInstance]hideLoader];

                }
            } else {
                self->isRefresh = NO;
            }
           }
       
        
        self->loading = false;
        self->noNetworkDidntLoad = NO;
                                                    
    
    }];
   
}

- (void)getPurchaseRecent {
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"username"]  forKey:PARAM_EMAIL_ID_KEY];
    [dictParam setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"beaconMajor"] forKey:PARAM_BEACON_MAJOR];
    [dictParam setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"beaconMinor"] forKey:PARAM_BEACON_MINOR];
    
    NSString *rootURL =  [[AppDelegate shared] rootUrl];

    AFNHelper *afn=[[AFNHelper alloc]init];
    [afn postDataFromPath:METHOD_PURCHASE_RECENT getUrl:rootURL withParamData:dictParam withBlock:^(id response, NSError *error)
     {
         if(error)
         {
             
         }
         else
         {
             NSDictionary *dictionary;
             dictionary=response;
             NSLog(@"response %@",response);
             _status=[dictionary valueForKey:STATUS];
             if([_status isEqualToString:@"Y"])
             {
                 recentPurchaseArray = [[NSMutableArray alloc]init];
                 recentPurchaseArray = [dictionary valueForKey:@"MC Recent Purchase History"];
                 [self.recentPurchaseBtn setHidden:NO];
                 self.recentPurchaseBtnHeightConstraint.constant = 30;
                 self.recentPurchaseBtnTopConstraint.constant = 8;
                 self.recentPurchaseBtnBottom.constant = 8;
                 [self adjustSize];
             }else{
                 [self.recentPurchaseBtn setHidden:YES];
                 self.recentPurchaseBtnHeightConstraint.constant = 0;
                 self.recentPurchaseBtnTopConstraint.constant = 0;
                 self.recentPurchaseBtnBottom.constant = 0;
                 [self adjustSize];
             }
         }
         
     }];
}

- (void)setScreenAnalytics {
    NSString * SessionGUID = [[NSUserDefaults standardUserDefaults] objectForKey:@"SessionGUID"];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString * checkInTime=[dateFormatter stringFromDate:[NSDate date]];
    NSString * micromarketid = [[NSUserDefaults standardUserDefaults] valueForKey:@"micromarketid"];
    NSString * micromarketname = [[NSUserDefaults standardUserDefaults] valueForKey:@"micromarketname"];
    NSString * userID = [[NSUserDefaults standardUserDefaults] objectForKey:@"username"];
    NSString * analyticsCount = [objDBHelper getScreenAnalyticsBySessionID:SessionGUID andScreenName:@"History"];
    int visitCount = [analyticsCount intValue] +1;
    if(visitCount == 1) {
        [objDBHelper insertScreenAnalytics:userID andsessionGUID:SessionGUID andMircoMarketID:[micromarketid intValue] andMicroMarketName:micromarketname andScreenName:@"History" andscreenVisits:visitCount andCheckInTime:checkInTime andCheckOutTime:@"" andSyncFlag:@"Y"];
    } else {
    [objDBHelper UpdateScreenAnalytics:SessionGUID andVisitCount:visitCount andScreenName:@"History"];
    }
}

- (void)updateScreenAnalyticsCheckOutTime {
    NSString * SessionGUID = [[NSUserDefaults standardUserDefaults] objectForKey:@"SessionGUID"];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString * checkOutTime=[dateFormatter stringFromDate:[NSDate date]];
    [objDBHelper UpdateScreenAnalyticsCheckOutTime:SessionGUID andCheckOutTime:checkOutTime andScreenName:@"History"];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillTerminateNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self updateScreenAnalyticsCheckOutTime];
    [super viewWillDisappear:true];
}

- (void)appWillTerminate:(NSNotification*)note {
[self updateScreenAnalyticsCheckOutTime];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
