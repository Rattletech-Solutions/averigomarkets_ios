//
//  ForgotPasswordController.h
//  VendBeam
//
//  Created by BTS on 28/08/17.
//  Copyright © 2017 BTS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JVFloatLabeledTextField.h"

@interface ForgotPasswordController : UIViewController<UITextFieldDelegate>

@property(strong,nonatomic) IBOutlet JVFloatLabeledTextField *emailTxt;
@property(strong,nonatomic) IBOutlet JVFloatLabeledTextField *otpTxt;
@property(strong,nonatomic) IBOutlet UIButton *getOtpBtn;
@property(strong,nonatomic) IBOutlet UIButton *verifyOtpBtn;
@property(strong,nonatomic) IBOutlet UIButton *submitBtn;
@property(strong,nonatomic) IBOutlet JVFloatLabeledTextField *passTxt;
@property(strong,nonatomic) IBOutlet JVFloatLabeledTextField *confirmTxt;
@property(strong,nonatomic) IBOutlet UIButton *closeBtn;
@property(strong,nonatomic) IBOutlet UIView *otpView;
@property(strong,nonatomic) IBOutlet UIView *resetView;

@property(strong,nonatomic) NSMutableArray *userdetails_array;
@property(strong,nonatomic) NSString *emailid;
@property(strong,nonatomic) NSString *otp;
@end
