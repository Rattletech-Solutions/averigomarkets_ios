//
//  FeedbackCentreController.m
//  AveriGo Markets
//
//  Created by LeniYadav on 30/10/18.
//  Copyright © 2018 BTS. All rights reserved.
//

#import "FeedbackCentreController.h"
#import <STPopup.h>

@interface FeedbackCentreController ()

@end

@implementation FeedbackCentreController

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self){
        [self customInit];
    }
    return self;
}

- (instancetype)init
{
    if (self = [super init]) {
        self.contentSizeInPopup = CGSizeMake(375, 200);
    }
    return self;
}

-(void)customInit{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    self.contentSizeInPopup = CGSizeMake(screenWidth - 25, 260);
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Feedback";
    self.helpBtn.layer.cornerRadius = 5;
    self.helpBtn.clipsToBounds = YES;
    self.helpBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    self.feedbackBtn.layer.cornerRadius = 5;
    self.feedbackBtn.clipsToBounds = YES;
    self.feedbackBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
   
}


-(IBAction)appRelatedFeedback:(id)sender {
    [[NSUserDefaults standardUserDefaults] setObject:@"A"  forKey:@"FeedbackType"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [self.popupController pushViewController:[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SendFeedbackController"] animated:YES];
}


-(IBAction)productRelatedFeedback:(id)sender {
    [[NSUserDefaults standardUserDefaults] setObject:@"P"  forKey:@"FeedbackType"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [self.popupController pushViewController:[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SendFeedbackController"] animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
