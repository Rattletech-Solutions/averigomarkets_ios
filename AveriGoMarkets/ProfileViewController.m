    //
    //  ProfileViewController.m
    //  Averigo
    //
    //  Created by BTS on 23/11/16.
    //  Copyright © 2016 BTS. All rights reserved.
    //

#import "ProfileViewController.h"
#import "KGModal.h"
#import "AllConstants.h"
#import "AFNHelper.h"
#import "MBProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "AlertVC.h"
#import "ActivityIndicator.h"
#import "Reachability.h"
#import "RealReachability.h"
#import "AppDelegate.h"

@interface ProfileViewController() {
    UIBarButtonItem *Edit;
    NSString *ifNews;
    NSString *ifSms;
    UIView *pop_view;
    NSString *stateid;
    NSString *countryid;
    bool isstate;
    NSString *image_str;
    NSString *image_name;
    UIToolbar *toolBar;
}

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title=@"Profile1";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor colorWithRed:0.98 green:0.96 blue:0.95 alpha:1.0],NSFontAttributeName:[UIFont fontWithName:FONT_MEDIUM size:20]}];
    
    objDBHelper = [[DataBaseHandler alloc]init];
    image_str=EMP_STR;
    image_name=EMP_STR;
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self InitNavBar];
    width=self.view.frame.size.width;
    height=self.view.frame.size.height;
    
    self.edit_btn.layer.cornerRadius = 5;
    self.edit_btn.clipsToBounds = YES;
    
    toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)]; //toolbar is uitoolbar object
    toolBar.barStyle = UISearchBarStyleDefault;
    UIBarButtonItem *btnDone = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:nil];
    [toolBar setItems:[NSArray arrayWithObject:btnDone]];
    ifNews=@"N";
    ifSms=@"N";
    self.signout_btn.layer.cornerRadius=5;
    self.signout_btn.clipsToBounds=YES;
    [self loadvalues];
    
    [self setTextFieldBorder:_firstname];
    [self setTextFieldBorder:_lastname];
    [self setTextFieldBorder:_emailid];
    [self setTextFieldBorder:_password];
    [self setTextFieldBorder:_number];
    
    self.submitBtn.layer.cornerRadius = 5;
    self.submitBtn.clipsToBounds = YES;
    
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    [textView setInputAccessoryView:toolBar];
    return YES;
}


-(void)InitNavBar {
    [self setdelegate];
}

-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setTextFieldBorder :(UITextField *)textField {
    
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1;
    NSLog(@"%f",textField.frame.size.width);
    border.borderColor = [UIColor lightGrayColor].CGColor;
    border.frame = CGRectMake(0, textField.frame.size.height - borderWidth, textField.frame.size.width + 120, textField.frame.size.height);
    border.borderWidth = borderWidth;
    [textField.layer addSublayer:border];
    textField.layer.masksToBounds = YES;
    
}

-(IBAction)SignOutClicked:(id)sender {
    [objDBHelper deleteCart];
    //[[NSUserDefaults standardUserDefaults]removeObjectForKey:@"signedin"];
    //[[NSUserDefaults standardUserDefaults]removeObjectForKey:@"remember"];
    [[NSNotificationCenter defaultCenter] postNotificationName: SCAN_NOTIFICATION object:@"signout"];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)CloseClicked:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


-(void)loadvalues {
    self.userdetails_array = [[NSUserDefaults standardUserDefaults]objectForKey:@"userdetailsarray"];
    NSDictionary *dictobject;
    dictobject=[self.userdetails_array objectAtIndex:0];
    self.firstname.text=[dictobject valueForKey:@"FIRST_NAME"];
    self.lastname.text=[dictobject valueForKey:@"LAST_NAME"];
    self.password.text=[dictobject valueForKey:@"PASSWORD"];
    self.emailid.text=[dictobject valueForKey:@"EMAIL_ID"];
    self.number.text=[dictobject valueForKey:@"PHONE_NO"];
}


-(void) setdelegate {
    self.firstname.delegate=self;
    self.firstname.enabled=NO;
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(10, 0, 5, 20)];
    [paddingView setBackgroundColor:[UIColor whiteColor]];
    
    
    self.lastname.delegate=self;
    self.lastname.enabled=NO;
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(10, 0, 5, 20)];
    [paddingView1 setBackgroundColor:[UIColor whiteColor]];
    
    self.password.delegate=self;
    self.password.enabled=NO;
    UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(10, 0, 5, 20)];
    [paddingView2 setBackgroundColor:[UIColor whiteColor]];
    
    self.emailid.delegate=self;
    self.emailid.enabled=NO;
    UIView *paddingView3 = [[UIView alloc] initWithFrame:CGRectMake(10, 0, 5, 20)];
    [paddingView3 setBackgroundColor:[UIColor whiteColor]];
    
    self.number.delegate=self;
    self.number.enabled=NO;
    UIView *paddingView4 = [[UIView alloc] initWithFrame:CGRectMake(10, 0, 5, 20)];
    [paddingView4 setBackgroundColor:[UIColor whiteColor]];
    
}
-(IBAction)EditClicked:(id)sender {
    self.password.enabled = YES;
    [self.submitBtn setEnabled:YES];
    [self.edit_btn setBackgroundColor:[UIColor colorWithRed:0.26 green:0.26 blue:0.26 alpha:1.0]];
    [self.edit_btn setTitleColor:[UIColor colorWithRed:0.60 green:0.60 blue:0.60 alpha:1.0] forState:UIControlStateNormal];
    [self.edit_btn setEnabled:NO];
    [self.submitBtn setBackgroundColor:[UIColor colorWithRed:0.27 green:0.56 blue:0.33 alpha:1.0]];
    [self.submitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}


-(void)resetPassword:(NSString *)password {
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:DateFormat];
    NSString *currentTime = [dateFormatter stringFromDate:today];
    NSLog(@"currentTime %@ , %@",currentTime , [[NSUserDefaults standardUserDefaults] objectForKey:UDID]);
    [[ActivityIndicator sharedInstance] showLoader];

    
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setObject:MICROMARKET     forKey:PARAM_COMPANYACRO];
    [dictParam setObject:MICROMARKET     forKey:PARAM_SERVICE_NAME];
    [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:UDID]      forKey:PARAM_DEVICE_UDID];
    [dictParam setObject:currentTime  forKey:PARAM_DEVICE_DATETIME];
    [dictParam setObject:self.firstname.text        forKey:PARAM_FIRSTNAME];
    [dictParam setObject:self.lastname.text         forKey:PARAM_LASTNAME];
    [dictParam setObject:EMP_STR            forKey:PARAM_COMPANY_NAME];
    [dictParam setObject:EMP_STR            forKey:PARAM_NICKNAME];
    [dictParam setObject:EMP_STR                  forKey:PARAM_PROFILE_IMAGE];
    [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"username"]        forKey:PARAM_EMAIL_ID];
    [dictParam setObject:password         forKey:PARAM_PASSWORD];
    [dictParam setObject:self.number.text           forKey:PARAM_PHONENO];
    [dictParam setObject:EMP_STR      forKey:PARAM_ADDRESS];
    [dictParam setObject:EMP_STR             forKey:PARAM_CITY];
    [dictParam setObject:EMP_STR              forKey:PARAM_ZIP];
    [dictParam setObject:EMP_STR         forKey:PARAM_STATE];
    [dictParam setObject:EMP_STR        forKey:PARAM_COUNTRY];
    [dictParam setObject:EMP_STR    forKey:PARAM_ENABEL_NEWSLETTER];
    [dictParam setObject:EMP_STR             forKey:PARAM_ENABLE_SMS];
    [dictParam setObject:@"UP"                forKey:PARAM_PROCESS_STATUS];
    [dictParam setObject:image_name forKey:@"PROFILE_IMAGE_NAME"];
    NSLog(@"DICTPARAM:%@",dictParam);
    
    NSString *rootURL =  [[AppDelegate shared] rootUrl];

    AFNHelper *afn=[[AFNHelper alloc]init];
    [afn postDataFromPath1:METHOD_SIGNUP getUrl:rootURL withParamData:dictParam withBlock:^(id response, NSError *error)
     {
     if(error) {
             [[ActivityIndicator sharedInstance] hideLoader];
             NSLog(@"ERROR DESC:%@",error.localizedDescription);
         } else {
         NSLog(@"%@",response);
         [[ActivityIndicator sharedInstance] hideLoader];
         NSDictionary *dictionary;
         dictionary=response;
         self.userdetails_array=[[NSMutableArray alloc]init];
         NSString *status=[dictionary valueForKey:STATUS];
         if([status isEqualToString:SUCCESS])
             {
             self.userdetails_array=[dictionary valueForKey:@"User"];
             NSDictionary *dictobject;
             dictobject=[self.userdetails_array objectAtIndex:0];
             [[NSUserDefaults standardUserDefaults] setObject:self.userdetails_array forKey:@"userdetailsarray"];
             
             [[NSUserDefaults standardUserDefaults] setObject:self.password.text forKey:@"password"];
            [self showAlert:PASSWORD_CHANGED_SUCCESSFULLY];
             _password.text = password;
             } else {
             NSString *message=[dictionary valueForKey:MESSAGE];
             [self showAlert:message];
            }
         }
        [[ActivityIndicator sharedInstance] hideLoader];
     }];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.firstname) {
        [textField resignFirstResponder];
        return NO;
    } else if (textField==self.lastname){
        [textField resignFirstResponder];
        return NO;
    } else if (textField == self.password) {
        [textField resignFirstResponder];
        return NO;
    } else if (textField==self.emailid){
        [textField resignFirstResponder];
        return NO;
    } else if (textField == self.number) {
        [textField resignFirstResponder];
        return NO;
    }
    
    return YES;
}

-(void)keyboardDidHide:(NSNotification *)notification {
    [self.view setFrame:CGRectMake(0,0,width,height)];
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

-(void)showAlert:(NSString *) message {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:APP_NAME message:message preferredStyle:UIAlertControllerStyleAlert];
    NSMutableAttributedString *titleAttr = [[NSMutableAttributedString alloc] initWithString:APP_NAME];
    [titleAttr addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:FONT_MEDIUM size:20]
                      range:NSMakeRange(0, [titleAttr length])];
    
    [alertController setValue:titleAttr forKey:@"attributedTitle"];
    
    NSMutableAttributedString *messageAttr = [[NSMutableAttributedString alloc] initWithString:message];
    [messageAttr addAttribute:NSFontAttributeName
                        value:[UIFont fontWithName:FONT_REGULAR size:14]
                        range:NSMakeRange(0, [messageAttr length])];
    
    
    [alertController setValue:messageAttr forKey:@"attributedMessage"];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alertController addAction:defaultAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

-(IBAction)changePassword:(id)sender {
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ChangePasswordController"]];
    popupController.style = STPopupStyleBottomSheet;
    [popupController presentInViewController:self];
}

- (void) updatePassword:(NSNotification *) notification {
    NSDictionary *userInfo = notification.userInfo;
    NSLog(@"Password:%@",[userInfo objectForKey:@"Password"]);
    [self resetPassword:[userInfo objectForKey:@"Password"]];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear: animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
     selector:@selector(updatePassword:)
     name:@"ChangePasswordNotification" object:nil];
    [self setScreenAnalytics];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillTerminate:) name:UIApplicationWillTerminateNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillTerminate:) name:UIApplicationDidEnterBackgroundNotification object:nil];
}



-(void)viewWillDisappear:(BOOL)animated {
  [self updateScreenAnalyticsCheckOutTime];
  [super viewWillDisappear:true];
}

- (BOOL)connected {
    ReachabilityStatus status = [GLobalRealReachability currentReachabilityStatus];
    NSLog(@"Initial reachability status:%@",@(status));
    
    return status == 1 || status == 2;
}

-(void)appWillTerminate:(NSNotification*)note {
[self updateScreenAnalyticsCheckOutTime];
}

-(void)setScreenAnalytics {
    NSString * SessionGUID = [[NSUserDefaults standardUserDefaults] objectForKey:@"SessionGUID"];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString * checkInTime=[dateFormatter stringFromDate:[NSDate date]];
    NSString * micromarketid = [[NSUserDefaults standardUserDefaults] valueForKey:@"micromarketid"];
    NSString * micromarketname = [[NSUserDefaults standardUserDefaults] valueForKey:@"micromarketname"];
    NSString * userID = [[NSUserDefaults standardUserDefaults] objectForKey:@"username"];
    NSString * analyticsCount = [objDBHelper getScreenAnalyticsBySessionID:SessionGUID andScreenName:@"Profile"];
    int visitCount = [analyticsCount intValue] +1;
    
    if(visitCount == 1) {
    [objDBHelper insertScreenAnalytics:userID andsessionGUID:SessionGUID andMircoMarketID:[micromarketid intValue] andMicroMarketName:micromarketname andScreenName:@"Profile" andscreenVisits:visitCount andCheckInTime:checkInTime andCheckOutTime:@"" andSyncFlag:@"Y"];
    } else {
    [objDBHelper UpdateScreenAnalytics:SessionGUID andVisitCount:visitCount andScreenName:@"Profile"];
    }
}

-(void)updateScreenAnalyticsCheckOutTime {
    NSString * SessionGUID = [[NSUserDefaults standardUserDefaults] objectForKey:@"SessionGUID"];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString * checkOutTime=[dateFormatter stringFromDate:[NSDate date]];
    [objDBHelper UpdateScreenAnalyticsCheckOutTime:SessionGUID andCheckOutTime:checkOutTime andScreenName:@"Profile"];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillTerminateNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
