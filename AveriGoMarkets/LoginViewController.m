//
//  LoginViewController.m
//  Averigo
//
//  Created by BTS on 08/11/16.
//  Copyright © 2016 BTS. All rights reserved.
//

#import "LoginViewController.h"
#import "TabBarViewController.h"
#import "RegisterViewController.h"
#import "AFNHelper.h"
#import "AllConstants.h"
#import "Reachability.h"
#import "MBProgressHUD.h"
#import "SendGrid.h"
#import "Base64.h"
#import "FBEncryptorAES.h"
#import "NSString+AESCrypt.h"
#import "ForgotPasswordController.h"
#import "EMCCountryPickerController.h"
#import "KGModal.h"
#import "AlertVC.h"
#import "ActivityIndicator.h"
#import "countryCode.h"
#import "AFHTTPSessionManager.h"
#import "WebViewController.h"
#import "IQKeyBoardManager/IQKeyboardReturnKeyHandler.h"
#import "UIImage+FontAwesome.h"
#import <STPopup.h>
#import "RealReachability.h"
#import "AppDelegate.h"

@interface LoginViewController ()
{
    NSString    *udid;
    NSString    *username;
    NSString    *password;
    NSString    *beacon_str;
    UIView      *popView;
    NSString    *txtEmail;
    NSString    *txtPopUpPhoneNum;
    UITextField *phoneNumField;
    UITextField *countryField;
    UITextField *emailField;
    UITextField *fieldOne;
    UITextField *fieldTwo;
    UITextField *fieldThree;
    UITextField *fieldFour;
    UIImageView *country_img;
    NSString    *imagePath;
    NSString    *otp;
    NSString    *registeredEmail;
    NSString    *registeredPhoneNumber;
    NSString    *isEmailRegistered;
    NSString    *isPhoneNumberRegistered;
    UIButton    *viewBtn;
    IQKeyboardReturnKeyHandler *returnKeyHandler;
    int        count;
    STPopupController *openPopUpController;
    BOOL isPasswordVisible;
    BOOL isResetPasswordVisible;
}

@end

@implementation LoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    i_count=1;
    self.login_btn.layer.cornerRadius=5;
    self.login_btn.clipsToBounds=YES;
    objDBHelper = [[DataBaseHandler alloc]init];

    country_img = [[UIImageView alloc]
                   initWithImage:[UIImage imageNamed:imagePath]];
    
    // ----Tap Gestures---
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(countryImgClicked)];
    singleTap.numberOfTapsRequired = 1;
    [country_img setUserInteractionEnabled:YES];
    [country_img addGestureRecognizer:singleTap];
    
    UITapGestureRecognizer *rememberMeTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleRememberMeTap:)];
    [self.rememberMeLbl addGestureRecognizer:rememberMeTap];
    [self.rememberMeLbl setUserInteractionEnabled:YES];
    
    UITapGestureRecognizer *sendCodeTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(sendCode:)];
    sendCodeTap.numberOfTapsRequired = 1;
    [viewBtn addGestureRecognizer:sendCodeTap];
    [viewBtn setUserInteractionEnabled:YES];
    
    [self setFieldBorder:self.username_field];
    [self setFieldBorder:self.password_field];
    
    
    NSString *buttonString = @"Dont have an account? Sign Up";
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:buttonString];
    NSString *normalString = @"Dont have an account?";
    NSRange normalRange = [buttonString rangeOfString:normalString];
    [attributedString addAttribute: NSFontAttributeName value:[UIFont fontWithName:FONT_REGULAR size:14] range:normalRange];
    [attributedString addAttribute: NSForegroundColorAttributeName value:[UIColor colorWithRed:0.17 green:0.24 blue:0.31 alpha:1.0] range:normalRange];
    [UIColor colorWithRed:0.17 green:0.24 blue:0.31 alpha:1.0];
    NSString *boldString = @"Sign Up";
    NSRange boldRange = [buttonString rangeOfString:boldString];
    [attributedString addAttribute: NSFontAttributeName value:[UIFont fontWithName:FONT_BOLD size:14] range:boldRange];
    [attributedString addAttribute: NSForegroundColorAttributeName value:[UIColor colorWithRed:0.45 green:0.73 blue:0.31 alpha:1.0]  range:boldRange];
    [_create_btn setAttributedTitle:attributedString forState:UIControlStateNormal];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.7 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        if([[NSUserDefaults standardUserDefaults]objectForKey:@"otpscreen"]!=nil){
            if([[[NSUserDefaults standardUserDefaults]objectForKey:@"otpscreen"] isEqual:@"true"]){
                RegisterViewController *vc;
                vc = [self.storyboard instantiateViewControllerWithIdentifier: REGISTER_VC];
                //[self presentViewController:vc animated:YES completion:nil];
                [self.navigationController pushViewController:vc animated:YES];
            }
        }
    });
 
    
    UIButton *eyeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    eyeBtn.frame = CGRectMake(0, 0, 24, 24);
    [eyeBtn setImage:[UIImage imageWithIcon:@"fa-eye" backgroundColor:[UIColor clearColor] iconColor:[UIColor grayColor] andSize:CGSizeMake(24,24)] forState:UIControlStateNormal];
    [eyeBtn addTarget:self action:@selector(eyeBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        // eyeBtn.imageEdgeInsets = UIEdgeInsetsMake(0, -16, 0, 0);
    self.password_field.rightViewMode = UITextFieldViewModeAlways;
    self.password_field.rightView = eyeBtn;
    
    UIButton *resetEyeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    resetEyeBtn.frame = CGRectMake(0, 0, 24, 24);
        //[resetEyeBtn setImage:[UIImage imageNamed:@"password_visible"] forState:UIControlStateNormal];
    [resetEyeBtn setImage:[UIImage imageWithIcon:@"fa-eye" backgroundColor:[UIColor clearColor] iconColor:[UIColor grayColor] andSize:CGSizeMake(24,24)] forState:UIControlStateNormal];
    [resetEyeBtn addTarget:self action:@selector(resetEyeBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        // resetEyeBtn.imageEdgeInsets = UIEdgeInsetsMake(0, -16, 0, 0);
    self.password_field.rightViewMode = UITextFieldViewModeAlways;
    self.password_field.rightView = resetEyeBtn;
    
    //returnKeyHandler = [[IQKeyboardReturnKeyHandler alloc] initWithViewController:self];
    //[returnKeyHandler setLastTextFieldReturnKeyType:UIReturnKeyDone];
}

-(IBAction)eyeBtnClicked:(id)sender{
    UIButton *eyeBtn = (UIButton*)sender;
    
    if(isPasswordVisible){
        [eyeBtn setImage:[UIImage imageWithIcon:@"fa-eye-slash" backgroundColor:[UIColor clearColor] iconColor:[UIColor grayColor] andSize:CGSizeMake(24,24)] forState:UIControlStateNormal];
        [self.password_field setSecureTextEntry:NO];
        isPasswordVisible = NO;
    }else{
        [eyeBtn setImage:[UIImage imageWithIcon:@"fa-eye" backgroundColor:[UIColor clearColor] iconColor:[UIColor grayColor] andSize:CGSizeMake(24,24)] forState:UIControlStateNormal];
        [self.password_field setSecureTextEntry:YES];
        isPasswordVisible = YES;
    }
}

-(IBAction)resetEyeBtnClicked:(id)sender{
    UIButton *resetEyeBtn = (UIButton*)sender;
    
    if(isResetPasswordVisible){
        [resetEyeBtn setImage:[UIImage imageWithIcon:@"fa-eye-slash" backgroundColor:[UIColor clearColor] iconColor:[UIColor grayColor] andSize:CGSizeMake(24,24)] forState:UIControlStateNormal];
        [self.password_field setSecureTextEntry:NO];
        isResetPasswordVisible = NO;
    }else{
        [resetEyeBtn setImage:[UIImage imageWithIcon:@"fa-eye" backgroundColor:[UIColor clearColor] iconColor:[UIColor grayColor] andSize:CGSizeMake(24,24)] forState:UIControlStateNormal];
        [self.password_field setSecureTextEntry:YES];
        isResetPasswordVisible = YES;
    }
}



-(void)setFieldBorder :(JVFloatLabeledTextField *)textField{
    
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1;
    NSLog(@"%f",textField.frame.size.height - borderWidth);
    NSLog(@"%f",textField.frame.size.width);
    NSLog(@"%f",textField.frame.size.height);
    border.borderColor = [UIColor lightGrayColor].CGColor;
    border.frame = CGRectMake(0, textField.frame.size.height - borderWidth, textField.frame.size.width + 120, textField.frame.size.height);
    border.borderWidth = borderWidth;
    [textField.layer addSublayer:border];
    textField.layer.masksToBounds = YES;
}


-(void)viewDidAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(closePopUp:)
                                                 name:@"ResetPassword" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(incorrectEmail:)
                                                 name:@"IncorrectEmail" object:nil];
    
}

-(void)closePopUp:(NSNotification *) notification {
    if(openPopUpController != nil){
        [openPopUpController dismiss];
        [AlertVC showAlertWithTitleForView:APP_NAME message:ONE_TIME_PASSWORD_SENT controller:self];
    }
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"forgotUserName"];
    
}

-(void)incorrectEmail:(NSNotification *)notification {
    if(openPopUpController != nil){
        [openPopUpController dismiss];
        [AlertVC showAlertWithTitleForView:APP_NAME message:@"Invalid Email Address & Mobile Number." controller:self];
    }
}

- (void)handleRememberMeTap:(UITapGestureRecognizer *)recognizer
{
    if(i_count==0)
    {
        [self.check_btn setImage:[UIImage imageWithIcon:@"fa-check-square" backgroundColor:[UIColor clearColor] iconColor:[UIColor colorWithRed:0.00 green:0.62 blue:0.83 alpha:1.0] andSize:CGSizeMake(35,35)] forState:UIControlStateNormal];
        i_count=1;
    }
    else
    {
        [self.check_btn setImage:[UIImage imageWithIcon:@"fa-square" backgroundColor:[UIColor clearColor] iconColor:[UIColor colorWithRed:0.00 green:0.62 blue:0.83 alpha:1.0] andSize:CGSizeMake(35,35)] forState:UIControlStateNormal];
        i_count=0;
         [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"signedin"];
    }
    
}

-(void)countryImgClicked
{
    [self showCountryPicker];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"signedin"]!=nil || [[NSUserDefaults standardUserDefaults] objectForKey:@"fromregister"] != nil)
    {
        self.username_field.text=[[NSUserDefaults standardUserDefaults]objectForKey:@"username"];
        self.password_field.text=[[NSUserDefaults standardUserDefaults]objectForKey:@"password"];
        self.username_field.tintColor = [UIColor colorWithRed:0.17 green:0.70 blue:0.34 alpha:1.0];
        self.password_field.tintColor = [UIColor colorWithRed:0.17 green:0.70 blue:0.34 alpha:1.0];
        
        [self.check_btn setImage:[UIImage imageWithIcon:@"fa-check-square" backgroundColor:[UIColor clearColor] iconColor:[UIColor colorWithRed:0.00 green:0.62 blue:0.83 alpha:1.0] andSize:CGSizeMake(35,35)] forState:UIControlStateNormal];
        i_count=1;
        
    }
    else
    {
    [self.check_btn setImage:[UIImage imageWithIcon:@"fa-check-square" backgroundColor:[UIColor clearColor] iconColor:[UIColor colorWithRed:0.00 green:0.62 blue:0.83 alpha:1.0] andSize:CGSizeMake(35,35)] forState:UIControlStateNormal];
    self.username_field.text=EMP_STR;
    self.password_field.text=EMP_STR;
        
    }
    
   
    
}

- (void)countryController:(id)sender didSelectCountry:(EMCCountry *)chosenCity
{
    [self dismissViewControllerAnimated:YES completion:nil];
    imagePath = [NSString stringWithFormat:@"EMCCountryPickerController.bundle/%@", chosenCity.countryCode];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"countryCode contains[c] %@ ",chosenCity.countryCode];
    countryCode * code = [[countryCode alloc] init];
    NSArray *filteredCountry = [code.getCoutries filteredArrayUsingPredicate:filter];
    
    countryField.text = filteredCountry[0][@"phoneNumber"];
    country_img.image = [UIImage imageNamed:imagePath];
    [[KGModal sharedInstance] showWithContentView:popView andAnimated:YES];
}

-(void)showCountryPicker
{
    [[KGModal sharedInstance]hideAnimated:YES];
    NSString * storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    EMCCountryPickerController * countryPicker = [storyboard instantiateViewControllerWithIdentifier:@"EMCCountryPickerController"];
    [self presentViewController:countryPicker animated:YES completion:nil];
    
    // default values
    countryPicker.showFlags = true;
    countryPicker.countryDelegate = self;
    countryPicker.drawFlagBorder = true;
    countryPicker.flagBorderColor = [UIColor grayColor];
    countryPicker.flagBorderWidth = 0.5f;
    //  countryPicker.availableCountryCodes = [NSSet setWithObjects:@"IT", @"ES", @"US", @"FR", nil];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.username_field) {
        [self.password_field becomeFirstResponder];
        return NO;
    }
    else if (textField==self.password_field){
        [textField resignFirstResponder];
        return NO;
    }
    else if (textField==emailField)
    {
        [textField resignFirstResponder];
        [phoneNumField becomeFirstResponder];
        return YES;
    }
    
    return YES;
}


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == phoneNumField)
    {
        UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
        numberToolbar.barStyle = UIBarStyleDefault;
        numberToolbar.items = [NSArray arrayWithObjects:
                               [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                               [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                               [[UIBarButtonItem alloc]initWithTitle:DONE style:UIBarButtonItemStyleDone target:self action:@selector(endEditing)],nil];
        textField.inputAccessoryView = numberToolbar;
        
    }
    
}


-(void) endEditing
{
    [phoneNumField resignFirstResponder];
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    NSString* totalString = [NSString stringWithFormat:@"%@%@",textField.text,string];
    
    if (textField.tag  == 20)
    {
        if (range.length == 1) {
            // Delete button was hit.. so tell the method to delete the last char.
            textField.text = [self formatPhoneNumber:totalString deleteLastChar:YES];
        } else {
            textField.text = [self formatPhoneNumber:totalString deleteLastChar:NO ];
        }
        return false;
    }
    
    return YES;
    
}

-(NSString*) formatPhoneNumber:(NSString*) simpleNumber deleteLastChar:(BOOL)deleteLastChar {
    if(simpleNumber.length==0) return EMP_STR;
    // use regex to remove non-digits(including spaces) so we are left with just the numbers
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[\\s-\\(\\)]" options:NSRegularExpressionCaseInsensitive error:&error];
    simpleNumber = [regex stringByReplacingMatchesInString:simpleNumber options:0 range:NSMakeRange(0, [simpleNumber length]) withTemplate:EMP_STR];
    
    // check if the number is to long
    if(simpleNumber.length>10) {
        // remove last extra chars.
        simpleNumber = [simpleNumber substringToIndex:10];
    }
    
    if(deleteLastChar) {
        // should we delete the last digit?
        simpleNumber = [simpleNumber substringToIndex:[simpleNumber length] - 1];
    }
    
    // 123 456 7890
    // format the number.. if it's less then 7 digits.. then use this regex.
    if(simpleNumber.length<7)
        simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d+)"
                                                               withString:@"$1-$2"
                                                                  options:NSRegularExpressionSearch
                                                                    range:NSMakeRange(0, [simpleNumber length])];
    
    else   // else do this one..
        simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d{3})(\\d+)"
                                                               withString:@"$1-$2-$3"
                                                                  options:NSRegularExpressionSearch
                                                                    range:NSMakeRange(0, [simpleNumber length])];
    return simpleNumber;
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    if (textField == countryField)
    {
        [self showCountryPicker];
        return NO;
    }
    else {
        return YES;
    }
}


-(void)sendMailToUser
{
    if([self connected])
    {
        NSDate *today = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:DATEFORMAT_TYPE];
        NSString *currentTime = [dateFormatter stringFromDate:today];
        
        //Validate Email Address.
        NSString *emailRegEx = EMAIL_REG_EX;
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
        username=registeredEmail;
        
        if ([emailTest evaluateWithObject:username] == YES)
        {
            udid=[[NSUserDefaults standardUserDefaults] objectForKey:UDID];
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
            [dictParam setObject:username         forKey:PARAM_EMAIL_ID_KEY];
            [dictParam setObject:MICROMARKET      forKey:PARAM_SERVICE_NAME_KEY];
            [dictParam setObject:udid             forKey:PARAM_DEVICE_UDID_KEY];
            [dictParam setObject:currentTime      forKey:PARAM_DEVICE_DATE_TIME_KEY];
            NSLog(@"username %@",dictParam);
            
            NSString *rootURL =  [[AppDelegate shared] rootUrl];

            AFNHelper *afn=[[AFNHelper alloc]init];
            [afn postDataFromPath:METHOD_CHANGEPASSWORD getUrl:rootURL withParamData:dictParam withBlock:^(id response, NSError *error)
             {
                 if(error)
                 {
                     NSLog(@"ERROR DESC:%@",error.localizedDescription);
                 }
                 else
                 {
                     NSDictionary *dictionary;
                     dictionary=response;
                     NSString *status=[dictionary valueForKey:STATUS];
                     
                     if([status isEqualToString:SUCCESS])
                     {
                         self->otp = [dictionary valueForKey:PARAM_OTP];
                         
                         if ([self->isEmailRegistered  isEqual: @"Y"] && [self->isPhoneNumberRegistered  isEqual: @"N"])
                         {
                             NSString *htmlbody = [NSString stringWithFormat: @"%s%@%s%@%s","<div style='font-size: 12pt; font-family: verdana; color: rgb(0, 0, 0); line-height: normal;'> <p>Greetings!</p><p>We received a request to reset your AveriGo Markets account password.</p><p>Enter this One-Time Password on the AveriGo Markets app, and then reset your password:</p><p><b>", otp,"</b> and the registered mobile number is <b>", registeredPhoneNumber, "</b></p><p>(This One-Time Password will only be valid for the next 4 hours)</p><p>Sincerely,<br/>The AveriGo Team</p></div>"];
                             SendGrid *sendgrid = [SendGrid apiUser:[dictionary valueForKey:@"mail_host_user"] apiKey:[dictionary valueForKey:@"mail_host_pass"]];
                             SendGridEmail *email = [[SendGridEmail alloc] init];
                             email.to       = username;
                             email.from     = [dictionary valueForKey:@"fromEmail"];
                             email.subject  = [dictionary valueForKey:@"subject"];
                             email.html     = htmlbody;
                             email.text     = htmlbody;
                             [sendgrid sendWithWeb:email];
                             self.username_field.text = registeredEmail;
                             [[KGModal sharedInstance]hideAnimated:YES];
                             [AlertVC showAlertWithTitleForView:APP_NAME message:ONE_TIME_PASSWORD_SENT controller:self];
                         }
                         else if ([isEmailRegistered  isEqual: @"N"] && [isPhoneNumberRegistered  isEqual: @"Y"])
                         {
                             [self sendSMSCode];
                             [[KGModal sharedInstance]hideAnimated:YES];
                         }
                         else
                         {
                             otp = [dictionary valueForKey:PARAM_OTP];
                             NSString *htmlbody = [NSString stringWithFormat: @"%s%@%s","<div style='font-size: 12pt; font-family: verdana; color: rgb(0, 0, 0); line-height: normal;'> <p>Greetings!</p><p>We received a request to reset your AveriGo Markets account password.</p><p>Enter this One-Time Password on the AveriGo Markets app, and then reset your password:</p><p><b>", otp,"</b></p><p>(This One-Time Password will only be valid for the next 4 hours)</p><p>Sincerely,<br/>The AveriGo Team</p></div>"];
                             SendGrid *sendgrid = [SendGrid apiUser:[dictionary valueForKey:@"mail_host_user"] apiKey:[dictionary valueForKey:@"mail_host_pass"]];
                             SendGridEmail *email = [[SendGridEmail alloc] init];
                             email.to       = registeredEmail;
                             email.from     = [dictionary valueForKey:@"fromEmail"];
                             email.subject  = [dictionary valueForKey:@"subject"];
                             email.html     = htmlbody;
                             email.text     = htmlbody;
                             [sendgrid sendWithWeb:email];
                             self.username_field.text = registeredEmail;
                             NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
                             // Formatting Phone Number
                             NSString *formattedPhoneNum = [[phoneNumField.text componentsSeparatedByCharactersInSet:
                                                             [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"]
                                                              invertedSet]]
                                                            componentsJoinedByString:EMP_STR];
                             NSLog(@"formated String %@", formattedPhoneNum);
                             
                             NSString *concatPhoneNum = [NSString stringWithFormat: @"%@%@", countryField.text, formattedPhoneNum];
                             NSLog(@"%@", concatPhoneNum);
                             NSString *concatPhoneEmail = [NSString stringWithFormat: @"%@", otp];
                             [dictParam setObject:[NSString stringWithFormat:@"%@",concatPhoneNum] forKey:@"Phone"];
                             [dictParam setObject:[NSString stringWithFormat:@"%@", concatPhoneEmail] forKey:@"code"];
                             [dictParam setObject:[NSString stringWithFormat:@"%@", @"F"] forKey:@"flag"];
                             
                             NSString *url = [NSString stringWithFormat:AVERIWARE_TWILIO_URL];
                             AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
                             [manager.requestSerializer setValue:APP_TYPE forHTTPHeaderField:CONTENT_TYPE];
                             manager.requestSerializer = [AFHTTPRequestSerializer serializer];
                             NSLog(@"Parameters:%@", dictParam);
                             
                             [manager POST:url parameters:dictParam progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                  NSLog(@"%@",responseObject);
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  NSLog(@"%@",error.localizedDescription);
                                   [[KGModal sharedInstance]hideAnimated:YES];
                                   [AlertVC showAlertWithTitleForView:APP_NAME message:ONE_TIME_PASSWORD_SENT controller:self];
                              }];
                         
                         }
                         
                     }
                     else
                     {
                         NSString *message=[dictionary valueForKey:MESSAGE];
                         [AlertVC showAlertWithTitleForView:APP_NAME message:message controller:self];
                         self.username_field.text = emailField.text;
                     }
                     
                 }
                
             }];
        }
        else
        {
            [AlertVC showAlertWithTitleForView:APP_NAME message:PLEASE_FILL_EMAIL  controller:self];
        }
    }
    else
    {
        [AlertVC showAlertWithTitleForView:APP_NAME message:NO_INTERNET_CONNECTION  controller:self];
    }
    
}

-(void)SetTextFieldBorder :(UITextField *)textField{
    
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1.5;
    border.borderColor = [UIColor blackColor].CGColor;
    border.frame = CGRectMake(0, textField.frame.size.height - borderWidth, textField.frame.size.width, textField.frame.size.height);
    border.borderWidth = borderWidth;
    
    [textField.layer addSublayer:border];
    textField.layer.masksToBounds = YES;
    textField.font =[UIFont fontWithName:FONT_MEDIUM size:15.f];
    textField.textAlignment=NSTextAlignmentCenter;
    textField.textColor=[UIColor blackColor];
    
}

-(void)showconfirmpopup
{
    NSDictionary *dictobject;
    dictobject=[self.temp_array objectAtIndex:0];
    popView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,self.view.frame.size.width-40, 300)];
    popView.backgroundColor     = [UIColor whiteColor];
    popView.layer.cornerRadius  = 5.0;
    popView.layer.masksToBounds = YES;
    
    UILabel *title_lbl=[[UILabel alloc]initWithFrame:CGRectMake(80,20,self.view.frame.size.width/2, 40)];
    title_lbl.text=@"Forgot Password";
    title_lbl.font=[UIFont fontWithName:FONT_MEDIUM size:17.f];
    title_lbl.textAlignment=NSTextAlignmentCenter;
    title_lbl.textColor=[UIColor blackColor];
    
    emailField= [[UITextField alloc]initWithFrame:CGRectMake(20, 100, self.view.frame.size.width/2 + 120 , 30)];
    [self SetTextFieldBorder:emailField];
    emailField.text = self.username_field.text;
    [emailField setKeyboardType:UIKeyboardTypeEmailAddress];
    emailField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    emailField.autocorrectionType = UITextAutocorrectionTypeNo;
    emailField.font=[UIFont fontWithName:FONT_MEDIUM size:16.f];
    emailField.textAlignment = NSTextAlignmentLeft;
    emailField.delegate = self;
    
    phoneNumField= [[UITextField alloc]initWithFrame:CGRectMake(140, 160, self.view.frame.size.width/2, 30)];
    [self SetTextFieldBorder:phoneNumField];
    phoneNumField.placeholder = @"Phone Number";
    [phoneNumField setKeyboardType:UIKeyboardTypeASCIICapableNumberPad];
    phoneNumField.font=[UIFont fontWithName:FONT_MEDIUM size:16.f];
    phoneNumField.textAlignment = NSTextAlignmentLeft;
    phoneNumField.tag = 20;
    phoneNumField.delegate = self;
    
    countryField = [[UITextField alloc]initWithFrame:CGRectMake(80, 160, 60, 30)];
    countryField.font=[UIFont fontWithName:FONT_MEDIUM size:16.f];
    countryField.delegate = self;
    countryField.text = @"+1";
    country_img.frame = CGRectMake(20 ,165 , 45, 20);
    imagePath = [NSString stringWithFormat:@"EMCCountryPickerController.bundle/%@", @"US" ];
    country_img.image = [UIImage imageNamed:imagePath];

    
    viewBtn=[[UIButton alloc]initWithFrame:CGRectMake(popView.frame.size.width/2 - 80 ,230 , 160, 45)];
    [viewBtn setTitle:@"Send Code" forState:UIControlStateNormal];
    [viewBtn.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:17]];
    [viewBtn addTarget:self action:@selector(sendCode:) forControlEvents:UIControlEventTouchUpInside];
    viewBtn.layer.cornerRadius=5.0;
    [viewBtn setBackgroundColor:[UIColor colorWithRed:0.00 green:0.51 blue:0.69 alpha:1.0]];
    [viewBtn setTintColor:[UIColor whiteColor]];
    
    [popView addSubview:viewBtn];
    [popView addSubview:title_lbl];
    [popView addSubview:emailField];
    [popView addSubview:phoneNumField];
    [popView addSubview:countryField];
    [popView addSubview:country_img];
    
    [[KGModal sharedInstance] showWithContentView:popView andAnimated:YES];
    
}

-(IBAction)helpClicked:(id)sender
{
    WebViewController *vc;
    vc=[self.storyboard instantiateViewControllerWithIdentifier:HELP_WEB_VC];
    vc.loadStr = @"http://www.averigo.com/markets/help";
    vc.titleStr = @"Help";
    vc.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:vc animated:YES completion:nil];
}

-(IBAction)privacyPolicyClicked:(id)sender{
    WebViewController *vc;
    vc=[self.storyboard instantiateViewControllerWithIdentifier:HELP_WEB_VC];
    vc.loadStr = @"http://www.averigo.com/privacy-policy";
    vc.titleStr = @"Privacy Policy";
    vc.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:vc animated:YES completion:nil];
}

-(IBAction)termsClicked:(id)sender{
    WebViewController *vc;
    vc=[self.storyboard instantiateViewControllerWithIdentifier:HELP_WEB_VC];
    vc.loadStr = @"http://www.averigo.com/terms-of-use";
    vc.titleStr = @"Terms of Use";
    vc.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:vc animated:YES completion:nil];
}

-(IBAction)forgotClicked:(id)sender
{
    if([self connected])
    {
        //Valid email address
        NSString *emailRegEx = EMAIL_REG_EX;
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
        username=self.username_field.text;
        
        if ([emailTest evaluateWithObject:username] == YES)
        {
           // [self showconfirmpopup];
            [[NSUserDefaults standardUserDefaults] setValue:username forKey:@"forgotUserName"];
        STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"VerifyPasswordController"]];
        popupController.style = STPopupStyleBottomSheet;
        openPopUpController = popupController;
        [popupController presentInViewController:self];
        }else
        {
            [[KGModal sharedInstance]hideAnimated:YES];
            [AlertVC showAlertWithTitleForView:APP_NAME message:@"Please enter valid email address."  controller:self];
        }
    }
    else
    {
        [AlertVC showAlertWithTitleForView:APP_NAME message:NO_INTERNET_CONNECTION  controller:self];
    }
    
}

-(void)sendSMSCode
{
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    // Formatting Phone Number
    NSString *formattedPhoneNum = [[phoneNumField.text componentsSeparatedByCharactersInSet:
                                    [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"]
                                     invertedSet]]
                                   componentsJoinedByString:EMP_STR];
    NSString *concatPhoneNum = [NSString stringWithFormat: @"%@%@", countryField.text, formattedPhoneNum];
    
    NSString *concatPhoneEmail = [NSString stringWithFormat: @"%@%s%@", otp, " and the registered email address is " ,registeredEmail];
    [dictParam setObject:[NSString stringWithFormat:@"%@",concatPhoneNum] forKey:@"Phone"];
    [dictParam setObject:[NSString stringWithFormat:@"%@", concatPhoneEmail] forKey:@"code"];
    [dictParam setObject:[NSString stringWithFormat:@"%@", @"F"] forKey:@"flag"];
    
    NSString *url = [NSString stringWithFormat:AVERIWARE_TWILIO_URL];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [manager.requestSerializer setValue:APP_TYPE forHTTPHeaderField:CONTENT_TYPE];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    [manager POST:url parameters:dictParam progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
     {
         NSLog(@"%@",responseObject);
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
     {
         NSLog(@"%@",error.localizedDescription);
         [AlertVC showAlertWithTitleForView:APP_NAME message:ONE_TIME_PASSWORD_SENT controller:self];
     }];
}





-(void)checkuser
{
    if([self connected])
    {
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setObject:MICROMARKET   forKey:PARAM_SERVICE_NAME_KEY];
        [dictParam setObject:emailField forKey:PARAM_EMAIL_ID_KEY];
        
        NSString *rootURL =  [[AppDelegate shared] rootUrl];

        AFNHelper *afn=[[AFNHelper alloc]init];
        [afn postDataFromPath:METHOD_CHECKUSER getUrl:rootURL withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             if(error)
             {
                 NSLog(@"ERROR DESC:%@",error.localizedDescription);
                 [AlertVC showAlertWithTitleForView:APP_NAME message:REQUEST_TIMED_OUT controller:self];
             }
             else
             {
                 NSDictionary *dictionary;
                 dictionary=response;
                 NSLog(@"response %@",response);
                 NSString *status=[dictionary valueForKey:STATUS];
                 if([status isEqualToString:SUCCESS])
                 {
                     [AlertVC showAlertWithTitleForView:APP_NAME message:INVALID_EMAIL_ADDRESS controller:self];
                 }
                 else
                 {
                     [self sendMailToUser];
                 }
             }
         }];
    }
    else
    {
        [AlertVC showAlertWithTitleForView:APP_NAME message:NO_INTERNET_CONNECTION controller:self];
    }
    
}

-(IBAction)createClicked:(id)sender
{
    RegisterViewController *vc;
    vc = [self.storyboard instantiateViewControllerWithIdentifier: REGISTER_VC];
    //[self presentViewController:vc animated:YES completion:nil];
    [self.navigationController pushViewController:vc animated:YES];

}

-(IBAction)loginClicked:(id)sender
{
    [_password_field resignFirstResponder];
    
    if([self connected])
    {
        username=self.username_field.text;
        password=self.password_field.text;
        if(username.length!=0&&password.length!=0)
            {
            NSDate *today = [NSDate date];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:DateFormat];
            NSString *currentTime = [dateFormatter stringFromDate:today];
            udid=[[NSUserDefaults standardUserDefaults] objectForKey:UDID];
        
            [[ActivityIndicator sharedInstance] showLoader];
            
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
            [dictParam setObject:username         forKey:PARAM_EMAIL_ID_KEY];
            [dictParam setObject:password         forKey:PARAM_PASSWORD_KEY];
            [dictParam setObject:MICROMARKET      forKey:PARAM_SERVICE_NAME_KEY];
            [dictParam setObject:udid             forKey:PARAM_DEVICE_UDID_KEY];
            [dictParam setObject:currentTime      forKey:PARAM_DEVICE_DATE_TIME_KEY];
            
            NSString *rootURL =  [[AppDelegate shared] rootUrl];

            AFNHelper *afn=[[AFNHelper alloc]init];
            [afn postDataFromPath:METHOD_SIGNIN getUrl:rootURL withParamData:dictParam withBlock:^(id response, NSError *error)
             {
             if(error)
                 {
                    [[ActivityIndicator sharedInstance] hideLoader];
                    NSLog(@"ERROR DESC:%@",error.localizedDescription);
                    if(error.code == NSURLErrorTimedOut){
                    [AlertVC showAlertWithTitleForView:APP_NAME message:ERROR_TIMED_OUT controller:self];
                    }else if(error.code == NSURLErrorNotConnectedToInternet){
                     [AlertVC showAlertWithTitleForView:APP_NAME message:NO_INTERNET_CONNECTION controller:self];
                    }else{
                     [AlertVC showAlertWithTitleForView:APP_NAME message:REQUEST_TIMED_OUT controller:self];
                    }
                 }
             else
                 {
                 
                 NSDictionary *dictionary;
                 dictionary=response;
                 self.userdetails_array=[[NSMutableArray alloc]init];
                 NSString *status=[dictionary valueForKey:STATUS];
                 self.userdetails_array=[dictionary valueForKey:@"User"];
                 if([status isEqualToString:SUCCESS])
                     {
                     if(self.userdetails_array.count != 0){
                         NSDictionary *dictobject;
                         dictobject=[self.userdetails_array objectAtIndex:0];
                         if ([[dictionary valueForKey:@"PasswordReset"]  isEqual: @"AllowPasswordReset"])
                             {
                            [[ActivityIndicator sharedInstance] hideLoader];
                             ForgotPasswordController *vc;
                             vc=[self.storyboard instantiateViewControllerWithIdentifier:@"ForgotPasswordController"];
                             vc.otp = self.password_field.text;
                             vc.emailid = self.username_field.text;
                             vc.modalPresentationStyle = UIModalPresentationFullScreen;
                             [self presentViewController:vc animated:YES completion:nil];
                             }
                         else{
                             self.userdetails_array=[dictionary valueForKey:@"User"];
                             NSDictionary *dictobject;
                             dictobject=[self.userdetails_array objectAtIndex:0];
                             if(i_count==1)
                             {
                             [[NSUserDefaults standardUserDefaults]setObject:@"signedin" forKey:@"signedin"];
                             }
                             [[ActivityIndicator sharedInstance] hideLoader];

                             [[NSUserDefaults standardUserDefaults] setObject:@"N" forKey:@"usertype"];
                             [[NSUserDefaults standardUserDefaults] setObject:self.userdetails_array forKey:@"userdetailsarray"];
                             [[NSUserDefaults standardUserDefaults]setObject:[dictobject valueForKey:@"Live_publishable_key"] forKey:@"stripekey"];
                             [[NSUserDefaults standardUserDefaults]setObject:[dictobject valueForKey:@"EMAIL_ID"] forKey:@"username"];// //
                             [[NSUserDefaults standardUserDefaults]setObject:password forKey:@"password"];
                             [[NSUserDefaults standardUserDefaults]setObject:[dictobject valueForKey:@"FIRST_NAME"] forKey:@"firstname"];
                             [[NSUserDefaults standardUserDefaults]setObject:[dictobject valueForKey:@"LAST_NAME"] forKey:@"lastname"];
                             [[NSUserDefaults standardUserDefaults]setObject:[dictobject valueForKey:@"PROFILE_IMAGE"]forKey:@"imagename"];
                             i_count=1;
                             TabBarViewController *vc;
                             vc=[self.storyboard instantiateViewControllerWithIdentifier:@"TabBarViewController"];
                             vc.navigationItem.hidesBackButton=YES;
                             vc.load=@"yes";
                             [self.navigationController pushViewController:vc animated:YES];
                         }
                     }
                     else
                         {
                         if ([[dictionary valueForKey:@"PasswordReset"]  isEqual: @"AllowPasswordReset"])
                             {
                            [[ActivityIndicator sharedInstance] hideLoader];
                             ForgotPasswordController *vc;
                             vc=[self.storyboard instantiateViewControllerWithIdentifier:@"ForgotPasswordController"];
                             vc.otp = self.password_field.text;
                             vc.emailid = self.username_field.text;
                             vc.modalPresentationStyle = UIModalPresentationFullScreen;
                             [self presentViewController:vc animated:YES completion:nil];
                             }
                        
                         }
                     
                     }
                 else{
                     [[ActivityIndicator sharedInstance] hideLoader];
                     NSString *message=[dictionary valueForKey:MESSAGE];
                     [AlertVC showAlertWithTitleForView:APP_NAME message:message  controller:self];
                  }
                }
             }];
          }
        else
        {
            [AlertVC showAlertWithTitleForView:APP_NAME message:PLEASE_FILL_REQUIRED_FIELDS controller:self];
        }
    }
    else
    {
        [AlertVC showAlertWithTitleForView:APP_NAME message:NO_INTERNET_CONNECTION controller:self];
    }
    
}


-(IBAction)rememberClicked:(id)sender
{
    if(i_count==0)
    {
        [self.check_btn setImage:[UIImage imageWithIcon:@"fa-check-square" backgroundColor:[UIColor clearColor] iconColor:[UIColor colorWithRed:0.00 green:0.62 blue:0.83 alpha:1.0] andSize:CGSizeMake(35,35)] forState:UIControlStateNormal];
        i_count=1;
    }
    else
    {
        [self.check_btn setImage:[UIImage imageWithIcon:@"fa-square" backgroundColor:[UIColor clearColor] iconColor:[UIColor colorWithRed:0.00 green:0.62 blue:0.83 alpha:1.0] andSize:CGSizeMake(35,35)] forState:UIControlStateNormal];
        i_count=0;
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"signedin"];
    }
    
    
}

- (BOOL)connected
{
    ReachabilityStatus status = [GLobalRealReachability currentReachabilityStatus];
    NSLog(@"Initial reachability status:%@",@(status));
    
    return status == 1 || status == 2;
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end


