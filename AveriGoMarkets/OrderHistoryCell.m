//
//  OrderHistoryCell.m
//  AveriGo Markets
//
//  Created by Rattletech on 29/10/18.
//  Copyright © 2018 BTS. All rights reserved.
//

#import "OrderHistoryCell.h"
#import "UIImageView+WebCache.h"

@implementation OrderHistoryCell

@synthesize delegate;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _locationImageView.image = [UIImage imageWithIcon:@"fa-map-marker" backgroundColor:[UIColor clearColor] iconColor:[UIColor darkGrayColor] andSize:CGSizeMake(35,35)];
    [_collectionview registerNib:[UINib nibWithNibName:@"ProductCell" bundle:nil] forCellWithReuseIdentifier:@"ProductCell"];
    _pageControl.numberOfPages = _items.count;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if(_items.count < (_collectionview.frame.size.width / 19))
    {
        _pageControl.numberOfPages = _items.count;
    }else{
        _pageControl.numberOfPages = _collectionview.frame.size.width/19;
    }
    
    return _items.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ProductCell *cell= (ProductCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"ProductCell" forIndexPath:indexPath];
    NSLog(@"items:%@",_items);
    NSDictionary * dict = [_items objectAtIndex:indexPath.row];

    NSString *urlString = [NSString stringWithFormat:@"https://%@",[[dict valueForKey:@"ITEM_IMAGE1"]stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]]];
    NSURL *url=[NSURL URLWithString:urlString];
    NSLog(@"%@",urlString);
    if([urlString isEqualToString:@"https://"]){
        [cell.productImage setContentMode:UIViewContentModeScaleAspectFit];
    }else{
         [cell.productImage setContentMode:UIViewContentModeScaleAspectFill];
    }

   // [cell.productImage sd_setImageWithURL:url placeholderImage:[UIImage imageWithIcon:@"fa-cutlery" backgroundColor:[UIColor clearColor] iconColor:[[UIColor lightGrayColor] colorWithAlphaComponent:0.5] fontSize:100]];
    [cell.productImage sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"cart_placeholder.png"]];
    cell.productName.text = [dict valueForKey:@"ITEM_DESC"];
    cell.productQty.text = [NSString stringWithFormat:@" %@",[dict valueForKey:@"QTY"]];
    NSString * buttonText = [dict valueForKey:@"BUTTON_TEXT"];
    NSString *ratingID = [NSString stringWithFormat: @"%@", [dict valueForKey:@"RATING_ID"]];
    
    NSString *ratingValue = [NSString stringWithFormat: @"%@", [dict valueForKey:@"RATING_VALUE"]];
    
    if (![ratingID isEqualToString: @"0"] && ![ratingID isKindOfClass: NULL] && ![ratingID isEqualToString: @""]) {
        cell.starView.hidden = NO;
        cell.ratingBtn.hidden = YES;
        cell.ratingView.value = [ratingValue floatValue];
    } else {
        cell.starView.hidden = YES;
        cell.ratingBtn.hidden = NO;
    }

    cell.productName.numberOfLines = 0;
    
    if (![buttonText  isEqual: @""]) {
        [cell.buyOptionBtn setTitle: buttonText forState: UIControlStateNormal];
        cell.buyOptionBtn.hidden = NO;
    } else {
        cell.buyOptionBtn.hidden = YES;
    }
    
    cell.productPrice.text = [NSString stringWithFormat:@"$%@",[dict valueForKey:@"ITEM_PRICE"]];
    
    cell.buyOptionBtn.tag = indexPath.row;
    
    cell.starView.tag = indexPath.row;
    
    cell.ratingBtn.tag = indexPath.row;

    [cell.starView addTarget:self action:@selector(ratingBtnClicked:) forControlEvents:UIControlEventTouchUpInside];

    [cell.buyOptionBtn addTarget:self action:@selector(buyOptionBtnClicked:) forControlEvents:UIControlEventTouchUpInside];

    [cell.ratingBtn addTarget:self action:@selector(ratingBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.ratingBtn.userInteractionEnabled = YES;
    
    cell.ratingBtn.backgroundColor = [UIColor colorWithRed: 116.0/255.0 green: 187.0/255.0 blue: 80.0/255.0 alpha: 1.0];
    
    if ([[dict valueForKey:@"BAR_CODE"]  isEqual: @""]) {
        
        cell.ratingBtn.userInteractionEnabled = NO;
        
        cell.ratingBtn.backgroundColor = [UIColor lightGrayColor];
    }

    return cell;
}

-(void)buyOptionBtnClicked:(id)sender {
    
    NSDictionary * dict = [_items objectAtIndex: [sender tag]];

    [self.delegate redirectToBuyOptionWebPage: dict];
}

-(void)ratingBtnClicked:(id)sender {
    
    [[NSUserDefaults standardUserDefaults] setObject: [NSString stringWithFormat: @"%@", self.microMarkettring] forKey:@"currentIndex"];
    
    [[NSUserDefaults standardUserDefaults] setInteger: [self.indexpath integerValue] forKey: @"indexpath"];
    
    NSDictionary * dict = [_items objectAtIndex: [sender tag]];

    [self.delegate redirectToReviewAndRatingView: dict];
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(collectionView.frame.size.width - 10, collectionView.frame.size.height);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView
                        layout:(UICollectionViewLayout *)collectionViewLayout
        insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 5, 0, 5);
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (scrollView == _collectionview) {
        
        CGFloat pageWidth = _collectionview.frame.size.width;
        float currentPage = _collectionview.contentOffset.x / pageWidth;
        if (0.0f != fmodf(currentPage, 1.0f))
        {
            _pageControl.currentPage = currentPage + 1;
        }
        else
        {
            _pageControl.currentPage = currentPage;
        }
        NSLog(@"finishPage: %ld", (long)_pageControl.currentPage);
        
        CGPoint currentCellOffset = _collectionview.contentOffset;
        currentCellOffset.x += _collectionview.frame.size.width / 2;
        NSIndexPath *indexPath = [_collectionview indexPathForItemAtPoint:currentCellOffset];
        [_collectionview scrollToItemAtIndexPath:indexPath
                                    atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                            animated:YES];
    }
}

@end
